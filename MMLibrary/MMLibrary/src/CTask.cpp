/*!	\file		CTask.h
*	\details	CTaskクラスの定義
*	\author		松裏征志
*	\date		2014/10/26
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Utility\CTask.h"

namespace MMLibrary
{
	namespace Utility
	{
		void CTaskReturnVoid::Run()
		{
			m_function();
			m_promise.set_value(true);
		}

	}
}