/*!	\file		CMemoryBlockHeader.cpp
*	\details	CMemoryBlockHeaderクラスの定義
*	\author		松裏征志
*	\date		2014/05/02
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Memory/CMemoryBlock.h"

namespace MMLibrary
{
	namespace Memory
	{
		CMemoryBlockHeader::CMemoryBlockHeader() 
			: m_size()
		{};

		uint_t	CMemoryBlockHeader::GetSize()
		{
			return m_size;
		};

		void		CMemoryBlockHeader::SetSize(uint_t size) 
		{
			m_size = size;
		};
	};
};