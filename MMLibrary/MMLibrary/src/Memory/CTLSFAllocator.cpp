/*!	\file		CTLSFAllocator.cpp
*	\details	CTLSFAllocatorクラスの定義
*	\author		松裏征志
*	\date		2014/05/02
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Utility\Macro.h"
#include "Memory/CTLSFAllocator.h"
#include "Math/Math.h"

namespace MMLibrary
{
	namespace Memory
	{
		
		CTLSFBlockHeader::CTLSFBlockHeader()
			: m_pPrev(nullptr)
			, m_pNext(nullptr) 
		{};
		
		CMemoryBlock<CTLSFBlockHeader, uint_t>&	
			CTLSFBlockHeader::Register(
			CMemoryBlock<CTLSFBlockHeader, uint_t>* src)
		{
			//呼び出し元は常にダミーリストの先頭ブロック
			src->Header().m_pNext				= m_pNext;
			src->Header().m_pPrev				= this;
			src->Header().m_pPrev->m_pNext		=&src->Header();

			if(src->Header().m_pNext)
				src->Header().m_pNext->m_pPrev	= &src->Header();

			return (CMemoryBlock<CTLSFBlockHeader, uint_t>&)(*this);
		};

		CMemoryBlock<CTLSFBlockHeader, uint_t>&	
			CTLSFBlockHeader::Remove(void)
		{
			m_pPrev->m_pNext	= m_pNext;

			if(m_pNext)
				m_pNext->m_pPrev	= m_pPrev;

			m_pPrev					= m_pNext	= nullptr;

			return (CMemoryBlock<CTLSFBlockHeader, uint_t>&)(*this);
		};

		CMemoryBlock<CTLSFBlockHeader, uint_t>&	
			CTLSFBlockHeader::ToBlock(void)
		{
			return (CMemoryBlock<CTLSFBlockHeader, uint_t>&)*this;
		};

		uint_t CTLSFBlockHeader::GetSize(void)
		{
			return CMemoryBlockHeader::GetSize() &
					~(THIS_BLOCK_USE_BIT_MASK | PREV_BLOCK_USE_BIT_MASK);
		}

		void CTLSFBlockHeader::SetSize(uint_t size)
		{
			uint_t oldSize = CMemoryBlockHeader::GetSize();

			CMemoryBlockHeader::SetSize(
				size | 
				(	oldSize & 
					(THIS_BLOCK_USE_BIT_MASK | PREV_BLOCK_USE_BIT_MASK)));
		}

		bool CTLSFBlockHeader::IsLast(void)
		{
			return 0 == GetSize();
		};

		bool CTLSFBlockHeader::IsFree(void)
		{
			return (CMemoryBlockHeader::GetSize() & THIS_BLOCK_USE_BIT_MASK) != 0;
		};
		
		void CTLSFBlockHeader::SetFree(void)
		{
			CMemoryBlockHeader::SetSize(
				CMemoryBlockHeader::GetSize() | THIS_BLOCK_USE_BIT_MASK);
		};

		void CTLSFBlockHeader::SetActive(void)
		{
			CMemoryBlockHeader::SetSize(
				CMemoryBlockHeader::GetSize() & ~THIS_BLOCK_USE_BIT_MASK);
		};

		bool CTLSFBlockHeader::IsPrevFree(void)
		{
			return (CMemoryBlockHeader::GetSize() & PREV_BLOCK_USE_BIT_MASK) != 0;
		};
		
		void CTLSFBlockHeader::SetPrevFree(void)
		{
			CMemoryBlockHeader::SetSize(
				CMemoryBlockHeader::GetSize() | PREV_BLOCK_USE_BIT_MASK);
		};

		void CTLSFBlockHeader::SetPrevActive(void)
		{
			CMemoryBlockHeader::SetSize(
				CMemoryBlockHeader::GetSize() & ~PREV_BLOCK_USE_BIT_MASK);
		};

		uint_t	CTLSFAllocator::GetSecondLevelIndex(
			uint_t	size, 
			uchar_t	MSB) const
		{
			// 最上位ビット未満のビット列だけを有効にするマスク
			// 1000 0000 -> 0111 1111
			const unsigned mask = (1 << MSB) - 1;  
    
			// 右へのシフト数を算出
			// 7 - 3 = 4 （8分割ならN=3です）
			const unsigned rs = MSB - N;    

			// 引数sizeにマスクをかけて、右へシフトすればインデックスに
			return (size & mask) >> rs;
		};

		uchar_t	CTLSFAllocator::GetFreeListSLI(
			uchar_t	mySLI, 
			uint_t	freeListBit) const
		{
			// 自分のSLI以上が立っているビット列を作成 (ID = 0なら0xffffffff）
			uint_t	myBit			= 0xffffffff << mySLI;

			// myBitとfreeListBitを論理積すれば、確保可能なブロックを持つSLIが出てきます
			//  freeListBit = 1100 ([192-223]と[224-255]にあるとします）
			uint_t	enableListBit	= freeListBit & myBit;

			// LSBを求めれば確保可能な一番サイズの小さいフリーリストブロックの発見
			if (enableListBit == 0)
				return -1;   // フリーリスト無し
			return Math::GetLSB(enableListBit);
		};
			
		uchar_t CTLSFAllocator::GetFreeListFLI(
			uchar_t	myFLI) const
		{
			// 自分のFLI以上が立っているビット列を作成 (ID = 0なら0xffffffff）
			uint_t	myBit			= 0xffffffff << myFLI;
			
			// myBitとglobalFLIを論理積すれば、確保可能なブロックを持つFLIが出てきます
			uint_t	enableFLIBit	= m_fliFreeListbit & myBit;

			if (enableFLIBit == 0)
				return -1;   // メモリが完全に無い
			return Math::GetLSB(enableFLIBit);
		};
		
		uchar_t CTLSFAllocator::GetFreeListIndex(
			size_t			size) const
		{
			char FLI;
			char SLI;

			if(size < SMALL_BLOCK_SIZE)
			{
				/* Store small blocks in first list. */
				FLI = 0;
				SLI =	(int)(size) /
						(SMALL_BLOCK_SIZE / SL_INDEX_COUNT);
			}
			else
			{
				FLI = Math::GetMSB(size);
				SLI =	(int)(size >>
						(FLI - SL_INDEX_COUNT_LOG2)) ^
						(1 << SL_INDEX_COUNT_LOG2);
				FLI -= (FL_INDEX_SHIFT - 1);
			}

			return GetFreeListIndex(FLI, SLI);
		};

		uchar_t CTLSFAllocator::GetFreeListIndex(
			uchar_t	FLI,
			uchar_t	SLI) const
		{
			return FLI * (uchar_t)pow(2.0f, N) + SLI - 1;
		};

		CTLSFAllocator::CTLSFAllocator(
			uint_t poolSize, 
			int maxExpectDivideNum)
		{
			Init(poolSize, maxExpectDivideNum);
		};

		void CTLSFAllocator::Init(	uint_t poolSize, 
					int maxExpectDivideNum)

		{
			//総分割数
			m_maxDivideNum	= maxExpectDivideNum;
			if(m_maxDivideNum == -1)
				m_maxDivideNum	= poolSize / (uint_t)pow(2.0f,N);

			//運用総メモリ
			//指定量を抑えるベキ乗数で固定(poolSize <= allSize = 2^k)
			int k	= Math::GetMSB(poolSize - 1) + 1;
			m_poolSize	= (uint_t)pow(2.0f,k);

			//定数設定
			BNS	= sizeof(CMemoryBlock<CTLSFBlockHeader>) + sizeof(uint_t);
			BBS	= sizeof(CMemoryBlock<CTLSFBlockHeader>);

			//総確保メモリ=運用総メモリ+管理タグ分*予想最大分割数分+初期状態の管理タグ分
			m_allAlocatedSize	= 
				m_poolSize + BNS * maxExpectDivideNum + BNS * 3;

			//メモリ確保
			m_pMemoryPool	= new char[m_allAlocatedSize];
			memset(m_pMemoryPool,0,m_allAlocatedSize);

			//初期チャンクのFLIとSLI
			char fli	= Math::GetMSB(m_allAlocatedSize - 1);
			char sli	= GetSecondLevelIndex(m_allAlocatedSize - 1,fli);

			//ブロックリスト数
			char bNum	= fli * (uint_t)pow(2.0f,N) + sli;

			//ブロックリスト確保
			m_pFreeBlockList	= new CMemoryBlock<CTLSFBlockHeader>[bNum];
			m_pActiveBlockList	= new CMemoryBlock<CTLSFBlockHeader>[bNum];
			
			//ブロックリストビット確保
			m_fliFreeListbit	= 0;					//フリーリストビット(第一カテゴリ)初期化
			m_pSliFreeListbit	= new uint_t[fli+1];	//フリーリストビット(第二カテゴリ)初期化
			memset(m_pSliFreeListbit, 0, sizeof(int) * (fli + 1));

			//始めのダミー
			CMemoryBlock<CTLSFBlockHeader>* bbp = 
				new(m_pMemoryPool) 
				CMemoryBlock<CTLSFBlockHeader>(0);
			bbp->Header().SetActive();
			bbp->Header().SetPrevActive();
			m_pActiveBlockList[0].Header().Register(bbp);

			//終末のダミー
			bbp						= 
				new(m_pMemoryPool + m_allAlocatedSize - BNS) 
				CMemoryBlock<CTLSFBlockHeader>(0);
			bbp->Header().SetActive();
			bbp->Header().SetPrevActive();
			m_pActiveBlockList[0].Header().Register(bbp);

			//始めはフリーリストに一つだけ大きなブロックがある
			bbp							=
				new(m_pMemoryPool+BNS) CMemoryBlock<CTLSFBlockHeader>(m_allAlocatedSize - BNS * 3);
			m_pFreeBlockList[bNum-1].Header().Register(bbp);
			bbp->Header().SetFree();
			bbp->Header().SetPrevActive();
			bbp->Next()->Header().SetPrevFree();
			
			//(第一カテゴリ)(第二カテゴリ)の最初のフリーリストビットを立てる
			m_fliFreeListbit = (1<<fli);
			m_pSliFreeListbit[fli] = (1<<sli);
		};

		CTLSFAllocator::~CTLSFAllocator(void)
		{
			memset(m_pMemoryPool,0,m_poolSize);
			Utility::SafeDeleteArray(m_pMemoryPool);
			Utility::SafeDeleteArray(m_pFreeBlockList);
			Utility::SafeDeleteArray(m_pActiveBlockList);
			Utility::SafeDeleteArray(m_pSliFreeListbit);
		};
		
		void* CTLSFAllocator::Allocate(const size_t size)
		{
			//最大のサイズのFLI(これを越えてはいけない)
			uint_t	maxFLI			= Math::GetMSB(m_allAlocatedSize - 1);

			size_t			allocateSize	= size + BNS;

			uint_t	index			=0;

			//必要サイズを第1カテゴリーで分類
			uchar_t	FLI = Math::GetMSB(allocateSize);

			//第2カテゴリーで分類
			uchar_t	SLI =	GetSecondLevelIndex(
										allocateSize, 
										FLI);
			//取得サイズを切り上げる
			allocateSize = (1 << FLI) | ((SLI + 1) << (FLI - N)); 

			//TODO:この下要修正
			//第2カテゴリにフリーリストがあるか？
			index = GetFreeListIndex(FLI, SLI);
			if(m_pFreeBlockList[index].Header().m_pNext == nullptr)
			{
				//もっと大きなフリーブロックを探す
				SLI	= GetFreeListSLI(SLI, m_pSliFreeListbit[FLI]);
				
				if(SLI == 255)
				{
					//より大きな第1カテゴリをチェック
					FLI = GetFreeListFLI(FLI);

					//メモリに空きがない
					if(FLI == 255 || FLI > maxFLI)
						return nullptr;

					//第2カテゴリーで分類
					SLI	= GetFreeListSLI(0, m_pSliFreeListbit[FLI]);

					//第2カテゴリにフリーリストがあるか？
					index = GetFreeListIndex(FLI, SLI);
					if(m_pFreeBlockList[index].Header().m_pNext == nullptr)
					{
						//メモリに空きがない
						return nullptr;
					}
				}
			}

			//fIndexチェック
			assert(0 <	FLI && FLI <=	maxFLI);
			assert(0 <= SLI && SLI <	pow(2.0f, N));
			assert(m_fliFreeListbit			& (1 << FLI));
			assert(m_pSliFreeListbit[FLI]	& (1 << SLI));
			assert(nullptr	!= m_pFreeBlockList[index].Header().m_pNext);

			//メモリを割り当てる
			//元々(分割前)のメモリブロック
			index = GetFreeListIndex(FLI, SLI);
			CMemoryBlock<CTLSFBlockHeader, uint_t>* 
				bbp	= &m_pFreeBlockList[index].Header().m_pNext->ToBlock();
			
			//フリーリストから外す
			bbp->Header().Remove();

			//フリーリストが無かったらビットをfalseに
			if(	m_pFreeBlockList[index].Header().m_pNext ==	nullptr){
				//フリーリストビット(第二カテゴリ)
				m_pSliFreeListbit[FLI]	&= ~(1 << SLI);
				//フリーリストビット(第一カテゴリ)
				if(m_pSliFreeListbit[FLI] == 0)
					m_fliFreeListbit	&= ~(1 << FLI);
			}

			//分割可能なら分割する
			if(bbp->IsEnableSplit(allocateSize))
			{
				CMemoryBlock<CTLSFBlockHeader, uint_t>*	pNewBlock	=
					bbp->Split(allocateSize);
				pNewBlock->Header().SetFree();
				pNewBlock->Header().SetPrevActive();

				//新しいフリーブロックをフリーブロックリストに登録する
				uchar_t	newFLI	= 
					Math::GetMSB(pNewBlock->GetMemorySize());
				uchar_t	newSLI	= 
					GetSecondLevelIndex(pNewBlock->GetMemorySize(), 
										newFLI);

				m_pFreeBlockList[GetFreeListIndex(newFLI, newSLI)].Header().Register(pNewBlock);
				
				//フリーリストビット(第二カテゴリ)
				m_pSliFreeListbit[newFLI]	|= (1 << newSLI);
				//フリーリストビット(第一カテゴリ)
				m_fliFreeListbit		|= (1 << newFLI);
			}
			
			//アクティブリストへ追加
			m_pActiveBlockList[index].Header().Register(bbp);
			bbp->Header().SetActive();
			bbp->Next()->Header().SetPrevActive();

			//0クリアして返す
			memset(bbp->GetMemory(), 0, bbp->GetMemorySize());
			return bbp->GetMemory();
		};
		
		bool CTLSFAllocator::Deallocate(void* pointer)
		{
			if(!pointer)
				return false;

			//前方・自身のブロック
			CMemoryBlock<CTLSFBlockHeader, uint_t>	
				*pThisBlock	= 
					(CMemoryBlock<CTLSFBlockHeader, uint_t>*)
					((char *)pointer - BBS),
				*pPrevBlock = pThisBlock->Prev();

			assert(!pThisBlock->Header().IsFree() && "アクティブなブロックではありません");

			pThisBlock->Next()->Header().SetPrevFree();
			pThisBlock->Header().SetFree();

			if(pPrevBlock->Header().IsFree())
			{
				pPrevBlock->Marge();
				pThisBlock = pPrevBlock;
			}
			
			if(pThisBlock->Next()->Header().IsFree())
			{
				pThisBlock->Marge();
				pThisBlock->Next()->Header().SetPrevFree();
			}
			
			char FLI;
			char SLI;

			//この処理が問題 SLIの上限は？
			FLI = Math::GetMSB(pThisBlock->GetBlockSize());
			SLI = GetSecondLevelIndex(pThisBlock->GetBlockSize(), FLI);
			
			m_pFreeBlockList[GetFreeListIndex(FLI, SLI)].Header().Register(pThisBlock);

			//フリーリストビット(第一カテゴリ)
			m_fliFreeListbit		|= (1 << FLI);
			//フリーリストビット(第二カテゴリ)
			m_pSliFreeListbit[FLI]	|= (1 << SLI);

			assert(0 <	FLI && FLI <=	Math::GetMSB(m_allAlocatedSize - 1));
			assert(0 <= SLI && SLI <	pow(2.0f, N));
			assert(m_fliFreeListbit			& (1 << FLI));
			assert(m_pSliFreeListbit[FLI]	& (1 << SLI));
			assert(pThisBlock->GetBlockSize()	& (CTLSFBlockHeader::THIS_BLOCK_USE_BIT_MASK | CTLSFBlockHeader::PREV_BLOCK_USE_BIT_MASK) );

			return true;
		};

		void	CTLSFAllocator::Clear(void)
		{
			//一旦解放して初期化し直す
			memset(m_pMemoryPool,0,m_poolSize);
			Utility::SafeDeleteArray(m_pMemoryPool);
			Utility::SafeDeleteArray(m_pFreeBlockList);
			Utility::SafeDeleteArray(m_pActiveBlockList);
			Utility::SafeDeleteArray(m_pSliFreeListbit);

			Init(m_poolSize,m_maxDivideNum);
		};
	};
};