/*!	\file		CFpsManager.h
*	\details	CFpsManager�N���X�̐錾
*	\author		�������u
*	\date		2014/02/06
*/
//***********************************************************
//�C���N���[�h
//***********************************************************
#include "Utility/CFpsManager.h"
#include "Utility/Macro.hpp"
#include <ratio>

namespace MMLibrary
{
	namespace Utility
	{
		using std::chrono::time_point;
		using std::chrono::milliseconds;
		using std::chrono::system_clock;
		using std::chrono::duration_cast;
		using std::kilo;

		CFpsManager::CFpsManager()
		{
			Initialize();
		}

		void CFpsManager::Initialize(
			size_t sampleSize,
			float targetFps)
		{
			m_sampleSize = sampleSize;
			m_targetFps = targetFps;

			if (m_msPerFrameSamples.size() > m_sampleSize)
			{
				auto removeSize = m_msPerFrameSamples.size() - m_sampleSize;

				for (; removeSize > 0; --removeSize)
				{
					m_msPerFrameSamples.pop_front();
				}

				m_msPerFrameSamples.resize(m_sampleSize);

				m_sumOfSamples = milliseconds(0);
				for (const auto& sample : m_msPerFrameSamples)
				{
					m_sumOfSamples += sample;
				}

			}
			else if (m_msPerFrameSamples.empty())
			{
				m_nowFrameStartTime = system_clock::now();
				m_sumOfSamples = milliseconds(0);
			}
		}

		float CFpsManager::GetFps()
		{
			return 1000 / GetMilliSecondsPerFrame();
		}

		float CFpsManager::GetMilliSecondsPerFrame()
		{
			return GetAverageOfSamples();
		}

		float CFpsManager::GetFpsAtPreviousFrame()
		{
			return 1000 / GetMilliSecondsPerFrameAtPreviousFrame();
		}

		float CFpsManager::GetMilliSecondsPerFrameAtPreviousFrame()
		{
			return	static_cast<float>(
				duration_cast<std::chrono::milliseconds>(
				*m_msPerFrameSamples.rbegin()).count());
		}

		bool CFpsManager::IsRequestUpdateFrame()
		{
			return (system_clock::now() - m_nowFrameStartTime) >
				std::chrono::milliseconds(static_cast<int>(1000 / m_targetFps));
		}

		void CFpsManager::FrameUpdate()
		{
			auto now = system_clock::now();
			if (m_msPerFrameSamples.size() == m_sampleSize)
			{
				m_sumOfSamples -= *m_msPerFrameSamples.begin();
				m_msPerFrameSamples.pop_front();
			}

			m_msPerFrameSamples.push_back(now - m_nowFrameStartTime);
			m_sumOfSamples += *m_msPerFrameSamples.rbegin();

			m_nowFrameStartTime = now;
		}

		float CFpsManager::GetAverageOfSamples()
		{
			if (m_msPerFrameSamples.empty())
				return 0.0f;

			return static_cast<float>(
				duration_cast<std::chrono::milliseconds>(
				m_sumOfSamples).count()) /
				m_msPerFrameSamples.size();
		}
	}
}