/*!	\file		CTransform.h
*	\details	CTransformNXΜθ`
*	\author		Ό ͺu
*	\date		2014/09/17
*/

//***********************************************************
//CN[h
//***********************************************************
#include "Graphic\CTransform.h"
#include "Math/Interpolation.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CTransform::CTransform(
			const CVector3&					position,
			const CVector3&					scale,
			const Math::CQuaternion<float>&	rotation
			)
			: tagTreeNodeBase(this)
			, m_position(position)
			, m_scale(scale)
			, m_rotation(rotation)
		{};

		CTransform&
			CTransform::MoveTo(
			const float x,
			const float y,
			const float z)
		{
			m_position[0] = x;
			m_position[1] = y;
			m_position[2] = z;

			return *this;
		};

		CTransform&
			CTransform::Move(
			const float x,
			const float y,
			const float z)
		{
			m_position[0] += x;
			m_position[1] += y;
			m_position[2] += z;

			return *this;
		};

		CTransform&
			CTransform::MoveLocal(
			const float x,
			const float y,
			const float z)
		{
			MoveLocal(CVector3({ x, y, z }));

			return *this;
		};

		CTransform&
			CTransform::MoveTo(
			const CVector3& position)
		{
			MoveTo(
				position[0],
				position[1],
				position[2]);

			return *this;
		};

		CTransform&
			CTransform::Move(
			const CVector3& position)
		{
			Move(
				position[0],
				position[1],
				position[2]);

			return *this;
		};

		CTransform&
			CTransform::MoveLocal(
			const CVector3& position)
		{
			CVector3 work =
				position;

			CVector3Helper::TransformNormal(
				work,
				CMatrix4x4::
				CreateByScaling(
				m_scale));

			CVector3Helper::TransformNormal(
				work,
				CMatrix4x4::
				CreateByRotationQuaternion(
				m_rotation));

			Move(work[0],
				work[1],
				work[2]);

			return *this;
		};

		CTransform&
			CTransform::ScalingTo(
			const float x,
			const float y,
			const float z)
		{
			m_scale[0] = x;
			m_scale[1] = y;
			m_scale[2] = z;

			return *this;
		};

		CTransform&
			CTransform::Scaling(
			const float x,
			const float y,
			const float z)
		{
			m_scale[0] *= x;
			m_scale[1] *= y;
			m_scale[2] *= z;

			return *this;
		};

		CTransform&
			CTransform::Scaling(
			const float scale)
		{
			m_scale *= scale;

			return *this;
		};

		CTransform&
			CTransform::ScalingLocal(
			const float x,
			const float y,
			const float z)
		{
			ScalingLocal(CVector3({ x, y, z }));

			return *this;
		};

		CTransform&
			CTransform::ScalingTo(
			const CVector3& scale)
		{
			ScalingTo(scale[0],
				scale[1],
				scale[2]);

			return *this;
		};

		CTransform&
			CTransform::Scaling(
			const CVector3& scale)
		{
			Scaling(scale[0],
				scale[1],
				scale[2]);

			return *this;
		};

		CTransform&
			CTransform::ScalingLocal(
			const CVector3& scale)
		{
			CVector3 work =
				scale;

			CVector3Helper::TransformNormal(
				work,
				CMatrix4x4::
				CreateByRotationQuaternion(
				m_rotation));

			Scaling(work[0],
				work[1],
				work[2]);

			return *this;
		};

		CTransform&
			CTransform::RotateTo(
			const Math::CQuaternion<float>& rotation)
		{
			m_rotation = rotation;

			return *this;
		};

		CTransform&
			CTransform::Rotate(
			const Math::CQuaternion<float>& rotation)
		{
			m_rotation.Multiply(rotation);

			return *this;
		};

		CTransform&
			CTransform::RotateLocal(
			const Math::CQuaternion<float>& rotation)
		{
			m_rotation = rotation.ToClone().
				Multiply(m_rotation);

			return *this;
		};

		CTransform&
			CTransform::SetDirection(
			const CVector3& direction)
		{
			m_rotation.SetByRotationAxis(
				CVector3({ 0, 0, 1 }).
				Cross(direction),
				acos(direction.
				Dot(CVector3({ 0, 0, 1 }))));

			return *this;
		};

		CVector3
			CTransform::GetDirection(void) const
		{
			return CVector3Helper::TransformNormal(
				CVector3({ 0, 0, 1 }),
				CMatrix4x4::CreateByRotationQuaternion(
				m_rotation));
		};

		CTransform&
			CTransform::LookAt(
			const CVector3& targetPos,
			float			angleRegulation)
		{
			auto direction = 
				targetPos - Position();
			direction.Normalize();

			// ρ]²Ζρ]pxπZo
			auto norm =
				GetDirection().
				Cross(direction).
				Normalize();
			float angle = 
				-acos(GetDirection().
				Dot(direction));

			if (angle > angleRegulation) angle = angleRegulation;
			if (angle < -angleRegulation) angle = -angleRegulation;

			if (std::isnan(angle))							return *this;
			if (norm.IsNan())								return *this;
			if (fabs(angle) < Math::DegreeToRadian(0.1f))	return *this;

			Rotate(Math::CQuaternion<>::CreateByRotationAxis(norm, angle));
			
			return *this;
		}

		CMatrix4x4
			CTransform::GetMatrix(void) const
		{
			CMatrix4x4 rtn;

			rtn.SetByRotationQuaternion(m_rotation);
			rtn.AsVector3(0) *= m_scale[0];
			rtn.AsVector3(1) *= m_scale[1];
			rtn.AsVector3(2) *= m_scale[2];
			rtn.AsVector3(3) = m_position;

			return rtn;
		};

		CMatrix4x4
			CTransform::GetLocalToWorldMatrix(void) const
		{
			if (HasParent())
				return GetMatrix() * GetParent()->GetLocalToWorldMatrix();
			else
				return GetMatrix();
		};

		CMatrix4x4
			CTransform::GetWorldToLocalMatrix(void) const
		{
			return GetLocalToWorldMatrix().SetByInverse();
		};

		CTransform CTransform::CreateByLarp(
			const CTransform&	matrix1,
			const CTransform&	matrix2,
			const float			ratio)
		{
			return CTransform(
				Math::Interpolation::Linear(matrix1.Position(), matrix2.Position(), ratio),
				Math::Interpolation::Linear(matrix1.Scale(), matrix2.Scale(), ratio),
				Math::CQuaternion<>::CreateBySlerp(matrix1.Rotation(), matrix2.Rotation(), ratio));
		};
	};
};