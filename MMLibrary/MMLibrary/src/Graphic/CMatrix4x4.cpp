/*!	\file		CMatrix4x4.cpp
*	\details	CMatrix4x4クラスの定義
*	\author		松裏征志
*	\date		2014/04/25
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/CMatrix4x4.h"
#include "Graphic\CVector3.h"
#include "Math\CQuaternion.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CMatrix4x4::CMatrix4x4(void)
		{};

		CMatrix4x4::CMatrix4x4(
			float _00, float _01, float _02, float _03,	
			float _10, float _11, float _12, float _13,
			float _20, float _21, float _22, float _23,
			float _30, float _31, float _32, float _33)
		{
			at(0).at(0) = _00;
			at(0).at(1) = _01;
			at(0).at(2) = _02;
			at(0).at(3) = _03;
			
			at(1).at(0) = _10;
			at(1).at(1) = _11;
			at(1).at(2) = _12;
			at(1).at(3) = _13;
			
			at(2).at(0) = _20;
			at(2).at(1) = _21;
			at(2).at(2) = _22;
			at(2).at(3) = _23;
			
			at(3).at(0) = _30;
			at(3).at(1) = _31;
			at(3).at(2) = _32;
			at(3).at(3) = _33;
		};

		CMatrix4x4::CMatrix4x4(float src)
		{
			SetBy1Element(src);
		};
		
		CMatrix4x4::CMatrix4x4(const MMLibrary::Math::CMatrix<float, 4, 4>& src)
		{
			*this = src;
		};

		CMatrix4x4& CMatrix4x4::operator=(const MMLibrary::Math::CMatrix<float, 4, 4>& src)
		{
			for(int i = 0; i < 4; ++i)
				for(int j = 0; j < 4; ++j)
					at(i).at(j) = src.at(i).at(j);

			return *this;
		};
		
		CMatrix4x4 CMatrix4x4::ToClone(void) const
		{
			return *this;
		};

		CVector3&			CMatrix4x4::Position(void)
		{
			return (CVector3&)at(3).at(0);
		};
			
		CVector3			CMatrix4x4::GetPosition(void) const
		{
			return (CVector3&)at(3).at(0);
		};

		CVector3			CMatrix4x4::GetScale(void) const
		{
			return CVector3{
				((CVector3&)at(0).at(0)).Length(),
				((CVector3&)at(1).at(0)).Length(),
				((CVector3&)at(2).at(0)).Length() };
		};

		Math::CQuaternion<float>	CMatrix4x4::GetRotation(void) const
		{
			return Math::CQuaternion<float>::CreateByRotationMatrix(
				*this);
		};

		CVector3& CMatrix4x4::AsVector3(size_t index)
		{
			return reinterpret_cast<CVector3&>(at(index));
		};

		const CVector3& CMatrix4x4::AsVector3(size_t index) const
		{
			return reinterpret_cast<const CVector3&>(at(index));
		};

		Math::CVector<float, 4>& CMatrix4x4::AsVector4(size_t index)
		{
			return reinterpret_cast<Math::CVector<float, 4>&>(at(index));
		};

		const Math::CVector<float, 4>& CMatrix4x4::AsVector4(size_t index) const
		{
			return reinterpret_cast<const Math::CVector<float, 4>&>(at(index));
		};

		CMatrix4x4 CMatrix4x4::CreateByAffineTransformation(
			const CVector3&	scaling,
			const CVector3&	rotationCenter,
			const CMatrix4x4&	rotationMatrix,
			const CVector3&	translation)
		{
			return CMatrix4x4().
				SetByAffineTransformation(
					scaling,
					rotationCenter,
					rotationMatrix,
					translation);
		};
		
		CMatrix4x4 CMatrix4x4::CreateByLookAtLH(
			const CVector3&	eye,
			const CVector3&	at,
			const CVector3&	up)
		{
			return CMatrix4x4().
				SetByLookAtLH(
					eye,
					at,
					up);
		};
		
		CMatrix4x4 CMatrix4x4::CreateByLookAtRH(
			const CVector3&	eye,
			const CVector3&	at,
			const CVector3&	up)
		{
			return CMatrix4x4().
				SetByLookAtRH(
					eye,
					at,
					up);
		};
		
		CMatrix4x4 CMatrix4x4::CreateByOrthoLH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			return CMatrix4x4().
				SetByOrthoLH(
					width,
					height,
					nearZ,
					farZ);
		};
		
		CMatrix4x4 CMatrix4x4::CreateByOrthoRH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			return CMatrix4x4().
				SetByOrthoRH(
				width,
				height,
				nearZ,
				farZ);
		};

		CMatrix4x4 CMatrix4x4::CreateByOrthoOffCenterLH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			return CMatrix4x4().
				SetByOrthoOffCenterLH(
					minX,
					maxX,
					minY,
					maxY,
					minZ,
					maxZ);
		};

		CMatrix4x4 CMatrix4x4::CreateByOrthoOffCenterRH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			return CMatrix4x4().
				SetByOrthoOffCenterRH(
					minX,
					maxX,
					minY,
					maxY,
					minZ,
					maxZ);
		};
		

		CMatrix4x4 CMatrix4x4::CreateByPerspectiveLH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			return CMatrix4x4().
				SetByPerspectiveLH(
					width,
					height,
					nearZ,
					farZ);
		};
		

		CMatrix4x4 CMatrix4x4::CreateByPerspectiveRH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			return CMatrix4x4().
				SetByPerspectiveRH(
					width,
					height,
					nearZ,
					farZ);
		};
		
		CMatrix4x4 CMatrix4x4::CreateByPerspectiveFovLH(
			const float	fovY,
			const float	aspect,
			const float nearZ,
			const float farZ)
		{
			return CMatrix4x4().
				SetByPerspectiveFovLH(
					fovY,
					aspect,
					nearZ,
					farZ);
		};
		
		CMatrix4x4 CMatrix4x4::CreateByPerspectiveFovRH(
			const float	fovY,
			const float	aspect,
			const float nearZ,
			const float farZ)
		{
			return CMatrix4x4().
				SetByPerspectiveFovRH(
				fovY,
				aspect,
				nearZ,
				farZ);
		};

		CMatrix4x4 CMatrix4x4::CreateByPerspectiveOffCenterLH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			return CMatrix4x4().
				SetByPerspectiveOffCenterLH(
				minX,
				maxX,
				minY,
				maxY,
				minZ,
				maxZ);
		};

		CMatrix4x4 CMatrix4x4::CreateByPerspectiveOffCenterRH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			return CMatrix4x4().
				SetByPerspectiveOffCenterRH(
					minX,
					maxX,
					minY,
					maxY,
					minZ,
					maxZ);
		};

		CMatrix4x4 CMatrix4x4::CreateByViewPort(
			const viewport_t& viewport)
		{
			return CMatrix4x4().
				SetByViewPort(viewport);
		};

		CMatrix4x4 CMatrix4x4::CreateByTranslation(
			const CVector3&	translation)
		{
			return CMatrix4x4().
				SetByTranslation(translation);
		};

		CMatrix4x4 CMatrix4x4::CreateByScaling(
			const CVector3&	scaling)
		{
			return CMatrix4x4().
				SetByScaling(scaling);
		};

		CMatrix4x4 CMatrix4x4::CreateByRotationAxis(
			const CVector3&	axis,
			const float		radian)
		{
			return CMatrix4x4().
				SetByRotationAxis(
					axis,
					radian);
		};
		
		CMatrix4x4 CMatrix4x4::CreateByRotationQuaternion(
			const Math::CQuaternion<>&	quaternion)
		{
			return CMatrix4x4().
				SetByRotationQuaternion(
					quaternion);
		};

		CMatrix4x4 CMatrix4x4::CreateByRotationX(
			const float radian)
		{
			return CMatrix4x4().
				SetByRotationX(radian);
		};

		CMatrix4x4 CMatrix4x4::CreateByRotationY(
			const float radian)
		{
			return CMatrix4x4().
				SetByRotationY(radian);
		};

		CMatrix4x4 CMatrix4x4::CreateByRotationZ(
			const float radian)
		{
			return CMatrix4x4().
				SetByRotationZ(radian);
		};

		CMatrix4x4 CMatrix4x4::CreateByRotationYawPitchRoll(
			const float	x,
			const float	y,
			const float	z)
		{
			return CMatrix4x4().
				SetByRotationYawPitchRoll(
					x,
					y,
					z);
		};

		CMatrix4x4 CMatrix4x4::CreateByRotationYawPitchRoll(
			const CVector3&	rotation)
		{
			return CMatrix4x4().
				SetByRotationYawPitchRoll(rotation);
		};
		
		CMatrix4x4& CMatrix4x4::SetByAffineTransformation(
			const CVector3&		scaling,
			const CVector3&		rotationCenter,
			const CMatrix4x4&	rotationMatrix,
			const CVector3&		translation)
		{
				SetByIdentity().
					Multiply(CreateByScaling(scaling)).
					Multiply(CreateByTranslation(rotationCenter).SetByInverse()).
					Multiply(rotationMatrix).
					Multiply(CreateByTranslation(rotationCenter)).
					Multiply(CreateByTranslation(translation));

			return *this;
		};
		
		CMatrix4x4& CMatrix4x4::SetByLookAtLH(
			const CVector3&	eye,
			const CVector3&	at,
			const CVector3&	up)
		{
			CVector3 zAxis = (at - eye).Normalize();
			CVector3 xAxis = up.ToClone().Cross(zAxis).Normalize();
			CVector3 yAxis = zAxis.ToClone().Cross(xAxis);
			
			SetByIdentity();
			
			for(int m = 0; m < 3; ++m)
			{
				this->at(m).at(0) = xAxis.at(m);
				this->at(m).at(1) = yAxis.at(m);
				this->at(m).at(2) = zAxis.at(m);
			}
			
			this->at(3).at(0) = -xAxis.Dot(eye);
			this->at(3).at(1) = -yAxis.Dot(eye);
			this->at(3).at(2) = -zAxis.Dot(eye);

			return *this;
		};
		
		CMatrix4x4& CMatrix4x4::SetByLookAtRH(
			const CVector3&	eye,
			const CVector3&	at,
			const CVector3&	up)
		{
			CVector3 zAxis = (eye - at).Normalize();
			CVector3 xAxis = up.ToClone().Cross(zAxis).Normalize();
			CVector3 yAxis = zAxis.ToClone().Cross(xAxis);
			
			SetByIdentity();
			
			for(int m = 0; m < 3; ++m)
			{
				this->at(m).at(0) = xAxis.at(m);
				this->at(m).at(1) = yAxis.at(m);
				this->at(m).at(2) = zAxis.at(m);
			}
			
			this->at(3).at(0) = -xAxis.Dot(eye);
			this->at(3).at(1) = -yAxis.Dot(eye);
			this->at(3).at(2) = -zAxis.Dot(eye);

			return *this;
		};
		
		CMatrix4x4& CMatrix4x4::SetByOrthoLH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			SetByIdentity();
			
			at(0).at(0) = 2.0f			/ width;
			at(1).at(1) = 2.0f			/ height;
			at(2).at(2) = 1.0f			/ (farZ - nearZ);
			at(3).at(2) = nearZ			/ (nearZ - farZ);

			return *this;
		};
		
		CMatrix4x4& CMatrix4x4::SetByOrthoRH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			SetByIdentity();
			
			at(0).at(0) = 2.0f			/ width;
			at(1).at(1) = 2.0f			/ height;
			at(2).at(2) = 1.0f			/ (nearZ - farZ);
			at(3).at(2) = nearZ			/ (nearZ - farZ);

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByOrthoOffCenterLH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			SetByIdentity();
			
			at(0).at(0) = 2.0f			/ (maxX - minX);
			at(3).at(0) = (maxX + minX)	/ (minX - maxX);
			at(1).at(1) = 2.0f			/ (minY - maxY);
			at(3).at(1) = (maxY + minY)	/ (maxY - minY);
			at(2).at(2) = 1.0f			/ (maxZ - minZ);
			at(3).at(2) = minZ			/ (minZ - maxZ);

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByOrthoOffCenterRH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			SetByIdentity();

			at(0).at(0) = 2.0f			/ (maxX - minX);
			at(3).at(0) = (maxX + minX)	/ (minX - maxX);
			at(1).at(1) = 2.0f			/ (minY - maxY);
			at(3).at(1) = (maxY + minY)	/ (maxY - minY);
			at(2).at(2) = 1.0f			/ (minZ - maxZ);
			at(3).at(2) = minZ			/ (minZ - maxZ);
			
			return *this;
		};
		

		CMatrix4x4& CMatrix4x4::SetByPerspectiveLH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			SetByIdentity();
			
			at(0).at(0) = 2.0f	* nearZ	/ width;
			at(1).at(1) = 2.0f	* nearZ	/ height;
			at(2).at(2) = 1.0f	* farZ	/ (farZ - nearZ);
			at(3).at(2) = nearZ	* farZ	/ (nearZ - farZ);

			return *this;
		};
		

		CMatrix4x4& CMatrix4x4::SetByPerspectiveRH(
			const float	width,
			const float	height,
			const float nearZ,
			const float farZ)
		{
			SetByIdentity();
			
			at(0).at(0) = 2.0f	* nearZ	/ width;
			at(1).at(1) = 2.0f	* nearZ	/ height;
			at(2).at(2) = 1.0f	* farZ	/ (nearZ - farZ);
			at(3).at(2) = nearZ	* farZ	/ (nearZ - farZ);
			at(2).at(3) = -1.0f;
			
			return *this;
		};
		
		CMatrix4x4& CMatrix4x4::SetByPerspectiveFovLH(
			const float	fovY,
			const float	aspect,
			const float nearZ,
			const float farZ)
		{
			float height	= 1 / tan(fovY / 2.0f);
			float width		= height / aspect;
			
			SetByIdentity();

			at(0).at(0) = width;
			at(1).at(1) = height;
			at(2).at(2) =  farZ			/ (farZ - nearZ);
			at(3).at(2) = -nearZ * farZ	/ (farZ - nearZ);
			at(2).at(3) = 1.0f;

			return *this;
		};
		
		CMatrix4x4& CMatrix4x4::SetByPerspectiveFovRH(
			const float	fovY,
			const float	aspect,
			const float nearZ,
			const float farZ)
		{
			float height	= 1 / tan(fovY / 2.0f);
			float width		= height / aspect;
			
			SetByIdentity();
			
			at(0).at(0) = width;
			at(1).at(1) = height;
			at(2).at(2) =   farZ		/ (nearZ - farZ);
			at(3).at(2) = nearZ * farZ	/ (nearZ - farZ);
			at(2).at(3) = -1.0f;
			
			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByPerspectiveOffCenterLH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			SetByIdentity();
			
			at(0).at(0) = 2.0f	* minZ	/ (maxX - minX);
			at(3).at(0) = (maxX + minX)	/ (minX - maxX);
			at(1).at(1) = 2.0f	* minZ	/ (maxY - minY);
			at(3).at(1) = (maxY + minY)	/ (minY - maxY);
			at(2).at(2) =		 maxZ	/ (maxZ - minZ);
			at(3).at(2) = minZ	* maxZ	/ (minZ - maxZ);

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByPerspectiveOffCenterRH(
			const float	minX,
			const float	maxX,
			const float minY,
			const float maxY,
			const float minZ,
			const float maxZ)
		{
			SetByIdentity();
			
			at(0).at(0) = 2.0f	* minZ	/ (maxX - minX);
			at(3).at(0) = (maxX + minX)	/ (maxX - minX);
			at(1).at(1) = 2.0f	* minZ	/ (maxY - minY);
			at(3).at(1) = (maxY + minY)	/ (maxY - minY);
			at(2).at(2) =		maxZ	/ (minZ - maxZ);
			at(3).at(2) = minZ	* maxZ	/ (minZ - maxZ);
			at(2).at(3) = -1.0f;

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByViewPort(
			const viewport_t& viewport)
		{
			SetByIdentity();

			at(0).at(0) = viewport.width	/ 2.0f;
			at(3).at(0) = viewport.width	/ 2.0f + viewport.x;

			at(1).at(1) = viewport.height	/ 2.0f;
			at(3).at(1) = viewport.height	/ 2.0f + viewport.y;
			
			at(2).at(2) = viewport.maxZ - viewport.maxZ;
			at(3).at(2) = viewport.maxZ ;

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByTranslation(
			const CVector3&	translation)
		{
			SetByIdentity();

			for(int i = 0; i < 3; ++i)
				at(3).at(i) = translation.at(i);

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByScaling(
			const CVector3&	scaling)
		{
			SetByIdentity();

			for(int i = 0; i < 3; ++i)
			{
				at(i).at(i) = scaling.at(i);
			}

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByRotationAxis(
			const CVector3&	axis,
			const float		radian)
		{
			SetByRotationQuaternion(
				Math::CQuaternion<float>::CreateByRotationAxis(
					axis,
					radian));

			return *this;
		};
		
		CMatrix4x4& CMatrix4x4::SetByRotationQuaternion(
			const Math::CQuaternion<float>&	quaternion)
		{
			SetByIdentity();

			at(0).at(0) = 
				1 - 2 * (	quaternion.Vector().at(1)	* 
							quaternion.Vector().at(1)	+ 
							quaternion.Vector().at(2)	* 
							quaternion.Vector().at(2));
			at(1).at(0) = 
					2 * (	quaternion.Vector().at(0)	* 
							quaternion.Vector().at(1)	+ 
							quaternion.Scalar()			* 
							quaternion.Vector().at(2));
			at(2).at(0) = 
					2 * (	quaternion.Vector().at(2)	* 
							quaternion.Vector().at(0)	- 
							quaternion.Scalar()			* 
							quaternion.Vector().at(1));

			at(0).at(1) = 
					2 * (	quaternion.Vector().at(0)	* 
							quaternion.Vector().at(1)	- 
							quaternion.Scalar()			* 
							quaternion.Vector().at(2));
			at(1).at(1) = 
				1 - 2 * (	quaternion.Vector().at(2)	* 
							quaternion.Vector().at(2)	+ 
							quaternion.Vector().at(0)	* 
							quaternion.Vector().at(0));
			at(2).at(1) = 
					2 * (	quaternion.Vector().at(1)	* 
							quaternion.Vector().at(2)	+ 
							quaternion.Scalar()			* 
							quaternion.Vector().at(0));
			
			at(0).at(2) = 
					2 * (	quaternion.Vector().at(0)	* 
							quaternion.Vector().at(2)	+ 
							quaternion.Scalar()			* 
							quaternion.Vector().at(1));
			at(1).at(2) = 
					2 * (	quaternion.Vector().at(1)	* 
							quaternion.Vector().at(2)	- 
							quaternion.Scalar()			* 
							quaternion.Vector().at(0));
			at(2).at(2) = 
				1 - 2 * (	quaternion.Vector().at(0)	* 
							quaternion.Vector().at(0)	+ 
							quaternion.Vector().at(1)	* 
							quaternion.Vector().at(1));

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByRotationX(
			const float radian)
		{
			SetByIdentity();
			
			at(1).at(1) =  cos(radian);
			at(1).at(2) =  sin(radian);
			at(2).at(1) = -sin(radian);
			at(2).at(2) =  cos(radian);

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByRotationY(
			const float radian)
		{
			SetByIdentity();
			
			at(0).at(0) =  cos(radian);
			at(2).at(0) =  sin(radian);
			at(0).at(2) = -sin(radian);
			at(2).at(2) =  cos(radian);

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByRotationZ(
			const float radian)
		{
			SetByIdentity();
			
			at(0).at(0) =  cos(radian);
			at(0).at(1) =  sin(radian);
			at(1).at(0) = -sin(radian);
			at(1).at(1) =  cos(radian);

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByRotationYawPitchRoll(
			const float	x,
			const float	y,
			const float	z)
		{
			SetByIdentity().
				Multiply(CreateByRotationZ(z)).
				Multiply(CreateByRotationX(x)).
				Multiply(CreateByRotationY(y));

			return *this;
		};

		CMatrix4x4& CMatrix4x4::SetByRotationYawPitchRoll(
			const CVector3&	rotation)
		{
			SetByRotationYawPitchRoll(
				rotation.at(0),
				rotation.at(1),
				rotation.at(2));

			return *this;
		};
	};
};