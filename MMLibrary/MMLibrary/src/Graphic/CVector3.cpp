/*!	\file		CVector3.cpp
*	\details	CVector3�N���X�̒�`
*	\author		�������u
*	\date		2014/04/25
*/

//***********************************************************
//�C���N���[�h
//***********************************************************
#include "Graphic/CVector3.h"
#include "Graphic/CMatrix4x4.h"

namespace MMLibrary
{
	namespace Graphic
	{
		Math::CMatrix<float, 1, 4> CVector3Helper::ToRowMatrix(
			CVector3&		vector)
		{
			Math::CMatrix<float, 1, 4> work;
			work[0][0] = vector[0];
			work[0][1] = vector[1];
			work[0][2] = vector[2];
			work[0][3] = 1;

			return work;
		}

		CVector3& CVector3Helper::Project(
			CVector3&			vector,
			const CMatrix4x4&	viewport,
			const CMatrix4x4&	projection,
			const CMatrix4x4&	view,
			const CMatrix4x4&	world)
		{
			auto work = ToRowMatrix(vector);

			work.
				Multiply(world).
				Multiply(view).
				Multiply(projection).
				Multiply(viewport);
			
			for(uint_t i = 0; i < 3; ++i)
			{
				vector[i] = work[0][i] / work[0][3];
			}

			return vector;
		};

		CVector3& CVector3Helper::UnProject(
			CVector3&			vector,
			const CMatrix4x4&	viewport,
			const CMatrix4x4&	projection,
			const CMatrix4x4&	view,
			const CMatrix4x4&	world)
		{
			auto work = ToRowMatrix(vector);

			work.
				Multiply(viewport.ToClone().SetByInverse()).
				Multiply(projection.ToClone().SetByInverse()).
				Multiply(view.ToClone().SetByInverse()).
				Multiply(world.ToClone().SetByInverse());
			
			for(uint_t i = 0; i < 3; ++i)
			{
				vector[i] = work[0][i] / work[0][3];
			}

			return vector;
		};
		
		CVector3& CVector3Helper::TransformCoord(
			CVector3&			vector,
			const CMatrix4x4&	matrix)
		{
			auto work = ToRowMatrix(vector);

			work.Multiply(matrix);
			
			if(work[0][3] == 0)
				for(uint_t i = 0; i < 3; ++i)
				{
					vector[i] = work[0][i];
				}
			else				
				for(uint_t i = 0; i < 3; ++i)
				{
					vector[i] = work[0][i] * work[0][3];
				}

			return vector;
		};
		
		CVector3& CVector3Helper::TransformNormal(
			CVector3&			vector,
			const CMatrix4x4&	matrix)
		{
			auto work = ToRowMatrix(vector);

			work[0][3] = 0;

			work.Multiply(matrix);
			
			for(uint_t i = 0; i < 3; ++i)
			{
				vector[i] = work[0][i];
			}

			return vector;
		};

		CVector3& CVector3Helper::RotateReg(
			CVector3&		vector,
			const CVector3& axis,
			float			radian,
			const CVector3& min,
			const CVector3& max)
		{
			//��]��x�N�g��
			auto after = vector;
			CVector3Helper::TransformCoord(
				after,
				CMatrix4x4::CreateByRotationAxis(axis, radian));

			// ��]���Ɖ�]�p�x���Z�o
			auto norm = 
				CVector3Helper::GetFront().
				Cross(after).
				Normalize();
			float angle = acos(CVector3Helper::GetFront().Dot(after));

			// ��]�s����쐬
			auto rotMat = 
				CMatrix4x4::CreateByRotationAxis(
				norm,
				angle);

			// ����]�p�x���Z�o
			// X�����
			float xLimit = Math::DegreeToRadian(80.0f);
			float sx = -rotMat.data()[3][2];    // sin(��x)
			float xAngle = asin(sx);   // X����茈��
			float cx = cos(xAngle);

			// �W���o�����b�N���
			if (fabs(xAngle) > xLimit)
			{
				xAngle = (xAngle<0) ? -xLimit : xLimit;
				cx = cos(xAngle);
			}

			// Y�����
			float sy = rotMat.data()[3][1] / cx;
			float cy = rotMat.data()[3][3] / cx;
			float yAngle = atan2(sy, cy);   // Y����茈��

			// Z�����
			float sz = rotMat.data()[1][2] / cx;
			float cz = rotMat.data()[2][2] / cx;
			float zAngle = atan2(sz, cz);

			// �p�x�̐���
			if (xAngle < min[0]) xAngle = min[0];
			if (xAngle > max[0]) xAngle = max[0];
			if (yAngle < min[1]) yAngle = min[1];
			if (yAngle > max[1]) yAngle = max[1];
			if (zAngle < min[2]) zAngle = min[2];
			if (zAngle > max[2]) zAngle = max[2];

			CVector3Helper::TransformCoord(
				vector,
				CMatrix4x4::CreateByRotationYawPitchRoll(
				yAngle,
				xAngle,
				zAngle));

			return vector;
		}
	};
};