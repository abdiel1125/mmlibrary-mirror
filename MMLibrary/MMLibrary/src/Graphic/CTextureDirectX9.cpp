/*!	\file		CTextureDirectX9.cpp
*	\details	CTextureの定義　DirectX9
*	\author		松裏征志
*	\date		2015/03/03 分離して作成
*/
#ifdef USING_DIRECTX
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Texture/CTexture.h"
#include "Utility/Macro.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace
		{
			DWORD Convert(TEXTURE_TYPE type)
			{
				switch (type)
				{
				case MMLibrary::Graphic::TEXTURE_TYPE::DEFAULT:
					return 0;
				case MMLibrary::Graphic::TEXTURE_TYPE::RENDER_TARGET:
					return D3DUSAGE_RENDERTARGET;
				case MMLibrary::Graphic::TEXTURE_TYPE::DYNAMIC:
					return D3DUSAGE_DYNAMIC;
				default:
					return 0;
				}
			}

			D3DFORMAT Convert(COLOR_FORMAT format)
			{
				D3DFORMAT returnFormat;

				switch (format)
				{
				case COLOR_FORMAT::GRAY8:
					returnFormat = D3DFMT_L8;
					break;
				case COLOR_FORMAT::R8G8B8:
					returnFormat = D3DFMT_R8G8B8;
					break;
				case COLOR_FORMAT::R8G8B8A8:
					returnFormat = D3DFMT_A8R8G8B8;
					break;
				case COLOR_FORMAT::DEPTH:
					returnFormat = D3DFMT_L8;
					break;
				}

				return returnFormat;
			}
		}

		CTextureBase::CTextureBase()
			:m_handle(nullptr){};

		CTextureBase::CTextureBase(CTextureBase&& other)
		{
			m_handle = other.m_handle;

			other.m_handle = nullptr;
		}

		void CTextureBase::Dispose(void)
		{
			Utility::SafeRelease(m_handle);
		}


		CTextureBase& CTextureBase::SetByColorArray(
			CColor32* pointer,
			size_t	width,
			size_t	height)
		{
			D3DLOCKED_RECT lockedRect;
			// ロック
			CheckDirectXErrer(m_handle->LockRect(0, &lockedRect, NULL, 0));

			// テクスチャメモリへのポインタ
			DWORD *pTexBuf = (DWORD*)lockedRect.pBits;

			for (size_t y = 0; y < height; ++y)
			{
				for (size_t x = 0; x < width; ++x)
				{
					CColor32& color = pointer[y * width + x];
					pTexBuf[y * width + x] = D3DCOLOR_RGBA(color.Red(), color.Green(), color.Blue(), color.Alpha());
				}
			}

			// アンロック
			m_handle->UnlockRect(0);

			return *this;
		};

		CTextureBase& CTextureBase::SetBySize(
			size_t			width,
			size_t			height,
			TEXTURE_TYPE	type,
			COLOR_FORMAT	colorFormat)
		{
			auto result = 
				CGraphicManager::GetManager().
				GetDevice()->CreateTexture(
				width,
				height,
				0,
				Convert(type),
				Convert(colorFormat),
				D3DPOOL_MANAGED,
				&m_handle,
				NULL
				);
			if (FAILED(result))
			{
				CheckDirectXErrer(
					CGraphicManager::GetManager().
					GetDevice()->CreateTexture(
					width,
					height,
					0,
					Convert(type),
					Convert(colorFormat),
					D3DPOOL_DEFAULT,
					&m_handle,
					NULL
					));
			}

			return *this;
		}

		void CTextureBase::SetToDevice(uint_t textureUnitID) const
		{
			CheckDirectXErrer(
				CGraphicManager::GetManager().GetDevice()->SetTexture(
				textureUnitID,
				m_handle));
		};
		void CTextureBase::UnSetFromDevice(uint_t textureUnitID)
		{
			CheckDirectXErrer(
				CGraphicManager::GetManager().GetDevice()->SetTexture(
				textureUnitID,
				nullptr));
		};
	}
}

#endif // USING_DIRECTX
