/*!	\file		CTextureOpenGL.cpp
*	\details	CTextureの定義　OpenGL
*	\author		松裏征志
*	\date		2015/03/03 分離して作成
*/
#ifdef USING_OPENGL
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Texture/CTexture.h"
#include "Utility/Macro.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CTextureBase::CTextureBase()
			:m_handle(0){};

		CTextureBase::CTextureBase(CTextureBase&& other)
		{
			m_handle = other.m_handle;

			other.m_handle = 0;
		}

		void CTextureBase::Dispose(void)
		{
			glDeleteTextures(1, &m_handle);
			m_handle = 0;
		}

		CTextureBase& CTextureBase::SetByColorArray(
			CColor32* pointer,
			size_t	width,
			size_t	height)
		{
			glBindTexture(GL_TEXTURE_2D, m_handle);

			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_RGBA,
				width,
				height,
				0,
				GL_RGBA,
				GL_UNSIGNED_BYTE,
				pointer);

			glBindTexture(GL_TEXTURE_2D, 0);

			CGLFunctions::GetInstance().CheckErrer();

			return *this;
		};

		CTextureBase& CTextureBase::SetBySize(
			size_t			width,
			size_t			height,
			TEXTURE_TYPE	type,
			COLOR_FORMAT	colorFormat)
		{
			glGenTextures(1, &m_handle);
			glBindTexture(GL_TEXTURE_2D, m_handle);

			GLenum format;

			switch (colorFormat)
			{
			case MMLibrary::Graphic::COLOR_FORMAT::GRAY8:
				format = GL_LUMINANCE;
				break;
			case MMLibrary::Graphic::COLOR_FORMAT::R8G8B8:
				format = GL_RGB;
				break;
			case MMLibrary::Graphic::COLOR_FORMAT::R8G8B8A8:
				format = GL_RGBA;
				break;
			case MMLibrary::Graphic::COLOR_FORMAT::DEPTH:
				format = GL_DEPTH_COMPONENT;
				break;
			}

			switch (type)
			{
			case MMLibrary::Graphic::TEXTURE_TYPE::DEFAULT:
				glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				break;
			case MMLibrary::Graphic::TEXTURE_TYPE::RENDER_TARGET:
				glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, nullptr);
				glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				break;
			case MMLibrary::Graphic::TEXTURE_TYPE::DYNAMIC:
				glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				break;
			}

			glBindTexture(GL_TEXTURE_2D, 0);

			CGLFunctions::GetInstance().CheckErrer();

			return *this;
		}

		void CTextureBase::SetToDevice(uint_t textureUnitID) const
		{
			CGLFunctions::GetInstance().glActiveTexture(textureUnitID + GL_TEXTURE0);
			glBindTexture(
				GL_TEXTURE_2D,
				m_handle);
			CGLFunctions::GetInstance().CheckErrer();
		};

		void CTextureBase::UnSetFromDevice(uint_t textureUnitID)
		{
			CGLFunctions::GetInstance().glActiveTexture(textureUnitID + GL_TEXTURE0);
			glBindTexture(
				GL_TEXTURE_2D,
				0);
			CGLFunctions::GetInstance().CheckErrer();
		};
	}
}

#endif // USING_OPENGL