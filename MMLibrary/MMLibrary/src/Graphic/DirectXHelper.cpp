/*!	\file		DirectXHelper.cpp
*	\details	DirectXの機能を補助するクラス・関数の定義
*	\author		松裏征志
*	\date		2014/11/16	作成
*/
//***********************************************************
//インクルード
//***********************************************************
#ifdef USING_DIRECTX
#include "Graphic\DirectXHelper.h"

namespace	MMLibrary
{
	namespace Graphic
	{
		bool CheckDirectXErrer(HRESULT errerCode)
		{
			switch (errerCode)
			{
			case D3D_OK: // = S_OK
				//	エラーは発生していない。
				return true;
				break;
			case D3DOK_NOAUTOGEN:
				//	    これは成功コードである。ただし、このフォーマットに対するミップマップの自動生成はサポートされていない。
				//		これは、リソース作成は成功するが、ミップマップ レベルは自動生成されないことを意味する。
				return true;
				break;
			case D3DERR_CONFLICTINGRENDERSTATE:
				assert(0 && "現在設定されているレンダリング ステートは同時には使えない。");
				break;
			case D3DERR_CONFLICTINGTEXTUREFILTER:
				assert(0 && "現在のテクスチャ フィルタは同時には使えない。");
				break;
			case D3DERR_CONFLICTINGTEXTUREPALETTE:
				assert(0 && "現在のテクスチャは同時には使えない。");
				break;
			case D3DERR_DEVICELOST:
				assert(0 && "デバイスは、消失しているが、現在リセットできない。したがって、レンダリングは不可能である。");
				break;
			case D3DERR_DEVICENOTRESET:
				assert(0 && "デバイスは、消失しているが、現在リセットできる。");
				break;
			case D3DERR_DRIVERINTERNALERROR:
				assert(0 && "内部ドライバ エラー。このエラーを受け取った場合、アプリケーションは一般にシャットダウンしなければならない。詳細については、(http://msdn.microsoft.com/ja-jp/library/cc324415.aspx)を参照すること。");
				break;
			case D3DERR_INVALIDCALL:
				assert(0 && "メソッドの呼び出しが無効である。たとえば、メソッドのパラメータが有効なポインタではない。");
				break;
			case D3DERR_INVALIDDEVICE:
				assert(0 && "要求されたデバイスの種類が無効である。");
				break;
			case D3DERR_MOREDATA:
				assert(0 && "指定されたバッファ サイズに保持できる以上のデータが存在する。");
				break;
			case D3DERR_NOTAVAILABLE:
				assert(0 && "このデバイスは、照会されたテクニックをサポートしていない。");
				break;
			case D3DERR_NOTFOUND:
				assert(0 && "要求された項目が見つからなかった。");
				break;
			case D3DERR_OUTOFVIDEOMEMORY:
				assert(0 && "Direct3D が処理を行うのに十分なディスプレイ メモリがない。");
				break;
			case D3DERR_TOOMANYOPERATIONS:
				assert(0 && "デバイスがサポートしている数より多くのテクスチャ フィルタリング処理を、アプリケーションが要求している。");
				break;
			case D3DERR_UNSUPPORTEDALPHAARG:
				assert(0 && "アルファ チャンネルに対して指定されているテクスチャ ブレンディング引数を、デバイスがサポートしていない。");
				break;
			case D3DERR_UNSUPPORTEDALPHAOPERATION:
				assert(0 && "アルファ チャンネルに対して指定されているテクスチャ ブレンディング処理を、デバイスがサポートしていない。");
				break;
			case D3DERR_UNSUPPORTEDCOLORARG:
				assert(0 && "色値に対して指定されているテクスチャ ブレンディング引数を、デバイスがサポートしていない。");
				break;
			case D3DERR_UNSUPPORTEDCOLOROPERATION:
				assert(0 && "色値に対して指定されているテクスチャ ブレンディング処理を、デバイスがサポートしていない。");
				break;
			case D3DERR_UNSUPPORTEDFACTORVALUE:
				assert(0 && "デバイスが指定されたテクスチャ係数値をサポートしていない。");
				break;
			case D3DERR_UNSUPPORTEDTEXTUREFILTER:
				assert(0 && "デバイスが指定されたテクスチャ フィルタをサポートしていない。");
				break;
			case D3DERR_WRONGTEXTUREFORMAT:
				assert(0 && "テクスチャ サーフェイスのピクセル フォーマットが無効である。");
				break;
			case E_FAIL:
				assert(0 && "Direct3D サブシステム内で原因不明のエラーが発生した。");
				break;
			case E_INVALIDARG:
				assert(0 && "無効なパラメータが関数に渡された。");
				break;
				//case E_INVALIDCALL:
				//	assert(0 && "メソッドの呼び出しが無効である。たとえば、メソッドのパラメータに無効な値が設定されている場合などである。");
				//	return false;
				//	break;
			case E_OUTOFMEMORY:
				assert(0 && "Direct3D が呼び出しを完了するための十分なメモリを割り当てることができなかった。");
				break;
			default:
				assert(0 && "未知のエラー");
			}
			return false;
		}
	}
}
#endif	//USING_DIRECTX