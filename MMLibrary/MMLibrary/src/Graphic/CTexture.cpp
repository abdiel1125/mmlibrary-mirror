/*!	\file		CTexture.cpp
*	\details	CTextureÌè`
*	\author		¼ ªu
*	\date		2014/12/12
*/
//***********************************************************
//CN[h
//***********************************************************
#include "Graphic/Texture/CTexture.h"
#include "Utility/Macro.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CTextureBase::~CTextureBase()
		{
			Dispose();
		}

		CTextureBase::CTextureBase(
			size_t			width,
			size_t			height,
			TEXTURE_TYPE	type,
			COLOR_FORMAT	colorFormat)
		{
			SetBySize(width, height, type, colorFormat);
		}

	}
}
