/*!	\file		CCamera.cpp
*	\details	CCameraクラスの定義
*	\author		松裏征志
*	\date		2014/05/15
*/
//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic/CCamera.h"

namespace	MMLibrary
{
	namespace	Graphic
	{
		CCamera::CCamera(void)
			:m_position({ 0, 0, -1 })
			, m_target({ 0, 0, 0 })
			, m_upVector({ 0, 1, 0 })
			, m_fovY(Math::DegreeToRadian(90))
			, m_aspect(640.0f / 480.0f)
			, m_nearZ(0.0001f)
			, m_farZ(1000.0f)
		{};

		CCamera& CCamera::MoveLocal(const CVector3& translation)
		{
			CMatrix4x4 mat;
			auto vec = Target() - Position();
			mat[2][0] = vec[0];
			mat[2][1] = vec[1];
			mat[2][2] = vec[2];

			mat[1][0] = UpVector()[0];
			mat[1][1] = UpVector()[1];
			mat[1][2] = UpVector()[2];

			vec = UpVector().ToClone().Cross(vec);
			mat[0][0] = vec[0];
			mat[0][1] = vec[1];
			mat[0][2] = vec[2];

#ifdef USING_OPENGL
#endif // USING_OPENGL

			auto buf = CVector3Helper::TransformCoord(translation.ToClone(), mat);
			Position() += buf;
			Target() += buf;

			return *this;
		};

		CCamera& CCamera::MoveLocal(const float& x, const float& y, const float& z)
		{
			MoveLocal(CVector3({ x, y, z }));
			return *this;
		};

		CCamera& CCamera::Rotation(const CMatrix4x4& rotation)
		{
			CVector3 vector = Target() - Position();
			auto nowRotation = Math::CQuaternion<>::CreateBy2Vectors(CVector3({ 0, 0, 1 }), vector);
			auto mat = rotation * CMatrix4x4::CreateByRotationQuaternion(nowRotation);

			CVector3Helper::TransformCoord(vector, mat);
			CVector3Helper::TransformNormal(UpVector(), mat);

			Target() = Position() + vector;

			return *this;
		};

		CCamera& CCamera::Rotation(const Math::CQuaternion<>& rotation)
		{
			Rotation(CMatrix4x4::CreateByRotationQuaternion(rotation));
			return *this;
		};

		CCamera& CCamera::Rotation(const CVector3& axis, const float& radian)
		{
			Rotation(CMatrix4x4::CreateByRotationAxis(axis, radian));
			return *this;
		};

		CMatrix4x4	CCamera::GetViewMatrix(void)
		{
			return CMatrix4x4::CreateByLookAtLH(
				m_position,
				m_target,
				m_upVector);
		};

		CMatrix4x4	CCamera::GetProjectionMatrix(void)
		{
			return CMatrix4x4::CreateByPerspectiveFovLH(
				m_fovY,
				m_aspect,
				m_nearZ,
				m_farZ);
		};
	};
};