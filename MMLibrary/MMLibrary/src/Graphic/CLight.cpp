/*!	\file		CLight.h
*	\details	CLightクラスの定義
*	\author		松裏征志
*	\date		2014/06/27
*/
//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic/CLight.h"
#include	"Utility/RangeAlgorithm.h"

namespace	MMLibrary
{
	namespace	Graphic
	{
		CLight::CLight(void)
			:m_type(LIGHT_DIRECTIONAL)
			,m_position(0)
			, m_direction({ 0, -1, 0 })
			,m_range(1000)
			,m_falloff(1)
			,m_attenuation0(0)
			,m_attenuation1(1)
			,m_attenuation2(0)
			,m_theta(Math::DegreeToRadian(30))
			,m_phi(Math::DegreeToRadian(30))
			,m_id(0)
		{
			m_diffuse.fill(1);
		};

		CColor32	CLight::GetDiffuse	(void) const
		{
			return CColor32(m_diffuse);
		};

		CColor32	CLight::GetAmbient	(void) const
		{
			return CColor32(m_ambient);
		};

		CColor32	CLight::GetSpecular	(void) const
		{
			return CColor32(m_specular);
		};

		uint_t	
					CLight::GetId	(void) const
		{
			return m_id;
		};
				
		CLight&		CLight::SetDiffuse	(const CColor32& diffuse)
		{
			m_diffuse.at(0) = ToFloatColor(diffuse.Red());
			m_diffuse.at(1) = ToFloatColor(diffuse.Green());
			m_diffuse.at(2) = ToFloatColor(diffuse.Blue());
			m_diffuse.at(3) = ToFloatColor(diffuse.Alpha());
			return *this;
		};

		CLight&		CLight::SetAmbient	(const CColor32& ambient)
		{
			m_ambient.at(0) = ToFloatColor(ambient.Red());
			m_ambient.at(1) = ToFloatColor(ambient.Green());
			m_ambient.at(2) = ToFloatColor(ambient.Blue());
			m_ambient.at(3) = ToFloatColor(ambient.Alpha());
			return *this;
		};

		CLight&		CLight::SetSpecular	(const CColor32& specular)
		{
			m_specular.at(0) = ToFloatColor(specular.Red());
			m_specular.at(1) = ToFloatColor(specular.Green());
			m_specular.at(2) = ToFloatColor(specular.Blue());
			m_specular.at(3) = ToFloatColor(specular.Alpha());
			return *this;
		};

		CLight&		CLight::SetId		(const uint_t id)
		{
			m_id = id;
			return *this;
		};

#ifdef USING_DIRECTX
		bool		CLight::IsEnable	(void) const
		{
			BOOL isEnable;
			
			CGraphicManager::GetManager().GetDevice()
				->GetLightEnable(m_id,&isEnable);

			return isEnable == TRUE;
		};
		
		CLight&		CLight::IsEnable	(const bool isEnable)
		{
			CGraphicManager::GetManager().GetDevice()
				->LightEnable(m_id,isEnable);
			return *this;
		};

		const CLight&		CLight::SetToDevice(void) const
		{
			CGraphicManager::GetManager().GetDevice()
				->SetLight(m_id, (D3DLIGHT9*)this);
			return *this;
		};
#endif
#ifdef USING_OPENGL
		bool		CLight::IsEnable	(void) const
		{
			return glIsEnabled(GL_LIGHT0 + m_id) == GL_TRUE;
		};

		CLight&		CLight::IsEnable	(const bool isEnable)
		{
			if(isEnable)
				glEnable(GL_LIGHT0 + m_id);
			else
				glDisable(GL_LIGHT0 + m_id);
			return *this;
		};

		const CLight&		CLight::SetToDevice(void) const
		{
			array_t<float, 4> position;

			Range::Copy(
				m_position,
				position.begin());
			if(m_type == LIGHT_DIRECTIONAL)
				position.at(3) = 0;
			else
				position.at(3) = 1;

			glLightfv(
				GL_LIGHT0 + m_id,
				GL_DIFFUSE,
				m_diffuse.data());
			glLightfv(
				GL_LIGHT0 + m_id,
				GL_SPECULAR,
				m_specular.data());
			glLightfv(
				GL_LIGHT0 + m_id,
				GL_AMBIENT,
				m_ambient.data());
			glLightfv(
				GL_LIGHT0 + m_id,
				GL_POSITION,
				position.data());
			glLightfv(
				GL_LIGHT0 + m_id,
				GL_SPOT_DIRECTION,
				m_direction.data());
			
			glLightf(
				GL_LIGHT0 + m_id,
				GL_CONSTANT_ATTENUATION,
				m_attenuation0);
			glLightf(
				GL_LIGHT0 + m_id,
				GL_LINEAR_ATTENUATION,
				m_attenuation1);
			glLightf(
				GL_LIGHT0 + m_id,
				GL_QUADRATIC_ATTENUATION,
				m_attenuation2);
			
			glLightf(
				GL_LIGHT0 + m_id,
				GL_SPOT_CUTOFF,
				Math::RadianToDegree(m_phi));
			glLightf(
				GL_LIGHT0 + m_id,
				GL_SPOT_EXPONENT,
				m_falloff);
			return *this;
		};
#endif
	};
};