/*!	\file		CVertexNormal.cpp
*	\details	CVertexNormalクラスの定義
*	\author		松裏征志
*	\date		2014/05/09
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Vertex/CVertexNormal.h"
#include "Graphic/CGraphicManager.h"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_OPENGL
		void CVertexNormal::RenderEnd(void)
		{
			glDisableClientState(GL_NORMAL_ARRAY);
		};

		void CVertexNormal::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_NORMAL_ARRAY);

			//頂点構成の設定
			glNormalPointer(
				GL_FLOAT,
				vertexSize,
				reinterpret_cast<GLvoid*>(offset));
		};

		void CVertexNormal::RenderBegin(
			CVertexNormal& vertexTop,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_NORMAL_ARRAY);

			//頂点構成の設定
			glNormalPointer(
				GL_FLOAT, 
				vertexSize,
				&(vertexTop.m_normal));
		};
#endif
#ifdef	USING_DIRECTX
		void CVertexNormal::RenderEnd(void)
		{
		};

		void CVertexNormal::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		void CVertexNormal::RenderBegin(
			CVertexNormal& vertexTop,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		D3DVERTEXELEMENT9	CVertexNormal::GetElement(uint_t offset)
		{
			D3DVERTEXELEMENT9 rtn =
			{
				0,
				offset,
				D3DDECLTYPE_FLOAT3,
				D3DDECLMETHOD_DEFAULT,
				D3DDECLUSAGE_NORMAL,
				0
			};

			return rtn;
		};
#endif
		CVertexNormal::CVertexNormal(
			float nx,
			float ny,
			float nz)
			:m_normal({ nx, ny, nz })
		{};

		CVertexNormal::CVertexNormal(
			const CVector3& normal)
			:m_normal(normal)
		{
		};

		CVector3& CVertexNormal::Normal(void)
		{
			return m_normal;
		};

		const CVector3& CVertexNormal::Normal(void) const
		{
			return m_normal;
		};
	};
};
