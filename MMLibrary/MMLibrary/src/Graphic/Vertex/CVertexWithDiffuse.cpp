/*!	\file		CVertexWithDiffuse.cpp
*	\details	CVertexWithDiffuseクラスの定義
*	\author		松裏征志
*	\date		2014/05/12
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Vertex/CVertexWithDiffuse.h"
#include "Utility\Macro.hpp"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_OPENGL
		void CVertexWithDiffuse::RenderEnd(void)
		{
			CVertexXYZ::RenderEnd();
			CVertexDiffuse::RenderEnd();
		};

		void CVertexWithDiffuse::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			CVertexWithDiffuse temp;

			CVertexXYZ::RenderBegin(
				offset + GetDistance(&temp, &(temp.Position())),
				vertexSize);

			CVertexDiffuse::RenderBegin(
				offset + GetDistance(&temp, &(temp.Color())),
				vertexSize);
		};

		void CVertexWithDiffuse::RenderBegin(
			CVertexWithDiffuse& vertexTop,
			size_t vertexSize)
		{
			CVertexWithDiffuse temp;

			CVertexXYZ::RenderBegin(
				GetDistance(&temp, &(temp.Position())),
				vertexSize);

			CVertexDiffuse::RenderBegin(
				GetDistance(&temp, &(temp.Color())),
				vertexSize);
		};

		void CVertexWithDiffuse::RenderBeginNoBuffer(
			CVertexWithDiffuse& vertexTop,
			size_t vertexSize)
		{
			CVertexXYZ::RenderBegin(
				vertexTop,
				vertexSize);

			CVertexDiffuse::RenderBegin(
				vertexTop,
				vertexSize);
		};
#endif
#ifdef	USING_DIRECTX
		void CVertexWithDiffuse::RenderEnd(void)
		{
		};

		void CVertexWithDiffuse::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				CVertexXYZ::GetElement(0),
				CVertexDiffuse::GetElement(sizeof(CVertexXYZ)),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		void CVertexWithDiffuse::RenderBegin(
			CVertexWithDiffuse& vertexTop,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				CVertexXYZ::GetElement(0),
				CVertexDiffuse::GetElement(sizeof(CVertexXYZ)),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};
#endif

		CVertexWithDiffuse::CVertexWithDiffuse(
			float x,
			float y,
			float z,
			uchar_t red,
			uchar_t green,
			uchar_t blue,
			uchar_t alpha)
			:CVertexXYZ(x, y, z)
			, CVertexDiffuse((red),
			(green),
			(blue),
			(alpha))
		{};
		CVertexWithDiffuse::CVertexWithDiffuse(
			const CVector3&	position,
			const CColor32&				color)
			:CVertexXYZ(position)
			, CVertexDiffuse(color)
		{};
	};
};