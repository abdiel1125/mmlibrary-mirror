/*!	\file		CVertexDiffuse.cpp
*	\details	CVertexDiffuseクラスの定義
*	\author		松裏征志
*	\date		2014/06/10
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Vertex/CVertexDiffuse.h"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_OPENGL
		void CVertexDiffuse::RenderEnd(void)
		{
			glDisableClientState(GL_COLOR_ARRAY);

		};
		void CVertexDiffuse::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_COLOR_ARRAY);

			//頂点構成の設定
			glColorPointer(
				4, 
				GL_UNSIGNED_BYTE, 
				vertexSize, 
				reinterpret_cast<GLvoid*>(offset));
		};

		void CVertexDiffuse::RenderBegin(
			CVertexDiffuse& vertexTop,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_COLOR_ARRAY);

			//頂点構成の設定
			glColorPointer(
				4,
				GL_UNSIGNED_BYTE,
				vertexSize,
				&(vertexTop.m_color));
		};

#endif
#ifdef	USING_DIRECTX
		void CVertexDiffuse::RenderEnd(void)
		{
		};

		void CVertexDiffuse::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		void CVertexDiffuse::RenderBegin(
			CVertexDiffuse& vertexTop,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		D3DVERTEXELEMENT9	CVertexDiffuse::GetElement(uint_t offset)
		{
			D3DVERTEXELEMENT9 rtn =
			{
				0,
				offset,
				D3DDECLTYPE_UBYTE4N,
				D3DDECLMETHOD_DEFAULT,
				D3DDECLUSAGE_COLOR,
				0
			};

			return rtn;
		};
#endif

		CVertexDiffuse::CVertexDiffuse(
			uchar_t red,
			uchar_t green,
			uchar_t blue,
			uchar_t alpha)
			:m_color((red),
			(green),
			(blue),
			(alpha))
		{};
		CVertexDiffuse::CVertexDiffuse(
			const CColor32&				color)
			:m_color(color)
		{};

		CColor32& CVertexDiffuse::Color(void)
		{
			return m_color;
		};

		const CColor32& CVertexDiffuse::Color(void) const
		{
			return m_color;
		};
	};
};