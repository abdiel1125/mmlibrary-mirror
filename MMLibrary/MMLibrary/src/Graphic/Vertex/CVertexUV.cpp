/*!	\file		CVertexUV.cpp
*	\details	CVertexUVクラスの定義
*	\author		松裏征志
*	\date		2014/05/27
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Vertex/CVertexUV.h"
#include "Graphic/CGraphicManager.h"
#include "Graphic/Texture/CTexture.h"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_OPENGL
		void CVertexUV::RenderEnd(void)
		{
			glDisable(GL_TEXTURE_2D);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		};

		void CVertexUV::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glEnable(GL_TEXTURE_2D);

			//頂点構成の設定
			glTexCoordPointer(
				2,
				GL_FLOAT,
				vertexSize,
				reinterpret_cast<GLvoid*>(offset));//この行がダメかも
		};

		void CVertexUV::RenderBegin(
			CVertexUV& vertexTop,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glEnable(GL_TEXTURE_2D);

			//頂点構成の設定
			glTexCoordPointer(
				2,	
				GL_FLOAT,			
				vertexSize,
				&(vertexTop.m_uv));
		};
#endif
#ifdef	USING_DIRECTX
		void CVertexUV::RenderEnd(void)
		{
			CTextureBase::UnSetFromDevice(0);
		};

		void CVertexUV::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		void CVertexUV::RenderBegin(
			CVertexUV& vertexTop,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		D3DVERTEXELEMENT9	CVertexUV::GetElement(uint_t offset)
		{
			D3DVERTEXELEMENT9 rtn =
			{
				0,
				offset,
				D3DDECLTYPE_FLOAT2,
				D3DDECLMETHOD_DEFAULT,
				D3DDECLUSAGE_TEXCOORD,
				0
			};

			return rtn;
		};
#endif

		CVertexUV::CVertexUV(
			float	u,
			float	v)
		{
			m_uv.at(0) = u;
			m_uv.at(1) = v;
		};

		CVertexUV::CVertexUV(
			const Math::CVector<float, 2>&	uv)
			:m_uv(uv)
		{};

		Math::CVector<float, 2>&
			CVertexUV::UV(void)
		{
			return m_uv;
		};

		const Math::CVector<float, 2>&
			CVertexUV::UV(void) const
		{
			return m_uv;
		};
	};
};