/*!	\file		Vertex.cpp
*	\details	VertexΦAΜθ`
*	\author		Ό ͺu
*	\date		2014/09/02
*/

//***********************************************************
//CN[h
//***********************************************************
#include "Utility\Macro.h"
#include "Graphic/Vertex/Vertex.h"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef USING_DIRECTX
		void SetDeclaration(const D3DVERTEXELEMENT9* pElement)
		{
			IDirect3DVertexDeclaration9 *decl;
			if(CGraphicManager::GetManager().
				GetDevice()->CreateVertexDeclaration(pElement, &decl) == D3D_OK)
			{
				CGraphicManager::GetManager().
					GetDevice()->SetVertexDeclaration(decl);
				Utility::SafeRelease(decl);
			}
		}
#endif
	};
};