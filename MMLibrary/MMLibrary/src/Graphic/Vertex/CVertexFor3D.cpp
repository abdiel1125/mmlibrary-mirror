/*!	\file		CVertexFor3D.cpp
*	\details	CVertexFor3Dクラスの定義
*	\author		松裏征志
*	\date		2014/05/29
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Vertex/CVertexFor3D.h"
#include "Utility\Macro.hpp"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_OPENGL
		void CVertexFor3D::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			CVertexFor3D temp;

			CVertexXYZ::RenderBegin(
				offset + GetDistance(&temp, &(temp.Position())),
				vertexSize);
			CVertexNormal::RenderBegin(
				offset + GetDistance(&temp, &(temp.Normal())),
				vertexSize);
			CVertexUV::RenderBegin(
				offset + GetDistance(&temp, &(temp.UV())),
				vertexSize);
		};
		void CVertexFor3D::RenderBegin(
			CVertexFor3D& vertexTop,
			size_t vertexSize)
		{
			CVertexXYZ::RenderBegin(
				vertexTop,
				vertexSize);
			CVertexNormal::RenderBegin(
				vertexTop,
				vertexSize);
			CVertexUV::RenderBegin(
				vertexTop,
				vertexSize);
		};

		void CVertexFor3D::RenderBeginNoBuffer(
			CVertexFor3D& vertexTop,
			size_t vertexSize)
		{
			CVertexXYZ::RenderBegin(
				vertexTop,
				vertexSize);
			CVertexNormal::RenderBegin(
				vertexTop,
				vertexSize);
			CVertexUV::RenderBegin(
				vertexTop,
				vertexSize);
		};
#endif
#ifdef	USING_DIRECTX
		void CVertexFor3D::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			CVertexFor3D temp;

			D3DVERTEXELEMENT9 elm[] = {
				CVertexXYZ::GetElement(GetDistance(&temp, &(temp.Position()))),
				CVertexNormal::GetElement(GetDistance(&temp, &(temp.Normal()))),
				CVertexUV::GetElement(GetDistance(&temp, &(temp.UV()))),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		void CVertexFor3D::RenderBegin(
			CVertexFor3D& vertexTop,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				CVertexXYZ::GetElement(GetDistance(&vertexTop, &(vertexTop.Position()))),
				CVertexNormal::GetElement(GetDistance(&vertexTop, &(vertexTop.Normal()))),
				CVertexUV::GetElement(GetDistance(&vertexTop, &(vertexTop.UV()))),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};
#endif

		CVertexFor3D::CVertexFor3D(
			float			x,
			float			y,
			float			z,
			float			nx,
			float			ny,
			float			nz,
			float			u,
			float			v)
			:CVertexXYZ(
			x,
			y,
			z)
			, CVertexNormal(nx, ny, nz)
			, CVertexUV(u, v)
		{};
		CVertexFor3D::CVertexFor3D(
			const CVector3&		position,
			const CVector3&		normal,
			const Math::CVector<float, 2>&	uv)
			:CVertexXYZ(position)
			, CVertexNormal(normal)
			, CVertexUV(uv)
		{};


		void CVertexFor3D::RenderEnd(void)
		{
			CVertexXYZ::RenderEnd();
			CVertexNormal::RenderEnd();
			CVertexUV::RenderEnd();
		};
	};
};