/*!	\file		CVertexFor2D.cpp
*	\details	CVertexFor2Dクラスの定義
*	\author		松裏征志
*	\date		2014/05/27
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Vertex/CVertexFor2D.h"
#include "Utility\Macro.hpp"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_OPENGL
		void CVertexFor2D::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			CVertexFor2D temp;

			CVertexXYZ::RenderBegin(
				offset + GetDistance(&temp, &(temp.Position())),
				vertexSize);
			CVertexDiffuse::RenderBegin(
				offset + GetDistance(&temp, &(temp.Color())),
				vertexSize);
			CVertexUV::RenderBegin(
				offset + GetDistance(&temp, &(temp.UV())),
				vertexSize);

			CGraphicManager::GetManager().
				IsLighting(false);
		};

		void CVertexFor2D::RenderBegin(
			CVertexFor2D& vertexTop,
			size_t vertexSize)
		{
			CVertexFor2D temp;

			CVertexXYZ::RenderBegin(
				GetDistance(&temp, &(temp.Position())),
				vertexSize);
			CVertexDiffuse::RenderBegin(
				GetDistance(&temp, &(temp.Color())),
				vertexSize);
			CVertexUV::RenderBegin(
				GetDistance(&temp, &(temp.UV())),
				vertexSize);

			CGraphicManager::GetManager().
				IsLighting(false);
		};

		void CVertexFor2D::RenderBeginNoBuffer(
			CVertexFor2D& vertexTop,
			size_t vertexSize)
		{
			CVertexXYZ::RenderBegin(
				vertexTop,
				vertexSize);
			CVertexDiffuse::RenderBegin(
				vertexTop,
				vertexSize);
			CVertexUV::RenderBegin(
				vertexTop,
				vertexSize);

			CGraphicManager::GetManager().
				IsLighting(false);
		};
#endif
#ifdef	USING_DIRECTX
		void CVertexFor2D::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			CVertexFor2D temp;

			D3DVERTEXELEMENT9 elm[] =
			{
				CVertexXYZ::GetElement(GetDistance(&temp, &(temp.Position()))),
				CVertexDiffuse::GetElement(GetDistance(&temp, &(temp.Color()))),
				CVertexUV::GetElement(GetDistance(&temp, &(temp.UV()))),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		void CVertexFor2D::RenderBegin(
			CVertexFor2D& vertexTop,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = 
			{
				CVertexXYZ::GetElement(GetDistance(&vertexTop, &(vertexTop.Position()))),
				CVertexDiffuse::GetElement(GetDistance(&vertexTop, &(vertexTop.Color()))),
				CVertexUV::GetElement(GetDistance(&vertexTop, &(vertexTop.UV()))),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};
#endif

		CVertexFor2D::CVertexFor2D(
			float			x,
			float			y,
			float			z,
			uchar_t	red,
			uchar_t	green,
			uchar_t	blue,
			uchar_t	alpha,
			float			u,
			float			v)
			:CVertexXYZ(
			x,
			y,
			z)
			, CVertexDiffuse(
			red,
			green,
			blue,
			alpha)
			, CVertexUV(u, v)
		{};
		CVertexFor2D::CVertexFor2D(
			const CVector3&		position,
			const CColor32&					color,
			const Math::CVector<float, 2>&	uv)
			:CVertexXYZ(position)
			, CVertexDiffuse(color)
			, CVertexUV(uv)
		{};

		void CVertexFor2D::RenderEnd(void)
		{
			CVertexXYZ::RenderEnd();
			CVertexDiffuse::RenderEnd();
			CVertexUV::RenderEnd();
		};
	};
};