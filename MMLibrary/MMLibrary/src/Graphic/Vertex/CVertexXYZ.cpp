/*!	\file		CVertexXYZ.cpp
*	\details	CVertexXYZクラスの定義
*	\author		松裏征志
*	\date		2014/05/09
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Vertex/CVertexXYZ.h"
#include "Graphic/CGraphicManager.h"

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_OPENGL
		void CVertexXYZ::RenderEnd(void)
		{
			glDisableClientState(GL_VERTEX_ARRAY);
		};

		void CVertexXYZ::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_VERTEX_ARRAY);

			//頂点構成の設定
			glVertexPointer(
				3, 
				GL_FLOAT, 
				vertexSize,
				reinterpret_cast<GLvoid*>(offset));
		};

		void CVertexXYZ::RenderBegin(
			CVertexXYZ& vertexTop,
			size_t vertexSize)
		{
			//有効化
			glEnableClientState(GL_VERTEX_ARRAY);

			//頂点構成の設定
			glVertexPointer(
				3, 
				GL_FLOAT, 
				vertexSize,
				&(vertexTop.m_position));
		};
#endif
#ifdef	USING_DIRECTX
		void CVertexXYZ::RenderEnd(void)
		{
		};

		void CVertexXYZ::RenderBegin(
			size_t offset,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		void CVertexXYZ::RenderBegin(
			CVertexXYZ& vertexTop,
			size_t vertexSize)
		{
			D3DVERTEXELEMENT9 elm[] = {
				GetElement(0),
				D3DDECL_END()
			};

			SetDeclaration(elm);
		};

		D3DVERTEXELEMENT9	CVertexXYZ::GetElement(uint_t offset)
		{
			D3DVERTEXELEMENT9 rtn =
			{
				0,
				offset,
				D3DDECLTYPE_FLOAT3,
				D3DDECLMETHOD_DEFAULT,
				D3DDECLUSAGE_POSITION,
				0
			};

			return rtn;
		};
#endif
		CVertexXYZ::CVertexXYZ(
			float x,
			float y,
			float z)
			:m_position({ x, y, z })
		{};

		CVertexXYZ::CVertexXYZ(
			const CVector3& position)
			:m_position(position)
		{
		};

		CVector3& CVertexXYZ::Position(void)
		{
			return m_position;
		};

		const CVector3& CVertexXYZ::Position(void) const
		{
			return m_position;
		};
	};
};
