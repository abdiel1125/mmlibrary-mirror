/*!	\file		CRenderTarget.h
*	\details	CRenderTargetクラスの定義
*	\author		松裏征志
*	\date		2015/03/03
*/
#ifdef USING_OPENGL
//===========================================================
//ファイルインクルード
//===========================================================
#include "Graphic/CRenderTarget.h"
#include "Graphic/Texture/CTexture.h"
#include "Utility/Macro.hpp"

namespace	MMLibrary
{
	namespace	Graphic
	{
		class CRenderTargetImpl : public IRenderTargetImpl
		{
		private:
			GLuint							m_frameBufferObject;
			std::shared_ptr<CTextureBase>	m_pTexture;
			std::shared_ptr<CTextureBase>	m_pDepth;

		public:
			CRenderTargetImpl()
				: m_frameBufferObject(0)
				, m_pTexture()
				, m_pDepth()
			{};
			CRenderTargetImpl(size_t width, size_t height);

			virtual ~CRenderTargetImpl() override;
			virtual Utility::CDoAtDestruct SetToDevice() override;
			virtual void SetByDevice() override;
			virtual std::shared_ptr<CTextureBase> GetTexture() override;
		};

		std::unique_ptr<IRenderTargetImpl> CreateRenderTargetImpl(size_t width, size_t height)
		{
			return std::make_unique<CRenderTargetImpl>(width, height);
		}

		CRenderTargetImpl::CRenderTargetImpl(size_t width, size_t height)
			: m_frameBufferObject(0)
			, m_pTexture(new CTextureBase(width, height, TEXTURE_TYPE::RENDER_TARGET))
			, m_pDepth(new CTextureBase(width, height, TEXTURE_TYPE::RENDER_TARGET, COLOR_FORMAT::DEPTH))
		{
			// フレームバッファオブジェクトを作成する
			CGLFunctions::GetInstance().glGenFramebuffersEXT(
				1,
				&m_frameBufferObject);
			CGLFunctions::GetInstance().CheckErrer();

			// フレームバッファオブジェクトをデバイスにセットする
			auto unseter = SetToDevice();

			// フレームバッファオブジェクトにカラーバッファとしてテクスチャを結合する
			CGLFunctions::GetInstance().glFramebufferTexture2DEXT(
				GL_FRAMEBUFFER_EXT,
				GL_COLOR_ATTACHMENT0_EXT,
				GL_TEXTURE_2D,
				m_pTexture->GetTextureHandle(),
				0);
			CGLFunctions::GetInstance().CheckErrer();

			// フレームバッファオブジェクトにデプスバッファとしてレンダーバッファを結合する
			CGLFunctions::GetInstance().glFramebufferTexture2DEXT(
				GL_FRAMEBUFFER_EXT,
				GL_DEPTH_ATTACHMENT_EXT,
				GL_TEXTURE_2D,
				m_pDepth->GetTextureHandle(),
				0);
			CGLFunctions::GetInstance().CheckErrer();
		}

		CRenderTargetImpl::~CRenderTargetImpl()
		{
		}

		Utility::CDoAtDestruct CRenderTargetImpl::SetToDevice()
		{
			//設定
			CGLFunctions::GetInstance().glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_frameBufferObject);
			CGLFunctions::GetInstance().CheckErrer();

			return Utility::CDoAtDestruct(
				[]()
			{
				//フラッシュ
				glFlush();

				//元に戻す処理
				CGLFunctions::GetInstance().glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
				CGLFunctions::GetInstance().CheckErrer();
			});
		}

		void CRenderTargetImpl::SetByDevice()
		{
			m_pTexture.reset(new CTextureBase());
		}

		std::shared_ptr<CTextureBase> CRenderTargetImpl::GetTexture()
		{
			return m_pTexture;
		}
	}
}

#endif