/*!	\file		CGraphicManager.cpp
*	\details	CGraphicManagerクラスの定義
*	\author		松裏征志
*	\date		2014/05/14
*/
//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic/CGraphicManager.h"
#include	"Graphic/Model/Model.h"
#include	"Graphic\CWindow.h"

void GLInit(void);

namespace	MMLibrary
{
	namespace Graphic
	{
#ifdef	USING_DIRECTX

		const CMatrix4x4
			CGraphicManager::CMatrixStack::GetTop(
			void) const
		{
			return *(CMatrix4x4*)CGraphicManager::GetManager().
				m_pD3DMatStack->GetTop();
		};

		CGraphicManager::CMatrixStack&
			CGraphicManager::CMatrixStack::LoadIdentity(
			void)
		{
			CGraphicManager::GetManager().m_pD3DMatStack->
				LoadIdentity();

			return *this;
		};

		CGraphicManager::CMatrixStack&
			CGraphicManager::CMatrixStack::LoadMatrix(
			const CMatrix4x4& src)
		{
			CGraphicManager::GetManager().m_pD3DMatStack->
				LoadMatrix((D3DXMATRIX*)&src);

			return *this;
		};

		CGraphicManager::CMatrixStack&
			CGraphicManager::CMatrixStack::Multiply(
			const CMatrix4x4& src)
		{
			CGraphicManager::GetManager().m_pD3DMatStack->
				MultMatrix((D3DXMATRIX*)&src);

			return *this;
		};

		CGraphicManager::CMatrixStack&
			CGraphicManager::CMatrixStack::MultiplyLocal(
			const CMatrix4x4& src)
		{
			CGraphicManager::GetManager().m_pD3DMatStack->
				MultMatrixLocal((D3DXMATRIX*)&src);

			return *this;
		};

		CGraphicManager::CMatrixStack&
			CGraphicManager::CMatrixStack::Pop(
			void)
		{
			CGraphicManager::GetManager().m_pD3DMatStack->
				Pop();

			return *this;
		};

		CGraphicManager::CMatrixStack&
			CGraphicManager::CMatrixStack::Push(
			void)
		{
			CGraphicManager::GetManager().m_pD3DMatStack->
				Push();

			return *this;
		};

		CGraphicManager::CGraphicManager(void)
			:m_pD3D(nullptr)
			, m_pD3DDevice(nullptr)
			, m_pD3DMatStack(nullptr)
			, m_adapter(D3DADAPTER_DEFAULT)
		{};

		bool	CGraphicManager::Init(
			CWindow& window)
		{
			HRESULT	hr;

			m_pWindow = &window;

			m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);	// Direct3D9インターフェース取得
			if (m_pD3D == nullptr)	return false;

			// アダプタの現在のディスプレイモードを取得する
			hr = m_pD3D->GetAdapterDisplayMode(m_adapter, &m_disp);
			if (hr != D3D_OK)
				return false;

			memset(&m_D3Dpp, 0, sizeof(m_D3Dpp));									// ゼロクリア
			m_D3Dpp.BackBufferFormat = m_disp.Format;						// 現在のビット深度
			m_D3Dpp.BackBufferWidth = window.GetWidth();					// バックバッファの幅をセット
			m_D3Dpp.BackBufferHeight = window.GetHeight();					// バックバッファの高さをセット
			m_D3Dpp.BackBufferCount = 1;									// バックバッファの数
			m_D3Dpp.SwapEffect = D3DSWAPEFFECT_FLIP;
			m_D3Dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;	// バックバッファをロック可能にする

			if (window.IsFullScreen())
				m_D3Dpp.Windowed = FALSE;	// フルスクリーンモード
			else
				m_D3Dpp.Windowed = TRUE;	// ウインドウモード

			// Ｚバッファの自動作成（ビット深度16）
			m_D3Dpp.EnableAutoDepthStencil = 1;
			m_D3Dpp.AutoDepthStencilFormat = D3DFMT_D16;
			m_D3Dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
			m_D3Dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;	// VSYNCを待たない

			// デバイス作成
			hr = m_pD3D->CreateDevice(m_adapter,
				D3DDEVTYPE_HAL,
				window.GetWindowHandle(),
				D3DCREATE_MIXED_VERTEXPROCESSING | 
				D3DCREATE_MULTITHREADED,
				&m_D3Dpp,
				&m_pD3DDevice);

			if (hr != D3D_OK)
				hr = m_pD3D->CreateDevice(m_adapter,
				D3DDEVTYPE_HAL,
				window.GetWindowHandle(),
				D3DCREATE_HARDWARE_VERTEXPROCESSING | 
				D3DCREATE_MULTITHREADED,
				&m_D3Dpp,
				&m_pD3DDevice);

			if (hr != D3D_OK)
				hr = m_pD3D->CreateDevice(m_adapter,
				D3DDEVTYPE_HAL,
				window.GetWindowHandle(),
				D3DCREATE_SOFTWARE_VERTEXPROCESSING | 
				D3DCREATE_MULTITHREADED,
				&m_D3Dpp,
				&m_pD3DDevice);

			if (hr != D3D_OK)
				hr = m_pD3D->CreateDevice(m_adapter,
				D3DDEVTYPE_REF,
				window.GetWindowHandle(),
				D3DCREATE_SOFTWARE_VERTEXPROCESSING | 
				D3DCREATE_MULTITHREADED,
				&m_D3Dpp,
				&m_pD3DDevice);

			if (hr != D3D_OK)
				return	false;
			
			D3DXCreateMatrixStack(0, &m_pD3DMatStack);

			SetCullingMode(CULL_NONE);

			return	true;
		};

		LPDIRECT3DDEVICE9 CGraphicManager::GetDevice(void)
		{
			return m_pD3DDevice;
		};

		void	CGraphicManager::Destroy(void)
		{
			Utility::SafeRelease(m_pD3DMatStack);
			Utility::SafeRelease(m_pD3DDevice);
			Utility::SafeRelease(m_pD3D);
		};

		void	CGraphicManager::RenderBegin(void)
		{
			ClearTargetBuffer();
			ClearDepthBuffer();
			IsWriteToZBuffer(true);
			// 描画の開始をDIRECTXに通知
			CheckDirectXErrer(
				m_pD3DDevice->BeginScene());

			RenderBegin3D();
		};

		void	CGraphicManager::RenderEnd(void)
		{
			HRESULT		hr;
			CheckDirectXErrer(
				m_pD3DDevice->EndScene());	// 描画の終了を待つ

			hr = m_pD3DDevice->Present(nullptr, nullptr, nullptr, nullptr);	// バックバッファからプライマリバッファへ転送
			if (hr != D3D_OK){
				m_pD3DDevice->Reset(&m_D3Dpp);
			}
		};

		void CGraphicManager::SetWorldMatrix(void)
		{
			CheckDirectXErrer(
				m_pD3DDevice->SetTransform(
				D3DTS_WORLD,
				(D3DMATRIX*)&Stack.GetTop()));
		};

		void CGraphicManager::SetCullingMode(
			CULLING_MODE mode)
		{
			CheckDirectXErrer(
				m_pD3DDevice->SetRenderState(
				D3DRS_CULLMODE,
				mode));
		};

		void CGraphicManager::IsLighting(bool isLighting)
		{
			if (isLighting)
				CheckDirectXErrer(
				m_pD3DDevice->SetRenderState(
				D3DRS_LIGHTING,
				TRUE));
			else
				CheckDirectXErrer(
				m_pD3DDevice->SetRenderState(
				D3DRS_LIGHTING,
				FALSE));
		};

		bool CGraphicManager::IsLighting(void)
		{
			DWORD rtn;

			CheckDirectXErrer(
				m_pD3DDevice->GetRenderState(
				D3DRS_LIGHTING,
				&rtn));

			return rtn != FALSE;
		};

		void CGraphicManager::IsWriteToZBuffer(bool isWriteToZBuffer)
		{
			CheckDirectXErrer(
				m_pD3DDevice->SetRenderState(
				D3DRS_ZENABLE,
				isWriteToZBuffer ? TRUE : FALSE));
		};

		bool CGraphicManager::IsWriteToZBuffer(void)
		{
			DWORD rtn;
			CheckDirectXErrer(
				m_pD3DDevice->GetRenderState(
				D3DRS_ZENABLE,
				&rtn));

			return rtn == TRUE;
		};

		void CGraphicManager::SetViewPort(const viewport_t& viewport)
		{
			CheckDirectXErrer(
				m_pD3DDevice->SetViewport(
				(D3DVIEWPORT9*)&viewport));
		};

		void	CGraphicManager::ClearTargetBuffer(void)
		{
			CheckDirectXErrer(
				m_pD3DDevice->Clear(
				0,
				nullptr,
				D3DCLEAR_TARGET,
				D3DCOLOR_RGBA(
				m_clearColor.Red(),
				m_clearColor.Green(),
				m_clearColor.Blue(),
				m_clearColor.Alpha()),
				1.0f,
				0));
		};

		void	CGraphicManager::ClearDepthBuffer(void)
		{
			CheckDirectXErrer(
				m_pD3DDevice->Clear(
				0,
				nullptr,
				D3DCLEAR_ZBUFFER,
				D3DCOLOR_RGBA(0, 0, 0, 0),
				1.0f,
				0));
		};


		void CGraphicManager::SetProjectionMatrixToDevice(const CMatrix4x4& matrix)
		{
			CheckDirectXErrer(
				m_pD3DDevice->SetTransform(
				D3DTS_PROJECTION,
				reinterpret_cast<const D3DMATRIX*>(&matrix)));
		};

		void CGraphicManager::SetViewMatrixToDevice(const CMatrix4x4& matrix)
		{
			CheckDirectXErrer(
				m_pD3DDevice->SetTransform(
				D3DTS_VIEW,
				reinterpret_cast<const D3DMATRIX*>(&matrix)));
		};

		void CGraphicManager::SetProjectionMatrix(const CMatrix4x4& matrix)
		{
			m_projectionMatrix = matrix;
			SetProjectionMatrixToDevice(m_projectionMatrix);
		};

		void CGraphicManager::SetViewMatrix(const CMatrix4x4& matrix)
		{
			m_viewMatrix = matrix;
			SetViewMatrixToDevice(m_viewMatrix);
		};

		void CGraphicManager::IsAlphaBlending(
			bool isAlphaBlending)
		{
			m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, isAlphaBlending ? TRUE : FALSE);
		};

		void CGraphicManager::SetAlphaBlendingState(
			BLEND_INTEGER srcFactor,
			BLEND_INTEGER dstFactor)
		{
			m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, srcFactor);
			m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, dstFactor);
		};

		void CGraphicManager::SetTextureBlendingState(
			TEXTURE_BLEND_MODE blendMode)
		{
			switch (blendMode)
			{
			case TEXTURE_BLEND_MODE::MODULATE:
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

				break;
			case TEXTURE_BLEND_MODE::REPLACE:
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);

				break;
			case TEXTURE_BLEND_MODE::BLEND:
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
				m_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_COMPLEMENT | D3DTA_TEXTURE);

				break;
			default:
				assert("failed set alpha blending mode");
				break;
			}
		};
#endif
#ifdef	USING_OPENGL

		const CMatrix4x4
			CGraphicManager::CMatrixStack::GetTop(
			void) const
		{
			CMatrix4x4 m;

			glMatrixMode(GL_MODELVIEW);
			glGetFloatv(GL_MODELVIEW_MATRIX, &m[0][0]);
			return m;
		};

		CGraphicManager::CMatrixStack&	
			CGraphicManager::CMatrixStack::LoadIdentity(
			void)
		{
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			return *this;
		};

		CGraphicManager::CMatrixStack&	
			CGraphicManager::CMatrixStack::LoadMatrix(
			const CMatrix4x4& src)
		{
			glMatrixMode(GL_MODELVIEW);

			glLoadMatrixf((float *)&src);

			return *this;
		};

		CGraphicManager::CMatrixStack&	
			CGraphicManager::CMatrixStack::Multiply(
			const CMatrix4x4& src)
		{
			glMatrixMode(GL_MODELVIEW);

			LoadMatrix(src * GetTop());

			return *this;
		};

		CGraphicManager::CMatrixStack&	
			CGraphicManager::CMatrixStack::MultiplyLocal(
			const CMatrix4x4& src)
		{
			glMatrixMode(GL_MODELVIEW);

			glMultMatrixf((float *)&src);

			return *this;
		};

		CGraphicManager::CMatrixStack&	
			CGraphicManager::CMatrixStack::Pop(
			void)
		{
			glMatrixMode(GL_MODELVIEW);

			glPopMatrix();

			return *this;
		};
		CGraphicManager::CMatrixStack&	
			CGraphicManager::CMatrixStack::Push(
			void)
		{
			glMatrixMode(GL_MODELVIEW);

			glPushMatrix();

			return *this;
		};

		CGraphicManager::CGraphicManager(void)
		{
		};

		bool	CGraphicManager::Init(CWindow& window)
		{
			m_pWindow = &window;

			Camera().Aspect() = 
				(float)window.GetWidth() /
				(float)window.GetHeight();

			SetCullingMode(CULL_NONE);

			PIXELFORMATDESCRIPTOR pfd =
			{
				sizeof(PIXELFORMATDESCRIPTOR),
				1,
				PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, //Flags
				PFD_TYPE_RGBA,                                              //The kind of framebuffer. RGBA or palette.
				32,                                                         //Colordepth of the framebuffer.
				0, 0, 0, 0, 0, 0,
				0,
				0,
				0,
				0, 0, 0, 0,
				24,                                                         //Number of bits for the depthbuffer
				8,                                                          //Number of bits for the stencilbuffer
				0,                                                          //Number of Aux buffers in the framebuffer.
				PFD_MAIN_PLANE,
				0,
				0, 0, 0
			};

			m_device = GetDC(window.GetWindowHandle());

			auto pixelFormat = ChoosePixelFormat(m_device, &pfd);
			assert(pixelFormat);
			if(!SetPixelFormat(m_device, pixelFormat, &pfd))
				assert("Failed SetPixelFormat");

			m_context = wglCreateContext(m_device);
			if (!wglMakeCurrent(
				m_device,
				m_context))
				assert("wglMakeCurrent");

			CGLFunctions::GetInstance();

			return true;
		};

		void	CGraphicManager::Destroy(void)
		{
			// カレントコンテキストを無効にする
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext(m_context);
		};

		CGraphicManager::device_t CGraphicManager::GetDevice(void)
		{
			return m_device;
		};

		void	CGraphicManager::RenderBegin(void)
		{
			if (!wglMakeCurrent(
				m_device,
				m_context))
				assert("wglMakeCurrent");

			ClearTargetBuffer();
			ClearDepthBuffer();
			IsWriteToZBuffer(true);

			RenderBegin3D();
		};

		void	CGraphicManager::RenderEnd(void)
		{
			glFlush();
			SwapBuffers(m_device);
			wglMakeCurrent(NULL, NULL);
		};

		void CGraphicManager::SetWorldMatrix(void)
		{
			glMatrixMode(GL_MODELVIEW);
		};

		void CGraphicManager::SetCullingMode(
			CULLING_MODE mode)
		{
			if(mode == CULL_NONE)
			{
				glDisable(GL_CULL_FACE);
			}
			else
			{
				glEnable(GL_CULL_FACE);
				glFrontFace(GL_CCW);
				glCullFace(mode);
			}
		};

		void CGraphicManager::IsLighting(bool isLighting)
		{
			if(isLighting)
				glEnable(GL_LIGHTING);
			else
				glDisable(GL_LIGHTING);
		};

		bool CGraphicManager::IsLighting(void)
		{
			return glIsEnabled(GL_LIGHTING) != GL_FALSE;
		};

		void CGraphicManager::IsWriteToZBuffer(bool isWriteToZBuffer)
		{
			if (isWriteToZBuffer)
				glEnable(GL_DEPTH_TEST);
			else
				glDisable(GL_DEPTH_TEST);
		};

		bool CGraphicManager::IsWriteToZBuffer(void)
		{
			GLboolean rtn[1];
			glGetBooleanv(GL_DEPTH_TEST, rtn);

			return rtn[0] == GL_TRUE;
		};

		void CGraphicManager::SetViewPort(const viewport_t& viewport)
		{
			glViewport(
				viewport.x,
				viewport.y,
				viewport.width,
				viewport.height);
		};

		void	CGraphicManager::ClearTargetBuffer(void)
		{
			glClearColor(
				ToFloatColor(m_clearColor.Red()),
				ToFloatColor(m_clearColor.Green()),
				ToFloatColor(m_clearColor.Blue()),
				ToFloatColor(m_clearColor.Alpha()));

			glClear(GL_COLOR_BUFFER_BIT);
		};

		void	CGraphicManager::ClearDepthBuffer(void)
		{
			glClear(GL_DEPTH_BUFFER_BIT);
		};

		void CGraphicManager::SetProjectionMatrixToDevice(const CMatrix4x4& matrix)
		{
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glMultMatrixf(&matrix[0][0]);
			glMultMatrixf(&GetViewMatrix()[0][0]);
		};

		void CGraphicManager::SetViewMatrixToDevice(const CMatrix4x4& matrix)
		{
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glMultMatrixf(&GetProjectionMatrix()[0][0]);
			glMultMatrixf(&matrix[0][0]);
		};

		void CGraphicManager::SetProjectionMatrix(const CMatrix4x4& matrix)
		{
			m_projectionMatrix = matrix;
			SetProjectionMatrixToDevice(m_projectionMatrix);
		};

		void CGraphicManager::SetViewMatrix(const CMatrix4x4& matrix)
		{
			m_viewMatrix = matrix;
			SetViewMatrixToDevice(m_viewMatrix);
		};

		void CGraphicManager::IsAlphaBlending(
			bool isAlphaBlending)
		{
			if (isAlphaBlending)
				glEnable(GL_BLEND);
			else
				glDisable(GL_BLEND);
		};

		void CGraphicManager::SetAlphaBlendingState(
			BLEND_INTEGER srcFactor,
			BLEND_INTEGER dstFactor)
		{
			glBlendFunc(srcFactor, dstFactor);
		};

		void CGraphicManager::SetTextureBlendingState(
			TEXTURE_BLEND_MODE blendMode)
		{
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, blendMode);
		};

#endif
		CGraphicManager*	CGraphicManager::m_pCurrentGraphicManager = nullptr;

		CGraphicManager::~CGraphicManager(void)
		{
			Destroy();
		};

		CGraphicManager& CGraphicManager::GetManager(void)
		{
			return *m_pCurrentGraphicManager;
		};

		CCamera& CGraphicManager::Camera(void)
		{
			return m_camera;
		};

		void CGraphicManager::SetClearColor(
			CColor32& color)
		{
			m_clearColor = color;
		};

		void	CGraphicManager::RenderBegin3D(void)
		{
			SetProjectionMatrix(m_camera.GetProjectionMatrix());
			SetViewMatrix(m_camera.GetViewMatrix());
		};

		void	CGraphicManager::RenderBegin2D(void)
		{
			SetProjectionMatrix(CMatrix4x4());
			SetViewMatrix(CMatrix4x4());
		};

		void	CGraphicManager::RenderBeginScreenSpace(void)
		{
			SetProjectionMatrix(CMatrix4x4());
			SetViewMatrix(
				CMatrix4x4(
				2.0f / Window().GetWidth(), 0, 0, 0,
				0, -2.0f / Window().GetHeight(), 0, 0,
				0, 0, 0, 0,
				-2.0f / Window().GetWidth(), 2.0f / Window().GetHeight(), 0, 1));
			SetCullingMode(CULLING_MODE::CULL_NONE);
		};


			};
		};
