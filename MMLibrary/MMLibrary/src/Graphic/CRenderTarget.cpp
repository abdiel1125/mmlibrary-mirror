/*!	\file		CRenderTarget.h
*	\details	CRenderTargetクラスの定義
*	\author		松裏征志
*	\date		2015/03/03
*/
//===========================================================
//ファイルインクルード
//===========================================================
#include "Graphic/CRenderTarget.h"

namespace	MMLibrary
{
	namespace	Graphic
	{
		std::unique_ptr<IRenderTargetImpl> CreateRenderTargetImpl(size_t width, size_t height);

		CRenderTarget::CRenderTarget(size_t width, size_t height)
			:m_pImpl(CreateRenderTargetImpl(width, height))
		{}

		Utility::CDoAtDestruct CRenderTarget::SetToDevice()
		{
			return m_pImpl->SetToDevice();
		}

		void CRenderTarget::SetByDevice()
		{
			m_pImpl->SetByDevice();
		}

		std::shared_ptr<CTextureBase> CRenderTarget::GetTexture()
		{
			return m_pImpl->GetTexture();
		}
	}
}