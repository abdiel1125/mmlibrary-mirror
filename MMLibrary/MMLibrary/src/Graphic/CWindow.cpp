/*!	\file		CWindow.h
*	\details	CWindowクラスの定義
*	\author		松裏征志
*	\date		2014/07/18
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CWindow.h"
#include "Graphic\CGraphicManager.h"

namespace MMLibrary
{
	namespace Graphic
	{
		LRESULT WINAPI WindowProcedure(
			HWND	hWnd,
			UINT	message,
			WPARAM	wParam,
			LPARAM	lParam);

		CWindow::~CWindow(void)
		{
#ifdef USING_OPENGL
			// デバイスコンテキスト解放
			ReleaseDC(m_windowHandle, m_graphicManager.m_device);
#endif

			auto result = GetWindowList().find(m_windowHandle);
			if(result != GetWindowList().end())
				GetWindowList().erase(result);
			DestroyWindow(m_windowHandle);
		};

		void CWindow::PrepareCreateWindow(const string_t<>& name)
		{
			// ウインドウクラス情報のセット
			m_wcex.hInstance		= GetModuleHandle(NULL);							// インスタンス値のセット
			m_wcex.lpszClassName	= name.c_str();												// クラス名
			m_wcex.lpfnWndProc		= (WNDPROC)WindowProcedure;							// ウインドウメッセージ関数
			m_wcex.style			= 0;												// ウインドウスタイル
			m_wcex.cbSize 			= sizeof(WNDCLASSEX);								// 構造体のサイズ
			m_wcex.hIcon			= LoadIcon((HINSTANCE)nullptr, IDI_APPLICATION);	// ラージアイコン
			m_wcex.hIconSm			= LoadIcon((HINSTANCE)nullptr, IDI_WINLOGO);		// スモールアイコン
			m_wcex.hCursor			= LoadCursor((HINSTANCE)nullptr, IDC_ARROW);		// カーソルスタイル
			m_wcex.lpszMenuName		= 0; 												// メニューなし
			m_wcex.cbClsExtra		= 0;												// エキストラなし
			m_wcex.cbWndExtra		= 0;					
			m_wcex.hbrBackground	= (HBRUSH)GetStockObject(WHITE_BRUSH);				// 背景色白
			
			if(RegisterClassEx(&m_wcex) == 0)
			{
				MessageBox(
					m_windowHandle,
					"ウインドウクラスの登録に失敗しました",
					"CWindowクラス初期化エラー",
					MB_OK);
				exit(-1);
			}
		};
		
		void CWindow::CreateWindowLocal(const string_t<>& title)
		{
			//ウィンドウの作成
			if(m_isFullScreen)
			{
				m_windowHandle = CreateWindow(
					title.c_str(),							// ウィンドウクラスの名前
					title.c_str(),							// タイトル
					WS_VISIBLE		| 
					WS_POPUP,						// ウィンドウスタイル
					0, 0,							// ウィンドウ位置 縦, 横
					m_width, m_height,				// ウィンドウサイズ
					nullptr,						// 親ウィンドウなし
					(HMENU)nullptr,					// メニューなし
					m_wcex.hInstance,				// インスタンスハンドル
					(LPVOID)nullptr);				// 追加引数なし
			}
			else
			{
				DWORD dwStyle;
				if(m_isResizable)
				{
					dwStyle = 
						WS_CAPTION		|
						WS_MAXIMIZEBOX	|
						WS_MINIMIZEBOX	|
						WS_SYSMENU		|
						WS_THICKFRAME;
				}
				else
				{
					dwStyle = 
						WS_CAPTION		|
						WS_SYSMENU;
				}

				RECT	windowRect, clientRect;

				m_windowHandle = CreateWindow(
					title.c_str(),							// ウィンドウクラスの名前
					title.c_str(),							// タイトル	
					dwStyle,						// ウィンドウスタイル
					0, 0,							// ウィンドウ位置 縦, 横(あとで中央に移動させます)
					m_width, m_height,				// ウィンドウサイズ
					HWND_DESKTOP,					// 親ウィンドウなし
					(HMENU)nullptr,					// メニューなし
					m_wcex.hInstance,				// インスタンスハンドル
					(LPVOID)nullptr);				// 追加引数なし
		
				// ウインドウサイズを再計算（Metricsだけでは、フレームデザインでクライアント領域サイズが変わってしまうので）
				GetWindowRect(m_windowHandle, &windowRect);
				GetClientRect(m_windowHandle, &clientRect);
				m_width  = 
					(windowRect.right - windowRect.left) - 
					(clientRect.right - clientRect.left) + 
					m_width;
				m_height = 
					(windowRect.bottom - windowRect.top) - 
					(clientRect.bottom - clientRect.top) + 
					m_height;
				SetWindowPos(
					m_windowHandle,
					nullptr,
					(GetSystemMetrics(SM_CXSCREEN) - m_width)	/ 2,
					(GetSystemMetrics(SM_CYSCREEN) - m_height)	/ 2,
					m_width		- 1,
					m_height	- 1,
					SWP_NOZORDER);
			}
			
			if(m_windowHandle == NULL)
			{
				MessageBox(
					m_windowHandle,
					"ウインドウの作成に失敗しました",
					"CWindowクラス初期化エラー",
					MB_OK);
				exit(-1);
			}
			
			// ウインドウを表示する
			ShowWindow(m_windowHandle, SW_SHOWNORMAL);
			UpdateWindow(m_windowHandle);
		};

		LRESULT WINAPI CWindow::WindowProcedureLocal(
				UINT	message,
				WPARAM	wParam,
				LPARAM	lParam)
		{
			SetCurrent();

			switch( message )
			{
			case WM_MOUSEMOVE:
				if(m_pMouseMoveProcedure)
					m_pMouseMoveProcedure(
						lParam & 0xffff, 
						(lParam >> 16) & 0xffff);
				break;
			case WM_SIZE:
				m_width		= lParam & 0xFFFF;
				m_height	= (lParam >> 16) & 0xFFFF;

#ifdef USING_DIRECTX
				CGraphicManager::GetManager().m_D3Dpp.BackBufferWidth	= m_width;					// バックバッファの幅をセット
				CGraphicManager::GetManager().m_D3Dpp.BackBufferHeight	= m_height;					// バックバッファの高さをセット
			
				CGraphicManager::GetManager().GetDevice()->Reset(&CGraphicManager::GetManager().m_D3Dpp);
#endif // USING_DIRECTX

#ifdef USING_OPENGL
				//:TODO

#endif

				if(m_pResizeProcedure)
					m_pResizeProcedure(m_width, m_height);
				break;
			case WM_DESTROY:
				GetWindowList().erase(m_windowHandle);
				PostQuitMessage(0);
				break;
			default:
				return DefWindowProc(m_windowHandle, message, wParam, lParam);
			}
			return 0;
		};

		LRESULT WINAPI WindowProcedure(
				HWND	hWnd,
				UINT	message,
				WPARAM	wParam,
				LPARAM	lParam)
		{
			
			if(CWindow::GetWindowList().size())
			{
				auto it = CWindow::GetWindowList().find(hWnd);
				if(	it != CWindow::GetWindowList().end())
				{
					return it->second->WindowProcedureLocal(
								message,
								wParam,
								lParam);
				}
			}
			
			return DefWindowProc(hWnd, message, wParam, lParam);
		};
				
		void CWindow::Render(void)
		{
			if(m_pRenderProcedure)
			{
				std::lock_guard<std::mutex> lock(m_mutex);
				SetCurrent();
				m_graphicManager.RenderBegin();
				m_pRenderProcedure();
				m_graphicManager.RenderEnd();
			}				
		};

		CWindow*	CWindow::m_pCurrentWindow	= nullptr;

		CWindow::CWindow(
			const string_t<>& title,
			int			width,
			int			height,
			bool		isFullScreen,
			bool		isResizable)
			:m_width(width)
			,m_height(height)
			,m_isFullScreen(isFullScreen)
			,m_isResizable(isResizable)
			,m_windowHandle(0)
			,m_pRenderProcedure(nullptr)
			,m_pResizeProcedure(nullptr)
			,m_pMouseMoveProcedure(nullptr)
		{
			// ウインドウクラスの登録
			PrepareCreateWindow(title);
			
			//ウィンドウの作成
			CreateWindowLocal(title);

			//グラフィックマネージャーの初期化
			m_graphicManager.Init(*this);

			SetCurrent();
			
			CWindow::GetWindowList().
				insert(std::make_pair(m_windowHandle, this));
		};

		void CWindow::RenderAllWindow(void)
		{
			for (auto& windowPair : CWindow::GetWindowList())
			{
				auto& window = *windowPair.second;

				if (window.m_hasRenderingRequest)
				{
					window.Render();
					window.m_hasRenderingRequest = false;
				}
			}
		};
		
		MSG CWindow::MainLoop(void)
		{
			MSG	msg;		// メッセージ構造体
			
			while (!CWindow::GetWindowList().empty())
			{	// メッセージ･ループ
				if (PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE))	// メッセージを取得
				{
					if (GetMessage(&msg, nullptr, 0, 0) == 0) 
						break;
					else
					{
						TranslateMessage(&msg); 			// 文字メッセージへのコンバート）
						DispatchMessage(&msg); 				// メッセージをWndProcへ送る
					}
				}
				RenderAllWindow();
			}

#pragma warning(suppress: 6001)
			return msg;
		};
	};
};