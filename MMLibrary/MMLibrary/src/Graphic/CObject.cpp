/*!	\file		CObject.h
*	\details	CObjectクラスの定義
*	\author		松裏征志
*	\date		2014/06/06
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CGraphicManager.h"
#include "Graphic\Object\CObject.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CObject::CObject()
			: m_transform()
			, m_isVisible(true)
			, m_pModel(nullptr)
			, m_pCollider(nullptr)
		{};

		CObject::CObject(const CObject& src)
			: m_transform(src.m_transform)
			, m_isVisible(src.m_isVisible)
			, m_pModel(src.m_pModel)
			, m_pCollider(src.m_pCollider)
		{};

		CObject::CObject(
			CVector3&					position,
			CVector3&					scale,
			Math::CQuaternion<float>&	rotation)
			: m_transform(position, scale, rotation)
			, m_isVisible(true)
			, m_pModel(nullptr)
			, m_pCollider(nullptr)
		{};

		void CObject::Render(void)
		{
			CGraphicManager::GetManager().
				Stack.
				Push().
				MultiplyLocal(Transform().GetLocalToWorldMatrix());
			CGraphicManager::GetManager().
				SetWorldMatrix();

			if (m_isVisible && m_pModel)
				m_pModel->Render();

			CGraphicManager::GetManager().
				Stack.Pop();
		};

		CObject&
			CObject::SetModel(Model::IModel& model)
		{
			m_pModel = &model;

			return *this;
		};

		CObject&
			CObject::SetModel(Model::IModel* pModel)
		{
			m_pModel = pModel;

			return *this;
		};

		Model::IModel*
			CObject::GetModel(void) const
		{
			return m_pModel;
		};

		bool
			CObject::IsVisible(void) const
		{
			return m_isVisible;
		};

		void
			CObject::IsVisible(
			bool isVisible)
		{
			m_isVisible = isVisible;
		};


		bool CObject::IsHit(const IObject& another) const
		{
			if (HasCollider() == false ||
				another.HasCollider() == false)
				return false;

			return Collider().IsHit(another.Collider(),
				Transform().GetMatrix(),
				another.Transform().GetMatrix());
		};

		bool CObject::HasCollider(void) const
		{
			return m_pCollider != nullptr;
		};

		const Collision::ICollider& CObject::Collider(void) const
		{
			return *m_pCollider;
		};

		CObject&
			CObject::SetCollisionData(
			Collision::ICollider& collision)
		{
			m_pCollider = &collision;

			return *this;
		};
	};
};