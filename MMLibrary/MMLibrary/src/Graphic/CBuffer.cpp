/*!	\file		CBuffer.cpp
*	\details	CBufferクラスの定義
*	\author		松裏征志
*	\date		2014/11/12
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CBuffer.h"
#include "Graphic\CGraphicManager.h"
#include "Graphic\CGLFunctions.h"
#include "Utility\Macro.h"

namespace	MMLibrary
{
	namespace Graphic
	{
#ifdef USING_DIRECTX

		CVertexBufferBase::CVertexBufferBase()
			: m_vertexBuffer(nullptr)
			, m_lockedDataAddress(nullptr)
			, m_lockedNum(0)
		{
		};

		void CVertexBufferBase::Bind(size_t stride)
		{
			if (m_vertexBuffer != nullptr)
			{
				CheckDirectXErrer(
					CGraphicManager::GetManager().
					GetDevice()->
					SetStreamSource(
					0,
					m_vertexBuffer,
					0,
					stride));
			}
		};

		void CVertexBufferBase::SetData(const void* pData, size_t size)
		{
			Dispose();

			CheckDirectXErrer(
				CGraphicManager::GetManager().
				GetDevice()->
				CreateVertexBuffer(
				size,
				0,
				0,
				D3DPOOL_MANAGED,
				&m_vertexBuffer,
				0));

			memcpy_s(Lock(), size, pData, size);
			UnLock();
		};

		void CVertexBufferBase::UnBind()
		{
		};

		void* CVertexBufferBase::Lock()
		{
			if (m_lockedDataAddress == nullptr)
			{
				CheckDirectXErrer(
					m_vertexBuffer->Lock(
					0,
					0,
					&m_lockedDataAddress,
					0));
			}
			++m_lockedNum;
			return m_lockedDataAddress;
		};

		void CVertexBufferBase::UnLock()
		{
			--m_lockedNum;
			if (m_lockedNum == 0)
			{
				CheckDirectXErrer(
					m_vertexBuffer->Unlock());
				m_lockedDataAddress = nullptr;
			}
			else if (m_lockedNum < 0)
				m_lockedNum = 0;
		};

		void CVertexBufferBase::Dispose()
		{
			if (m_vertexBuffer != nullptr && m_lockedNum != 0)
				m_lockedNum = 1;
			UnLock();
			Utility::SafeRelease(m_vertexBuffer);
		};

		CIndexBuffer::CIndexBuffer()
			: m_indexBuffer(0)
			, m_lockedDataAddress(nullptr)
			, m_lockedNum(0)
			, m_num(0)
		{
		};

		void CIndexBuffer::Bind()
		{
			if (m_indexBuffer != nullptr)
			{
				CheckDirectXErrer(
					CGraphicManager::GetManager().GetDevice()->
					SetIndices(m_indexBuffer));
			}
		};

		void CIndexBuffer::SetData(const CIndexBuffer::index_t* pData, size_t size)
		{
			Dispose();

			CheckDirectXErrer(
				CGraphicManager::GetManager().
				GetDevice()->
				CreateIndexBuffer(
				size * 4,
				0,
				D3DFMT_INDEX32,
				D3DPOOL_DEFAULT,
				&m_indexBuffer,
				0));

			m_num = size;
			memcpy_s(Lock(), size * 4, pData, size * 4);
			UnLock();
		};

		void CIndexBuffer::UnBind()
		{
		};

		CIndexBuffer::index_t* CIndexBuffer::LockImpl()
		{
			if (m_lockedDataAddress == nullptr)
			{
				CheckDirectXErrer(
					m_indexBuffer->Lock(
					0,
					0,
					reinterpret_cast<void**>(&m_lockedDataAddress),
					0));
			}
			++m_lockedNum;
			return m_lockedDataAddress;
		};

		void CIndexBuffer::UnLock()
		{
			--m_lockedNum;
			if (m_lockedNum == 0)
			{

				CheckDirectXErrer(
					m_indexBuffer->Unlock());
				m_lockedDataAddress = nullptr;
			}
			else if (m_lockedNum < 0)
				m_lockedNum = 0;
		};

		void CIndexBuffer::Dispose()
		{
			if (m_indexBuffer != nullptr)
			{
				m_lockedNum = std::min(1, m_lockedNum);
				UnLock();
			}
			Utility::SafeRelease(m_indexBuffer);
		};

#endif // USING_DIRECTX
#ifdef USING_OPENGL

		CVertexBufferBase::CVertexBufferBase()
			: m_vertexBuffer(0)
			, m_lockedDataAddress(nullptr)
			, m_lockedNum(0)
		{
		};

		void CVertexBufferBase::Bind(size_t stride)
		{
			bool temp = false;
			GLboolean flag = GL_FALSE;

			CGLFunctions::GetInstance().CheckErrer();
			CGLFunctions::GetInstance().
				glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

			glGetBooleanv(GL_ARRAY_BUFFER_BINDING, &flag);
			assert(flag == GL_TRUE && "Buffer is not Binding.");

			CGLFunctions::GetInstance().CheckErrer();
		};

		void CVertexBufferBase::SetData(const void* pData, size_t size)
		{
			bool isBuffer = CGLFunctions::GetInstance().glIsBuffer(m_vertexBuffer) == GL_TRUE;
			GLint param= 0;
			if (isBuffer)
			{
				Bind(0);
				CGLFunctions::GetInstance().
					glGetBufferParameteriv(
					GL_ARRAY_BUFFER,
					GL_BUFFER_SIZE,
					&param);
			}
			CGLFunctions::GetInstance().CheckErrer();

			if (!isBuffer || param != size)
			{
				Dispose();
				CGLFunctions::GetInstance().
					glGenBuffers(1, &m_vertexBuffer);
			}
			Bind(0);

			CGLFunctions::GetInstance().
				glBufferData(GL_ARRAY_BUFFER, size, pData, GL_DYNAMIC_DRAW);

			assert(
				CGLFunctions::GetInstance().glIsBuffer(m_vertexBuffer) == GL_TRUE &&
				"Generated Buffer is not Buffer.");

			CGLFunctions::GetInstance().
				glGetBufferParameteriv(
				GL_ARRAY_BUFFER,
				GL_BUFFER_SIZE,
				&param);
			assert(param == size && "Buffer size Errer.");

			UnBind();
		};

		void CVertexBufferBase::UnBind()
		{
			CGLFunctions::GetInstance().
				glBindBuffer(GL_ARRAY_BUFFER, 0);

			CGLFunctions::GetInstance().CheckErrer();
		};

		void* CVertexBufferBase::Lock()
		{
			if (m_lockedDataAddress == nullptr)
			{
				Bind(0);
				m_lockedDataAddress =
					CGLFunctions::GetInstance().
					glMapBuffer(
					GL_ARRAY_BUFFER,
					GL_READ_WRITE);
				UnBind();
			}
			++m_lockedNum;

			CGLFunctions::GetInstance().CheckErrer();
			return m_lockedDataAddress;
		};

		void CVertexBufferBase::UnLock()
		{
			--m_lockedNum;
			if (m_lockedNum == 0)
			{
				Bind(0);
				CGLFunctions::GetInstance().
					glUnmapBuffer(GL_ARRAY_BUFFER);
				m_lockedDataAddress = nullptr;
				UnBind();
			}
			else if (m_lockedNum < 0)
				m_lockedNum = 0;

			CGLFunctions::GetInstance().CheckErrer();
		};

		void CVertexBufferBase::Dispose()
		{
			if (m_lockedDataAddress != nullptr)
			{
				m_lockedNum = 1;
				UnLock();
			}

			if (m_vertexBuffer != 0)
			{
				UnBind();

				CGLFunctions::GetInstance().
					glDeleteBuffers(
					1, 
					&m_vertexBuffer);
				m_vertexBuffer = 0;
			}

			CGLFunctions::GetInstance().CheckErrer();
		};

		CIndexBuffer::CIndexBuffer()
			: m_indexBuffer(0)
			, m_lockedDataAddress(nullptr)
			, m_lockedNum(0)
			, m_num(0)
		{
		};

		void CIndexBuffer::Bind()
		{
			CGLFunctions::GetInstance().
				glBindBuffer(
				GL_ELEMENT_ARRAY_BUFFER, 
				m_indexBuffer);

			CGLFunctions::GetInstance().CheckErrer();
		};

		void CIndexBuffer::SetData(const index_t* pData, size_t size)
		{
			Dispose();

			CGLFunctions::GetInstance().
				glGenBuffers(1, &m_indexBuffer);

			Bind();
			CGLFunctions::GetInstance().
				glBufferData(
				GL_ELEMENT_ARRAY_BUFFER, 
				size * sizeof(index_t), 
				pData,
				GL_STATIC_DRAW);
			m_num = size;
			UnBind();

			CGLFunctions::GetInstance().CheckErrer();
		};

		void CIndexBuffer::UnBind()
		{
			CGLFunctions::GetInstance().
				glBindBuffer(
				GL_ELEMENT_ARRAY_BUFFER,
				0);

			CGLFunctions::GetInstance().CheckErrer();
		};

		CIndexBuffer::index_t* CIndexBuffer::LockImpl()
		{
			if (m_lockedDataAddress == nullptr)
			{
				Bind();
				m_lockedDataAddress = static_cast<index_t*>(
					CGLFunctions::GetInstance().
					glMapBuffer(
					GL_ELEMENT_ARRAY_BUFFER,
					GL_READ_WRITE));
				UnBind();
			}
			++m_lockedNum;

			CGLFunctions::GetInstance().CheckErrer();
			return m_lockedDataAddress;
		};

		void CIndexBuffer::UnLock()
		{
			--m_lockedNum;
			if (m_lockedNum == 0)
			{
				Bind();
				CGLFunctions::GetInstance().
					glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
				m_lockedDataAddress = nullptr;
				UnBind();
			}
			else if (m_lockedNum < 0)
				m_lockedNum = 0;

			CGLFunctions::GetInstance().CheckErrer();
		};

		void CIndexBuffer::Dispose()
		{
			if (m_lockedDataAddress = nullptr)
			{
				m_lockedNum = 1;
				UnLock();
			}

			if (m_indexBuffer != 0)
				UnBind();

			CGLFunctions::GetInstance().
				glDeleteBuffers(
				1,
				&m_indexBuffer);

			m_indexBuffer = 0;

			CGLFunctions::GetInstance().CheckErrer();
	};

#endif // USING_OPENGL
}
}