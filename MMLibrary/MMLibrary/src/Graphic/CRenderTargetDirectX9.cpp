/*!	\file		CRenderTarget.h
*	\details	CRenderTargetクラスの定義
*	\author		松裏征志
*	\date		2015/03/03
*/
#ifdef USING_DIRECTX
//===========================================================
//ファイルインクルード
//===========================================================
#include "Graphic/CRenderTarget.h"
#include "Graphic/Texture/CTexture.h"
#include "Utility/Macro.hpp"

namespace	MMLibrary
{
	namespace	Graphic
	{
		class CRenderTargetImpl : public IRenderTargetImpl
		{
		private:
			std::shared_ptr<CTextureBase>	m_pTexture;
			LPDIRECT3DSURFACE9				m_pTextureSurface;
			LPDIRECT3DSURFACE9				m_pDepthSurface;

		public:
			CRenderTargetImpl()
				: m_pTexture()
				, m_pTextureSurface(nullptr)
				, m_pDepthSurface(nullptr)
			{};
			CRenderTargetImpl(size_t width, size_t height);

			virtual ~CRenderTargetImpl() override;
			virtual Utility::CDoAtDestruct SetToDevice() override;
			virtual void SetByDevice() override;
			virtual std::shared_ptr<CTextureBase> GetTexture() override;
		};

		std::unique_ptr<IRenderTargetImpl> CreateRenderTargetImpl(size_t width, size_t height)
		{
			return std::make_unique<CRenderTargetImpl>(width, height);
		}

		CRenderTargetImpl::CRenderTargetImpl(size_t width, size_t height)
			: m_pTexture(new CTextureBase(width, height, TEXTURE_TYPE::RENDER_TARGET))
			, m_pTextureSurface(nullptr)
			, m_pDepthSurface(nullptr)
		{
			auto* pDevice = CGraphicManager::GetManager().GetDevice();

			//デプスステンシルサーフェイスの作成
			if (CheckDirectXErrer(
				pDevice->CreateDepthStencilSurface(
				width,
				height,
				D3DFMT_D16,
				D3DMULTISAMPLE_NONE,
				0,
				TRUE,
				&m_pDepthSurface,
				nullptr)) == false)
			{
				assert(0 && "Failed to create depth stencil surface.");
			}

			//テクスチャサーフェイスの取得
			if (CheckDirectXErrer(m_pTexture->GetTextureHandle()->GetSurfaceLevel(0, &m_pTextureSurface)) == false)
			{
				assert(0 && "Failed to get texture surface.");

				m_pTextureSurface = nullptr;
			}
		}

		CRenderTargetImpl::~CRenderTargetImpl()
		{
			Utility::SafeRelease(m_pTextureSurface);
			Utility::SafeRelease(m_pDepthSurface);
		}

		Utility::CDoAtDestruct CRenderTargetImpl::SetToDevice()
		{
			auto* pDevice = CGraphicManager::GetManager().GetDevice();

			//元のデータを取得
			LPDIRECT3DSURFACE9 pTextureSurface;
			LPDIRECT3DSURFACE9 pDepthSurface;
			pDevice->GetDepthStencilSurface(&pDepthSurface);
			pDevice->GetRenderTarget(0, &pTextureSurface);

			//設定
			pDevice->SetRenderTarget(0, m_pTextureSurface);
			pDevice->SetDepthStencilSurface(m_pDepthSurface);

			return Utility::CDoAtDestruct(
				[pDevice, pTextureSurface, pDepthSurface]()
			{
				//元に戻す処理
				pDevice->SetRenderTarget(0, pTextureSurface);
				pDevice->SetDepthStencilSurface(pDepthSurface);
			});
		}
		
		void CRenderTargetImpl::SetByDevice()
		{
			auto* pDevice = CGraphicManager::GetManager().GetDevice();

			pDevice->GetDepthStencilSurface(&m_pDepthSurface);
			pDevice->GetRenderTarget(0, &m_pTextureSurface);

			m_pTexture.reset(new CTextureBase());
		}

		std::shared_ptr<CTextureBase> CRenderTargetImpl::GetTexture()
		{
			return m_pTexture;
		}
	}
}

#endif