/*!	\file		Color.cpp
*	\details	ColorΦAΜθ`
*	\author		Ό ͺu
*	\date		2014/05/01
*/

//***********************************************************
//CN[h
//***********************************************************
#include "Graphic/Color/Color.h"

namespace MMLibrary
{
	namespace Graphic
	{
		uchar_t To8bitColor(const float src)
		{
			return (uchar_t)(src * 255);
		};

		float ToFloatColor(const uchar_t src)
		{
			return src / 255.0f;
		};
	};
};