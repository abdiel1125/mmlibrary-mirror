/*!	\file	CValueArray.cpp
\details	CValueArrayクラスを明示的にインスタンス化する
\author		松裏征志
\date		2015/02/24
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Math/CValueArray.h"

namespace MMLibrary
{
	namespace Math
	{
		template class CValueArray < float, 3 >;
		template class CValueArray < float, 4 >;
		template class CValueArray < CValueArray < float, 4 >, 4 >;
	};
};

#define MMLIBRARY_CVALUE_ARRAY_INSTANTIATED