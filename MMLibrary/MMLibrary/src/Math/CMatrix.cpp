/*!	\file		CMatrix.cpp
*	\details	CMatrixクラスの明示的インスタンス化
*	\author		松裏征志
*	\date		2015/02/24 作成
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Math/CMatrix.hpp"


namespace MMLibrary
{
	namespace Math
	{
		template class CMatrix < float, 4, 4 > ;
	};
};

#define MMLIBRARY_CMATRIX_INSTANTIATED