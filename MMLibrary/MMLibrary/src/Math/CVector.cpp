/*!	\file		CVector.cpp
*	\details	CVectorクラスの明示的インスタンス化
*	\author		松裏征志
*	\date		2015/02/24
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Math/CVector.hpp"

namespace MMLibrary
{
	namespace Math
	{
		template class CVector < float, 3 >;
	};
};

#define MMLIBRARY_CVECTOR_INSTANTIATED