/*!	\file		CPlane.cpp
*	\details	CPlaneクラスの定義
*	\author		松裏征志
*	\date		2014/06/03
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Math/CPlane.h"

namespace MMLibrary
{
	namespace Math
	{
		CPlane::CPlane(void)
		:a(0)
		,b(0)
		,c(0)
		,d(0)
		{};

		CPlane::CPlane(const float* pArray)
		:a(pArray[0])
		,b(pArray[1])
		,c(pArray[2])
		,d(pArray[3])
		{};

		CPlane::CPlane(	
			const float a,
			const float b,
			const float c,
			const float d)
		:a(a)
		,b(b)
		,c(c)
		,d(d)
		{};

		CPlane CPlane::ToClone(void) const
		{
			return *this;
		};

		CPlane::operator float*()
		{
			return (float*)this;
		};

		CPlane::operator const float*() const
		{
			return (float*)this;
		};

		CPlane::operator CVector<float, 4>()
		{
			return *(CVector<float, 4>*)this;
		};

		CPlane::operator const CVector<float, 4>() const
		{
			return *(CVector<float, 4>*)this;
		};

		CPlane CPlane::operator+(CPlane& src) const
		{
			return std::move(ToClone() += src);
		};

		CPlane CPlane::operator-(CPlane& src) const
		{
			return std::move(ToClone() -= src);
		};

		CPlane& CPlane::operator+=(CPlane& src)
		{
			a += src.a;
			b += src.b;
			c += src.c;
			d += src.d;

			return *this;
		};

		CPlane& CPlane::operator-=(CPlane& src)
		{
			a -= src.a;
			b -= src.b;
			c -= src.c;
			d -= src.d;

			return *this;
		};
			
		bool CPlane::operator == ( const CPlane& src) const
		{
			return	a == src.a &&
					b == src.b &&
					c == src.c &&
					d == src.d ;
		};
			
		bool CPlane::operator != ( const CPlane& src) const
		{
			return !(*this == src);
		};

		float CPlane::Dot(const CVector<float, 4>& vector) const
		{
			return vector.Dot(*this);
		};
		
		float CPlane::DotCoord(const CVector<float, 3>& vector) const
		{
			CVector<float, 4> temp;
			
			temp.at(0) = vector.at(0);
			temp.at(1) = vector.at(1);
			temp.at(2) = vector.at(2);
			temp.at(3) = 1;
			
			return Dot(temp);
		};
		
		float CPlane::DotNormal(const CVector<float, 3>& vector) const
		{
			CVector<float, 4> temp;
			
			temp.at(0) = vector.at(0);
			temp.at(1) = vector.at(1);
			temp.at(2) = vector.at(2);
			temp.at(3) = 0;
			
			return Dot(temp);
		};
		
		CVector<float, 3>& CPlane::Normal(void)
		{
			return *(CVector<float, 3>*)(this);
		};
		
		const CVector<float, 3>& CPlane::Normal(void) const
		{
			return *(CVector<float, 3>*)(this);
		};

		CVector<float, 3> CPlane::GetPoint(void) const
		{
			return Normal() * d;
		};

		CVector<float, 3> CPlane::GetInterSectWithLine(
			const CVector<float, 3>& begin,
			const CVector<float, 3>& end) const
		{
			CVector<float, 3> point = GetPoint();
			CVector<float, 3> pbeg = point - begin;
			CVector<float, 3> pend = point - end;
			
			float pbegDot = DotNormal(pbeg);
			float pendDot = DotNormal(pend);
			
			if(pbegDot < 0.000001f) pbegDot = 0;
			if(pendDot < 0.000001f) pendDot = 0;

			//
			if(pbegDot == 0 && pendDot == 0)
				return CVector<float,3>();
			else 
			{
				if((pbegDot >= 0) != (pendDot <= 0))
				{
					CVector<float,3> line = end - begin;
					float				ratio = 
						abs(pbegDot) / (abs(pbegDot) + abs(pendDot));
					line *= ratio;

					return begin + line;
				}
				else
					return CVector<float,3>();
			}
		};
		
		CPlane& CPlane::Normalize(void)
		{
			Normal().Normalize();

			return *this;
		};
		
		CPlane& CPlane::Transform(CMatrix<float, 4, 4>& matrix)
		{
			*this = *(CPlane*)
				&
				((CVector<float, 4>)*this).
				ToRawMatrix().
				Multiply(matrix.ToTransposedMatrix());

			return *this;
		};

		CPlane CPlane::CreateByPointAndNormal(
			const CVector<float, 3>& point,
			const CVector<float, 3>& normal)
		{
			return CPlane().SetByPointAndNormal(
				point,
				normal);
		};

		CPlane CPlane::CreateByPoint(
				const CVector<float, 3>& point1,
				const CVector<float, 3>& point2,
				const CVector<float, 3>& point3)
		{
			return CPlane().SetByPoint(
				point1,
				point2,
				point3);
		};
		
		CPlane& CPlane::SetByPointAndNormal(
			const CVector<float, 3>& point,
			const CVector<float, 3>& normal)
		{
			a = normal.at(0);
			b = normal.at(1);
			c = normal.at(2);
			d = normal.Dot(point);

			return *this;
		};

		CPlane& CPlane::SetByPoint(
				const CVector<float, 3>& point1,
				const CVector<float, 3>& point2,
				const CVector<float, 3>& point3)
		{
			CVector<float, 3> normal = point2 - point1;

			normal.Cross(point1 - point2).Normalize();

			SetByPointAndNormal(point1,normal);

			return *this;
		};
	};
};