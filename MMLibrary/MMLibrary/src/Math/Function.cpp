/*!	\file		Function.cpp
*	\details	Math関連の関数などの定義
*	\author		松裏征志
*	\date		2014/04/25
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Math/Function.h"

//***********************************************************
//共通の構造体等の定義
//***********************************************************
namespace MMLibrary
{
	namespace Math
	{
		float DegreeToRadian(float degree)
		{
			return degree * PI<float>() / 180;
		};

		float RadianToDegree(float radian)
		{
			return radian * 180 / PI<float>();
		};

		int	CountBit(uchar_t		value)
		{
			uchar_t count = (value & 0x55) + ((value >> 1) & 0x55);
			count = (count & 0x33) + ((count >> 2) & 0x33);
			return					(count & 0x0f) + ((count >> 4) & 0x0f);
		};

		int	CountBit(ushort_t		value)
		{
			ushort_t count = (value & 0x5555) + ((value >> 1) & 0x5555);
			count = (count & 0x3333) + ((count >> 2) & 0x3333);
			count = (count & 0x0f0f) + ((count >> 4) & 0x0f0f);
			return						(count & 0x00ff) + ((count >> 8) & 0x00ff);
		};

		int	CountBit(uint_t		value)
		{
			uint_t count = (value & 0x55555555) + ((value >> 1) & 0x55555555);
			count = (count & 0x33333333) + ((count >> 2) & 0x33333333);
			count = (count & 0x0f0f0f0f) + ((count >> 4) & 0x0f0f0f0f);
			count = (count & 0x00ff00ff) + ((count >> 8) & 0x00ff00ff);
			return						(count & 0x0000ffff) + ((count >> 16) & 0x0000ffff);
		};

		int	GetMSB(uchar_t		value)
		{
			if (value == 0) return -1;

			value |= (value >> 1);
			value |= (value >> 2);
			value |= (value >> 4);

			return CountBit(value) - 1;
		};

		int	GetMSB(ushort_t		value)
		{
			if (value == 0) return -1;

			value |= (value >> 1);
			value |= (value >> 2);
			value |= (value >> 4);
			value |= (value >> 8);

			return CountBit(value) - 1;
		};

		int	GetMSB(uint_t			value)
		{
			if (value == 0) return -1;

			value |= (value >> 1);
			value |= (value >> 2);
			value |= (value >> 4);
			value |= (value >> 8);
			value |= (value >> 16);

			return CountBit(value) - 1;
		};

		int	GetLSB(uchar_t		value)
		{
			if (value == 0) return -1;

			value |= (value << 1);
			value |= (value << 2);
			value |= (value << 4);

			return 8 - CountBit(value);
		};

		int	GetLSB(ushort_t		value)
		{
			if (value == 0) return -1;

			value |= (value << 1);
			value |= (value << 2);
			value |= (value << 4);
			value |= (value << 8);

			return 16 - CountBit(value);
		};

		int	GetLSB(uint_t			value)
		{
			if (value == 0) return -1;

			value |= (value << 1);
			value |= (value << 2);
			value |= (value << 4);
			value |= (value << 8);
			value |= (value << 16);

			return 32 - CountBit(value);
		};

		int GetNextPow2(uchar_t	value)
		{
			if (value == 0) return 0;
			return 1 << (GetMSB((uchar_t)(value - 1)) + 1);
		};

		int GetNextPow2(ushort_t	value)
		{
			if (value == 0) return 0;
			return 1 << (GetMSB((ushort_t)(value - 1)) + 1);
		};

		int GetNextPow2(uint_t	value)
		{
			if (value == 0) return 0;
			return 1 << (GetMSB((uint_t)(value - 1)) + 1);
		};

		float GetFloatRandam(void)
		{
			return (float)rand() / RAND_MAX;
		};
	};
};