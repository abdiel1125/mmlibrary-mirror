/*!	\file		CAxisAlignedBoundingBox.h
*	\details	CAxisAlignedBoundingBoxクラスの定義
*	\author		松裏征志
*	\date		2014/06/24
*/

//***********************************************************
//インクルード
//***********************************************************
#include <limits>
#include "Collision\CAxisAlignedBoundingBox.h"
#include "Collision\CBoundingSphere.h"
#include "Graphic\Model\CModel.h"

namespace MMLibrary
{
	namespace Collision
	{
		CAxisAlignedBoundingBox::CAxisAlignedBoundingBox(void)
			: m_max(::std::numeric_limits<float>::min())
			, m_min(::std::numeric_limits<float>::max())
		{};

		Graphic::CVector3& 
			CAxisAlignedBoundingBox::Min(void)
		{
			return m_min;
		};

		const Graphic::CVector3& 
			CAxisAlignedBoundingBox::GetMin(void) const
		{
			return m_min;
		};

		Graphic::CVector3& 
			CAxisAlignedBoundingBox::Max(void)
		{
			return m_max;
		};

		const Graphic::CVector3& 
			CAxisAlignedBoundingBox::GetMax(void) const
		{
			return m_max;
		};

		Graphic::CVector3 
			CAxisAlignedBoundingBox::GetCenter(void) const
		{
			return m_min + (m_max - m_min) * 0.5f;
		};

		CAxisAlignedBoundingBox 
			CAxisAlignedBoundingBox::GetTransformed(
			const Graphic::CMatrix4x4& matrix) const
		{
			CAxisAlignedBoundingBox rtn;
			rtn.Min() = (Graphic::CVector3&)(matrix.at(3));
			rtn.Max() = rtn.Min();

			for(int i = 0; i < 3; i++)
			{
				for(int j = 0; j < 3; j++)
				{
					float e = m_min[j] * matrix.at(j).at(i);
					float f = m_max[j] * matrix.at(j).at(i);
					if(e < f)
					{
						rtn.Min().at(i) +=  e;
						rtn.Max().at(i) +=  f;

					}
					else
					{
						rtn.Min().at(i) +=  f;
						rtn.Max().at(i) +=  e;
					}
				}
			}

			return rtn;
		};

		void CAxisAlignedBoundingBox::CheckMinMax(
			const Graphic::CVector3& src)
		{
			for(uint_t i = 0; i < m_max.size(); ++i)
			{
				if(src[i] > m_max[i])
					m_max[i] = src[i];
				else if(src[i] < m_min[i])
					m_min[i] = src[i];
			}
		};

		void CAxisAlignedBoundingBox::CheckMinMax(
			const CAxisAlignedBoundingBox& src)
		{
			for(uint_t i = 0; i < m_max.size(); ++i)
			{
				if(src.GetMax()[i] > m_max[i])
					m_max.at(i) = src.GetMax()[i];

				if (src.GetMin()[i] < m_min[i])
					m_min[i] = src.GetMin()[i];
			}
		};
		
		bool CAxisAlignedBoundingBox::IsHit(
			const ICollider&	another,
			const Graphic::CMatrix4x4& thisMatrix,
			const Graphic::CMatrix4x4& anotherMatrix) const
		{
			return another.IsHit(
				*this,
				anotherMatrix,
				thisMatrix);
		};

		bool CAxisAlignedBoundingBox::IsHit(
			const CAxisAlignedBoundingBox&	another,
			const Graphic::CMatrix4x4& thisMatrix,
			const Graphic::CMatrix4x4& anotherMatrix) const
		{
			auto thisBuffer	= GetTransformed(thisMatrix);
			auto anotherBuffer = GetTransformed(anotherMatrix);

			for(int i = 0; i < 3; ++i)
				if( (thisBuffer.Min().at(i) > anotherBuffer.Max().at(i)) ||
					(thisBuffer.Max().at(i) < anotherBuffer.Min().at(i))) return false;
			return true;
		};

		bool CAxisAlignedBoundingBox::IsHit(
			const CBoundingSphere&	another,
			const Graphic::CMatrix4x4& thisMatrix,
			const Graphic::CMatrix4x4& anotherMatrix) const
		{
			return another.IsHit(
				*this,
				anotherMatrix,
				thisMatrix);
		};

		void CAxisAlignedBoundingBox::Render(
			const Graphic::CMatrix4x4& matrix) const
		{
			Graphic::Model::CModel<> mesh;
			mesh.SetByBox();

			//auto*	device = Graphic::CGraphicManager::GetManager().GetDevice();

			//device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
			//device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
			//device->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

			//device->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
			//device->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			//device->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

			Graphic::Model::CMaterial<USE_ALLOCATOR> material;
			material.SetDiffuse(Graphic::CColor32(255, 255, 255, 64));
			material.SetSpecular(Graphic::CColor32(255, 255, 255, 64));
			material.SetAmbient(Graphic::CColor32(255, 255, 255, 64));
			material.SetEmissive(Graphic::CColor32(255, 255, 255, 64));
			mesh.SetMaterial(0, material);

			auto transformed = GetTransformed(matrix);
			auto mat = Graphic::CMatrix4x4::CreateByScaling(transformed.Max() - transformed.Min());
			mat.GetPosition() = transformed.GetCenter();
			Graphic::CGraphicManager::GetManager().
				Stack.
				Push().
				MultiplyLocal(mat);

			Graphic::CGraphicManager::GetManager().SetWorldMatrix();
			mesh.Render();
			Graphic::CGraphicManager::GetManager().Stack.Pop();
		};

		float CAxisAlignedBoundingBox::GetLengthFromPoint(
			const Graphic::CVector3& point) const
		{
			float SqLen = 0;
			for (int i = 0; i < 3; i++)
			{
				if (point[i] < GetMin()[i])
					SqLen += (point[i] - GetMin()[i]) * (point[i] - GetMin()[i]);
				if (point[i] > GetMax()[i])
					SqLen += (point[i] - GetMax()[i]) * (point[i] - GetMax()[i]);
			}
			return sqrt(SqLen);
		};
	};
};