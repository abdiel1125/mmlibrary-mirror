/*!	\file		CBoundingSphere.cpp
*	\details	CBoundingSphereクラスの定義
*	\author		松裏征志
*	\date		2014/09/05
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Utility\Macro.h"
#include "Collision\ICollider.h"
#include "Collision\CBoundingSphere.h"
#include "Collision\CAxisAlignedBoundingBox.h"
#include "Graphic\Model\CModel.h"
#include "Graphic\CVector3.h"
#include "Graphic\CMatrix4x4.h"

namespace MMLibrary
{
	namespace Collision
	{
		CBoundingSphere::CBoundingSphere(void)
			:m_radius(0)
			,m_center(0)
		{};

		bool CBoundingSphere::IsHit(
			const ICollider&	another,
			const Graphic::CMatrix4x4& thisMatrix,
			const Graphic::CMatrix4x4& anotherMatrix) const
		{
			return another.IsHit(
				*this,
				anotherMatrix,
				thisMatrix);
		};

		bool CBoundingSphere::IsHit(
			const CAxisAlignedBoundingBox&	another,
			const Graphic::CMatrix4x4& thisMatrix,
			const Graphic::CMatrix4x4& anotherMatrix) const
		{
			auto thisBuffer = GetTransformed(thisMatrix);
			auto anotherBuffer = another.GetTransformed(anotherMatrix);

			return 
				thisBuffer.GetRadius() >
				anotherBuffer.
					GetLengthFromPoint(thisBuffer.GetCenter());
		};

		bool CBoundingSphere::IsHit(
			const CBoundingSphere&	another,
			const Graphic::CMatrix4x4& thisMatrix,
			const Graphic::CMatrix4x4& anotherMatrix) const
		{
			auto thisBuffer = GetTransformed(thisMatrix);
			auto anotherBuffer = another.GetTransformed(anotherMatrix);

			return
				thisBuffer.GetRadius() + anotherBuffer.GetRadius() >
				thisBuffer.GetLengthFromPoint(anotherBuffer.GetCenter());
		};

		void CBoundingSphere::Render(
			const Graphic::CMatrix4x4& matrix) const
		{
			Graphic::Model::CModel<> mesh;
			mesh.SetBySphere(12, 12, GetRadius());

			//auto*	device = Graphic::CGraphicManager::GetManager().GetDevice();

			//device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
			//device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
			//device->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

			//device->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
			//device->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			//device->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

			Graphic::Model::CMaterial<USE_ALLOCATOR> material;
			material.SetDiffuse(Graphic::CColor32(255, 255, 255, 64));
			material.SetSpecular(Graphic::CColor32(255, 255, 255, 64));
			material.SetAmbient(Graphic::CColor32(255, 255, 255, 64));
			material.SetEmissive(Graphic::CColor32(255, 255, 255, 64));
			mesh.SetMaterial(0, material);

			Graphic::CGraphicManager::GetManager().
				Stack.
				Push().
				MultiplyLocal(matrix).
				MultiplyLocal(Graphic::CMatrix4x4::CreateByTranslation(GetCenter()));

			Graphic::CGraphicManager::GetManager().SetWorldMatrix();
			mesh.Render();
			Graphic::CGraphicManager::GetManager().Stack.Pop();
		};

		float CBoundingSphere::GetLengthFromPoint(
			const Graphic::CVector3& point) const
		{
			float length = (point - GetCenter()).Length() - GetRadius();

			if(length < 0) return 0;
			return length;
		};

		CBoundingSphere CBoundingSphere::GetTransformed(const Graphic::CMatrix4x4& matrix) const
		{
			auto rtn = *this;
			auto scale = matrix.GetScale();

			rtn.Center() += matrix.GetPosition();
			rtn.Radius() *= std::max(scale[0], std::max(scale[1], scale[2]));

			return rtn;
		}
	};
};