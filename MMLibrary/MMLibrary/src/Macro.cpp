/*!	\file		Macro.cpp
*	\details	MMLibraryで使用するマクロやちょっとしたクラス等の定義
*	\author		松裏征志
*	\date		2014/04/19
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Utility\Macro.h"

namespace MMLibrary
{
	namespace Utility
	{
		size_t GetFileSize(std::ifstream& file)
		{
			uint_t first = (unsigned)file.seekg(0, file.beg).tellg();
			return (uint_t)file.seekg(0, file.end).tellg() - first;
		}

		bool IsFirstCodeOfMultiByteCode(char code)
		{
			uchar_t uCode = code;
			return	
				(0x80 <= uCode && uCode < 0xa0) ||
				(0xe0 <= uCode && uCode < 0xf0);
		}

		int GetCodeNumByFirstCodeOfUTF8(wchar_t code)
		{
			if (0x00 <= code && code <= 0x7f) return 1;
			if (0xc0 <= code && code <= 0xdf) return 2;
			if (0xe0 <= code && code <= 0xef) return 3;

			return -1;
		}

		uint_t Packing(wchar_t code)
		{
			return (code & 0xff);
		};

		uint_t Packing(wchar_t code1, wchar_t code2)
		{
			return
				((code1 & 0xff) << 8) +
				(code2 & 0xff);
		};

		uint_t Packing(wchar_t code1, wchar_t code2, wchar_t code3)
		{
			return 
				((code1 & 0xff) << 16) +
				((code2 & 0xff) << 8 ) +
				(code3 & 0xff);
		};

		uint_t Packing(wchar_t code1, wchar_t code2, wchar_t code3, wchar_t code4)
		{
			return 
				((code1 & 0xff) << 24) + 
				((code2 & 0xff) << 16) +
				((code3 & 0xff) << 8 ) +
				(code4 & 0xff);
		};

		uint_t Packing(char code)
		{
			return static_cast<uchar_t>(code);
		};

		uint_t Packing(char code1, char code2)
		{
			return
				(static_cast<uchar_t>(code1) << 8) +
				static_cast<uchar_t>(code2);
		};

		uint_t Packing(char code1, char code2, char code3)
		{
			return
				(static_cast<uchar_t>(code1) << 16) +
				(static_cast<uchar_t>(code2) << 8 ) +
				static_cast<uchar_t>(code3);
		};

		uint_t Packing(char code1, char code2, char code3, char code4)
		{
			return
				(static_cast<uchar_t>(code1) << 24) +
				(static_cast<uchar_t>(code2) << 16) +
				(static_cast<uchar_t>(code3) << 8 ) +
				static_cast<uchar_t>(code4);
		};
	};
};