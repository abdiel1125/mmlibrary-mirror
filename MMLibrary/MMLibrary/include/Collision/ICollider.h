/*!	\file		ICollider.h
*	\details	IColliderクラスの定義
*	\author		松裏征志
*	\date		2014/09/05
*/
#ifndef MMLIBRARY_ICOLLIDER_H_
#define MMLIBRARY_ICOLLIDER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CVector3.h"
#include "Graphic\CMatrix4x4.h"

namespace MMLibrary
{
	namespace Collision
	{
		class CAxisAlignedBoundingBox;
		class CBoundingSphere;

		//!	コリジョンクラスの基底クラス
		class ICollider
		{
		public:
			//!	デストラクタ
			virtual ~ICollider(void){};

#pragma region IsHit
			//!	@{

			//!	相手がわからない場合の衝突判定
virtual		bool IsHit(
				const ICollider&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const = 0;

			//!	相手がCAxisAlignedBoundingBoxである場合の衝突判定
virtual		bool IsHit(
				const CAxisAlignedBoundingBox&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const = 0;

			//!	相手がCBoundingSphereである場合の衝突判定
virtual		bool IsHit(
				const CBoundingSphere&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const = 0;

			//!	@}
#pragma endregion

			//!	描画
virtual		void Render(
				const Graphic::CMatrix4x4& matrix) const = 0;
		};
	};
};

#endif	//	MMLIBRARY_ICOLLIDER_H_