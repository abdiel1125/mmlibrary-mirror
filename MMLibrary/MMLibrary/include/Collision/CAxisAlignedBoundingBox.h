/*!	\file		CAxisAlignedBoundingBox.h
*	\details	CAxisAlignedBoundingBoxクラスの定義
*	\author		松裏征志
*	\date		2014/06/24
*/
#ifndef MMLIBRARY_CAXIS_ALIGNED_BOUNDING_BOX_H_
#define MMLIBRARY_CAXIS_ALIGNED_BOUNDING_BOX_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Utility\Macro.h"
#include "Collision\ICollider.h"
#include "Graphic\CVector3.h"
#include "Graphic\CMatrix4x4.h"
#include "Graphic\Model\CModel.h"

namespace MMLibrary
{
	namespace Collision
	{
		//!	軸並行境界ボックスによる当たり判定クラス
		class CAxisAlignedBoundingBox : public ICollider
		{
		private:
			Graphic::CVector3	m_min;
			Graphic::CVector3	m_max;

			void CheckMinMax(const Graphic::CVector3& src);

			void CheckMinMax(const CAxisAlignedBoundingBox& src);

		public:
			//!	コンストラクタ
			CAxisAlignedBoundingBox(void);
#pragma region Reference

			//!	最小の3次元座標
			Graphic::CVector3& Min(void);

			//!	最大の3次元座標
			Graphic::CVector3& Max(void);

#pragma endregion

#pragma region Getter

			//!	最小の3次元座標を取得する
			const Graphic::CVector3& GetMin(void) const;

			//!	最大の3次元座標を取得する
			const Graphic::CVector3& GetMax(void) const;

#pragma endregion

			//!	中心の取得
			Graphic::CVector3 GetCenter(void) const;

			//!	座標変換行列を適応したものを返す
			CAxisAlignedBoundingBox GetTransformed(const Graphic::CMatrix4x4& matrix) const;

#pragma region IsHit

			bool IsHit(
				const ICollider&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

			bool IsHit(
				const CAxisAlignedBoundingBox&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

			bool IsHit(
				const CBoundingSphere&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

#pragma endregion
			//!	描画
			void Render(
				const Graphic::CMatrix4x4& matrix) const override;

			//!	指定した座標からの距離を求める
			float GetLengthFromPoint(
				const Graphic::CVector3& point) const;

#pragma region Creator

			//!	CVerticesから作成する
			template<class VERTEX, template<class> class Allocator = USE_ALLOCATOR >
			static		CAxisAlignedBoundingBox CreateByCVertices(
				const Graphic::Model::CVertices<VERTEX, Allocator>& primitive)
			{
				return CAxisAlignedBoundingBox().
					SetByCVertices<VERTEX, Allocator>(primitive);
			};

			//!	CModelから作成する
			template<template<class> class Allocator>
			static		CAxisAlignedBoundingBox CreateByCModel(
				const Graphic::Model::CModel<Allocator>& mesh)
			{
				return CAxisAlignedBoundingBox().
					SetByCModel(mesh);
			};

#pragma endregion

#pragma region Setter

			//!	CVerticesから設定する
			template<class VERTEX, template<class> class Allocator = USE_ALLOCATOR>
			CAxisAlignedBoundingBox& SetByCVertices(
				const Graphic::Model::CVertices<VERTEX, Allocator>& primitive)
			{
				for (auto& vertex : primitive.Vertices())
				{
					CheckMinMax(vertex.Position());
				}

				return *this;
			};

			//!	CModelから設定する
			template<template<class> class Allocator>
			CAxisAlignedBoundingBox& SetByCModel(
				const Graphic::Model::CModel<Allocator>& model)
			{
				Min().fill(0);
				Max().fill(0);

				for (const Graphic::Model::CMesh<Allocator>& mesh : model.GetMeshes())
				{
					CAxisAlignedBoundingBox temp;
					//アニメーションがない場合
					if (mesh.SkinWeights().empty())
					{
						temp.SetByCVertices(mesh);
						uint_t boneIndex = mesh.GetBoneIndex();
						if (boneIndex != -1)
						{
							temp = temp.GetTransformed(model.GetBones()[boneIndex].GetWorldMatrix(model.GetBones()));
						}
					}
					//アニメーションがある場合
					else
					{
						//位置情報バッファの作成
						vector_t<Graphic::CVector3> positions;
						positions.reserve(mesh.Vertices().size());
						for (const auto& vertex : mesh.Vertices())
						{
							positions.emplace_back(vertex.Position());
						}

						//ボーン情報の適用
						for (uint_t index = 0; index < positions.size(); ++index)
						{
							CMatrix4x4 matrixComposit;
							matrixComposit.SetBy1Element(0);
							for (int i = 0; i < 4; ++i)
							{
								size_t	skinWeightIndex = mesh.SkinWeightIndeces()[index][i];
								float	skinWeight = mesh.SkinWeights()[index][i];

								if (skinWeightIndex != -1)
									matrixComposit += model.Bones()[skinWeightIndex].DrawMatrix() * skinWeight;
							}
							positions[index].TransformCoord(matrixComposit);
						}

						//aabbの作成
						for (const auto& position : positions)
						{
							temp.CheckMinMax(position);
						}
					}

					Combine(temp);
				}

				return *this;
			};

#pragma endregion

			//!	指定したCAxisAlignedBoundingBoxを自身に取り込む
			CAxisAlignedBoundingBox& Combine(const CAxisAlignedBoundingBox& other)
			{
				for (int i = 0; i < 3; i++)
				{
					Min().at(i) = std::min(GetMin().at(i), other.GetMin().at(i));
					Max().at(i) = std::max(GetMax().at(i), other.GetMax().at(i));
				}

				return *this;
			}
		};
	};
};

#endif