/*!	\file		CBoundingSphere.h
*	\details	CBoundingSphereクラスの宣言
*	\author		松裏征志
*	\date		2014/09/05
*/
#ifndef MMLIBRARY_CBOUNDING_SPHERE_H_
#define MMLIBRARY_CBOUNDING_SPHERE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Collision\ICollider.h"
#include "Graphic/CVector3.h"
#include "Graphic\Model\CVertices.h"
#include "Graphic\Model\CModel.h"

namespace MMLibrary
{
	namespace Graphic
	{
		class CMatrix4x4;
	}
	namespace Collision
	{
		//!	境界球による当たり判定クラス
		class CBoundingSphere : public ICollider
		{
		private:
			Graphic::CVector3	m_center;
			float				m_radius;

		public:
			//!	コンストラクタ
			CBoundingSphere(void);
			
#pragma region Reference

			//!	中心座標
			Graphic::CVector3& Center(void)
			{
				return m_center;
			};

			//!	半径
			float& Radius(void)
			{
				return m_radius;
			};

#pragma endregion
#pragma region Getter

			//!	中心座標を取得する
			const Graphic::CVector3& GetCenter(void) const
			{
				return m_center;
			};

			//!	半径を取得する
			const float& GetRadius(void) const
			{
				return m_radius;
			};
			
#pragma endregion
#pragma region IsHit

			bool IsHit(
				const ICollider&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

			bool IsHit(
				const CAxisAlignedBoundingBox&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

			bool IsHit(
				const CBoundingSphere&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

#pragma endregion
			//!	描画
			void Render(
				const Graphic::CMatrix4x4& matrix) const override;

			//!	指定した座標からの距離を求める
			float GetLengthFromPoint(
				const Graphic::CVector3& point) const;

			//!	座標変換行列を適応したものを返す
			CBoundingSphere GetTransformed(const Graphic::CMatrix4x4& matrix) const;

#pragma region Creator

			//!	CVerticesから作成する
			template<class VERTEX, template<class> class Allocator>
static		CBoundingSphere CreateByCVertices(
	const Graphic::Model::CVertices<VERTEX>& primitive)
			{
				return CBoundingSphere().
					SetByCVertices(primitive);
			};

			//!	CModelから作成する
			template<template<class> class Allocator>
static		CBoundingSphere CreateByCModel(
				const Graphic::Model::CModel<Allocator>& model)
			{
				return CBoundingSphere().
					SetByCModel(model);
			};

#pragma endregion

#pragma region Setter

			//!	CVerticesから設定する
			template<class VERTEX, template<class> class Allocator>
			CBoundingSphere& SetByCVertices(
				const Graphic::Model::CVertices<VERTEX, Allocator>& primitive)
			{
				//重心取得
				Center() = primitive.GetCenter();
								
				//半径取得
				Radius() = 0;
				for (auto it = primitive.Vertices().begin();
					it != primitive.Vertices().end();
					++it)
				{
					float radius = (it->Position() - Center()).Length();

					if(radius > Radius())
						Radius() = radius;
				}

				return *this;
			};

			//!	CModelから設定する
			template<template<class> class Allocator>
			static		CBoundingSphere SetByCModel(
				const Graphic::Model::CModel<Allocator>& model)
			{
				Center() = CVector(0, 0, 0);
				Radius() = 0;

				for (const Graphic::Model::CMesh<Allocator>& mesh : model.GetMeshes())
				{
					CBoundingSphere temp = CreateByCVertices(mesh);
					uint_t boneIndex = mesh.GetBoneIndex();
					if (boneIndex != -1)
					{
						temp = temp.GetTransformed(model.GetBones()[boneIndex].GetWorldMatrix(model.GetBones()));
					}

					Combine(temp);
				}

				return *this;
			};

#pragma endregion

			//!	指定したCBoundingSphereを自身に取り込む
			CBoundingSphere& Combine(const CBoundingSphere& other)
			{
				auto center = GetCenter();
				Center() += other.GetCenter();
				Center() /= 2;

				Radius() = GetRadius() + other.GetLengthFromPoint(center) + other.GetRadius();
				Radius() /= 2;
				
				return *this;
			}
		};
	};
};

#endif