/*!	\file		COrientedBoundingBox.h
*	\details	COrientedBoundingBoxクラスの定義
*	\author		松裏征志
*	\date		2014/12/04	作成
*/
#ifndef MMLIBRARY_CORIENTED_BOUNDING_BOX_H_
#define MMLIBRARY_CORIENTED_BOUNDING_BOX_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Collision\ICollider.h"
#include "Graphic\CTransform.h"
#include "Utility\DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Collision
	{
		class CAxisAlignedBoundingBox;
		class CBoundingSphere;

		//!	有向境界ボックスによる当たり判定クラス
		class COrientedBoundingBox : public ICollider
		{
		private:
			Graphic::CTransform m_transform;

		public:
			//!	コンストラクタ
			COrientedBoundingBox(void);

			//!	コンストラクタ
			COrientedBoundingBox(const CAxisAlignedBoundingBox&);

			CREATE_TYPE_SPECIFIED_REFERRENCE(Position, m_transform.Position(), Graphic::CVector3);
			CREATE_TYPE_SPECIFIED_CONST_REFERRENCE(Position, m_transform.Position(), Graphic::CVector3);

			CREATE_TYPE_SPECIFIED_REFERRENCE(Length, m_transform.Scale(), Graphic::CVector3);
			CREATE_TYPE_SPECIFIED_CONST_REFERRENCE(Length, m_transform.Scale(), Graphic::CVector3);

			CREATE_TYPE_SPECIFIED_REFERRENCE(Rotation, m_transform.Rotation(), Math::CQuaternion<float>);
			CREATE_TYPE_SPECIFIED_CONST_REFERRENCE(Rotation, m_transform.Rotation(), Math::CQuaternion<float>);

			//!	描画
			void Render(
				const Graphic::CMatrix4x4& matrix) const override;

			//!	指定した座標からの距離を求める
			float GetLengthFromPoint(const Graphic::CVector3& point) const;

#pragma region IsHit
			//!	各コライダーとの当たり判定

			bool IsHit(
				const ICollider&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

			bool IsHit(
				const CAxisAlignedBoundingBox&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

			bool IsHit(
				const CBoundingSphere&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override;

			bool IsHit(
				const COrientedBoundingBox&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const ;

#pragma endregion

		};
	}
}

#include "Utility\UndefineClassCreateHelpers.h"
#endif	//	MMLIBRARY_CORIENTED_BOUNDING_BOX_H_