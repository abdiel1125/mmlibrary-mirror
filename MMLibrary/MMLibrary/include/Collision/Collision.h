/*!	\file		Collision.h
*	\details	Collisionクラスの定義
*	\author		松裏征志
*	\date		2014/09/05
*/
#ifndef MMLIBRARY_COLLISION_H_
#define MMLIBRARY_COLLISION_H_

namespace MMLibrary
{
	//!	MMLibraryの当たり判定に関わる要素はこの名前空間に属する
	namespace Collision
	{
	};
};
//***********************************************************
//インクルード
//***********************************************************
#include "Collision\ICollider.h"
#include "Collision\CAxisAlignedBoundingBox.h"
#include "Collision\COrientedBoundingBox.h"
#include "Collision\CBoundingSphere.h"
#include "Collision\CColliderBinder.h"

#endif