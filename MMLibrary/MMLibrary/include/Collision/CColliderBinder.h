/*!	\file		CColliderBinder.h
*	\details	CColliderBinderクラスの定義
*	\author		松裏征志
*	\date		2014/11/11	作成
*/
#ifndef MMLIBRARY_CCOLLISION_BINDER_H_
#define MMLIBRARY_CCOLLISION_BINDER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Collision\ICollider.h"

namespace MMLibrary
{
	namespace Collision
	{
		class CAxisAlignedBoundingBox;
		class CBoundingSphere;

		//! 2つのコリジョンクラスを1つにまとめるクラスの基底クラス
		class CColliderBinderBase : public ICollider
		{
		public:
			//!	コリジョンクラスのポインタとその相対的な座標変換行列の組
			using pairType = std::pair < ICollider*, Graphic::CMatrix4x4 > ;

			//!	コンストラクタ
			CColliderBinderBase(std::initializer_list<pairType> args)
				:m_collisions(args.size())
			{
				auto it = args.begin();
				for (auto& collision : m_collisions)
				{
					collision = *it;
					++it;
				}
			};

			//!	描画
			virtual		void Render(
				const Graphic::CMatrix4x4& matrix) const override
			{
				for (const auto& collision : m_collisions)
				{
					collision.first->Render(matrix);
				}
			};

		protected:
			//!	登録されたコリジョンクラスについての情報の集合
			std::vector< pairType > m_collisions;
		};

		//!	2つのコリジョンクラスを1つにまとめるクラス(AND)
		class CColliderAndBinder : public CColliderBinderBase
		{
		public:
			//!	コンストラクタ
			CColliderAndBinder(std::initializer_list<pairType> args)
				:CColliderBinderBase(args)
			{};

#pragma region IsHit
			//!	衝突判定
			//!	@{

			virtual		bool IsHit(
				const ICollider&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override
			{
				return	IsHitImpl(another, thisMatrix, anotherMatrix);
			};

			virtual		bool IsHit(
				const CAxisAlignedBoundingBox&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override
			{
				return	IsHitImpl(another, thisMatrix, anotherMatrix);
			};

			virtual		bool IsHit(
				const CBoundingSphere&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override
			{
				return	IsHitImpl(another, thisMatrix, anotherMatrix);
			};
			//!	@}
#pragma endregion

			private:
				template<class T>
				bool IsHitImpl(
					const T&	another,
					const Graphic::CMatrix4x4& thisMatrix,
					const Graphic::CMatrix4x4& anotherMatrix) const
				{
					bool rtn = true;

					for (const auto& collision : m_collisions)
					{
						rtn = rtn && collision.first->IsHit(another, thisMatrix * collision.second, anotherMatrix);
					}

					return	rtn;
				};
		};

		//!	2つのコリジョンクラスを1つにまとめるクラス(OR)
		class CColliderOrBinder : public CColliderBinderBase
		{
		public:
			//!	コンストラクタ
			CColliderOrBinder(std::initializer_list<pairType> args)
				:CColliderBinderBase(args)
			{};

#pragma region IsHit
			//!	衝突判定
			//!	@{

			virtual		bool IsHit(
				const ICollider&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override
			{
				return	IsHitImpl(another, thisMatrix, anotherMatrix);
			};

			virtual		bool IsHit(
				const CAxisAlignedBoundingBox&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override
			{
				return	IsHitImpl(another, thisMatrix, anotherMatrix);
			};

			virtual		bool IsHit(
				const CBoundingSphere&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const override
			{
				return	IsHitImpl(another, thisMatrix, anotherMatrix);
			};
			//!	@}
#pragma endregion

		private:
			template<class T>
			bool IsHitImpl(
				const T&	another,
				const Graphic::CMatrix4x4& thisMatrix,
				const Graphic::CMatrix4x4& anotherMatrix) const
			{
				bool rtn = false;

				for (const auto& collision : m_collisions)
				{
					rtn = rtn || collision.first->IsHit(another, thisMatrix * collision.second, anotherMatrix);
				}

				return	rtn;
			};
		};

	};
};

#endif	//	MMLIBRARY_CCOLLISION_BINDER_H_