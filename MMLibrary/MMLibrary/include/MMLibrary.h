/*!	\file		MMLibrary.h
	\details	MMLibraryの全てのヘッダをインクルードする
	\author		松裏征志
	\date		2014/04/16
*/
#ifndef MMLIBRARY_H_
#define MMLIBRARY_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility\Macro.h"
#include "Math\Math.h"
#include "Collision\Collision.h"
#include "Memory\Memory.h"

#ifdef USING_GRAPHIC
#	include "Graphic\Graphic.h"
#endif

#endif

/*!	\mainpage
*	
*	これはテンプレートクラスを使用した個人制作ライブラリです。\n
*	\n
*	このライブラリのグラフィック機能はWindows向けです。\n
*	グラフィック機能を使う際はUSING_GRAPHICをdefineし以下のどちらかをdefineしてください。\n
*	OpenGLを使用する場合はUSING_OPENGLをdefineしてください。\n
*	DirectX9を使用する場合はUSING_DIRECTXをdefineしてください。
*	C++11以降の機能を利用しているのでVS2013以降で利用してください
*/

/*!	\namespace	MMLibrary
*	\brief	MMLibraryのすべての要素はこの名前空間に属する
*/
namespace MMLibrary
{
};