/*!	\file		CGraphicManager.h
*	\details	CGraphicManagerクラスの宣言
*	\author		松裏征志
*	\date		2014/05/14
*/
#ifndef MMLIBRARY_CGRAPHICMANAGER_H_
#define MMLIBRARY_CGRAPHICMANAGER_H_

//***********************************************************
//インクルード
//***********************************************************
#include	<memory>
#include	"MMLibraryDefine.h"
#include	"Graphic\CCamera.h"
#include	"Graphic\Color\Color.h"
#include	"Graphic\CGLFunctions.h"
#include	"Graphic\DirectXHelper.h"
#include	"Graphic\CBuffer.h"
#include	"Utility\tagNonCopyable.h"
#include	"Utility\DefineClassCreateHelpers.h"

namespace	MMLibrary
{
	namespace Graphic
	{
		//!	描画のための処理を提供するクラス
		class CGraphicManager : private Utility::tagNonCopyable
		{
			friend class CWindow;
#ifdef	USING_DIRECTX
		private:
			//DIRECT3D9オブジェクト
			LPDIRECT3D9					m_pD3D;		
			// プレゼンテーションパラメータ
			D3DPRESENT_PARAMETERS		m_D3Dpp;			
			// ディスプレイモード
			D3DDISPLAYMODE				m_disp;				
			// ディスプレイアダプタ番号
			int							m_adapter;
			//DIRECT3DDEVICE9オブジェクト
			LPDIRECT3DDEVICE9			m_pD3DDevice;
			//D3DXMATRIXSTACKオブジェクト
			LPD3DXMATRIXSTACK			m_pD3DMatStack;

		public:
			using device_t = LPDIRECT3DDEVICE9;
#endif
#ifdef USING_OPENGL
		private:
			HDC		m_device;
			HGLRC	m_context;
		public:
			using device_t = HDC&;
#endif // USING_OPENGL

		private:
			CCamera				m_camera;
			CMatrix4x4			m_projectionMatrix;
			CMatrix4x4			m_viewMatrix;
			CColor32			m_clearColor;
			CULLING_MODE		m_cullingMode;
			CWindow*			m_pWindow;

static		CGraphicManager*	m_pCurrentGraphicManager;

			//!	コンストラクタ
			CGraphicManager(void);

			//!	投影変換行列をデバイスに設定する
			void SetProjectionMatrixToDevice(const CMatrix4x4& matrix);

			//!	ビュー変換行列をデバイスに設定する
			void SetViewMatrixToDevice(const CMatrix4x4& matrix);

		public:
			//!	スタック関連処理をまとめたCGraphicManagerの内部クラス
			class CMatrixStack
			{
			public:
				//!	現在の行列を取得する
				const CMatrix4x4 GetTop(void) const;

				//!	現在の行列に単位行列を設定する
				CMatrixStack&	LoadIdentity(void);

				//!	現在の行列に指定した行列を設定する
				CMatrixStack&	LoadMatrix(
					const CMatrix4x4& src);
				//!	現在の行列に指定した行列を掛け合わせる
				CMatrixStack&	Multiply(
					const CMatrix4x4& src);

				//!	現在の行列に指定した行列を逆順に掛け合わせる
				CMatrixStack&	MultiplyLocal(
					const CMatrix4x4& src);

				//!	現在の行列をコピーした行列をポップアップする
				CMatrixStack&	Pop(void);

				//!	現在の行列をプッシュダウンする
				CMatrixStack&	Push(void);
			};
									
			//!	CMatrixStackのメソッドへのアクセッサ
			CMatrixStack	Stack;
			
			//!	デストラクタ
			~CGraphicManager(void);

			//!	初期化処理
			bool	Init(CWindow& window);

			//!	インスタンスへの参照
			static CGraphicManager& GetManager(void);
			
			//!	デバイスを取得する
			device_t GetDevice(void);

			//!	終了処理
			void	Destroy(void);
			
			//!	描画開始処理
			void	RenderBegin(void);
			
			//!	描画終了処理
			void	RenderEnd(void);

			//!	カメラ
			CCamera& Camera(void);

			//! 3Dでの描画を準備する
			void	RenderBegin3D(void);

			//! 2Dでの描画を準備する
			void	RenderBegin2D(void);

			//! スクリーン座標系での描画を準備する
			void	RenderBeginScreenSpace(void);

			//!	描画ターゲットバッファを初期化する
			void	ClearTargetBuffer(void);

			//!	深度バッファを初期化する
			void	ClearDepthBuffer(void);
#pragma region Setter

			//!	スタックトップをワールド座標変換行列に設定
			void SetWorldMatrix(void);

			//!	テクスチャを設定する
			void SetTexture(textureHandle_t textureHandle);

			//!	背景色を設定する
			void SetClearColor(CColor32& color);

			//!	カリングモードを設定する
			void SetCullingMode(CULLING_MODE mode);

			//!	ライティング計算をするか否かを設定する
			void IsLighting(bool isLighting);
			
			//!	Zバッファに書き込むか否かを設定する
			void IsWriteToZBuffer(bool isWriteToZBuffer);

			//!	ビューポートを設定する
			void SetViewPort(const viewport_t& viewport);

			//!	投影変換行列を設定する
			void SetProjectionMatrix(const CMatrix4x4& matrix);

			//!	ビュー変換行列を設定する
			void SetViewMatrix(const CMatrix4x4& matrix);

			//!	アルファブレンドをするか否かを設定する
			void IsAlphaBlending(
				bool isAlphaBlending);

			//!	アルファブレンドの方法を設定する
			void SetAlphaBlendingState(
				BLEND_INTEGER srcFactor,
				BLEND_INTEGER dstFactor);

			//!	アルファブレンドモードを設定する
			void SetTextureBlendingState(
				TEXTURE_BLEND_MODE blendMode);

#pragma endregion

#pragma region Getter

			//!	ライティング計算をするか否かを取得する
			bool IsLighting(void);

			//!	Zバッファに書き込むか否かを取得する
			bool IsWriteToZBuffer(void);

			//!	対応するウィンドウを取得する
			CREATE_TYPE_SPECIFIED_CONST_REFERRENCE(Window, *m_pWindow, CWindow);

			//!	投影変換行列を取得する
			CREATE_GETTER(ProjectionMatrix, m_projectionMatrix);

			//!	ビュー変換行列を取得する
			CREATE_GETTER(ViewMatrix, m_viewMatrix);

#pragma endregion
		};
	};
};

#include	"Utility\UndefineClassCreateHelpers.h"
#endif