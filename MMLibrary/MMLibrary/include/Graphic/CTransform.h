/*!	\file		CTransform.h
*	\details	CTransformクラスの宣言
*	\author		松裏征志
*	\date		2014/09/17
*/
#ifndef MMLIBRARY_CTRANSFORM_H_
#define MMLIBRARY_CTRANSFORM_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CVector3.h"
#include "Math\CQuaternion.h"
#include "Graphic\CMatrix4x4.h"
#include "Utility\tagTreeNodeBase.h"
#include "Utility\DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	3次元空間における位置・拡縮・回転を表すクラス
		class CTransform : public Utility::tagTreeNodeBase<CTransform>
		{
		private:
			CVector3					m_position;
			CVector3					m_scale;
			Math::CQuaternion<float>	m_rotation;

		public:
			//!	コンストラクタ
			CTransform(void)
				: m_position()
				, m_scale({ 1, 1, 1 })
				, m_rotation()
				, tagTreeNodeBase(this)
			{};

			//!	引数付きコンストラクタ
			CTransform(
				const CVector3&					position,
				const CVector3&					scale,
				const Math::CQuaternion<float>&	rotation);

			CREATE_REFERRENCE(Position, m_position);
			CREATE_REFERRENCE(Scale, m_scale);
			CREATE_REFERRENCE(Rotation, m_rotation);

			CREATE_CONST_REFERRENCE(Position, m_position);
			CREATE_CONST_REFERRENCE(Scale, m_scale);
			CREATE_CONST_REFERRENCE(Rotation, m_rotation);

			//!	指定位置への移動
			CTransform&
				MoveTo(
				const float x,
				const float y,
				const float z);
			//!	World座標系での移動
			CTransform&
				Move(
				const float x,
				const float y,
				const float z);
			//!	Local座標系での移動
			CTransform&
				MoveLocal(
				const float x,
				const float y,
				const float z);
			//!	指定位置への移動
			CTransform&
				MoveTo(
				const CVector3& position);
			//!	World座標系での移動
			CTransform&
				Move(
				const CVector3& position);
			//!	Local座標系での移動
			CTransform&
				MoveLocal(
				const CVector3& position);

			//!	拡縮の設定
			CTransform&
				ScalingTo(
				const float x,
				const float y,
				const float z);
			//!	拡縮の設定
			CTransform&
				ScalingTo(
				const CVector3& scale);
			//!	World座標系での拡縮
			CTransform&
				Scaling(
				const float x,
				const float y,
				const float z);
			//!	World座標系での拡縮
			CTransform&
				Scaling(
				const float scale);
			//!	World座標系での拡縮
			CTransform&
				Scaling(
				const CVector3& scale);
			//!	Local座標系での拡縮
			CTransform&
				ScalingLocal(
				const float x,
				const float y,
				const float z);
			//!	Local座標系での拡縮
			CTransform&
				ScalingLocal(
				const CVector3& scale);

			//!	回転の設定
			CTransform&
				RotateTo(
				const Math::CQuaternion<float>& rotation);
			//!	World座標系での回転
			CTransform&
				Rotate(
				const Math::CQuaternion<float>& rotation);
			//!	Local座標系での回転
			CTransform&
				RotateLocal(
				const Math::CQuaternion<float>& rotation);
			//!	向きの設定
			CTransform&
				SetDirection(
				const CVector3& direction);
			//!	向きの取得
			CVector3
				GetDirection(void) const;
			//!	指定座標を向く
			CTransform&
				LookAt(
				const CVector3& targetPos,
				float			angleRegulation);

			//!	親からの相対的な座標変換行列の取得
			CMatrix4x4 GetMatrix(void) const;

			//!	LocalToWorld座標変換行列の取得
			CMatrix4x4 GetLocalToWorldMatrix(void) const;

			//!	WorldToLocal座標変換行列の取得
			CMatrix4x4 GetWorldToLocalMatrix(void) const;

			//!	線形補間による作成
			static CTransform CreateByLarp(
				const CTransform&	matrix1,
				const CTransform&	matrix2,
				const float			ratio);

		};
	};
};

#include "Utility\UndefineClassCreateHelpers.h"
#endif	//MMLIBRARY_CTRANSFORM_H_
