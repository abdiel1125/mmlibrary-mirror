/*!	\file		CMatrix4x4.h
*	\details	CMatrix4x4クラスの宣言
*	\author		松裏征志
*	\date		2014/04/24
*/
#ifndef MMLIBRARY_CMATRIX_4x4_H_
#define MMLIBRARY_CMATRIX_4x4_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Math\CMatrix.h"
#include "Graphic\Other.h"
#include "Graphic/CVector3.h"
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Math
	{
		template<typename T>
		class CQuaternion;
	};
	namespace Graphic
	{
		//!	4x4行列を表現するクラス
		/*!
		*	CMatrix<float, 4, 4>を継承する。
		*/
		class CMatrix4x4 :
			public
			Utility::tagEqualComparable<CMatrix4x4,
			Utility::tagCanAdditionAssignment<CMatrix4x4,
			Utility::tagCanSubtractionAssignment<CMatrix4x4,
			Utility::tagCanMultiplicationAssignment2nd<CMatrix4x4, float,
			Utility::tagCanDivisionAssignment2nd<CMatrix4x4, float,
			Math::CMatrix<float, 4, 4 > > > > > >
		{
		public:
			using myType = CMatrix4x4;
			using myBase = Math::CMatrix<float, 4, 4>;
			using T = float;
#pragma region BaseClassMethod
			//!	加算代入演算子
			friend myType& operator+=(myType& left, const myType& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) +=
					static_cast<const myBase&>(right));
			};

			//! 減算代入演算子
			friend myType& operator-=(myType& left, const myType& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) -=
					static_cast<const myBase&>(right));
			};

			//!	乗算代入演算子
			friend myType& operator*=(myType& left, const T& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) *=
					static_cast<const T&>(right));
			};

			//!	除算代入演算子
			friend myType& operator/=(myType& left, const T& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) /=
					static_cast<const T&>(right));
			};

			//!	等価演算子
			friend bool operator==(const myType& left, const myType& right)
			{
				return
					static_cast<const myBase&>(left) ==
					static_cast<const myBase&>(right);
			};
#pragma endregion

			//!	コンストラクタ
			/*!
			*	単位行列で初期化する
			*/
			CMatrix4x4(void);

			//!	引数付コンストラクタ
			/*!	
			*	与えられた引数で初期化する
			*/
			CMatrix4x4(
				float _00, float _01, float _02, float _03,	
				float _10, float _11, float _12, float _13,
				float _20, float _21, float _22, float _23,
				float _30, float _31, float _32, float _33);

			//!	引数付コンストラクタ
			/*!
			*	与えられた引数で全要素を初期化する
			*/
			CMatrix4x4(float src);

			//!	コピーコンストラクタ
			/*!
			*	MMLibrary::Math::CMatrix<float, 4, 4>からのコピーに対応する
			*/
			CMatrix4x4(const Math::CMatrix<float, 4, 4>& src);

			//!	代入演算子
			/*!
			*	\param	[in] src 演算に用いる行列
			*
			*	\return	演算後の自身への参照を返す
			*/
			CMatrix4x4& operator=(const Math::CMatrix<float, 4, 4>& src);

			//!	自身のコピーを返す。
			/*!
			*	\return	自身のコピー
			*/
			CMatrix4x4					ToClone(void) const;

			//!	位置を表す部分への参照を取得する
			/*!
			*	\return	位置を表す部分への参照
			*/
			CVector3&					Position(void);

			//!	位置を表す部分を取得する
			/*!
			*	\return	位置を表す部分
			*/
			CVector3					GetPosition(void) const;

			CVector3					GetScale(void) const;

			Math::CQuaternion<float>	GetRotation(void) const;

			//!	ベクトルとして指定の行を取得する
			CVector3& AsVector3(size_t index);
			const CVector3& AsVector3(size_t index) const;

			//!	ベクトルとして指定の行を取得する
			Math::CVector<float, 4>& AsVector4(size_t index);
			const Math::CVector<float, 4>& AsVector4(size_t index) const;

			/*!	
			*	\brief	アフィン変換行列を作成する。
			*	
			*	\param	[in]	scaling			各軸方向についてのスケーリング係数を表す、	CVector3クラスへの参照
			*	\param	[in]	rotationCenter	回転の中心座標を表す、						CVector3クラスへの参照
			*	\param	[in]	rotationMatrix	回転行列を表す、							CMatrix4x4クラスへの参照
			*	\param	[in]	translation		平行移動を表す、							CVector3クラスへの参照
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使ってアフィン変換行列を計算する。
			*	Ms * (Mrc)^-1 * Mr * Mrc * Mt。
			*	ここで、
			*	Ms はスケーリング行列、
			*	Mrc は回転行列の中心、
			*	Mr は回転行列、そして 
			*	Mt は平行移動行列である。
			*/
			static CMatrix4x4 CreateByAffineTransformation(
				const CVector3&	scaling,
				const CVector3&	rotationCenter,
				const CMatrix4x4&	rotationMatrix,
				const CVector3&	translation);

			/*!	
			*	\brief	左手座標系ビュー行列を作成する。
			*	
			*	\param	[in]	eye	視点を定義する、										CVector3への参照
			*	\param	[in]	at	カメラの注視対象を定義する、							CVector3への参照
			*	\param	[in]	up	カレント ワールドの上方、一般には [0, 1, 0] を定義する、CVector3への参照
			*
			*	\return	演算結果を返す
			*
			*	次の式を使ってビュー行列を計算する。\n
			*	zAxis = (at - eye).Normalize()		\n
			*	xAxis = up.Cross(zAxis).Normalize()	\n
			*	yAxis = zAxis.Cross(xAxis)			\n
			*										\n
			*	|  xAxis.x			yAxis.x			zAxis.x         0 |\n
			*	|  xAxis.y			yAxis.y			zAxis.y         0 |\n
			*	|  xAxis.z			yAxis.z			zAxis.z         0 |\n
			*	| -xAxis.Dot(eye)  -yAxis.Dot(eye) -zAxis.Dot(eye)  1 |\n
			*/
			static CMatrix4x4 CreateByLookAtLH(
				const CVector3&	eye,
				const CVector3&	at,
				const CVector3&	up);

			/*!	
			*	\brief	右手座標系ビュー行列を作成する。
			*	
			*	\param	[in]	eye	視点を定義する、										CVector3への参照
			*	\param	[in]	at	カメラの注視対象を定義する、							CVector3への参照
			*	\param	[in]	up	カレント ワールドの上方、一般には [0, 1, 0] を定義する、CVector3への参照
			*
			*	\return	演算結果を返す
			*
			*	次の式を使ってビュー行列を計算する。\n
			*	zAxis = (eye - at).Normalize()		\n
			*	xAxis = up.Cross(zAxis).Normalize()	\n
			*	yAxis = zAxis.Cross(xAxis)			\n
			*										\n
			*	|  xAxis.x			yAxis.x			zAxis.x         0 |\n
			*	|  xAxis.y			yAxis.y			zAxis.y         0 |\n
			*	|  xAxis.z			yAxis.z			zAxis.z         0 |\n
			*	| -xAxis.Dot(eye)  -yAxis.Dot(eye) -zAxis.Dot(eye)  1 |\n
			*/
			static CMatrix4x4 CreateByLookAtRH(
				const CVector3&	eye,
				const CVector3&	at,
				const CVector3&	up);

			/*!	
			*	\brief	左手座標系正射影行列を作成する。
			*
			*	\param	[in]	width	ビューボリュームの幅
			*	\param	[in]	height	ビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの最小Z値
			*	\param	[in]	farZ	ビューボリュームの最大Z値
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	| 2 / w    0                0           0 |\n
			*	|   0    2 / h              0           0 |\n
			*	|   0      0        1 / (farZ - nearZ)  0 |\n
			*	|   0      0    nearZ / (nearZ - farZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	OrthoLH 関数のパラメータはすべて、カメラ空間の距離である。
			*	このパラメータは、ビュー ボリュームのディメンジョンを記述する。
			*/
			static CMatrix4x4 CreateByOrthoLH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	右手座標系正射影行列を作成する。
			*
			*	\param	[in]	width	ビューボリュームの幅
			*	\param	[in]	height	ビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの最小Z値
			*	\param	[in]	farZ	ビューボリュームの最大Z値
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って右手座標系正射影行列を計算する。	\n
			*													\n
			*	| 2 / w    0                0           0 |\n
			*	|   0    2 / h              0           0 |\n
			*	|   0      0        1 / (nearZ - farZ)  0 |\n
			*	|   0      0    nearZ / (nearZ - farZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	OrthoLH 関数のパラメータはすべて、カメラ空間の距離である。
			*	このパラメータは、ビュー ボリュームのディメンジョンを記述する。
			*/
			static CMatrix4x4 CreateByOrthoRH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	カスタマイズした左手座標系正射影行列を作成する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	|          2 / (maxX - minX)                   0                          0           0 |\n
			*	|            0		                         2 / (maxY - minY)            0           0 |\n
			*	|            0             	                   0                   1 / (maxZ - minZ)  0 |\n
			*	| (minX + maxX) / (minX - maxX)  (maxY + minY) / (minY - maxY)  minZ / (minZ - maxZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	OrthoOffCenterLH を使って同じ射影を作成するには、次の値を使う\n
			*	minX = -w/2、maxX = w/2、minY = -h/2、maxY = h/2。
			*/
			static CMatrix4x4 CreateByOrthoOffCenterLH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	カスタマイズした左手座標系正射影行列を作成する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	|          2 / (maxX - minX)                   0                          0           0 |\n
			*	|            0		                         2 / (maxY - minY)            0           0 |\n
			*	|            0             	                   0                   1 / (minZ - maxZ)  0 |\n
			*	| (minX + maxX) / (minX - maxX)  (maxY + minY) / (minY - maxY)  minZ / (minZ - maxZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	OrthoOffCenterLH を使って同じ射影を作成するには、次の値を使う\n
			*	minX = -w/2、maxX = w/2、minY = -h/2、maxY = h/2。
			*/
			static CMatrix4x4 CreateByOrthoOffCenterRH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	左手座標系パースペクティブ射影行列を作成する。
			*
			*	\param	[in]	width	近くのビュー平面でのビューボリュームの幅
			*	\param	[in]	height	近くのビュー平面でのビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って左手座標系パースペクティブ射影行列を計算する。	\n
			*	| 2 * nearZ / width            0                      0                 0 |\n
			*	|     0		         2 * nearZ / height               0                 0 |\n
			*	|     0                        0	         1 * farZ / (farZ - nearZ)  0 |\n
			*	|     0                        0         nearZ * farZ / (nearZ - farZ)  1 |\n
			*/
			static CMatrix4x4 CreateByPerspectiveLH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	右手座標系パースペクティブ射影行列を作成する。
			*
			*	\param	[in]	width	近くのビュー平面でのビューボリュームの幅
			*	\param	[in]	height	近くのビュー平面でのビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って右手座標系パースペクティブ射影行列を計算する。	\n
			*	| 2 * nearZ / width            0                      0                 0 |\n
			*	|     0		         2 * nearZ / height               0                 0 |\n
			*	|     0                        0	         1 * farZ / (nearZ - farZ)  0 |\n
			*	|     0                        0         nearZ * farZ / (nearZ - farZ)  1 |\n
			*/
			static CMatrix4x4 CreateByPerspectiveRH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	視野に基づいて、左手座標系パースペクティブ射影行列を作成する。
			*
			*	\param	[in]	fovY	Y方向の視野角
			*	\param	[in]	aspect	画面のアスペクト比
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って左手座標系パースペクティブ射影行列を計算する。	\n
			*	height	= nearZ * tan(fovY / 2.0f) * 2.0f		\n
			*	width	= height * aspect						\n
			*													\n
			*	| 2 / width       0                  0           0 |\n
			*	|     0		 2 / height              0           0 |\n
			*	|     0           0	         1 / (farZ - nearZ)  0 |\n
			*	|     0           0      nearZ / (nearZ - farZ)  1 |\n
			*/
			static CMatrix4x4 CreateByPerspectiveFovLH(
				const float	fovY,
				const float	aspect,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	視野に基づいて、右手座標系パースペクティブ射影行列を作成する。
			*
			*	\param	[in]	fovY	Y方向の視野角
			*	\param	[in]	aspect	画面のアスペクト比
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って右手座標系パースペクティブ射影行列を計算する。	\n
			*	height	= nearZ * tan(fovY / 2.0f) * 2.0f		\n
			*	width	= height * aspect						\n
			*													\n
			*	| 2 / width       0                  0           0 |\n
			*	|     0		 2 / height              0           0 |\n
			*	|     0           0	         1 / (nearZ - farZ)  0 |\n
			*	|     0           0      nearZ / (nearZ - farZ)  1 |\n
			*/
			static CMatrix4x4 CreateByPerspectiveFovRH(
				const float	fovY,
				const float	aspect,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	カスタマイズした左手座標系パースペクティブ射影行列を作成する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	|      2 * minZ / (maxX - minX)                0                            0                0 |\n
			*	|               0		             2 * minZ  / (maxY - minY)              0                0 |\n
			*	| (minX + maxX) / (minX - maxX)  (maxY + minY) / (minY - maxY)	   1 * maxZ / (maxZ - minZ)  0 |\n
			*	|               0                              0                minZ * maxZ / (minZ - maxZ)  1 |\n
			*	
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	
			*/
			static CMatrix4x4 CreateByPerspectiveOffCenterLH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	カスタマイズした右手座標系パースペクティブ射影行列を作成する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使って右手座標系正射影行列を計算する。	\n
			*													\n
			*	|      2 * minZ / (maxX - minX)                0                            0                0 |\n
			*	|               0		             2 * minZ  / (maxY - minY)              0                0 |\n
			*	| (minX + maxX) / (maxX - minX)  (maxY + minY) / (maxY - minY)	   1 * maxZ / (minZ - maxZ)  0 |\n
			*	|               0                              0                minZ * maxZ / (minZ - maxZ)  1 |\n
			*	
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	
			*/
			static CMatrix4x4 CreateByPerspectiveOffCenterRH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	ビューポート行列を作成する。
			*
			*	\param	[in]	viewport	演算に使用するviewport_t構造体への参照
			*	
			*	\return	演算結果を返す
			*
			*	次の式を使ってビューポート行列を計算する。\n
			*	| width / 2            0              0       0 |\n
			*	|     0		       - height / 2       0       0 |\n
			*	|     0                0         maxZ - MinZ  0 |\n
			*	| x + width / 2  y + height / 2      MinZ     1 |\n
			*	
			*	この行列は、ビューポートのサイズおよび指定の深度範囲に従って頂点をスケーリングし、\n
			*	その頂点をレンダーターゲットサーフェスの適切な位置に平行移動します。\n
			*	また、この行列は、y 座標を反転して、y が下方に増加する左上角のスクリーン原点を反映します。
			*/
			static CMatrix4x4 CreateByViewPort(
				const viewport_t& viewport);

			/*!	
			*	\brief	オフセットを指定して平行移動行列を作成する。
			*	
			*	\param	[in]	translation		平行移動を表す、CVector3クラスへの参照
			*
			*	\return	演算結果を返す
			*
			*	次の式を使って平行移動行列を計算する。\n
			*	| 1  0  0  0 |\n
			*	| 0	 1  0  0 |\n
			*	| 0  0  1  0 |\n
			*	| x  y  z  1 |\n
			*/
			static CMatrix4x4 CreateByTranslation(
				const CVector3&	translation);

			/*!	
			*	\brief	x 軸、y 軸、z 軸に沿ってスケーリングする行列を作成する。
			*	
			*	\param	[in]	scaling	各軸方向についてのスケーリング係数を表す、CVector3クラスへの参照
			*
			*	\return	演算結果を返す
			*
			*	次の式を使ってスケール行列を計算する。\n
			*	| Sx  0   0   0 |\n
			*	| 0   Sy  0   0 |\n
			*	| 0   0   Sz  0 |\n
			*	| 0   0   0   1 |\n
			*/
			static CMatrix4x4 CreateByScaling(
				const CVector3&	scaling);

			/*!	
			*	\brief	任意の軸を回転軸にして回転行列を作成する。
			*	
			*	\param	[in]	axis	軸の角度を識別する、CVector3クラスへの参照
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算結果を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*			|     0   -axis.z  axis.y |\n
			*	R =		|  axis.z     0   -axis.x |\n
			*			| -axis.y  axis.x    0    |\n
			*														\n
			*	M = I + sin(radian) * R + (1 - cos(radian)) * R * R	\n
			*														\n
			*	|       	    0 |\n
			*	|      M        0 |\n
			*	|               0 |\n
			*	| 0    0    0   1 |\n
			*/
			static CMatrix4x4 CreateByRotationAxis(
				const CVector3&	axis,
				const float					radian);

			/*!
			*	\brief	クォータニオンから回転行列を作成する。
			*
			*	\param	[in]	quaternion	処理の基になるCQuaternionへの参照
			*
			*	\return	演算結果を返す
			*/
			static CMatrix4x4 CreateByRotationQuaternion(
				const Math::CQuaternion<float>&	quaternion);

			/*!	
			*	\brief	x 軸についての回転行列を作成する。
			*	
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算結果を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*	| 1    0	0   0 |\n
			*	| 0   cos  sin  0 |\n
			*	| 0  -sin  cos  0 |\n
			*	| 0    0    0   1 |\n
			*/
			static CMatrix4x4 CreateByRotationX(
				const float	radian);

			/*!	
			*	\brief	y 軸についての回転行列を作成する。
			*	
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算結果を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*	| cos  0 -sin  0 |\n
			*	| 0    1   0   0 |\n
			*	| sin  0  cos  0 |\n
			*	| 0    0   0   1 |\n
			*/
			static CMatrix4x4 CreateByRotationY(
				const float	radian);

			/*!	
			*	\brief	z 軸についての回転行列を作成する。
			*	
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算結果を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*	|  cos  sin  0  0 |\n
			*	| -sin  cos  0  0 |\n
			*	|   0    0   1  0 |\n
			*	|   0    0   0  1 |\n
			*/
			static CMatrix4x4 CreateByRotationZ(
				const float	radian);

			/*!	
			*	\brief	ヨー、ピッチ、およびロールを指定して行列を作成する。
			*	
			*	\param	[in]	x	演算に使用するX軸のラジアン角度(ピッチ)
			*	\param	[in]	y	演算に使用するY軸のラジアン角度(ヨー)
			*	\param	[in]	z	演算に使用するZ軸のラジアン角度(ロール)
			*
			*	\return	演算結果を返す
			*
			*	トランスフォームの順序は、最初にロール、次にピッチ、最後にヨーである。\n
			*	これは、オブジェクトのローカル座標軸を基準として、z 軸の周囲での回転、
			*	x 軸の周囲での回転、y 軸の周囲での回転と同じになる。
			*/
			static CMatrix4x4 CreateByRotationYawPitchRoll(
				const float	x,
				const float	y,
				const float	z);

			/*!	
			*	\brief	ヨー、ピッチ、およびロールを指定して行列を作成する。
			*	
			*	\param	[in]	rotation	演算に使用する軸ごとのラジアン角度
			*
			*	\return	演算結果を返す
			*
			*	トランスフォームの順序は、最初にロール、次にピッチ、最後にヨーである。\n
			*	これは、オブジェクトのローカル座標軸を基準として、z 軸の周囲での回転、
			*	x 軸の周囲での回転、y 軸の周囲での回転と同じになる。
			*/
			static CMatrix4x4 CreateByRotationYawPitchRoll(
				const CVector3&	rotation);

			/*!	
			*	\brief	アフィン変換行列を作成し、自身に設定する。
			*	
			*	\param	[in]	scaling			各軸方向についてのスケーリング係数を表す、	CVector3クラスへの参照
			*	\param	[in]	rotationCenter	回転の中心座標を表す、						CVector3クラスへの参照
			*	\param	[in]	rotationMatrix	回転行列を表す、							CMatrix4x4クラスへの参照
			*	\param	[in]	translation		平行移動を表す、							CVector3クラスへの参照
			*	
			*	\return			演算後の自身への参照を返す
			*
			*	次の式を使ってアフィン変換行列を計算する。
			*	Ms * (Mrc)^-1 * Mr * Mrc * Mt。
			*	ここで、
			*	Ms はスケーリング行列、
			*	Mrc は回転行列の中心、
			*	Mr は回転行列、そして 
			*	Mt は平行移動行列である。
			*/
			CMatrix4x4& SetByAffineTransformation(
				const CVector3&	scaling,
				const CVector3&	rotationCenter,
				const CMatrix4x4&	rotationMatrix,
				const CVector3&	translation);

			/*!	
			*	\brief	左手座標系ビュー行列を作成し、自身に設定する。
			*	
			*	\param	[in]	eye	視点を定義する、										CVector3への参照
			*	\param	[in]	at	カメラの注視対象を定義する、							CVector3への参照
			*	\param	[in]	up	カレント ワールドの上方、一般には [0, 1, 0] を定義する、CVector3への参照
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使ってビュー行列を計算する。\n
			*	zAxis = (at - eye).Normalize()		\n
			*	xAxis = up.Cross(zAxis).Normalize()	\n
			*	yAxis = zAxis.Cross(xAxis)			\n
			*										\n
			*	|  xAxis.x			yAxis.x			zAxis.x         0 |\n
			*	|  xAxis.y			yAxis.y			zAxis.y         0 |\n
			*	|  xAxis.z			yAxis.z			zAxis.z         0 |\n
			*	| -xAxis.Dot(eye)  -yAxis.Dot(eye) -zAxis.Dot(eye)  1 |\n
			*/
			CMatrix4x4& SetByLookAtLH(
				const CVector3&	eye,
				const CVector3&	at,
				const CVector3&	up);

			/*!	
			*	\brief	右手座標系ビュー行列を作成し、自身に設定する。
			*	
			*	\param	[in]	eye	視点を定義する、										CVector3への参照
			*	\param	[in]	at	カメラの注視対象を定義する、							CVector3への参照
			*	\param	[in]	up	カレント ワールドの上方、一般には [0, 1, 0] を定義する、CVector3への参照
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使ってビュー行列を計算する。\n
			*	zAxis = (eye - at).Normalize()		\n
			*	xAxis = up.Cross(zAxis).Normalize()	\n
			*	yAxis = zAxis.Cross(xAxis)			\n
			*										\n
			*	|  xAxis.x			yAxis.x			zAxis.x         0 |\n
			*	|  xAxis.y			yAxis.y			zAxis.y         0 |\n
			*	|  xAxis.z			yAxis.z			zAxis.z         0 |\n
			*	| -xAxis.Dot(eye)  -yAxis.Dot(eye) -zAxis.Dot(eye)  1 |\n
			*/
			CMatrix4x4& SetByLookAtRH(
				const CVector3&	eye,
				const CVector3&	at,
				const CVector3&	up);

			/*!	
			*	\brief	左手座標系正射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	width	ビューボリュームの幅
			*	\param	[in]	height	ビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの最小Z値
			*	\param	[in]	farZ	ビューボリュームの最大Z値
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	| 2 / w    0                0           0 |\n
			*	|   0    2 / h              0           0 |\n
			*	|   0      0        1 / (farZ - nearZ)  0 |\n
			*	|   0      0    nearZ / (nearZ - farZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	OrthoLH 関数のパラメータはすべて、カメラ空間の距離である。
			*	このパラメータは、ビュー ボリュームのディメンジョンを記述する。
			*/
			CMatrix4x4& SetByOrthoLH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	右手座標系正射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	width	ビューボリュームの幅
			*	\param	[in]	height	ビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの最小Z値
			*	\param	[in]	farZ	ビューボリュームの最大Z値
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って右手座標系正射影行列を計算する。	\n
			*													\n
			*	| 2 / w    0                0           0 |\n
			*	|   0    2 / h              0           0 |\n
			*	|   0      0        1 / (nearZ - farZ)  0 |\n
			*	|   0      0    nearZ / (nearZ - farZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	OrthoLH 関数のパラメータはすべて、カメラ空間の距離である。
			*	このパラメータは、ビュー ボリュームのディメンジョンを記述する。
			*/
			CMatrix4x4& SetByOrthoRH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	カスタマイズした左手座標系正射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	|          2 / (maxX - minX)                   0                          0           0 |\n
			*	|            0		                         2 / (maxY - minY)            0           0 |\n
			*	|            0             	                   0                   1 / (maxZ - minZ)  0 |\n
			*	| (minX + maxX) / (minX - maxX)  (maxY + minY) / (minY - maxY)  minZ / (minZ - maxZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	OrthoOffCenterLH を使って同じ射影を作成するには、次の値を使う\n
			*	minX = -w/2、maxX = w/2、minY = -h/2、maxY = h/2。
			*/
			CMatrix4x4& SetByOrthoOffCenterLH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	カスタマイズした左手座標系正射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	|          2 / (maxX - minX)                   0                          0           0 |\n
			*	|            0		                         2 / (maxY - minY)            0           0 |\n
			*	|            0             	                   0                   1 / (minZ - maxZ)  0 |\n
			*	| (minX + maxX) / (minX - maxX)  (maxY + minY) / (minY - maxY)  minZ / (minZ - maxZ)  1 |\n
			*	
			*	正射影行列は反転可能な行列である。その行列の反転は、行列の転置に等しい。\n
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	OrthoOffCenterLH を使って同じ射影を作成するには、次の値を使う\n
			*	minX = -w/2、maxX = w/2、minY = -h/2、maxY = h/2。
			*/
			CMatrix4x4& SetByOrthoOffCenterRH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	左手座標系パースペクティブ射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	width	近くのビュー平面でのビューボリュームの幅
			*	\param	[in]	height	近くのビュー平面でのビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って左手座標系パースペクティブ射影行列を計算する。	\n
			*	| 2 * nearZ / width            0                      0                 0 |\n
			*	|     0		         2 * nearZ / height               0                 0 |\n
			*	|     0                        0	         1 * farZ / (farZ - nearZ)  0 |\n
			*	|     0                        0         nearZ * farZ / (nearZ - farZ)  1 |\n
			*/
			CMatrix4x4& SetByPerspectiveLH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	右手座標系パースペクティブ射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	width	近くのビュー平面でのビューボリュームの幅
			*	\param	[in]	height	近くのビュー平面でのビューボリュームの高さ
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って右手座標系パースペクティブ射影行列を計算する。	\n
			*	| 2 * nearZ / width            0                      0                 0 |\n
			*	|     0		         2 * nearZ / height               0                 0 |\n
			*	|     0                        0	         1 * farZ / (nearZ - farZ)  0 |\n
			*	|     0                        0         nearZ * farZ / (nearZ - farZ)  1 |\n
			*/
			CMatrix4x4& SetByPerspectiveRH(
				const float	width,
				const float	height,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	視野に基づいて、左手座標系パースペクティブ射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	fovY	Y方向の視野角
			*	\param	[in]	aspect	画面のアスペクト比
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って左手座標系パースペクティブ射影行列を計算する。	\n
			*	height	= nearZ * tan(fovY / 2.0f) * 2.0f		\n
			*	width	= height * aspect						\n
			*													\n
			*	| 2 / width       0                  0           0 |\n
			*	|     0		 2 / height              0           0 |\n
			*	|     0           0	         1 / (farZ - nearZ)  0 |\n
			*	|     0           0      nearZ / (nearZ - farZ)  1 |\n
			*/
			CMatrix4x4& SetByPerspectiveFovLH(
				const float	fovY,
				const float	aspect,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	視野に基づいて、右手座標系パースペクティブ射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	fovY	Y方向の視野角
			*	\param	[in]	aspect	画面のアスペクト比
			*	\param	[in]	nearZ	ビューボリュームの近平面までの距離
			*	\param	[in]	farZ	ビューボリュームの遠平面までの距離
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って右手座標系パースペクティブ射影行列を計算する。	\n
			*	height	= nearZ * tan(fovY / 2.0f) * 2.0f		\n
			*	width	= height * aspect						\n
			*													\n
			*	| 2 / width       0                  0           0 |\n
			*	|     0		 2 / height              0           0 |\n
			*	|     0           0	         1 / (nearZ - farZ)  0 |\n
			*	|     0           0      nearZ / (nearZ - farZ)  1 |\n
			*/
			CMatrix4x4& SetByPerspectiveFovRH(
				const float	fovY,
				const float	aspect,
				const float nearZ,
				const float farZ);

			/*!	
			*	\brief	カスタマイズした左手座標系パースペクティブ射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って左手座標系正射影行列を計算する。	\n
			*													\n
			*	|      2 * minZ / (maxX - minX)                0                            0                0 |\n
			*	|               0		             2 * minZ  / (maxY - minY)              0                0 |\n
			*	| (minX + maxX) / (minX - maxX)  (maxY + minY) / (minY - maxY)	   1 * maxZ / (maxZ - minZ)  0 |\n
			*	|               0                              0                minZ * maxZ / (minZ - maxZ)  1 |\n
			*	
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	
			*/
			CMatrix4x4& SetByPerspectiveOffCenterLH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	カスタマイズした右手座標系パースペクティブ射影行列を作成し、自身に設定する。
			*
			*	\param	[in]	minX	ビューボリュームの最小X値
			*	\param	[in]	maxX	ビューボリュームの最大X値
			*	\param	[in]	minY	ビューボリュームの最小Y値
			*	\param	[in]	maxY	ビューボリュームの最大Y値
			*	\param	[in]	minZ	ビューボリュームの最小Z値
			*	\param	[in]	maxZ	ビューボリュームの最大Z値
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って右手座標系正射影行列を計算する。	\n
			*													\n
			*	|      2 * minZ / (maxX - minX)                0                            0                0 |\n
			*	|               0		             2 * minZ  / (maxY - minY)              0                0 |\n
			*	| (minX + maxX) / (maxX - minX)  (maxY + minY) / (maxY - minY)	   1 * maxZ / (minZ - maxZ)  0 |\n
			*	|               0                              0                minZ * maxZ / (minZ - maxZ)  1 |\n
			*	
			*	SetByOrthoLH 関数は、SetByOrthoOffCenterLH 関数の特殊なケースである。\n
			*	
			*/
			CMatrix4x4& SetByPerspectiveOffCenterRH(
				const float	minX,
				const float	maxX,
				const float minY,
				const float maxY,
				const float minZ,
				const float maxZ);

			/*!	
			*	\brief	ビューポート行列を作成し、自身に設定する。
			*
			*	\param	[in]	viewport	演算に使用するviewport_t構造体への参照
			*	
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使ってビューポート行列を計算する。\n
			*	| width / 2            0              0       0 |\n
			*	|     0		       - height / 2       0       0 |\n
			*	|     0                0         maxZ - MinZ  0 |\n
			*	| x + width / 2  y + height / 2      MinZ     1 |\n
			*	
			*	この行列は、ビューポートのサイズおよび指定の深度範囲に従って頂点をスケーリングし、\n
			*	その頂点をレンダーターゲットサーフェスの適切な位置に平行移動します。\n
			*	また、この行列は、y 座標を反転して、y が下方に増加する左上角のスクリーン原点を反映します。
			*/
			CMatrix4x4& SetByViewPort(
				const viewport_t& viewport);

			/*!	
			*	\brief	オフセットを指定して平行移動行列を作成し、自身に設定する。
			*	
			*	\param	[in]	translation		平行移動を表す、CVector3クラスへの参照
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って平行移動行列を計算する。\n
			*	| 1  0  0  0 |\n
			*	| 0	 1  0  0 |\n
			*	| 0  0  1  0 |\n
			*	| x  y  z  1 |\n
			*/
			CMatrix4x4& SetByTranslation(
				const CVector3&	translation);

			/*!	
			*	\brief	x 軸、y 軸、z 軸に沿ってスケーリングする行列を作成し、自身に設定する。
			*	
			*	\param	[in]	scaling	各軸方向についてのスケーリング係数を表す、CVector3クラスへの参照
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使ってスケール行列を計算する。\n
			*	| Sx  0   0   0 |\n
			*	| 0   Sy  0   0 |\n
			*	| 0   0   Sz  0 |\n
			*	| 0   0   0   1 |\n
			*/
			CMatrix4x4& SetByScaling(
				const CVector3&	scaling);

			/*!	
			*	\brief	任意の軸を回転軸にして回転行列を作成し、自身に設定する。
			*	
			*	\param	[in]	axis	軸の角度を識別する、CVector3クラスへの参照
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*			|     0   -axis.z  axis.y |\n
			*	R =		|  axis.z     0   -axis.x |\n
			*			| -axis.y  axis.x    0    |\n
			*														\n
			*	M = I + sin(radian) * R + (1 - cos(radian)) * R * R	\n
			*														\n
			*	|       	    0 |\n
			*	|      M        0 |\n
			*	|               0 |\n
			*	| 0    0    0   1 |\n
			*/
			CMatrix4x4& SetByRotationAxis(
				const CVector3&	axis,
				const float					radian);

			/*!
			*	\brief	クォータニオンから回転行列を作成し、自身に設定する。
			*
			*	\param	[in]	quaternion	処理の基になるCQuaternionへの参照
			*
			*	\return	演算後の自身への参照を返す
			*/
			CMatrix4x4& SetByRotationQuaternion(
				const Math::CQuaternion<float>&	quaternion);

			/*!	
			*	\brief	x 軸についての回転行列を作成し、自身に設定する。
			*	
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*	| 1    0	0   0 |\n
			*	| 0   cos  sin  0 |\n
			*	| 0  -sin  cos  0 |\n
			*	| 0    0    0   1 |\n
			*/
			CMatrix4x4& SetByRotationX(
				const float	radian);

			/*!	
			*	\brief	y 軸についての回転行列を作成し、自身に設定する。
			*	
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*	| cos  0 -sin  0 |\n
			*	| 0    1   0   0 |\n
			*	| sin  0  cos  0 |\n
			*	| 0    0   0   1 |\n
			*/
			CMatrix4x4& SetByRotationY(
				const float	radian);

			/*!	
			*	\brief	z 軸についての回転行列を作成し、自身に設定する。
			*	
			*	\param	[in]	radian	演算に使用するラジアン角度
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次の式を使って回転行列を計算する。\n
			*	|  cos  sin  0  0 |\n
			*	| -sin  cos  0  0 |\n
			*	|   0    0   1  0 |\n
			*	|   0    0   0  1 |\n
			*/
			CMatrix4x4& SetByRotationZ(
				const float	radian);

			/*!	
			*	\brief	ヨー、ピッチ、およびロールを指定して行列を作成し、自身に設定する。
			*	
			*	\param	[in]	x	演算に使用するX軸のラジアン角度(ピッチ)
			*	\param	[in]	y	演算に使用するY軸のラジアン角度(ヨー)
			*	\param	[in]	z	演算に使用するZ軸のラジアン角度(ロール)
			*
			*	\return	演算後の自身への参照を返す
			*
			*	トランスフォームの順序は、最初にロール、次にピッチ、最後にヨーである。\n
			*	これは、オブジェクトのローカル座標軸を基準として、z 軸の周囲での回転、
			*	x 軸の周囲での回転、y 軸の周囲での回転と同じになる。
			*/
			CMatrix4x4& SetByRotationYawPitchRoll(
				const float	x,
				const float	y,
				const float	z);

			/*!	
			*	\brief	ヨー、ピッチ、およびロールを指定して行列を作成し、自身に設定する。
			*	
			*	\param	[in]	rotation	演算に使用する軸ごとのラジアン角度
			*
			*	\return	演算後の自身への参照を返す
			*
			*	トランスフォームの順序は、最初にロール、次にピッチ、最後にヨーである。\n
			*	これは、オブジェクトのローカル座標軸を基準として、z 軸の周囲での回転、
			*	x 軸の周囲での回転、y 軸の周囲での回転と同じになる。
			*/
			CMatrix4x4& SetByRotationYawPitchRoll(
				const CVector3&	rotation);
		};
	};
};
#endif