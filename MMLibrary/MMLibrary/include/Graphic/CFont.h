/*!	\file		CFont.h
*	\details	CFontクラスの宣言
*	\author		松裏征志
*	\date		2014/04/29
*/
#ifndef MMLIBRARY_CFONT_H_
#define MMLIBRARY_CFONT_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility/Macro.h"
#include "Utility/CCountableBase.h"
#include "Graphic/Other.h"
#include "Graphic/CWindow.h"
#include "Graphic/Model/CVerticesHasTexture.h"
#include "Graphic/Vertex/CVertexFor2D.h"
#include "Graphic/Texture/Texture.h"
#include "Graphic/Texture/CTextureBank.h"
#include "Graphic/Object/CObjectsBinder.h"

#include "Utility/DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	フォントのアンチエイリアシングレベルを表す
		enum class FONT_ANTIALIASING_LEVEL : char
		{
			LEVEL_1 = 1,
			LEVEL_2,
			LEVEL_3,
			LEVEL_4
		};

		//!	フォントの太さを表す
		enum class FONT_WEIGHT : LONG
		{
			DONTCARE = 0,
			THIN = 100,
			EXTRALIGHT = 200,
			ULTRALIGHT = 200,
			LIGHT = 300,
			NORMAL = 400,
			REGULAR = 400,
			MEDIUM = 500,
			SEMIBOLD = 600,
			DEMIBOLD = 600,
			BOLD = 700,
			EXTRABOLD = 800,
			ULTRABOLD = 800,
			HEAVY = 900,
			BLACK = 900
		};

		//!	GetGlyphOutlineで使うフォーマット情報を取得する
		UINT GetGGOFormat(FONT_ANTIALIASING_LEVEL level);

		//!	フォントのアンチエイリアシングレベルに対応したGetGlyphOutlineで取得したビットマップの各画素の最大値を返す
		UINT GetGGOMaxGrad(FONT_ANTIALIASING_LEVEL level);

		namespace Model
		{
			//!	文字のモデルを表すクラス
			template<template<class> class Allocator = USE_ALLOCATOR>
			class CFontCharacter : public CVerticesHasTexture < CVertexFor2D, Allocator >
			{
			private:
				size_t					m_fontSize;

				Math::CVector<float, 2>	m_scale;
				Math::CVector<float, 2>	m_position;
				float					m_offsetForNextCharacter;
				uint_t					m_characterCode;

				void	SetTexture(
					const string_t<Allocator>& fontName,
					uint_t characterCode,
					FONT_ANTIALIASING_LEVEL level,
					FONT_WEIGHT fontWeight = FONT_WEIGHT::NORMAL)
				{
					// フォントハンドルの生成
					LOGFONT lf =
					{
						m_fontSize, 0, 0, 0, static_cast<LONG>(fontWeight), 0, 0, 0,
						SHIFTJIS_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_DEFAULT_PRECIS,
						PROOF_QUALITY, DEFAULT_PITCH | FF_MODERN,
						0
					};

					for (size_t i = 0; i < LF_FACESIZE && i < fontName.size(); ++i)
					{
						lf.lfFaceName[i] = fontName[i];
					}

					HFONT hFont = CreateFontIndirect(&lf);

					// 現在のウィンドウに適用
					// デバイスにフォントを持たせないとGetGlyphOutline関数はエラーとなる
					HDC hDC = GetDC(CWindow::GetCurrent()->GetWindowHandle());
					HFONT oldFont = (HFONT)SelectObject(hDC, hFont);

					//	フォント情報の取得
					CONST MAT2 mat = { { 0, 1 }, { 0, 0 }, { 0, 0 }, { 0, 1 } };
					GLYPHMETRICS			glyphMetrics;
					DWORD size = GetGlyphOutline(hDC, characterCode, GetGGOFormat(level), &glyphMetrics, 0, NULL, &mat);

					if (size == 0)
					{
						SelectObject(hDC, oldFont);
						ReleaseDC(NULL, hDC);
						return;
					}

					m_scale[0] = static_cast<float>(glyphMetrics.gmBlackBoxX) / m_fontSize;
					m_scale[1] = static_cast<float>(glyphMetrics.gmBlackBoxY) / m_fontSize;
					m_position[0] = static_cast<float>(glyphMetrics.gmptGlyphOrigin.x) / m_fontSize;
					m_position[1] = (static_cast<float>(glyphMetrics.gmptGlyphOrigin.y) - glyphMetrics.gmBlackBoxY) / m_fontSize;
					m_offsetForNextCharacter = static_cast<float>(glyphMetrics.gmCellIncX) / m_fontSize;

					//	テクスチャがすでに作られているかを確認する
					string_t<Allocator> textureName = CreateTextureName(fontName, characterCode, level, m_fontSize, fontWeight);

					if (CTextureBank<Allocator>::GetInstance().IsRegistered(textureName))
					{
						CVerticesHasTexture < CVertexFor2D, Allocator >::SetTexture(
							CTextureBank<Allocator>::GetInstance().Request(textureName));

						return;
					}
					else
					{
						//	フォントビットマップの作成
						vector_t<BYTE, Allocator> fontBMP(size);
						GetGlyphOutline(hDC, characterCode, GetGGOFormat(level), &glyphMetrics, size, fontBMP.data(), &mat);

						//	テクスチャ化用画像データに変更
						int fontWidth = (glyphMetrics.gmBlackBoxX + 3) / 4 * 4;
						int fontHeight = glyphMetrics.gmBlackBoxY;
						auto grad = GetGGOMaxGrad(level);
						vector_t<CColor32, Allocator> image;
						image.reserve(fontWidth * fontHeight);

						for (int i = 0; i < fontWidth * fontHeight; ++i)
						{
							image.emplace_back(255, 255, 255, fontBMP[i] * 255 / grad);
						}

						auto pTexture = std::make_shared<CTexture<Allocator>>(textureName, image.data(), fontWidth, fontHeight);
						CVerticesHasTexture<CVertexFor2D, Allocator>::SetTexture(pTexture);

						CTextureBank<>::GetInstance().
							Register(
							textureName,
							pTexture);
					}

					// デバイスコンテキストとフォントハンドルはもういらないので解放
					SelectObject(hDC, oldFont);
					ReleaseDC(NULL, hDC);
				};
			public:
				//!	コンストラクタ
				CFontCharacter(
					const string_t<Allocator>& fontName,
					uint_t characterCode,
					FONT_ANTIALIASING_LEVEL level = FONT_ANTIALIASING_LEVEL::LEVEL_3,
					size_t fontSize = 64,
					FONT_WEIGHT fontWeight = FONT_WEIGHT::NORMAL)
					: m_fontSize(fontSize)
					, m_offsetForNextCharacter(0)
					, m_characterCode(characterCode)
				{
					assert(fontName.empty() == false && "Please input fontName.");

					SetByQuads();
					for (auto& vertex : Vertices())
					{
						vertex.Position() += CVector3({ 0.5f, 0.5f, 0 });
					}
					SetVertexData(Vertices());

					SetTexture(fontName, characterCode, level, fontWeight);
				}

				//!	描画
				void Render(void) override
				{
					CGraphicManager& gm = CGraphicManager::GetManager();

					gm.Stack.
						Push().
						MultiplyLocal(CMatrix4x4::CreateByTranslation(	CVector3({ m_position[0],	m_position[1],	0 }))).
						MultiplyLocal(CMatrix4x4::CreateByScaling(		CVector3({ m_scale[0],		m_scale[1],		1 })));
					gm.SetWorldMatrix();

					CVerticesHasTexture<CVertexFor2D, Allocator>::Render();

					gm.Stack.
						Pop();
				}

				//!	次の文字へのオフセットを取得する
				float GetOffsetForNextCharacter() const
				{
					return m_offsetForNextCharacter;
				}

				static string_t<Allocator> CreateTextureName(
					const string_t<Allocator>& fontName,
					uint_t characterCode,
					FONT_ANTIALIASING_LEVEL level,
					size_t fontSize,
					FONT_WEIGHT fontWeight)
				{
					string_t<Allocator> textureName = fontName;
					textureName += "-";
					textureName += std::to_string(static_cast<char>(level));
					textureName += "-";
					textureName += std::to_string(fontSize);
					textureName += "-";
					textureName += std::to_string(static_cast<LONG>(fontWeight));
					textureName += "-";
					textureName += std::to_string(characterCode);

					return textureName;
				};

				CREATE_GETTER_PRIMITIVE(CharacterCode, m_characterCode);
			};

		}

		//!	文字列のモデルを表すクラス
		template<bool IsBillBoard = false, template<class> class Allocator = USE_ALLOCATOR>
		class CText : public Utility::CCountableBase < CText<IsBillBoard, Allocator >, CObjectsBinder<IsBillBoard, CObject, Allocator> >
		{
		private:
			string_t<Allocator>		m_text;
			string_t<Allocator>		m_fontName;
			CColor32				m_color;
			FONT_ANTIALIASING_LEVEL m_level;
			size_t					m_fontSize;
			FONT_WEIGHT				m_fontWeight;

			//Shift-JISから文字コードを作成
			bool CreateCharacterCode(const char* pOriginCharcterCode, uint_t& createdCharacterCode)
			{
				if (Utility::IsFirstCodeOfMultiByteCode(pOriginCharcterCode[0]))
				{
					createdCharacterCode = Packing(pOriginCharcterCode[0], pOriginCharcterCode[1]);
					return true;
				}
				else
				{
					createdCharacterCode = Packing(pOriginCharcterCode[0]);
					return false;
				}
			}

			CText& CreateObjects(void)
			{
				m_objects.reserve(m_text.size());
				float previouceX = 0;
				float offsetX = 0;
				uint_t character = 0;

				for (size_t i = 0; i < m_text.size(); ++i)
				{
					//	オブジェクト生成
					std::shared_ptr<CObject> pObject = Memory::MakeShared<CObject, Allocator>();

					push_back(pObject);

					//Shift-JISから文字コードを作成
					if (CreateCharacterCode(&m_text[i], character)) ++i;

					previouceX += offsetX;

					pObject->Transform().
						MoveTo(previouceX, 0, 0);

					//モデル取得
					auto modelName = CreateModelName(character);

					if (Model::CModelBank<Allocator>::GetInstance().IsRegistered(modelName))
					{
						std::shared_ptr<Model::IModel> pModel = Model::CModelBank<Allocator>::GetInstance().
							Request(modelName);
						pObject->SetModel(*pModel);

						offsetX = reinterpret_cast<Model::CFontCharacter<Allocator>*>(pModel.get())->GetOffsetForNextCharacter();
					}
					else
					{
						auto pModel = Memory::MakeShared<Model::CFontCharacter<Allocator>, Allocator>(
							m_fontName,
							character, 
							m_level,
							m_fontSize,
							m_fontWeight);

						Model::CModelBank<Allocator>::GetInstance().Register(modelName, pModel);

						for (auto& vertex : pModel->Vertices())
						{
							vertex.Color() = m_color;
						}
						pModel->SetVertexData(pModel->Vertices());

						pObject->SetModel(pModel.get());

						offsetX = pModel->GetOffsetForNextCharacter();
					}
				}

				m_objects.shrink_to_fit();

				return *this;
			}

			string_t<> CreateModelName(uint_t characterCode) const
			{
				string_t<> rtn = "FontModel-";
				rtn += std::to_string(characterCode);
				rtn += "-";
				rtn += std::to_string(m_color);
				return rtn;
			}

			//!	モデルの破棄
			void DisposeAllModel()
			{
				//	登録済みモデルがなければ終了
				if (Model::CModelBank<Allocator>::GetInstance().GetRegisteredSize() < 1) return;

				uint_t character = 0;
				for (size_t i = 0; i < m_text.size(); ++i)
				{
					if (CreateCharacterCode(&m_text[i], character)) ++i;

					Model::CModelBank<Allocator>::GetInstance().Dispose(CreateModelName(character));
				}
			}

		public:
			//!	コンストラクタ
			CText(const string_t<>& text,
				const CColor32& color,
				const string_t<Allocator>& fontName,
				FONT_ANTIALIASING_LEVEL level = FONT_ANTIALIASING_LEVEL::LEVEL_3,
				size_t fontSize = 64,
				FONT_WEIGHT fontWeight = FONT_WEIGHT::NORMAL)
				: m_text(text)
				, m_fontName(fontName)
				, m_color(color)
				, m_level(level)
				, m_fontSize(fontSize)
				, m_fontWeight(fontWeight)
			{
				CreateObjects();
			};

			//!	デストラクタ
			~CText()
			{
				DisposeAllModel();
			}

			//!	テキストを変更する
			CText& SetText(const string_t<>& text)
			{
				if (m_text != text)
				{
					m_objects.resize(0);

					m_text = text;
					CreateObjects();
				}

				return *this;
			}

			CREATE_CONST_REFERRENCE(Text, m_text);
		};
	}
}

#include "Utility/UndefineClassCreateHelpers.h"

#endif	