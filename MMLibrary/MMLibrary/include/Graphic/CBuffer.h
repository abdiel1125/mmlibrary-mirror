/*!	\file		CBuffer.h
*	\details	CBufferクラスの宣言
*	\author		松裏征志
*	\date		2014/11/12
*/
#ifndef MMLIBRARY_CBUFFER_H_
#define MMLIBRARY_CBUFFER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Other.h"
#include "MMLibraryDefine.h"
#include "Utility\DefineClassCreateHelpers.h"
#include <assert.h>

namespace	MMLibrary
{
	namespace Graphic
	{
		//!	グラフィックスメモリに頂点配列を保存するクラスの基底クラス
		class CVertexBufferBase
		{
		private:
			vertexBuffer_t m_vertexBuffer;
			void*	m_lockedDataAddress;
			int		m_lockedNum;

		public:
			//!	コンストラクタ
			CVertexBufferBase();

			//!	ムーブコンストラクタ
			CVertexBufferBase(CVertexBufferBase&& src)
				: CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_vertexBuffer)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_lockedDataAddress)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_lockedNum)
			{};

			//!	デストラクタ
			~CVertexBufferBase()
			{
				Dispose();
			};

			//! バッファのバインド
			void Bind(size_t stride);

			//! バッファにデータを登録する
			void SetData(const void* pData, size_t size);

			//!	バッファのバインドを解除する
			void UnBind();

			//!	バッファデータをロックし、データの先頭アドレスを返す
			void* Lock();

			//!	バッファデータをアンロックする
			void UnLock();

			//!	初期化する
			void Dispose();
		};

		//!	グラフィックスメモリに頂点配列を保存するクラス
		template<class Vertex>
		class CVertexBuffer : private CVertexBufferBase
		{
		public:
			//!	RAIIによるバッファデータへの参照クラス
			class CVertexBufferPointer
			{
				template<class Vertex>
				friend class CVertexBuffer;
			public:
				//!	デストラクタ
				~CVertexBufferPointer()
				{
					m_buffer.UnLock();
				};

				//!	Vertex*へのキャスト
				operator Vertex*()
				{
					return m_pData;
				}

				//!	const Vertex*へのキャスト
				operator const Vertex*() const
				{
					return m_pData;
				}

				//!	添え字演算子
				Vertex& operator[](size_t index)
				{
					return m_pData[index];
				}

				//!	stl風アクセッサ
				Vertex& at(size_t index)
				{
					assert(index < size() && "CVertexBufferPointer at() was access over array end.");
					return m_pData[index];
				}

				//!	バッファの要素数を取得する
				size_t size() const
				{
					return m_buffer.m_num;
				}

				//!	空であるかを取得する
				bool empty() const
				{
					return size() == 0;
				}

				//!	先頭アドレスを取得する
				Vertex* begin()
				{
					return m_pData;
				}

				//!	先頭アドレスを取得する
				const Vertex* begin() const
				{
					return m_pData;
				}

				//!	終端アドレスを取得する
				Vertex* end()
				{
					return m_pData + size();
				}

				//!	終端アドレスを取得する
				const Vertex* end() const
				{
					return m_pData + size();
				}
			private:
				//!	コンストラクタ
				CVertexBufferPointer(CVertexBuffer<Vertex>& buffer)
					: m_buffer(buffer)
				{
					m_pData = static_cast<Vertex*>(m_buffer.CVertexBufferBase::Lock());
				};

				CVertexBuffer<Vertex>& m_buffer;
				Vertex*	m_pData;
			};


			//!	コンストラクタ
			CVertexBuffer()
			:m_num(0){};

			//!	コピーコンストラクタ
			CVertexBuffer(const CVertexBuffer& src)
				: CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_num)
			{
				SetData(const_cast<CVertexBuffer&>(src).Lock(), src.m_num);
			};

			//!	ムーブコンストラクタ
			CVertexBuffer(CVertexBuffer&& src)
				: CVertexBufferBase(src)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_num)
			{};

			//! バッファのバインド
			/*!
			*	RenderBeginより先に呼ぶ必要がある
			*/
			void Bind()
			{
				CVertexBufferBase::Bind(sizeof(Vertex));
			};

			//! バッファにデータを登録する
			void SetData(const Vertex* pData, size_t num)
			{
				CVertexBufferBase::SetData(pData, num * sizeof(Vertex));
				m_num = num;
			};

			//!	バッファのバインドを解除する
			void UnBind()
			{
				CVertexBufferBase::UnBind();
			};

			//!	バッファデータへの参照を返す
			CVertexBufferPointer Lock()
			{
				return CVertexBufferPointer(*this);
			};

			//!	バッファデータのコピーを格納したvector_tを作成する
			template<template<class> class Allocator>
			vector_t<Vertex, Allocator> CreateVector() const
			{
				auto vertices = const_cast<CVertexBuffer<Vertex>*>(this)->Lock();
				vector_t<Vertex, Allocator> rtn;

				rtn.resize(vertices.size());
				for (uint_t i = 0; i < vertices.size(); i++)
				{
					rtn[i] = vertices[i];
				}

				return rtn;
			};

			//!	頂点数を取得する
			size_t GetSize() const
			{
				return m_num;
			}

			//!	初期化する
			void Dispose()
			{
				CVertexBufferBase::Dispose();
				m_num = 0;
			};

		private:
			size_t m_num;
		};

		//!	グラフィックスメモリにポリゴンの構成情報配列を保存するクラスの基底クラス
		class CIndexBuffer
		{
		public:
			using index_t = unsigned int;

			//!	RAIIによるバッファデータへの参照クラス
			class CIndexBufferPointer
			{
				friend class CIndexBuffer;
			public:

				//!	デストラクタ
				~CIndexBufferPointer()
				{
					m_buffer.UnLock();
				};

				//!	index_t*へのキャスト
				operator index_t *()
				{
					return m_pData;
				}

				//!	添え字演算子
				index_t& operator[](size_t index)
				{
					return m_pData[index];
				}

				//!	stl風アクセッサ
				index_t& at(size_t index)
				{
					assert(index < size() && "CIndexBufferPointer at() was access over array end.");
					return m_pData[index];
				}

				//!	バッファの要素数を取得する
				size_t size()
				{
					return m_buffer.m_num;
				}

			private:
				//!	コンストラクタ
				CIndexBufferPointer(CIndexBuffer& buffer)
					: m_buffer(buffer)
				{
					m_pData = m_buffer.LockImpl();
				};

				CIndexBuffer& m_buffer;
				index_t*	m_pData;
			};

		private:
			//!	バッファデータをロックし、データの先頭アドレスを返す
			index_t* LockImpl();

			//!	バッファデータをアンロックする
			void UnLock();

			indexBuffer_t m_indexBuffer;
			index_t*	m_lockedDataAddress;
			int		m_lockedNum;
			size_t	m_num;
		public:
			//!	コンストラクタ
			CIndexBuffer();

			//!	コピーコンストラクタ
			CIndexBuffer(const CIndexBuffer& src)
				: m_indexBuffer(0)
				, m_lockedDataAddress(nullptr)
				, m_lockedNum(0)
				, m_num(0)
			{
				SetData(const_cast<CIndexBuffer&>(src).Lock(), src.GetSize());
			};

			//!	ムーブコンストラクタ
			CIndexBuffer(CIndexBuffer&& src)
				: CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_indexBuffer)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_lockedDataAddress)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_lockedNum)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_num)
			{
				src.m_indexBuffer = 0;
				src.m_lockedDataAddress = nullptr;
				src.m_lockedNum = 0;
				src.m_num = 0;
			};

			//!	デストラクタ
			~CIndexBuffer()
			{
				Dispose();
			};

			//!	バッファデータへの参照を返す
			CIndexBufferPointer Lock()
			{
				return CIndexBufferPointer(*this);
			}

			//! バッファのバインド
			void Bind();

			//! バッファにデータを登録する
			void SetData(const index_t* pData, size_t size);

			//!	バッファのバインドを解除する
			void UnBind();


			//!	添字数を取得する
			size_t GetSize() const
			{
				return m_num;
			}

			//!	バッファデータのコピーを格納したvector_tを作成する
			template<template<class> class Allocator = USE_ALLOCATOR>
			vector_t<index_t, Allocator> CreateVector() const;

			//!	初期化する
			void Dispose();
		};

		template<template<class> class Allocator>
		vector_t<CIndexBuffer::index_t, Allocator> CIndexBuffer::CreateVector() const
		{
			auto* indeces = const_cast<CIndexBuffer*>(this)->LockImpl();
			vector_t<index_t, Allocator> rtn;

			rtn.resize(GetSize());
			for (uint_t i = 0; i < GetSize(); i++)
			{
				rtn[i] = indeces[i];
			}

			const_cast<CIndexBuffer*>(this)->UnLock();

			return rtn;
		};
	}
}
#include "Utility\UndefineClassCreateHelpers.h"

#endif // MMLIBRARY_CBUFFER_H_