/*!	\file		CParticleEmitter.h
*	\details	CParticleEmitterクラスの定義
*	\author		松裏征志
*	\date		2014/09/02
*/
#ifndef MMLIBRARY_CPARTICLE_EMITTER_HPP_
#define MMLIBRARY_CPARTICLE_EMITTER_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CParticleEmitter.h"

namespace MMLibrary
{
	namespace Graphic
	{
		template<
			template <class> class Allocator>
		void	CParticleEmitter<Allocator>::Emit(void)
		{
			auto it = Range::FindIf(
				m_particles,
				[&](CParticle& it)->bool
			{
				return it.GetLife() <= 0;
			});

			if(it != m_particles.end())
			{
				CVector3 direction(
						GetFloatRandam() * 2 - 1,
						GetFloatRandam() * 2 - 1,
						GetFloatRandam() * 2 - 1);
				direction.Normalize();

				it->m_life = (int)(m_life.at(0) + GetFloatRandam() * (m_life.at(1) - m_life.at(0)));
				it->m_speed = m_defSpeed * direction;
				it->SetParticleControler(m_pParticleControler);

				it->Transform().
					MoveTo(
						Transform().GetLocalToWorldMatrix().GetPosition() +
						direction * 
						GetFloatRandam() * 
						m_emitRange);

				it->SetModel(*GetModel());
				it->Transform().
					ScalingTo(Transform().GetLocalToWorldMatrix().GetScale());
			}

			m_oldFrame = m_frameCounter;
		};

		template<
			template <class> class Allocator>
		CParticleEmitter<Allocator>::CParticleEmitter(
			int		lifeMin,
			int		lifeMax,
			int		emitInterval,
			float	emitRange,
			float	defSpeed)
			:m_emitInterval(emitInterval)
			,m_emitRange(emitRange)
			,m_defSpeed(defSpeed)
			,m_pParticleControler(nullptr)
		{
			m_life.at(0) = lifeMin;
			m_life.at(1) = lifeMax;
		};
			
		template<
			template <class> class Allocator >
		void	CParticleEmitter<Allocator>::Update(void)
		{
			m_frameCounter++;
			if(m_frameCounter - m_oldFrame >= m_emitInterval)
				Emit();

			for (auto& particle : m_particles)
			{
				if (particle.m_life > 0)
					particle.Update();
			}
		};

		template<
			template <class> class Allocator >
		void	CParticleEmitter<Allocator>::Render(void)
		{
			for (auto& particle : m_particles)
			{
				if (particle.m_life > 0)
					particle.Render();
			}
		};
	};
};
#endif