/*!	\file		CObject.h
*	\details	CObjectクラスの宣言
*	\author		松裏征志
*	\date		2014/05/29
*/
#ifndef MMLIBRARY_COBJECT_H_
#define MMLIBRARY_COBJECT_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Graphic\Object\IObject.h"
#include "Graphic\Model\IModel.h"
#include "Collision\ICollider.h"
#include "Graphic\CTransform.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	基本的な物体クラス
		class CObject : public IObject
		{
		private:
			CTransform				m_transform;
			bool					m_isVisible;

			Model::IModel*			m_pModel;			
			Collision::ICollider*	m_pCollider;
		public:
			//!	コンストラクタ
			CObject();

			//!	コピーコンストラクタ
			CObject(const CObject& src);
			
			//!	引数付きコンストラクタ
			CObject(
				CVector3&					position,
				CVector3&					scale,
				Math::CQuaternion<float>&	rotation);
			
			//!	デストラクタ
			virtual	~CObject(){};

			//!	更新関数
			virtual	void	Update(void) override{};

			//!	描画関数
			virtual	void	Render(void) override;

			//!	トランスフォーム
			virtual CTransform&	Transform() override
			{
				return const_cast <CTransform&>
					(static_cast<const IObject*>(this)->Transform());
			};

			//!	トランスフォーム
			virtual const CTransform&	Transform() const override
			{
				return m_transform;
			};

			//!	描画するモデルを設定する
			CObject&
				SetModel(Model::IModel& model);
			CObject&
				SetModel(Model::IModel* pModel);
			//!	描画するモデルを取得する
			Model::IModel*
				GetModel(void) const;

			//!	描画するか否かを取得する
			virtual bool IsVisible(void) const override;

			//!	描画するか否かを設定する
			virtual void IsVisible(bool isVisible) override;

			//!	指定したオブジェクトと衝突しているかを取得する
			virtual bool IsHit(const IObject& another) const override;

			//!	衝突判定データを持っているか否かを取得する
			virtual bool HasCollider(void) const override;

			//!	衝突判定データへの参照を取得する
			Collision::ICollider& Collider(void) override
			{
				return const_cast <Collision::ICollider&>
					(static_cast<const IObject*>(this)->Collider());
			};

			//!	衝突判定データへの参照を取得する
			virtual const Collision::ICollider& Collider(void) const override;

			//!	衝突判定用のクラスを設定する
			CObject&
				SetCollisionData(
				Collision::ICollider& collision);
		};
	};
};
#endif