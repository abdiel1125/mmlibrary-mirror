/*!	\file		CObjectsBinder.h
*	\details	CObjectsBinderクラスの宣言
*	\author		松裏征志
*	\date		2014 / 12 / 10
*/

#ifndef MMLIBRARY_COBJECTS_BINDER_H_
#define MMLIBRARY_COBJECTS_BINDER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "CObject.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	複数のオブジェクトを束ねるクラスの基底クラス
		template<class Object = IObject, template<class> class Allocator = USE_ALLOCATOR>
		class CObjectsBinderBase : public CObject
		{
		public:
			using object_t = typename Object;
			using pObject_t = typename std::shared_ptr < Object >;
			using objects_t = typename vector_t < pObject_t, Allocator >;

			//!	コンストラクタ
			CObjectsBinderBase()
				: CObject()
				, m_objects()
			{};

			//!	コピーコンストラクタ
			CObjectsBinderBase(const CObjectsBinderBase& src)
				: CObject(src)
			{
				reserve(src.m_objects.size());
				for (const auto& srcObj : src.m_objects)
				{
					emplace_back(*srcObj);
				}
			};

			//!	代入演算子
			CObjectsBinderBase& operator=(const CObjectsBinderBase& src)
			{
				CObject::operator=(src);

				reserve(src.m_objects.size());
				for (const auto& srcObj : src.m_objects)
				{
					emplace_back(*srcObj);
				}

				return *this;
			}
			//!	データアドレスの取得
			pObject_t* data()
			{
				return m_objects.data();
			}

			//!	指定した要素への参照
			pObject_t& operator[](size_t index)
			{
				return m_objects[index];
			}

			//!	指定した要素への参照
			pObject_t& at(size_t index)
			{
				return m_objects.at(index);
			}

			//!	要素数の取得
			size_t size(void)
			{
				return m_objects.size();
			}

			//!	要素数の設定
			void resize(size_t size)
			{
				m_objects.clear();
				m_objects.reserve(size);
				for (size_t i = 0; i < size; ++i)
					m_objects.emplace_back(pObject_t(Memory::NewWithAllocator<Object, Allocator>()));
			}

			//!	要素数上限の設定
			void reserve(size_t size)
			{
				m_objects.reserve(size);
			}

			//!	先頭の取得
			typename objects_t::iterator begin(void)
			{
				return m_objects.begin();
			}

			//!	終端の取得
			typename objects_t::iterator end(void)
			{
				return m_objects.end();
			}

			//!	要素の追加
			void push_back(const pObject_t& pObject)
			{
				return m_objects.push_back(pObject);
			}

			//!	要素の追加
			template<class... Args>
			void emplace_back(Args&&... args)
			{
				return m_objects.emplace_back(Memory::NewWithAllocator<Object, Allocator>(args...));
			}

			//!	要素の全削除
			void clear()
			{
				m_objects.clear();
			}

		protected:
			objects_t m_objects;

		};


		//!	複数のオブジェクトを束ねるクラス
		template<bool IsBillboard = false, class Object = IObject, template<class> class Allocator = USE_ALLOCATOR>
		class CObjectsBinder : public CObjectsBinderBase<Object, Allocator>
		{
			//!	コンストラクタ
			CObjectsBinder()
				: CObjectsBinderBase()
			{};
			//!	コピーコンストラクタ
			CObjectsBinder(const CObjectsBinder& src)
				: CObjectsBinderBase(src)
			{};
		};

		//!	複数のオブジェクトを束ねるクラス
		template<class Object, template<class> class Allocator>
		class CObjectsBinder<true, Object, Allocator> : public CObjectsBinderBase<Object, Allocator>
		{
		public:
			//!	コンストラクタ
			CObjectsBinder()
				: CObjectsBinderBase()
			{};

			//!	コピーコンストラクタ
			CObjectsBinder(const CObjectsBinder& src)
				: CObjectsBinderBase(src)
			{};

			//!	代入演算子
			CObjectsBinder& operator=(const CObjectsBinder& src)
			{
				CObjectsBinderBase::operator=(src);
				return *this;
			}

			//!	描画
			void Render() override
			{
				if (IsVisible() == false) return;

				auto viewMatrix = CGraphicManager::GetManager().GetViewMatrix();
				CGraphicManager::GetManager().SetViewMatrix(CMatrix4x4());

				CGraphicManager::GetManager().Stack.
					Push().MultiplyLocal(Transform().GetLocalToWorldMatrix());
				CMatrix4x4 worldMatrix =
					CGraphicManager::GetManager().Stack.
					GetTop();
				worldMatrix.Multiply(viewMatrix);
				auto scale = worldMatrix.GetScale();
				auto position = worldMatrix.GetPosition();

				CGraphicManager::GetManager().Stack.
					Push().
					LoadIdentity().
					MultiplyLocal(CMatrix4x4(
					scale[0], 0, 0, 0,
					0, scale[1], 0, 0,
					0, 0, scale[2], 0,
					position[0], position[1], position[2], 1));
				CGraphicManager::GetManager().SetWorldMatrix();

				for (auto& object : m_objects)
					object->Render();

				CGraphicManager::GetManager().Stack.
					Pop().Pop();

				CGraphicManager::GetManager().SetViewMatrix(viewMatrix);
			};
		};

		//!	複数のオブジェクトを束ねるクラス
		template<class Object, template<class> class Allocator>
		class CObjectsBinder<false, Object, Allocator> : public CObjectsBinderBase<Object, Allocator>
		{
		public:
			//!	コンストラクタ
			CObjectsBinder()
				: CObjectsBinderBase()
			{};

			//!	コピーコンストラクタ
			CObjectsBinder(const CObjectsBinder& src)
				: CObjectsBinderBase(src)
			{};

			//!	代入演算子
			CObjectsBinder& operator=(const CObjectsBinder& src)
			{
				CObjectsBinderBase::operator=(src);
				return *this;
			}
			//!	描画
			void Render() override
			{
				if (IsVisible() == false) return;

				CGraphicManager::GetManager().Stack.
					Push().MultiplyLocal(Transform().GetLocalToWorldMatrix());

				for (auto& object : m_objects)
					object->Render();

				CGraphicManager::GetManager().Stack.
					Pop();
			};
		};
	}
}
#include "Utility\UndefineClassCreateHelpers.h"
#endif	//	MMLIBRARY_COBJECTS_BINDER_H_