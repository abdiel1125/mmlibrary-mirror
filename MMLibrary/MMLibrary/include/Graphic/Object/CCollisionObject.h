/*!	\file		CColliderObject.h
*	\details	CColliderObjectクラスの宣言
*	\author		松裏征志
*	\date		2014/11/25
*/
#ifndef MMLIBRARY_CCOLLISION_OBJECT_H_
#define MMLIBRARY_CCOLLISION_OBJECT_H_

//***********************************************************
//インクルード
//***********************************************************
#include "CObject.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	当たり判定描画オブジェクト
		class CColliderObject : public IObject
		{
		private:
			CObject&	m_object;
			bool		m_isVisible;
		public:
			//!	引数付きコンストラクタ
			CColliderObject(CObject& object)
				:m_object(object)
			{};

			//!	デストラクタ
			virtual	~CColliderObject(){};

			//!	更新関数
			virtual	void	Update(void) override{};

			//!	描画関数
			virtual	void	Render(void) override
			{
				if (m_object.HasCollider() && IsVisible())
					m_object.Collider().Render(m_object.Transform().GetLocalToWorldMatrix());
			};

			//!	トランスフォーム
			virtual CTransform&	Transform() override
			{
				return const_cast <CTransform&>
					(static_cast<const IObject*>(this)->Transform());
			};

			//!	トランスフォーム
			virtual const CTransform&	Transform() const override
			{
				return m_object.Transform();
			};

			//!	描画するか否かを取得する
			virtual bool IsVisible(void) const override
			{
				return m_isVisible;
			};

			//!	描画するか否かを設定する
			virtual void IsVisible(bool isVisible) override
			{
				m_isVisible = isVisible;
			};

			//!	指定したオブジェクトと衝突しているかを取得する
			virtual bool IsHit(const IObject& another) const override
			{
				return m_object.IsHit(another);
			};

			//!	衝突判定データを持っているか否かを取得する
			virtual bool HasCollider(void) const override
			{
				return m_object.HasCollider();
			};

			//!	衝突判定データへの参照を取得する
			Collision::ICollider& Collider(void) override
			{
				return const_cast <Collision::ICollider&>
					(static_cast<const IObject*>(this)->Collider());
			};

			//!	衝突判定データへの参照を取得する
			virtual const Collision::ICollider& Collider(void) const override
			{
				return m_object.Collider();
			};
		};
	};
};
#endif