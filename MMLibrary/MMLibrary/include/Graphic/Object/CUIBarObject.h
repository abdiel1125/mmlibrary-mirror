/*!	\file		CUIBarObject.h
*	\details	CUIBarObjectクラスの宣言
*	\author		松裏征志
*	\date		2014 / 12 / 08
*/

#ifndef MMLIBRARY_CUI_BAR_OBJECT_H_
#define MMLIBRARY_CUI_BAR_OBJECT_H_

//***********************************************************
//インクルード
//***********************************************************
#include "CObject.h"
#include "CObjectsBinder.h"
#include "Graphic\Color\CColor32.h"
#include "Graphic\Model\CVerticesHasTexture.h"
#include "Graphic\Model\CBillBoard.h"
#include "Graphic\Texture\CTextureBank.h"
#include "Math\CRangeBindedValue.h"
#include "Utility\DefineClassCreateHelpers.h"
#include "Memory\Memory.h"
#include "Utility\CCountableBase.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	体力バーなどの表現に用いるクラス
		template<class T, bool IsBillboard = false, template<class> class Allocator = USE_ALLOCATOR>
		class CUIBarObject : 
			public Utility::CCountableBase<CUIBarObject<T, IsBillboard, Allocator>, 
			CObjectsBinder<IsBillboard, IObject, Allocator>>
		{
		private:
			Math::CRangeBindedValue<T, Math::CDynamicValueRange<T>, Math::tagClamp> m_value;

			void CreateModel(
				CObject& object,
				const CColor32& color,
				const string_t<Allocator>& textureName,
				const string_t<Allocator>& modelName)
			{
				Model::CVerticesHasTexture<CVertexFor2D, Allocator>* pModel =
					Memory::NewWithAllocator <
					Model::CVerticesHasTexture<CVertexFor2D, Allocator>,
					Allocator >();

				pModel->SetByQuads();
				for (auto& vertex : pModel->Vertices())
					vertex.Color() = color;
				pModel->SetVertexData(pModel->Vertices());

				if (textureName.empty() == false)
				pModel->SetTexture(
					CTextureBank<Allocator>::GetInstance().
					Request(textureName));

				Model::CModelBank<Allocator>::GetInstance().
					Register(
					modelName + "-" + std::to_string(IsBillboard) + "-" + std::to_string(GetID()),
					shared_ptr<Model::IModel>(pModel));

				object.SetModel(*pModel);
			};

		public:
			//!	コンストラクタ
			CUIBarObject(
				T startValue,
				T maxValue,
				const CColor32&	backGroundColor,
				const CColor32&	valueColor,
				const string_t<Allocator>& backGroundTextureName = "",
				const string_t<Allocator>& ValueTextureName = "")
				:m_value(startValue)
			{
				m_value.SetMin(0);
				m_value.SetMax(maxValue);

				m_objects.reserve(2);
				std::shared_ptr<IObject> pObject;
				CObject* pCObject;
				pCObject = Memory::NewWithAllocator<CObject, Allocator>();
				pObject.reset(pCObject, Memory::DeleteWithAllocator<CObject, Allocator>);
				m_objects.push_back(pObject);

				CreateModel(
					*pCObject,
					backGroundColor,
					backGroundTextureName,
					"BarBackGround");

				pCObject = Memory::NewWithAllocator<CObject, Allocator>();
				pObject.reset(pCObject, Memory::DeleteWithAllocator<CObject, Allocator>);
				m_objects.push_back(pObject);

				CreateModel(
					*pCObject,
					valueColor,
					ValueTextureName,
					"BarValue");
			};

			//!	値への参照
			CREATE_REFERRENCE(Value, m_value);
			
			//!	描画
			void Render()
			{
				CGraphicManager::GetManager().IsAlphaBlending(true);
				CGraphicManager::GetManager().SetAlphaBlendingState(
					BLEND_INTEGER::SRC_ALPHA,
					BLEND_INTEGER::ONE_MINUS_SRC_ALPHA);

				m_objects[0]->Transform().MoveTo(m_value.GetRatioBetweenMinMax() * 0.5f, 0, 0);
				m_objects[0]->Transform().ScalingTo(1 - m_value.GetRatioBetweenMinMax(), 1, 1);
				m_objects[1]->Transform().MoveTo(-(1 - m_value.GetRatioBetweenMinMax()) * 0.5f, 0, 0);
				m_objects[1]->Transform().ScalingTo(m_value.GetRatioBetweenMinMax(), 1, 1);

				CObjectsBinder<IsBillboard, IObject, Allocator>::Render();
			}
		};
	}
}
#include "Utility\UndefineClassCreateHelpers.h"
#endif	//	MMLIBRARY_CUI_BAR_OBJECT_H_