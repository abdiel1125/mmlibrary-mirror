/*!	\file		CSelectableObject.h
*	\details	CSelectableObjectクラスの宣言
*	\author		松裏征志
*	\date		2014/11/26
*/
#ifndef MMLIBRARY_CSELECTABLE_OBJECT_H_
#define MMLIBRARY_CSELECTABLE_OBJECT_H_

//***********************************************************
//インクルード
//***********************************************************
#include <functional>
#include "CObject.h"
#include "CObjectsBinder.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	選択に関する機能を提供するクラス
		template<class B>
		class CSelectable : public B
		{
		public:
			using myType = CSelectable < B > ;

			//!	コンストラクタ
			template<class... Args>
			CSelectable(Args&&... args)
				: B(args...)
				, m_isSelected(false){};

			//!	更新
			virtual void Update(void) override
			{
				if (IsSelected())
					Selecting();
				else
					Unselecting();
			};

			//!	選択時の処理を実行
			myType& Select(void)
			{
				IsSelected(true);

				if (m_onSelect)
					m_onSelect();

				return *this;
			};

			//!	選択中の処理を実行
			myType&  Selecting(void)
			{
				if (m_onSelecting)
					m_onSelecting();

				return *this;
			};

			//!	選択解除時の処理を実行
			myType&  Unselect(void)
			{
				IsSelected(false);

				if (m_onUnselect)
					m_onUnselect();

				return *this;
			};

			//!	非選択中の処理を実行
			myType&  Unselecting(void)
			{
				if (m_onUnselecting)
					m_onUnselecting();

				return *this;
			};

			//!	選択時の処理を設定
			template<class Function>
			myType&  SetSelectFunction(Function&& function)
			{
				m_onSelect = function;

				return *this;
			};

			//!	選択中の処理を設定
			template<class Function>
			myType&  SetSelectingFunction(Function&& function)
			{
				m_onSelecting = function;

				return *this;
			};

			//!	選択解除時の処理を設定
			template<class Function>
			myType&  SetUnselectFunction(Function&& function)
			{
				m_onUnselect = function;

				return *this;
			};

			//!	非選択中の処理を設定
			template<class Function>
			myType&  SetUnselectingFunction(Function&& function)
			{
				m_onUnselecting = function;

				return *this;
			};

			//!選択されているか否かを返す
			bool IsSelected()
			{
				return m_isSelected;
			};

			//!選択されているか否かを設定する
			void IsSelected(bool isSelect)
			{
				m_isSelected = isSelect;
			};

		private:
			std::function<void(void)> m_onSelect;
			std::function<void(void)> m_onSelecting;
			std::function<void(void)> m_onUnselect;
			std::function<void(void)> m_onUnselecting;

			bool	m_isSelected;
		};

		//!	選択可能な物体クラス
		template<class Object = IObject>
		class CSelectableObject : public CSelectable<Object>
		{
			static_assert(std::is_base_of<IObject, Object>::value, "Object is need base IObject.");

		public:
			//!	コンストラクタ
			template<class... Args>
			CSelectableObject(Args&&... args)
				: CSelectable<Object>(args...){};
		};

		//!	選択可能な物体のリストクラス
		template<class Object = IObject, template<class> class Allocator = USE_ALLOCATOR>
		class CSelectableObjects : public CObjectsBinder<false, CSelectableObject<Object>, Allocator>
		{
		public:
			using myType = CSelectableObjects < Object, Allocator >;
			using myBase = CObjectsBinder < false, CSelectableObject<Object>, Allocator > ;

			//!	コンストラクタ
			template<class... Args>
			CSelectableObjects(Args&&... args)
				: myBase(args...)
				, m_selectedIndex(0)
			{};

			virtual void Update(void) override
			{
				for (auto& selectable : *this)
				{
					selectable->Update();
				}
			};

			//!	次の選択可能な物体に変更
			myType& ToNext()
			{
				m_objects[m_selectedIndex]->Unselect();

				if (m_selectedIndex == m_objects.size() - 1)
					m_selectedIndex = 0;
				else
					++m_selectedIndex;
				
				m_objects[m_selectedIndex]->Select();

				return *this;
			};

			//!	前の選択可能な物体に変更
			myType& ToPrevious()
			{
				m_objects[m_selectedIndex]->Unselect();

				if (m_selectedIndex == 0)
					m_selectedIndex = m_objects.size() - 1;
				else
					--m_selectedIndex;

				m_objects[m_selectedIndex]->Select();
			
				return *this;
			};

			//!	選択中のオブジェクトへの参照を取得する
			pObject_t& GetSelected()
			{
				return m_objects[m_selectedIndex];
			}

			//!	選択中のオブジェクトの番号を取得する
			size_t& GetSelectedIndex()
			{
				return m_selectedIndex;
			}

		private:
			size_t	m_selectedIndex;

		};
	};
};
#endif	//MMLIBRARY_CSELECTABLE_OBJECT_H_