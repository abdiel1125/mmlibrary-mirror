/*!	\file		IObject.h
*	\details	IObjectクラスの宣言
*	\author		松裏征志
*	\date		2014/11/25
*/
#ifndef MMLIBRARY_IOBJECT_H_
#define MMLIBRARY_IOBJECT_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Collision
	{
		class ICollider;
	}

	namespace Graphic
	{
		class CTransform;

		//!	物体クラスの基底クラス
		class IObject
		{
		public:
			//!	デストラクタ
			virtual	~IObject(){};

			//!	更新関数
			virtual	void	Update(void) = 0;

			//!	描画関数
			virtual	void	Render(void) = 0;

			//!	トランスフォーム
			virtual CTransform&	Transform() = 0;

			//!	トランスフォーム
			virtual const CTransform&	Transform() const = 0;

			//!	描画するか否かを取得する
			virtual bool IsVisible(void) const = 0;

			//!	描画するか否かを設定する
			virtual void IsVisible(bool isVisible) = 0;

			//!	指定したオブジェクトと衝突しているかを取得する
			virtual bool IsHit(const IObject& another) const = 0;

			//!	衝突判定データを持っているか否かを取得する
			virtual bool HasCollider(void) const = 0;

			//!	衝突判定データへの参照を取得する
			virtual Collision::ICollider& Collider(void) = 0;

			//!	衝突判定データへの参照を取得する
			virtual const Collision::ICollider& Collider(void) const = 0;
		};
	};
};
#endif