/*!	\file		Object.h
*	\details	Object関連の宣言・インクルード
*	\author		松裏征志
*	\date		2014/12/10
*/
#ifndef MMLIBRARY_OBJECT_H_
#define MMLIBRARY_OBJECT_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Object/IObject.h"
#include "Graphic/Object/CObject.h"
#include "Graphic/Object/CCollisionObject.h"
#include "Graphic/Object/CObjectsBinder.h"
#include "Graphic/Object/CUIBarObject.h"
#include "Graphic/Object/CSelectableObject.h"

#endif	//	MMLIBRARY_OBJECT_H_