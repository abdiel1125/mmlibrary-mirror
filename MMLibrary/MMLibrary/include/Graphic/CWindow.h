/*!	\file		CWindow.h
*	\details	CWindowクラスの宣言
*	\author		松裏征志
*	\date		2014/07/18
*/
#ifndef MMLIBRARY_CWINDOW_H_
#define MMLIBRARY_CWINDOW_H_

//***********************************************************
//インクルード
//***********************************************************
#include	<Windows.h>
#include	<map>
#include	<mutex>
#include	"Graphic\CGraphicManager.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	ウィンドウを表すクラス
		class CWindow
		{
		public:
			//!	描画関数用ポインタの型
			using renderProcedure_t = void(*)(void);

			//!	サイズ変更関数用ポインタの型
			using resizeProcedure_t = void (*)(int w, int h);

			//!	マウス移動関数用ポインタの型
			using mouseMoveProcedure_t = void (*)(int x, int y);

		private:
			friend LRESULT WINAPI WindowProcedure(
				HWND	hWnd,
				UINT	message,
				WPARAM	wParam,
				LPARAM	lParam);

			LRESULT WINAPI WindowProcedureLocal(
				UINT	message,
				WPARAM	wParam,
				LPARAM	lParam);
#ifdef	USING_OPENGL_
		public:
			typedef GLint windowHandle_t;
#endif
		public:
			typedef HWND windowHandle_t;
		private:
static		CWindow*				m_pCurrentWindow;

			WNDCLASSEX				m_wcex;			

			CGraphicManager			m_graphicManager;
			windowHandle_t			m_windowHandle;
			int						m_width;
			int						m_height;
			bool					m_isFullScreen;
			bool					m_isResizable;
			renderProcedure_t		m_pRenderProcedure;
			resizeProcedure_t		m_pResizeProcedure;
			mouseMoveProcedure_t	m_pMouseMoveProcedure;
			bool					m_hasRenderingRequest;
			std::mutex				m_mutex;
			void PrepareCreateWindow(const string_t<>& name);

			void CreateWindowLocal(const string_t<>& title);

			void Render(void);

		public:
			//!	コンストラクタ
			CWindow(
				const string_t<>&	title,
				int					width,
				int					height,
				bool				isFullScreen	= false,
				bool				isResizable		= true);

			//!	デストラクタ
			~CWindow(void);

			//!	フルスクリーンとして設定されているかを取得する
			bool IsFullScreen(void) const
			{
				return m_isFullScreen;
			};

			//!	ウィンドウハンドルを取得する
			windowHandle_t GetWindowHandle(void) const
			{
				return m_windowHandle;
			};

			//!	ウィンドウの幅を取得する
			int	 GetWidth(void) const
			{
				return m_width;
			};

			//!	ウィンドウの高さを取得する
			int	 GetHeight(void) const
			{
				return m_height;
			};
			
			//!	ウィンドウのリスト
static		std::map<windowHandle_t, CWindow*>&	 GetWindowList(void)
			{
				//	ToDo:　小粋なカウンタイディオムを実装するべき
				static std::map<windowHandle_t, CWindow*> windowList;

				return windowList;
			};

			//!	描画更新を要求する
			void PostRenderingRequest(void)
			{
				m_hasRenderingRequest = true;
			};

			//!	全てのウィンドウを描画更新する
static		void RenderAllWindow(void);

			//!	描画用の関数ポインタを登録する
			void SetRenderProcedure(renderProcedure_t pRenderProcedure)
			{
				m_pRenderProcedure = pRenderProcedure;
			};
			
			//!	描画用の関数ポインタを取得する
			renderProcedure_t GetRenderProcedure(void)
			{
				return m_pRenderProcedure;
			};
			
			//!	サイズ変更用の関数ポインタを登録する
			void SetResizeProcedure(resizeProcedure_t pResizeProcedure)
			{
				if(m_isResizable)
					m_pResizeProcedure = pResizeProcedure;
			};
			
			//!	サイズ変更用の関数ポインタを取得する
			resizeProcedure_t GetResizeProcedure(void)
			{
				return m_pResizeProcedure;
			};
			
			//!	マウス移動用の関数ポインタを登録する
			void SetMouseMoveProcedure(mouseMoveProcedure_t pMouseMoveProcedure)
			{
				m_pMouseMoveProcedure = pMouseMoveProcedure;
			};
			
			//!	マウス移動用の関数ポインタを取得する
			resizeProcedure_t GetMouseMoveProcedure(void)
			{
				return m_pMouseMoveProcedure;
			};

			//!	このウィンドウをカレントウィンドウにセットする
			void SetCurrent(void)
			{ 
				if (this != m_pCurrentWindow)
				{
					m_pCurrentWindow = this;
					CGraphicManager::m_pCurrentGraphicManager = &m_graphicManager;
				}
			}
			
			//!	カレントウィンドウを取得する
static		CWindow* GetCurrent(void)
			{ 
				return m_pCurrentWindow;
			}

			//!	メインループを実行する
static		MSG MainLoop(void);

			//!	アンロックされるのを待つ
			void WaitUnlock(void)
			{
				std::lock_guard<std::mutex> lock(m_mutex);
			};
		};
	};
};

#endif