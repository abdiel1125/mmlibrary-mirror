/*!	\file		CFont.cpp
*	\details	CFontNXΜιΎ
*	\author		Ό ͺu
*	\date		2014/04/29
*/
//***********************************************************
//CN[h
//***********************************************************
#include "Graphic/CFont.h"

namespace MMLibrary
{
	namespace Graphic
	{
		UINT GetGGOFormat(FONT_ANTIALIASING_LEVEL level)
		{
			UINT rtn;

			switch (level)
			{
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_1:
				rtn = GGO_BITMAP;
				break;
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_2:
				rtn = GGO_GRAY2_BITMAP;
				break;
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_3:
				rtn = GGO_GRAY4_BITMAP;
				break;
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_4:
				rtn = GGO_GRAY8_BITMAP;
				break;
			}

			return rtn;
		};

		UINT GetGGOMaxGrad(FONT_ANTIALIASING_LEVEL level)
		{
			UINT rtn;

			switch (level)
			{
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_1:
				rtn = 1;
				break;
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_2:
				rtn = 4;
				break;
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_3:
				rtn = 16;
				break;
			case MMLibrary::Graphic::FONT_ANTIALIASING_LEVEL::LEVEL_4:
				rtn = 64;
				break;
			}

			return rtn;
		};
	}
}