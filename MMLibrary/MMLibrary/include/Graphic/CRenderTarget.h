/*!	\file		CRenderTarget.h
*	\details	CRenderTargetクラスの定義
*	\author		松裏征志
*	\date		2015/03/03
*/
#ifndef MMLIBRARY_CRENDER_TARGET_H_
#define MMLIBRARY_CRENDER_TARGET_H_
//===========================================================
//ファイルインクルード
//===========================================================
#include <memory>
#include "Utility/CDoAtDestruct.h"

namespace	MMLibrary
{
	namespace	Graphic
	{
		class CTextureBase;

		//!	描画対象を表すクラスの実装の基底クラス
		class IRenderTargetImpl
		{
		public:
			virtual ~IRenderTargetImpl(){};
			virtual Utility::CDoAtDestruct SetToDevice() = 0;
			virtual void SetByDevice() = 0;
			virtual std::shared_ptr<CTextureBase> GetTexture() = 0;
		};

		//!	描画対象を表すクラス
		class CRenderTarget
		{
		private:
			std::unique_ptr<IRenderTargetImpl> m_pImpl;

		public:
			//!	コンストラクタ
			CRenderTarget(size_t width, size_t height);

			//!	デバイスに設定する
			Utility::CDoAtDestruct SetToDevice();

			//!	デバイスから設定する
			void SetByDevice();

			//!	テクスチャを取得する
			std::shared_ptr<CTextureBase> GetTexture();
		};
	}
}


#endif	//	MMLIBRARY_CRENDER_TARGET_H_