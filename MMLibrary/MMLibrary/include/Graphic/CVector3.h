/*!	\file		CVector3.h
*	\details	CVector3クラスの宣言
*	\author		松裏征志
*	\date		2014/04/25
*/
#ifndef MMLIBRARY_CVECTOR3_H_
#define MMLIBRARY_CVECTOR3_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Math/CVector.h"
#include "Math\Function.h"

namespace MMLibrary
{
	namespace Graphic
	{
		class CMatrix4x4;

		//!	3次元ベクトルを表現するクラス
		using CVector3 = Math::CVector < float, 3 > ;

		//!	3次元ベクトルに関する関数を提供するクラス
		class CVector3Helper
		{
		public:
			//!コンストラクタ
			CVector3Helper(CVector3& vector)
				:m_vector(vector){}

			
			//!	オブジェクト空間からスクリーン空間にベクトルを射影する。
			/*!
			*	\param	[in]	viewport	ビューポートを表す	viewport_t構造体への参照
			*	\param	[in]	projection	射影行列を表す		CMatrix4x4クラスへの参照
			*	\param	[in]	view		ビュー行列を表す	CMatrix4x4クラスへの参照
			*	\param	[in]	world		ワールド行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*/
			CVector3& Project(
				const CMatrix4x4&	viewport,
				const CMatrix4x4&	projection,
				const CMatrix4x4&	view,
				const CMatrix4x4&	world)
			{
				return Project(
					m_vector,
					viewport,
					projection,
					view,
					world);
			}

			//!	スクリーン空間からオブジェクト空間にベクトルを射影する。
			/*!
			*	\param	[in]	viewport	ビューポートを表す	viewport_t構造体への参照
			*	\param	[in]	projection	射影行列を表す		CMatrix4x4クラスへの参照
			*	\param	[in]	view		ビュー行列を表す	CMatrix4x4クラスへの参照
			*	\param	[in]	world		ワールド行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*/
			CVector3& UnProject(
				const CMatrix4x4&	viewport,
				const CMatrix4x4&	projection,
				const CMatrix4x4&	view,
				const CMatrix4x4&	world)
			{
				return UnProject(
					m_vector,
					viewport,
					projection,
					view,
					world);
			}

			//!	指定された行列によりベクトル (x, y, z, 1) をトランスフォームする。
			/*!
			*	\param	[in]	matrix		行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*
			*	向きではなく、位置のトランスフォームを求める際に使う。
			*/
			CVector3& TransformCoord(
				const CMatrix4x4&	matrix)
			{
				return TransformCoord(
					m_vector,
					matrix);
			}


			//!	指定された行列によりベクトル (x, y, z, 0) をトランスフォームする。
			/*!
			*	\param	[in]	matrix		行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*
			*	位置ではなく、向きのトランスフォームを求める際に使う。
			*/
			CVector3& TransformNormal(
				const CMatrix4x4&	matrix)
			{
				return TransformNormal(
					m_vector,
					matrix);
			}

			//!	制限付きで回転させる
			CVector3& RotateReg(
				const CVector3& axis,
				float			radian,
				const CVector3& min,
				const CVector3& max)
			{
				return RotateReg(
					m_vector,
					axis,
					radian,
					min,
					max);
			}

#pragma region static
			//!	オブジェクト空間からスクリーン空間にベクトルを射影する。
			/*!
			*	\param	[in]	viewport	ビューポートを表す	viewport_t構造体への参照
			*	\param	[in]	projection	射影行列を表す		CMatrix4x4クラスへの参照
			*	\param	[in]	view		ビュー行列を表す	CMatrix4x4クラスへの参照
			*	\param	[in]	world		ワールド行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*/
			static CVector3& Project(
				CVector3&		vector,
				const CMatrix4x4&	viewport,
				const CMatrix4x4&	projection,
				const CMatrix4x4&	view,
				const CMatrix4x4&	world);

			//!	スクリーン空間からオブジェクト空間にベクトルを射影する。
			/*!
			*	\param	[in]	viewport	ビューポートを表す	viewport_t構造体への参照
			*	\param	[in]	projection	射影行列を表す		CMatrix4x4クラスへの参照
			*	\param	[in]	view		ビュー行列を表す	CMatrix4x4クラスへの参照
			*	\param	[in]	world		ワールド行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*/
			static CVector3& UnProject(
				CVector3&		vector,
				const CMatrix4x4&	viewport,
				const CMatrix4x4&	projection,
				const CMatrix4x4&	view,
				const CMatrix4x4&	world);

			//!	指定された行列によりベクトル (x, y, z, 1) をトランスフォームする。
			/*!
			*	\param	[in]	matrix		行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*
			*	向きではなく、位置のトランスフォームを求める際に使う。
			*/
			static CVector3& TransformCoord(
				CVector3&		vector,
				const CMatrix4x4&	matrix);

			//!	指定された行列によりベクトル (x, y, z, 0) をトランスフォームする。
			/*!
			*	\param	[in]	matrix		行列を表す	CMatrix4x4クラスへの参照
			*
			*	\return	演算後の自身への参照
			*
			*	位置ではなく、向きのトランスフォームを求める際に使う。
			*/
			static CVector3& TransformNormal(
				CVector3&		vector,
				const CMatrix4x4&	matrix);

			//!	制限付きで回転させる
			static CVector3& RotateReg(
				CVector3&		vector,
				const CVector3& axis,
				float			radian,
				const CVector3& min,
				const CVector3& max);

			//!	上方向ベクトルを取得する
			static CVector3 GetUp()
			{
				return CVector3{ 0, 1, 0 };
			}
			//!	下方向ベクトルを取得する
			static CVector3 GetDown()
			{
				return CVector3{ 0, -1, 0 };
			}
			//!	右方向ベクトルを取得する
			static CVector3 GetRight()
			{
				return CVector3{ 1, 0, 0 };
			}
			//!	左方向ベクトルを取得する
			static CVector3 GetLeft()
			{
				return CVector3{ -1, 0, 0 };
			}
			//!	前方向ベクトルを取得する
			static CVector3 GetFront()
			{
				return CVector3{ 0, 0, 1 };
			}
			//!	後ろ方向ベクトルを取得する
			static CVector3 GetBack()
			{
				return CVector3{ 0, 0, -1 };
			}
#pragma endregion
			
		private:
			CVector3& m_vector;

			static Math::CMatrix<float, 1, 4> ToRowMatrix(
				CVector3&		vector);
		};
	};
};

#endif