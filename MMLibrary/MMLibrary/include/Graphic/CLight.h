/*!	\file		CLight.h
*	\details	CLightクラスの宣言
*	\author		松裏征志
*	\date		2014/06/27
*/
#ifndef MMLIBRARY_CLIGHT_H_
#define MMLIBRARY_CLIGHT_H_

//***********************************************************
//インクルード
//***********************************************************
#include	"MMLibraryDefine.h"
#include	"Graphic/CVector3.h"
#include	"Graphic/CMatrix4x4.h"
#include	"Graphic/Color/CColor32.h"
#include	"Graphic\CGraphicManager.h"

namespace	MMLibrary
{
	namespace	Graphic
	{
		/*!
		*	\brief	ライトを表現するクラス
		*/
		class	CLight
		{
		public:
			enum LIGHTTYPE 
			{
				LIGHT_POINT			= 1,
				LIGHT_SPOT			= 2,
				LIGHT_DIRECTIONAL	= 3,
				LIGHT_FORCE_DWORD	= 0x7fffffff
			};
		private:
			LIGHTTYPE				m_type;
			array_t<float, 4>	m_diffuse;
			array_t<float, 4>	m_specular;
			array_t<float, 4>	m_ambient;
			CVector3				m_position;
			CVector3				m_direction;			
			float					m_range;
			float					m_falloff;
			float					m_attenuation0;
			float					m_attenuation1;
			float					m_attenuation2;
			float					m_theta;
			float					m_phi;
			uint_t			m_id;

		public:
			/*!
			*	\brief	コンストラクタ
			*/
			CLight(void);

			LIGHTTYPE		GetType			(void) const
			{return m_type;};
			CColor32		GetDiffuse		(void) const;
			CColor32		GetAmbient		(void) const;
			CColor32		GetSpecular		(void) const;
			CVector3		GetPosition		(void) const
			{return m_position;};
			CVector3		GetDirection	(void) const
			{return m_direction;};
			float			GetRange		(void) const
			{return m_range;};
			float			GetFalloff		(void) const
			{return m_falloff;};
			float			GetAttenuation0	(void) const
			{return m_attenuation0;};
			float			GetAttenuation1	(void) const
			{return m_attenuation1;};
			float			GetAttenuation2	(void) const
			{return m_attenuation2;};
			float			GetTheta		(void) const
			{return m_theta;};
			float			GetPhi			(void) const
			{return m_phi;};
			uint_t	GetId			(void) const;
			bool			IsEnable		(void) const;
							
			CLight&		SetType			(const LIGHTTYPE type)
			{m_type = type;return *this;};
			CLight&		SetDiffuse		(const CColor32& diffuse);
			CLight&		SetDiffuse		(
				const float r, 
				const float g, 
				const float b, 
				const float a)
			{
				m_diffuse.at(0) = r;
				m_diffuse.at(1) = g;
				m_diffuse.at(2) = b;
				m_diffuse.at(3) = a;
				return *this;
			};
			CLight&		SetSpecular		(const CColor32& specular);
			CLight&		SetSpecular		(
				const float r, 
				const float g, 
				const float b, 
				const float a)
			{
				m_specular.at(0) = r;
				m_specular.at(1) = g;
				m_specular.at(2) = b;
				m_specular.at(3) = a;
				return *this;
			};
			CLight&		SetAmbient		(const CColor32& ambient);
			CLight&		SetAmbient		(
				const float r, 
				const float g, 
				const float b, 
				const float a)
			{
				m_ambient.at(0) = r;
				m_ambient.at(1) = g;
				m_ambient.at(2) = b;
				m_ambient.at(3) = a;
				return *this;
			};
			CLight&		SetPosition		(
				const CVector3& position)
			{	
				m_position = position;
				return *this;
			};
			CLight&		SetPosition		(
				const float x,
				const float y,
				const float z)
			{
				m_position.at(0) = x;
				m_position.at(1) = y;
				m_position.at(2) = z;
				return *this;
			};
			CLight&		Move			(const CVector3& vector)
			{
				m_position += vector;
				return *this;
			};
			CLight&		Move			(
				const float x,
				const float y,
				const float z)
			{
				m_position.at(0) += x;
				m_position.at(1) += y;
				m_position.at(2) += z;
				return *this;
			};
			CLight&		SetDirection	(const CVector3& direction)
			{
				m_direction = direction;
				return *this;
			};
			CLight&		SetDirection	(
				const float x,
				const float y,
				const float z)
			{
				m_direction.at(0) = x;
				m_direction.at(1) = y;
				m_direction.at(2) = z;
				return *this;
			};
			CLight&		Rotate			(const Math::CQuaternion<float> rotation)
			{
				CVector3Helper::TransformNormal( 
					m_direction,
					CMatrix4x4::CreateByRotationQuaternion(rotation));
				return *this;
			};
			CLight&		SetRange		(const float range)
			{
				m_range = range;
				return *this;
			};
			CLight&		SetFalloff		(const float falloff)
			{
				m_falloff = falloff;
				return *this;
			};
			CLight&		SetAttenuation	(	
				const float constantAttenuation,
				const float linearAttenuation,
				const float quadraticAttenuation);
			CLight&		SetTheta		(const float theta)
			{
				m_theta = theta;
				return *this;
			};
			CLight&		SetPhi			(const float phi)
			{
				m_phi = phi;
				return *this;
			};
			CLight&		SetId			(const uint_t id);
			CLight&		IsEnable		(const bool isEnable);

			const CLight&	SetToDevice	(void) const;
		};
	};
};

#endif