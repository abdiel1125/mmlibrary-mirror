/*!	\file		Graphic.h
*	\details	Graphic関連の宣言・インクルード
*	\author		松裏征志
*	\date		2014/04/25
*/
#ifndef MMLIBRARY_GRAPHIC_H_
#define MMLIBRARY_GRAPHIC_H_

//***********************************************************
//共通の構造体等の定義
//***********************************************************
namespace MMLibrary
{
	/*!	\namespace	MMLibrary::Graphic
	*	\brief	MMLibraryの描画に関わる要素はこの名前空間に属する
	*/
	namespace Graphic
	{
	};
};

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/CWindow.h"
#include "Graphic/CMatrix4x4.h"
#include "Graphic/CVector3.h"
#include "Graphic/Color/Color.h"
#include "Graphic/Model/Model.h"
#include "Graphic/Object/Object.h"
#include "Graphic/CGraphicManager.h"
#include "Graphic/Texture/CTextureBank.h"
#include "Graphic/Texture/LoadTexture.h"
#include "Graphic/CTransform.h"
#include "Graphic/Object/CObject.h"
#include "Graphic/CParticleEmitter.h"
#include "Graphic/CCamera.h"
#include "Graphic/CLight.h"
#include "Graphic/CBuffer.h"
#include "Graphic/CFont.h"

#ifdef	USING_OPENGL
#	ifdef _DEBUG
#		pragma comment(lib,"MMLibraryGL_Debug.lib")	//DEBUGのときのライブラリ
#include "glext.h"
#	else
#		pragma comment(lib,"MMLibraryGL.lib")			//RELEASEのときのライブラリ
#	endif
#endif

#ifdef	USING_DIRECTX
#	ifdef _DEBUG
#		pragma comment(lib,"MMLibraryDX_Debug.lib")	//DEBUGのときのライブラリ
#	else
#		pragma comment(lib,"MMLibraryDX.lib")			//RELEASEのときのライブラリ
#	endif
#endif
#endif