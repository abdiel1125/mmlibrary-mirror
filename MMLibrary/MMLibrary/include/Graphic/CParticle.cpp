/*!	\file		CParticle.h
*	\details	CParticleクラスの定義
*	\author		松裏征志
*	\date		2014/09/02
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CParticle.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CParticle::CParticle(void)
			:m_speed(0)
			,m_life(0)
			,m_pParticleControler(nullptr)
			,CObject()
		{};
			
		void	CParticle::Update(void)
		{
			m_life--;

			if(m_pParticleControler)
				m_pParticleControler(*this);

			Transform().Move(m_speed);
		};

		void	CParticle::Render(void)
		{
			if (m_life > 0)
				CObject::Render();
		};
	};
};