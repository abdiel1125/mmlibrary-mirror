/*!	\file		DirectXHelper.h
*	\details	DirectXの機能を補助するクラス・関数の宣言
*	\author		松裏征志
*	\date		2014/11/16	作成
*/
#ifndef MMLIBRARY_DIRECTXHELPER_H_
#define MMLIBRARY_DIRECTXHELPER_H_
#ifdef USING_DIRECTX

//***********************************************************
//インクルード
//***********************************************************
#include <assert.h>

namespace	MMLibrary
{
	namespace Graphic
	{
		//!	DirectXのエラーチェック関数
		bool CheckDirectXErrer(HRESULT errerCode);
	}
}
#endif	//USING_DIRECTX
#endif	//MMLIBRARY_DIRECTXHELPER_H_