/*!	\file		Texture.h
*	\details	Texture関連の宣言
*	\author		松裏征志
*	\date		2014/12/12
*/
#ifndef MMLIBRARY_TEXTURE_H_
#define MMLIBRARY_TEXTURE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Graphic/Other.h"

namespace MMLibrary
{
	namespace Graphic
	{
		class CColor32;

		//!	画像データからテクスチャを作成する関数
		textureHandle_t CreateTexture(
			CColor32* ptr,
			size_t	width,
			size_t	height);
	}
}

#endif	//MMLIBRARY_TEXTURE_H_