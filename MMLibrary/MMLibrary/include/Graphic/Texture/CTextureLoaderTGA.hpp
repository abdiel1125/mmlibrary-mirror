/*!	\file		CTextureLoaderTGA.cpp
*	\details	CTextureLoaderTGAクラスの定義
*	\author		松裏征志
*	\date		2014/06/12
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Texture/CTextureLoaderTGA.h"


namespace MMLibrary
{
	namespace Graphic
	{
		template<template<class> class Allocator>
		void CTextureLoaderTGA<Allocator>::LoadImageDataGrayScale(void)
		{
			std::copy(
				m_memory.begin() + 18,
				m_memory.begin() + 18 + m_image.size(),
				m_image.begin());
		};

		template<template<class> class Allocator>
		void CTextureLoaderTGA<Allocator>::LoadImageDataGrayScaleRLE(void)
		{
			uchar_t	command;
			int				cnt;
			int				isCompress;
			auto			memoryIt = m_memory.begin() + 18;
			auto			imageIt = m_image.begin();

			while (memoryIt != m_memory.end())
			{
				command = *memoryIt;
				++memoryIt;
				cnt = (command & 0x7f) + 1;
				isCompress = (command & 0x80);

				if (isCompress)
				{
					uchar_t pixel = *memoryIt;
					++memoryIt;

					for (int i = 0; i < cnt; ++i)
					{
						*imageIt = pixel;
						++imageIt;
					}
				}
				else
				{
					for (int i = 0; i < cnt; ++i)
					{
						*imageIt = *memoryIt;
						++memoryIt;
						++imageIt;
					}
				}
			}
		};

		template<template<class> class Allocator>
		void CTextureLoaderTGA<Allocator>::LoadImageDataFullColor(void)
		{
			std::copy(
				m_memory.begin() + 18,
				m_memory.begin() + 18 + m_image.size(),
				m_image.begin());
		};

		template<template<class> class Allocator>
		void CTextureLoaderTGA<Allocator>::LoadImageDataFullColorRLE(void)
		{
			uchar_t	command;
			int				cnt;
			int				isCompress;
			auto			memoryIt = m_memory.begin() + 18;
			auto			imageIt = m_image.begin();
			uchar_t	pixel[4];
			int				interval = m_colorDepth / 8;

			while (imageIt != m_image.end())
			{
				command = *memoryIt;
				++memoryIt;
				cnt = (command & 0x7f) + 1;
				isCompress = (command & 0x80);

				if (isCompress)
				{
					for (int i = 0; i < interval; ++i)
					{
						pixel[i] = *memoryIt;
						++memoryIt;
					}

					for (int i = 0; i < cnt; ++i)
					{
						for (int j = 0; j < interval; ++j)
						{
							*imageIt = pixel[j];
							++imageIt;
						}
					}
				}
				else
				{
					std::copy(
						memoryIt,
						memoryIt + cnt * interval,
						imageIt);

					imageIt += cnt * interval;
					memoryIt += cnt * interval;
				}
			}
		};

		template<template<class> class Allocator>
		void CTextureLoaderTGA<Allocator>::OrderModification(void)
		{
			uint_t interval = m_colorDepth / 8;
			uchar_t	attribute = m_memory.at(17);

			if (attribute & (1 << 4))
			{
				for (int y = 0;
					y < m_height;
					++y)
				{
					for (int x = 0;
						x < ((m_width + 1) / 2);
						++x)
					{
						for (uint_t i = 0; i < interval; ++i)
						{
							std::swap(m_image[(y * m_width + x) * interval + i],
								m_image[(y * m_width + (m_width - 1 - x)) * interval + i]);
						}
					}
				}
			}

			if (!(attribute & (1 << 5)))
			{
				for (int y = 0;
					y < ((m_height + 1) / 2);
					++y)
				{
					for (int x = 0;
						x < m_width;
						++x)
					{
						for (unsigned i = 0; i < interval; ++i)
						{
							std::swap(m_image[(y * m_width + x) * interval + i],
								m_image[((m_height - 1 - y) * m_width + x) * interval + i]);
						}
					}
				}
			}
		};

		template<template<class> class Allocator>
		void CTextureLoaderTGA<Allocator>::ColorOrderModification(void)
		{
			uint_t interval = m_colorDepth / 8;

			for (auto it = m_image.begin();
				it != m_image.end();
				it += interval)
			{
				std::swap(*it, *(it + 2));
			}
		};

		template<template<class> class Allocator>
		textureHandle_t CTextureLoaderTGA<Allocator>::LoadTextureFromFile(
			const string_t<Allocator>& filePath)
		{
			Utility::LoadFileTo<uchar_t, Allocator>(filePath, m_memory, false);

			GetFormat();

			LoadTextureFromMemory();

			return InitTexture();
		};

		template<template<class> class Allocator>
		void CTextureLoaderTGA<Allocator>::LoadTextureFromMemory(void)
		{
			uchar_t	imageType = m_memory[2];

			m_colorDepth = m_memory[16];
			m_width = reinterpret_cast<ushort_t&>(m_memory[12]);
			m_height = reinterpret_cast<ushort_t&>(m_memory[14]);

			switch (imageType)
			{
			case 2:
				m_image.resize(m_height * m_width * m_colorDepth / 8);
				LoadImageDataFullColor();
				break;
			case 3:
				m_image.resize(m_height * m_width);
				LoadImageDataGrayScale();
				break;
			case 10:
				m_image.resize(m_height * m_width * m_colorDepth / 8);
				LoadImageDataFullColorRLE();
				break;
			case 11:
				m_image.resize(m_height * m_width);
				LoadImageDataGrayScaleRLE();
				break;
			};

			OrderModification();
			ColorOrderModification();
		};

#ifdef	USING_OPENGL

		template<template<class> class Allocator>
		int CTextureLoaderTGA<Allocator>::GetFormat(void)
		{
			uchar_t	imageType =
				m_memory.at(2);

			m_colorDepth = m_memory.at(16);

			if (imageType == 3 || imageType == 11)
			{
				m_format = GL_LUMINANCE;
			}
			else
			{
				if (m_colorDepth == 32)
					m_format = GL_RGBA;
				else if (m_colorDepth == 24)
					m_format = GL_RGB;
			}

			return m_format;
		};

		template<template<class> class Allocator>
		textureHandle_t CTextureLoaderTGA<Allocator>::InitTexture(void)
		{
			textureHandle_t textureHandle;

			glGenTextures(1, &textureHandle);
			glBindTexture(GL_TEXTURE_2D, textureHandle);
			glPixelStorei(
				GL_UNPACK_ALIGNMENT,
				m_colorDepth == 32 ? 4 : 1);
			glTexParameteri(
				GL_TEXTURE_2D,
				GL_TEXTURE_WRAP_S,
				GL_CLAMP);
			glTexParameteri(
				GL_TEXTURE_2D,
				GL_TEXTURE_WRAP_T,
				GL_CLAMP);
			glTexParameteri(
				GL_TEXTURE_2D,
				GL_TEXTURE_MAG_FILTER,
				GL_LINEAR);
			glTexParameteri(
				GL_TEXTURE_2D,
				GL_TEXTURE_MIN_FILTER,
				GL_LINEAR);
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				m_format,
				m_width,
				m_height,
				0,
				m_format,
				GL_UNSIGNED_BYTE,
				&m_image.at(0));

			return textureHandle;
		};
#endif
	};
};