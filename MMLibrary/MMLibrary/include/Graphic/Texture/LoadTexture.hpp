/*!	\file		LoadTexture.cpp
*	\details	LoadTexture関数の宣言
*	\author		松裏征志
*	\date		2014/05/28
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Texture/LoadTexture.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!テクスチャをロードする関数
		template<template<class> class Allocator>
		textureHandle_t LoadTextureFromFile(
			const string_t<Allocator>& filePath)
		{
			textureHandle_t textureHandle = 0;
#ifdef	USING_OPENGL
			auto fnex = Utility::GetFileExtention(filePath);
			//拡張子を取得する

			if(	fnex == "tga")
				textureHandle = 
				CTextureLoaderTGA<Allocator>().
						LoadTextureFromFile(
						filePath);

#endif
#ifdef	USING_DIRECTX

			if(	FAILED(D3DXCreateTextureFromFile(
				CGraphicManager::GetManager().GetDevice(),
				filePath.c_str(),
				&textureHandle)))
			{
				string_t<Allocator> message = filePath;

				message += "の読み込みに失敗しました。";

				MessageBox(nullptr,message.c_str(),"テクスチャの読み込みエラー",MB_OK);
				textureHandle = nullptr;
			}
#endif
			return textureHandle;
		};
	};
};