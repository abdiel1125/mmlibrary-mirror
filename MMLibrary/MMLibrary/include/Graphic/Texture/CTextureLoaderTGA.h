/*!	\file		CTextureLoaderTGA.h
*	\details	CTextureLoaderTGAクラスの宣言
*	\author		松裏征志
*	\date		2014/06/12
*/
#ifndef MMLIBRARY_CTEXTURE_LOADER_TGA_H_
#define MMLIBRARY_CTEXTURE_LOADER_TGA_H_

//***********************************************************
//インクルード
//***********************************************************
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include "MMLibraryDefine.h"
#include "Graphic\CGraphicManager.h"

namespace MMLibrary
{
	namespace Graphic
	{
		/*!
		*	\brief	テクスチャをTGAファイルからロードするクラス
		*/
		template<template<class> class Allocator = USE_ALLOCATOR>
		class CTextureLoaderTGA
		{
		private:
			int								m_width;
			int								m_height;
			int								m_format;
			uchar_t							m_colorDepth;
			vector_t< uchar_t , Allocator>	m_image;
			vector_t< uchar_t , Allocator>	m_memory;

			int GetFormat(void);

			void LoadTextureFromMemory(void);
			
			textureHandle_t InitTexture(void);
			
			void LoadImageDataGrayScale(void);

			void LoadImageDataGrayScaleRLE(void);
			
			void LoadImageDataFullColor(void);
			
			void LoadImageDataFullColorRLE(void);
			
			void OrderModification(void);

			void ColorOrderModification(void);
		public:
			textureHandle_t LoadTextureFromFile(
				const string_t<Allocator>& filePath);
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Texture/CTextureLoaderTGA.hpp"

#endif