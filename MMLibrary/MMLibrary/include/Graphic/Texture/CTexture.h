/*!	\file		CTexture.h
*	\details	CTextureクラスの宣言
*	\author		松裏征志
*	\date		2014/12/12
*/
#ifndef MMLIBRARY_CTEXTURE_H_
#define MMLIBRARY_CTEXTURE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Graphic/CGraphicManager.h"
#include "Utility/DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	色データのフォーマットを表す
		enum class COLOR_FORMAT
		{
			GRAY8,
			R8G8B8,
			R8G8B8A8,
			DEPTH
		};

		//!	テクスチャの種類を表す
		enum class TEXTURE_TYPE
		{
			DEFAULT,
			RENDER_TARGET,
			DYNAMIC
		};

		//!	テクスチャを表すクラスの基底クラス
		class CTextureBase
		{
		protected:
			textureHandle_t		m_handle;

		public:
			//! コンストラクタ
			CTextureBase();

			//! コンストラクタ
			CTextureBase(
				size_t			width,
				size_t			height,
				TEXTURE_TYPE	type = TEXTURE_TYPE::DEFAULT,
				COLOR_FORMAT	colorFormat = COLOR_FORMAT::R8G8B8A8);

			//!	ムーブコンストラクタ
			CTextureBase(CTextureBase&& other);

			//!	デストラクタ
			~CTextureBase();

			//!	テクスチャを破棄する
			void Dispose(void);

			//!	テクスチャハンドルを取得する
			CREATE_GETTER_PRIMITIVE(TextureHandle, m_handle);

			//!	色配列からテクスチャを作成する
			CTextureBase& SetByColorArray(
				CColor32* pointer,
				size_t	width,
				size_t	height);

			//!	指定サイズのテクスチャを作成する
			CTextureBase& SetBySize(
				size_t			width,
				size_t			height,
				TEXTURE_TYPE	type = TEXTURE_TYPE::DEFAULT,
				COLOR_FORMAT	colorFormat = COLOR_FORMAT::R8G8B8A8);

			//!	デバイスにセットする
			void SetToDevice(uint_t textureUnitID) const;

			//!	デバイスから外す
			static void UnSetFromDevice(uint_t textureUnitID);
		};

		//!	テクスチャを表すクラス
		template<template<class> class Allocator = USE_ALLOCATOR>
		class CTexture : public CTextureBase
		{
		private:
			string_t<Allocator> m_name;

		public:
			//! コンストラクタ
			CTexture()
				: CTextureBase()
				, m_name()
			{};

			//! コンストラクタ
			CTexture(
				const string_t<Allocator>& name,
				CColor32* pointer,
				size_t	width,
				size_t	height)
				: m_name(name)
			{
				SetBySize(width, height);
				SetByColorArray(pointer, width, height);
			};

			CREATE_GETTER(Name, m_name);

			//!	色配列からテクスチャを作成する
			static CTexture CreateByArray(
				const string_t<Allocator>& name,
				CColor32* pointer,
				size_t	width,
				size_t	height)
			{
				return CTexture().SetByArray(name, pointer, width, height);
			};

			//!	色配列からテクスチャを作成する
			CTexture& SetByArray(
				const string_t<Allocator>& name,
				CColor32* pointer,
				size_t	width,
				size_t	height)
			{
				m_name = name;
				SetBySize(width, height);
				SetByColorArray(pointer, width, height);

				return *this;
			};

			//!	テクスチャをロードする
			CTexture& SetByLoadFromFile(const string_t<Allocator>& filePath)
			{
				if (filePath.empty()) return *this;

				m_name = filePath;
#ifdef	USING_OPENGL
				//拡張子を取得する
				string_t<Allocator> fnex = Utility::GetFileExtention(filePath);

				//拡張子を小文字化する
				for (auto ite = fnex.begin();
					ite != fnex.end();
					++ite)
					*ite = tolower(*ite);

				if (fnex == "tga")
					m_handle =
					CTextureLoaderTGA<Allocator>().
					LoadTextureFromFile(
					filePath);

#endif
#ifdef	USING_DIRECTX

				if (FAILED(D3DXCreateTextureFromFile(
					CGraphicManager::GetManager().GetDevice(),
					filePath.c_str(),
					&m_handle)))
				{
					string_t<Allocator> message = filePath;

					message += "の読み込みに失敗しました。";

					MessageBox(nullptr, message.c_str(), "テクスチャの読み込みエラー", MB_OK);
					m_handle = nullptr;
				}
#endif
				return *this;
			}
		};
	}
}
#include "Utility/UndefineClassCreateHelpers.h"
#endif	//	MMLIBRARY_CTEXTURE_H_