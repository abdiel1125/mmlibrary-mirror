/*!	\file		CTextureBank.h
*	\details	CTextureBankクラスの宣言
*	\author		松裏征志
*	\date		2014/05/21
*/
#ifndef MMLIBRARY_CTEXTURE_BANK_H_
#define MMLIBRARY_CTEXTURE_BANK_H_

//***********************************************************
//インクルード
//***********************************************************
#include <map>
#include <string>
#include "MMLibraryDefine.h"
#include "Graphic/CGraphicManager.h"
#include "Math/CVector.h"
#include "Graphic/Texture/LoadTexture.h"
#include "Graphic/Texture/CTexture.h"
#include "Utility\CSingletonBase.h"
#include "Utility\CDirectionaryBase.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	テクスチャの取得、保存を行うシングルトンなクラス
		template<template<class> class Allocator = USE_ALLOCATOR>
		class CTextureBank : 
			public Utility::CSingletonBase<CTextureBank<Allocator> >,
			public Utility::CDirectionaryBase<std::shared_ptr<CTexture<Allocator> >, Allocator>
		{
		private:
		public:
			//!	コンストラクタ
			CTextureBank()
				:Utility::CDirectionaryBase<std::shared_ptr<CTexture<Allocator> >, Allocator>(
				[](const string_t<Allocator>& name)
			{
				std::shared_ptr<CTexture<Allocator>> rtn = std::make_shared<CTexture<Allocator>>();
				rtn->SetByLoadFromFile(name);
				return rtn;
			})
			{};
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
//#include	"Graphic/Texture/CTextureBank.hpp"

#endif