/*!	\file		LoadTexture.h
*	\details	LoadTexture関数の宣言
*	\author		松裏征志
*	\date		2014/05/28
*/
#ifndef MMLIBRARY_LOAD_TEXTURE_H_
#define MMLIBRARY_LOAD_TEXTURE_H_

//***********************************************************
//インクルード
//***********************************************************
#include <string>
#include <iostream>
#include <fstream>
#include "MMLibraryDefine.h"
#include "Graphic/CGraphicManager.h"
#include "Graphic/Texture/CTextureLoaderTGA.h"

namespace MMLibrary
{
	namespace Graphic
	{
		/*!
		*	\brief	テクスチャをファイルからロードする関数
		*/
		template<template<class> class Allocator = USE_ALLOCATOR>
		textureHandle_t LoadTextureFromFile(const char* pFilePath);
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Texture/LoadTexture.hpp"

#endif