///*!	\file		CTextureBank.h
//*	\details	CTextureBankクラスの宣言
//*	\author		松裏征志
//*	\date		2014/05/21
//*/
//
////***********************************************************
////インクルード
////***********************************************************
//#include "Graphic/Texture/CTextureBank.h"
//
//namespace MMLibrary
//{
//	namespace Graphic
//	{
//#ifdef	USING_OPENGL
//
//		template<template<class> class Allocator>
//		void	CTextureBank<Allocator>::DisposeTexture(
//			const string_t<Allocator>&		name)
//		{
//			auto ite = m_list.find(name);
//			if(ite != m_list.end())
//			{
//				glDeleteTextures(1, &ite->second);
//			}
//		};
//
//		template<template<class> class Allocator>
//		void	CTextureBank<Allocator>::DisposeAllTexture(void)
//		{};
//
//		template<template<class> class Allocator>
//		Math::CVector<int, 2>	CTextureBank<Allocator>::GetTextureSize(
//			textureHandle_t	handle)
//		{
//			Math::CVector<int, 2> rtn;
//
//			glGetTexEnviv(
//				GL_TEXTURE_ENV, 
//				GL_TEXTURE_WIDTH,
//				&rtn.at(0));
//
//			glGetTexEnviv(
//				GL_TEXTURE_ENV, 
//				GL_TEXTURE_HEIGHT,
//				&rtn.at(1));
//
//			return rtn;
//		};
//#endif
//#ifdef	USING_DIRECTX			
//		template<template<class> class Allocator>
//		void				CTextureBank<Allocator>::DisposeTexture(
//			const string_t<Allocator>&		name)
//		{
//			SafeRelease(m_list[name]);
//
//			m_list.erase(m_list.find(name));
//		};
//
//		template<template<class> class Allocator>
//		void				CTextureBank<Allocator>::DisposeAllTexture(void)
//		{
//			for (std::map<string_t<Allocator>, textureHandle_t>::iterator it = m_list.begin();
//				it != m_list.end();
//				it++)
//			{
//				Utility::SafeRelease((*it).second);
//			}
//			m_list.clear();
//		};
//
//		template<template<class> class Allocator>
//		Math::CVector<int, 2>	CTextureBank<Allocator>::GetTextureSize(
//			textureHandle_t	handle)
//		{
//			Math::CVector<int, 2>	rtn;
//
//			// テクスチャサイズの取得
//			D3DSURFACE_DESC desc;
//
//			if (FAILED(handle->GetLevelDesc(0, &desc)))
//			{
//				MessageBox(nullptr, "テクスチャサイズの取得に失敗",
//					"テクスチャサイズの取得に失敗しました", MB_OK);
//			}
//
//			rtn.at(0) = desc.Width;
//			rtn.at(1) = desc.Height;
//
//			return rtn;
//		};
//#endif
//		template<template<class> class Allocator>
//		CTextureBank<Allocator>::~CTextureBank(void)
//		{
//			DisposeAllTexture();
//		};
//
//		template<template<class> class Allocator>
//		textureHandle_t		CTextureBank<Allocator>::RequestTexture(
//			const string_t<Allocator>&	name)
//		{
//			//ファイルパスチェック
//			if (name == "")
//			{
//				return NULL;
//			}
//
//			if (m_list.find(name) == m_list.end())
//			{
//				//テクスチャの読み込み
//				textureHandle_t textureHandle;
//
//				textureHandle = LoadTextureFromFile<Allocator>(
//					name.c_str());
//
//				//テクスチャの保存
//				m_list.insert(
//					std::map<string_t<Allocator>, textureHandle_t>::
//					value_type(name, textureHandle));
//
//				return textureHandle;
//			}
//			else
//			{
//				return m_list[name];
//			}
//		};
//
//		template<template<class> class Allocator>
//		bool				CTextureBank<Allocator>::RegisterTexture(
//			const string_t<Allocator>&	name,
//			textureHandle_t		handle)
//		{
//			if (m_list.find(name) == m_list.end())
//			{
//				//テクスチャの保存
//				m_list.insert(
//					std::map<string_t<Allocator>, textureHandle_t>::
//					value_type(name, handle));
//
//				return true;
//			}
//			return false;
//		};
//
//		template<template<class> class Allocator>
//		void				CTextureBank<Allocator>::DisposeTexture(
//			textureHandle_t		handle)
//		{
//			for (auto ite = m_list.begin();
//				ite != m_list.end();
//				++ite)
//			{
//				if (ite->second == handle)
//				{
//					DisposeTexture(ite->first);
//					return;
//				}
//			}
//		};
//
//		template<template<class> class Allocator>
//		Math::CVector<int, 2>	CTextureBank<Allocator>::GetTextureSize(
//			const string_t<Allocator>&		name)
//		{
//			return GetTextureSize(RequestTexture(name));
//		};
//	};
//};