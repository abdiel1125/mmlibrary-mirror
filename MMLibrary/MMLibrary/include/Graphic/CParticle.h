/*!	\file		CParticle.h
*	\details	CParticleクラスの宣言
*	\author		松裏征志
*	\date		2014/09/02
*/
#ifndef MMLIBRARY_CPARTICLE_H_
#define MMLIBRARY_CPARTICLE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\Object\CObject.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	パーティクルクラス
		class CParticle	:	public CObject
		{
			template<template<class> class Allocator>
			friend class CParticleEmitter;
		private:
			CVector3	m_speed;
			int			m_life;
		public:
			typedef void (*particleControler_t)(CParticle &);
			particleControler_t	m_pParticleControler;
			//!	コンストラクタ
			CParticle(void);
			
			//!	更新関数
			virtual void	Update(void) override;

			//!	描画関数
			virtual void	Render(void) override;

			//!	残り寿命を返す
			int	GetLife(void)
			{return m_life;};

			//!	速度を返す
			const CVector3&	GetSpeed(void) const
			{return m_speed;};

			//!	速度を設定する
			void	SetSpeed(const CVector3& speed)
			{m_speed = speed;};

			//!	パーティクル制御関数を登録する
			void	SetParticleControler(particleControler_t pParticleControler)
			{m_pParticleControler = pParticleControler;};
		};
	};
};
#endif