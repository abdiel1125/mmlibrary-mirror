/*!	\file		CColor.h
*	\details	CColorクラスの宣言
*	\author		松裏征志
*	\date		2014/04/29
*/
#ifndef MMLIBRARY_CCOLOR_H_
#define MMLIBRARY_CCOLOR_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility\DefineClassCreateHelpers.h"
#include "Math\Math.h"
#include <sstream>

namespace MMLibrary
{
	namespace Graphic
	{
		uchar_t To8bitColor(const float src);

		float ToFloatColor(const uchar_t src);
		
		//!	色値を表現するクラス
		/*!
		*	0〜255の間の値で赤、緑、青を表現する
		*/
		class CColor24 :
			private
			Utility::tagEqualComparable<CColor24,
			Utility::tagCanAdditionAssignment<CColor24,
			Utility::tagCanSubtractionAssignment<CColor24,
			Utility::tagCanMultiplicationAssignment<CColor24,
			Utility::tagCanAdditionAssignment2nd<CColor24, float,
			Utility::tagCanSubtractionAssignment2nd<CColor24, float,
			Utility::tagCanMultiplicationAssignment2nd<CColor24, float,
			Utility::tagInjectAddMethod<CColor24,
			Utility::tagInjectSubtractMethod<CColor24,
			Utility::tagInjectMultiplyMethod<CColor24,
			Utility::tagInjectAddMethod2nd<CColor24, float,
			Utility::tagInjectSubtractMethod2nd<CColor24, float,
			Utility::tagInjectMultiplyMethod2nd<CColor24, float > > > > > > > > > > > > >
		{
		protected:
			//!	色の赤成分
			uchar_t	m_red;

			//!	色の緑成分
			uchar_t	m_green;

			//!	色の青成分
			uchar_t	m_blue;

		public:
			//!	コンストラクタ
			/*!
			*	すべての要素を０で初期化する
			*/
			CColor24(void);

			//!	引数付コンストラクタ
			/*!
			*	与えられた引数で全ての値を初期化する
			*/
			CColor24(const int src);

			//!	引数付コンストラクタ
			/*!
			*	\param	[in]	red		色の赤成分
			*	\param	[in]	green	色の緑成分
			*	\param	[in]	blue	色の青成分
			*
			*	与えられた引数でそれぞれのメンバを初期化する
			*/
			CColor24(	const float red,
						const float green, 
						const float blue);

			//!	引数付コンストラクタ
			/*!
			*	\param	[in]	red		色の赤成分
			*	\param	[in]	green	色の緑成分
			*	\param	[in]	blue	色の青成分
			*
			*	与えられた引数でそれぞれのメンバを初期化する
			*/
			CColor24(	const int red,
						const int green, 
						const int blue);

			CREATE_REFERRENCE(Red, m_red);
			CREATE_REFERRENCE(Green, m_green);
			CREATE_REFERRENCE(Blue, m_blue);

			CREATE_CONST_REFERRENCE(Red, m_red);
			CREATE_CONST_REFERRENCE(Green, m_green);
			CREATE_CONST_REFERRENCE(Blue, m_blue);

			//!	加算代入演算子
			friend CColor24&	operator+=(CColor24& left, const CColor24& right);

			//!	減算代入演算子
			friend CColor24&	operator-=(CColor24& left, const CColor24& right);

			//!	乗算代入演算子
			friend CColor24&	operator*=(CColor24& left, const CColor24& right);

			//!	乗算代入演算子
			friend CColor24&	operator*=(CColor24& left, const float& right);

			//!	除算代入演算子
			friend CColor24&	operator/=(CColor24& left, const float& right);

			//!	等価演算子
			friend bool	operator==(const CColor24& left, const CColor24& right);

			//!	2 つの色をブレンドする。
			/*!
			*	\param	[in]	src	この引数の値を自身とブレンドする
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例に示すように、この関数は対応する 2 つの色成分の積を求めて 2 種類の色をブレンドする。\n
			*	m_red = m_red * src.m_red;
			*/
			CColor24&	Modulate(const CColor24& src);
			
			//!	色のコントラスト値を調整する。
			/*!
			*	\param	[in]	contrast	コントラスト値。このパラメータは、50% グレーと指定色 pC の間で線形補間する。\n
			*								c の値に制限はない。このパラメータが 0 の場合、返される色は 50% グレーである。\n
			*								このパラメータが 1 の場合、返される色は元の色である。
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例で示すように、この関数は 50% グレーと指定コントラスト値の間で CColor24クラスの赤 (red)、緑 (green)、青 (blue) の色成分を補間する。\n
			*	m_red = 0.5f + contrast * (m_red - 0.5f)\n
			*	contrast が 0 より大きく 1 より小さい場合、コントラストは小さくなる。contrast が 1 より大きい場合、コントラストは大きくなる。
			*/
			CColor24&	AdjustContrast(const float contrast);
			
			//!	色の彩度値を調整する。
			/*!
			*	\param	[in]	saturation	飽和値。このパラメータは、グレー スケールに変換された色と元の色 pC の間で線形補間する。s の値に制限はない。\n
			*								s が 0 の場合、返される色はグレースケール カラーである。s が 1 の場合、返される色は元の色である。
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例で示すように、この関数は未飽和色とあるカラーの間でCColor24クラスの赤 (red)、緑 (green)、青 (blue) の色成分を補間する。\n
			*	float grey = m_red * 0.2125f + m_green * 0.7154f + m_blue * 0.0721f;\n
			*	m_red = grey + saturation * (m_red - grey)\n
			*	saturation が 0 より大きく 1 より小さい場合、彩度は小さくなる。saturation が 1 より大きい場合、彩度は大きくなる。
			*/
			CColor24&	AdjustSaturation(const float saturation);
			
			//!	線形補間を使用して色値を作成する。
			/*!
			*	\param	[in]	C1	処理の基になるCColor24クラスへの参照
			*	\param	[in]	C2	処理の基になるCColor24クラスへの参照
			*	\param	[in]	s	色C1とC2を4Dベクトルとして処理し、これらの間を線形補間するパラメータ。s の値に制限はない。
			*
			*	\return			演算結果を返す
			*
			*	次の例で示すように、この関数は2つの色の間でCColor24クラスの赤 (red)、緑 (green)、青 (blue) の色成分を補間する。\n
			*	rtn->m_red = C1->m_red + s * (C2->m_red - C1->m_red)\n
			*	A 色と B 色の間で線形補間する場合に、s が 0 のとき、結果色は A となる。 s が 1 のとき、結果色は B となる。
			*/
			static CColor24	CreateByLerp(
				const CColor24&	C1,
				const CColor24&	C2,
				const float s);
			
			//!	ある色値に対する負の色値を作成する。
			/*!
			*	\return			演算結果を返す
			*
			*	次の例で示すように、この関数は2つの色の間で色成分から 1.0 を減算することで、負の色値を返す。\n
			*	rtn->m_red = 1.0f - m_red
			*/
			CColor24	CreateByNegative(void) const;

			//!	色値をスケーリングする。
			/*!
			*	\param	[in]	scaling	スケーリング係数。色を 4D ベクトルのように処理して、それをスケーリングする。\n
			*					scalingの値に制限はない。scalingが1の場合、結果の色は元の色である。
			*
			*	\return			演算結果を返す
			*
			*	次の例に示すように、この関数は、各色成分に指定されたスケーリング係数を乗算することで、スケーリングした色値を算出する。\n
			*	rtn->m_red = m_red * scaling
			*/
			CColor24	CreateByScale(const float scaling) const;

			//!	HSV系の値から色値を作成する。
			/*!
			*	\param	[in]	hue			色相　0〜1
			*	\param	[in]	saturation	彩度　0〜1
			*	\param	[in]	value		明度　0〜1
			*
			*	\return			演算結果を返す
			*/
			static CColor24	CreateByHSV(
				const float hue,
				const float saturation,
				const float value);
			
			//!	線形補間を使用して色値を作成する。
			/*!
			*	\param	[in]	C1	処理の基になるCColor24クラスへの参照
			*	\param	[in]	C2	処理の基になるCColor24クラスへの参照
			*	\param	[in]	s	色C1とC2を4Dベクトルとして処理し、これらの間を線形補間するパラメータ。s の値に制限はない。
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例で示すように、この関数は2つの色の間でCColor24クラスの赤 (red)、緑 (green)、青 (blue) の色成分を補間する。\n
			*	rtn->m_red = C1->m_red + s * (C2->m_red - C1->m_red)\n
			*	A 色と B 色の間で線形補間する場合に、s が 0 のとき、結果色は A となる。 s が 1 のとき、結果色は B となる。
			*/
			CColor24&	SetByLerp(
				const CColor24&	C1,
				const CColor24&	C2,
				const float s);
						
			//!	ある色値に対する負の色値を作成し、自身に代入する。
			/*!
			*	\return			演算後の自身への参照を返す
			*
			*	次の例で示すように、この関数は2つの色の間で色成分から 1.0 を減算することで、負の色値を返す。\n
			*	rtn->m_red = 1.0f - m_red
			*/
			CColor24&	SetByNegative(void);
			
			//!	色値をスケーリングし、自身に代入する。
			/*!
			*	\param	[in]	scaling	スケーリング係数。色を 4D ベクトルのように処理して、それをスケーリングする。\n
			*					scalingの値に制限はない。scalingが1の場合、結果の色は元の色である。
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例に示すように、この関数は、各色成分に指定されたスケーリング係数を乗算することで、スケーリングした色値を算出する。\n
			*	rtn->m_red = m_red * scaling
			*/
			CColor24&	SetByScale(const float scaling);

			//!	HSV系の値から色値を作成し、自身に代入する。
			/*!
			*	\param	[in]	hue			色相　0〜1
			*	\param	[in]	saturation	彩度　0〜1
			*	\param	[in]	value		明度　0〜1
			*
			*	\return			演算後の自身への参照を返す
			*/
			CColor24&	SetByHSV(
				const float hue,
				const float saturation,
				const float value);			
		};
	};
};

#include "Utility\UndefineClassCreateHelpers.h"
#endif