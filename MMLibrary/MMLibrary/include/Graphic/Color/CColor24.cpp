/*!	\file		CColor24.cpp
*	\details	CColor24クラスの定義
*	\author		松裏征志
*	\date		2014/06/10
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Color/CColor24.h"
#include "Utility\Macro.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CColor24::CColor24(void)
			:m_red(0)
			,m_green(0)
			,m_blue(0)
		{};

		CColor24::CColor24(const int src)
			:m_red(src)
			,m_green(src)
			,m_blue(src)
		{};

		CColor24::CColor24(	
			const float red,
			const float green, 
			const float blue)
			:m_red	(To8bitColor(red))
			,m_green(To8bitColor(green))
			,m_blue	(To8bitColor(blue))
		{};

		CColor24::CColor24(	
			const int red,
			const int green, 
			const int blue)
			:m_red	(red)
			,m_green(green)
			,m_blue	(blue)
		{};

		CColor24&	operator+=(CColor24& left, const CColor24& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, left.Red() + right.Red());
			left.Green() = Utility::ClampIn<int>(0, 255, left.Green() + right.Green());
			left.Blue() = Utility::ClampIn<int>(0, 255, left.Blue() + right.Blue());

			return	left;
		};

		CColor24&	operator-=(CColor24& left, const CColor24& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, left.Red() - right.Red());
			left.Green() = Utility::ClampIn<int>(0, 255, left.Green() - right.Green());
			left.Blue() = Utility::ClampIn<int>(0, 255, left.Blue() - right.Blue());

			return	left;
		};

		CColor24&	operator*=(CColor24& left, const CColor24& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, left.Red() * right.Red());
			left.Green() = Utility::ClampIn<int>(0, 255, left.Green() * right.Green());
			left.Blue() = Utility::ClampIn<int>(0, 255, left.Blue() * right.Blue());

			return	left;
		};

		CColor24&	operator*=(CColor24& left, const float& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Red() * right));
			left.Green() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Green() * right));
			left.Blue() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Blue() * right));

			return	left;
		};

		CColor24&	operator/=(CColor24& left, const float& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Red() / right));
			left.Green() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Green() / right));
			left.Blue() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Blue() / right));

			return	left;
		};

		bool	operator==(const CColor24& left, const CColor24& right)
		{
			return
				left.Red() == right.Red() &&
				left.Green() == right.Green() &&
				left.Blue() == right.Blue();
		};

		CColor24&	CColor24::Modulate(const CColor24& src)
		{
			*this *= src;
			return *this;
		};

		CColor24&	CColor24::AdjustContrast(const float contrast)
		{
			Red()	= (uchar_t)(128 + contrast * (Red()	- 128));
			Green()	= (uchar_t)(128 + contrast * (Green()	- 128));
			Blue()	= (uchar_t)(128 + contrast * (Blue()	- 128));

			return *this;
		};

		CColor24&	CColor24::AdjustSaturation(const float saturation)
		{
			uchar_t	grey =	(uchar_t)(Red()	* 0.2125f + 
													Green()	* 0.7154f + 
													Blue()	* 0.0721f);

			Red()	= (uchar_t)(grey + saturation * (Red()	- grey));
			Green()	= (uchar_t)(grey + saturation * (Green()	- grey));
			Blue()	= (uchar_t)(grey + saturation * (Blue()	- grey));

			return *this;
		};

		CColor24	CColor24::CreateByLerp(
			const CColor24&	C1,
			const CColor24&	C2,
			const float s)
		{
			return std::move(CColor24().SetByLerp(C1, C2, s));
		};

		CColor24	CColor24::CreateByNegative(void) const
		{
			return std::move(CColor24().SetByNegative());
		};

		CColor24	CColor24::CreateByScale(const float scaling) const
		{
			return std::move(CColor24().SetByScale(scaling));
		};
		
		CColor24	CColor24::CreateByHSV(
			const float hue,
			const float saturation,
			const float value)
		{
			return std::move(CColor24().
				SetByHSV(hue,saturation,value));
		};

		CColor24&	CColor24::SetByLerp(
			const CColor24&	C1,
			const CColor24&	C2,
			const float s)
		{
			Red()	= Math::Interpolation::Linear(C1.Red()	, C2.Red()	, s);
			Green()	= Math::Interpolation::Linear(C1.Green(), C2.Green(), s);
			Blue()	= Math::Interpolation::Linear(C1.Blue()	, C2.Blue()	, s);

			return *this;
		};

		CColor24&	CColor24::SetByNegative(void)
		{
			Red()	= 255 - Red();
			Green()	= 255 - Green();
			Blue()	= 255 - Blue();

			return *this;
		};

		CColor24&	CColor24::SetByScale(const float scaling)
		{
			*this *= scaling;

			return *this;
		};
		
		CColor24&	CColor24::SetByHSV(
			const float hue,
			const float saturation,
			const float value)
		{
			if(saturation == 0)
				return *this;

			int		hDash	= (int)floor(hue * 6);
			float	f		= hue * 6 - floor(hue * 6);
			float	p		= value * (1.0f - (saturation));
			float	q		= value * (1.0f - (saturation) * f);
			float	t		= value * (1.0f - (saturation) * (1.0f - f));

			switch(hDash)
			{
			case 0:
				Red()	= To8bitColor(value);
				Green()	= To8bitColor(t);
				Blue()	= To8bitColor(p);
				break;
			case 1:
				Red()	= To8bitColor(q);
				Green()	= To8bitColor(value);
				Blue()	= To8bitColor(p);
				break;
			case 2:
				Red()	= To8bitColor(p);
				Green()	= To8bitColor(value);
				Blue()	= To8bitColor(t);
				break;
			case 3:
				Red()	= To8bitColor(p);
				Green()	= To8bitColor(q);
				Blue()	= To8bitColor(value);
				break;
			case 4:
				Red()	= To8bitColor(t);
				Green()	= To8bitColor(p);
				Blue()	= To8bitColor(value);
				break;
			case 5:
				Red()	= To8bitColor(value);
				Green()	= To8bitColor(p);
				Blue()	= To8bitColor(q);
				break;
			}

			return *this;
		};
	};
};