/*!	\file		Color.h
*	\details	Color関連の宣言・インクルード
*	\author		松裏征志
*	\date		2014/06/10
*/
#ifndef MMLIBRARY_COLOR_H_
#define MMLIBRARY_COLOR_H_
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Graphic
	{
		/*!
		*	\brief	float表現の色を8bit表現に変換する
		*/
		uchar_t To8bitColor(const float src);

		/*!
		*	\brief	8bit表現の色をfloat表現に変換する
		*/
		float ToFloatColor(const uchar_t src);
	};
};
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\Color\CColor24.h"
#include "Graphic\Color\CColor32.h"

#endif