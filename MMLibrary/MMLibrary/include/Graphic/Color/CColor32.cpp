/*!	\file		CColor32.cpp
*	\details	CColor32クラスの定義
*	\author		松裏征志
*	\date		2014/06/10
*/

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Color/CColor32.h"
#include "Utility\Macro.h"

namespace MMLibrary
{
	namespace Graphic
	{
		CColor32::CColor32(void)
			:m_alpha(0)
		{};

		CColor32::CColor32(const int src)
			:m_alpha(src)
		{
			Red()	= src;
			Green() = src;
			Blue()	= src;
		};

		CColor32::CColor32(	
			const float red,
			const float green, 
			const float blue, 
			const float alpha)
			:m_alpha(To8bitColor(alpha))
		{
			Red() = To8bitColor(red);
			Green() = To8bitColor(green);
			Blue() = To8bitColor(blue);
		};

		CColor32::CColor32(	
			const int red,
			const int green, 
			const int blue, 
			const int alpha)
			:m_alpha(alpha)
		{
			Red() = red;
			Green() = green;
			Blue() = blue;
		};
		
		CColor32::CColor32(	const array_t<float, 4>& color)
		{
			Red()	= To8bitColor(color[0]);
			Green() = To8bitColor(color[1]);
			Blue()	= To8bitColor(color[2]);
			Alpha() = To8bitColor(color[3]);
		};

		CColor32::operator std::array<float, 4>(void)
		{
			std::array<float, 4> rtn;

			rtn[0] = ToFloatColor(Red());
			rtn[1] = ToFloatColor(Green());
			rtn[2] = ToFloatColor(Blue());
			rtn[3] = ToFloatColor(Alpha());

			return rtn;
		};

		CColor32&	operator+=(CColor32& left, const CColor32& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, left.Red() + right.Red());
			left.Green() = Utility::ClampIn<int>(0, 255, left.Green() + right.Green());
			left.Blue() = Utility::ClampIn<int>(0, 255, left.Blue() + right.Blue());
			left.Alpha() = Utility::ClampIn<int>(0, 255, left.Alpha() + right.Alpha());

			return	left;
		};

		CColor32&	operator-=(CColor32& left, const CColor32& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, left.Red() - right.Red());
			left.Green() = Utility::ClampIn<int>(0, 255, left.Green() - right.Green());
			left.Blue() = Utility::ClampIn<int>(0, 255, left.Blue() - right.Blue());
			left.Alpha() = Utility::ClampIn<int>(0, 255, left.Alpha() - right.Alpha());

			return	left;
		};

		CColor32&	operator*=(CColor32& left, const CColor32& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, left.Red() * right.Red());
			left.Green() = Utility::ClampIn<int>(0, 255, left.Green() * right.Green());
			left.Blue() = Utility::ClampIn<int>(0, 255, left.Blue() * right.Blue());
			left.Alpha() = Utility::ClampIn<int>(0, 255, left.Alpha() * right.Alpha());

			return	left;
		};

		CColor32&	operator*=(CColor32& left, const float& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Red() * right));
			left.Green() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Green() * right));
			left.Blue() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Blue() * right));
			left.Alpha() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Alpha() * right));

			return	left;
		};

		CColor32&	operator/=(CColor32& left, const float& right)
		{
			left.Red() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Red() / right));
			left.Green() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Green() / right));
			left.Blue() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Blue() / right));
			left.Alpha() = Utility::ClampIn<int>(0, 255, static_cast<int>(left.Alpha() / right));

			return	left;
		};

		bool	operator==(const CColor32& left, const CColor32& right)
		{
			return	
				left.Red() == right.Red() &&
				left.Green() == right.Green() &&
				left.Blue() == right.Blue() &&
				left.Alpha() == right.Alpha();
		};
		
		CColor32&	CColor32::Modulate(const CColor32& src)
		{
			*this *= src;
			return *this;
		};
				
		CColor32	CColor32::CreateByLerp(
			const CColor32&	C1,
			const CColor32&	C2,
			const float s)
		{
			return std::move(
				CColor32().SetByLerp(C1, C2, s));
		};

		CColor32	CColor32::CreateByNegative(void) const
		{
			return std::move(
				CColor32().SetByNegative());
		};

		CColor32	CColor32::CreateByScale(const float scaling) const
		{
			return std::move(
				CColor32().SetByScale(scaling));
		};
		
		CColor32	CColor32::CreateByHSV(
			const float hue,
			const float saturation,
			const float value, 
			const float alpha)
		{
			return std::move(
				CColor32().SetByHSV(hue,saturation,value,alpha));
		};

		CColor32&	CColor32::SetByLerp(
			const CColor32&	C1,
			const CColor32&	C2,
			const float s)
		{
			*this = Math::Interpolation::Linear(C1, C2, s);

			return *this;
		};

		CColor32&	CColor32::SetByNegative(void)
		{
			CColor24::SetByNegative();

			return *this;
		};

		CColor32&	CColor32::SetByScale(const float scaling)
		{
			*this *= scaling;

			return *this;
		};
		
		CColor32&	CColor32::SetByHSV(
			const float hue,
			const float saturation,
			const float value, 
			const float alpha)
		{
			CColor24::SetByHSV(hue, saturation, value);
			Alpha() = To8bitColor(alpha);

			return *this;
		};	
	};
};

namespace std
{
	MMLibrary::string_t<> to_string(const MMLibrary::Graphic::CColor32& color)
	{
		MMLibrary::string_t<> rtn = "(";
		rtn += std::to_string(color.Red());
		rtn += ",";
		rtn += std::to_string(color.Green());
		rtn += ",";
		rtn += std::to_string(color.Blue());
		rtn += ",";
		rtn += std::to_string(color.Alpha());
		rtn += ")";

		return rtn;
	}	
}