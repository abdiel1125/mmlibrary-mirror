/*!	\file		CColor32.h
*	\details	CColor32クラスの宣言
*	\author		松裏征志
*	\date		2014/06/10
*/
#ifndef MMLIBRARY_CCOLOR_32_H_
#define MMLIBRARY_CCOLOR_32_H_

//***********************************************************
//インクルード
//***********************************************************
#include <string>
#include <sstream>
#include <array>
#include "Graphic\Color\CColor24.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	色値を表現するクラス
		/*!
		*	0〜255の間の値で赤、緑、青、アルファ値を表現する
		*/
		class CColor32 :
			public 
			Utility::tagEqualComparable<CColor32,
			Utility::tagCanAdditionAssignment<CColor32,
			Utility::tagCanSubtractionAssignment<CColor32,
			Utility::tagCanMultiplicationAssignment<CColor32,
			Utility::tagCanAdditionAssignment2nd<CColor32, float,
			Utility::tagCanSubtractionAssignment2nd<CColor32, float,
			Utility::tagCanMultiplicationAssignment2nd<CColor32, float,
			CColor24> > > > > > >
		{
		protected:
			//!	色のアルファ値成分
			uchar_t	m_alpha;

		public:
			//!	コンストラクタ
			/*!
			*	すべての要素を０で初期化する
			*/
			CColor32(void);
	
			//!	引数付コンストラクタ
			/*!
			*	与えられた引数で全ての値を初期化する
			*/
			CColor32(const int src);
			
			//!	引数付コンストラクタ
			/*!	
			*	\param	[in]	red		色の赤成分
			*	\param	[in]	green	色の緑成分
			*	\param	[in]	blue	色の青成分
			*	\param	[in]	alpha	色のアルファ値成分
			*
			*	与えられた引数でそれぞれのメンバを初期化する
			*/
			CColor32(	const float red,
						const float green, 
						const float blue, 
						const float alpha);

			//!	引数付コンストラクタ
			/*!
			*	\param	[in]	red		色の赤成分
			*	\param	[in]	green	色の緑成分
			*	\param	[in]	blue	色の青成分
			*	\param	[in]	alpha	色のアルファ値成分
			*
			*	与えられた引数でそれぞれのメンバを初期化する
			*/
			CColor32(	const int red,
						const int green, 
						const int blue, 
						const int alpha);
			
			//!	引数付コンストラクタ
			/*!
			*	\param	[in]	color	色成分の配列(r,g,b,aの順)
			*
			*	与えられた引数でそれぞれのメンバを初期化する
			*/
			CColor32(	const array_t<float, 4>& color);

			//!	std::array<float, 4>へのキャスト
			operator std::array<float, 4>(void);

			CREATE_REFERRENCE(Alpha, m_alpha);

			CREATE_CONST_REFERRENCE(Alpha, m_alpha);

			//!	加算代入演算子
			friend CColor32&	operator+=(CColor32& left, const CColor32& right);

			//!	減算代入演算子
			friend CColor32&	operator-=(CColor32& left, const CColor32& right);
			
			//!	乗算代入演算子
			friend CColor32&	operator*=(CColor32& left, const CColor32& right);
			
			//!	乗算代入演算子
			friend CColor32&	operator*=(CColor32& left, const float& right);
			
			//!	除算代入演算子
			friend CColor32&	operator/=(CColor32& left, const float& right);

			//!	等価演算子
			friend bool	operator==(const CColor32& left, const CColor32& right);

			//!	2 つの色をブレンドする。
			/*!
			*	\param	[in]	src	この引数の値を自身とブレンドする
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例に示すように、この関数は対応する 2 つの色成分の積を求めて 2 種類の色をブレンドする。\n
			*	m_red = m_red * src.m_red;
			*/
			CColor32&	Modulate(const CColor32& src);
			
			//!	線形補間を使用して色値を作成する。
			/*!
			*	\param	[in]	C1	処理の基になるCColor32クラスへの参照
			*	\param	[in]	C2	処理の基になるCColor32クラスへの参照
			*	\param	[in]	s	色C1とC2を4Dベクトルとして処理し、これらの間を線形補間するパラメータ。s の値に制限はない。
			*
			*	\return			演算結果を返す
			*
			*	次の例で示すように、この関数は2つの色の間でCColor32クラスの赤 (red)、緑 (green)、青 (blue) の色成分を補間する。\n
			*	rtn->m_red = C1->m_red + s * (C2->m_red - C1->m_red)\n
			*	A 色と B 色の間で線形補間する場合に、s が 0 のとき、結果色は A となる。 s が 1 のとき、結果色は B となる。
			*/
			static CColor32	CreateByLerp(
				const CColor32&	C1,
				const CColor32&	C2,
				const float s);
			
			//!	ある色値に対する負の色値を作成する。
			/*!
			*	\return			演算結果を返す
			*
			*	アルファ チャンネルは修正されない。\n
			*	次の例で示すように、この関数は2つの色の間で色成分から 1.0 を減算することで、負の色値を返す。\n
			*	rtn->m_red = 1.0f - m_red
			*/
			CColor32	CreateByNegative(void) const;

			//!	色値をスケーリングする。
			/*!
			*	\param	[in]	scaling	スケーリング係数。色を 4D ベクトルのように処理して、それをスケーリングする。\n
			*					scalingの値に制限はない。scalingが1の場合、結果の色は元の色である。
			*
			*	\return			演算結果を返す
			*
			*	次の例に示すように、この関数は、各色成分に指定されたスケーリング係数を乗算することで、スケーリングした色値を算出する。\n
			*	rtn->m_red = m_red * scaling
			*/
			CColor32	CreateByScale(const float scaling) const;

			//!	HSV系の値から色値を作成する。
			/*!
			*	\param	[in]	hue			色相　0〜1
			*	\param	[in]	saturation	彩度　0〜1
			*	\param	[in]	value		明度　0〜1
			*	\param	[in]	alpha		色のアルファ値成分
			*
			*	\return			演算結果を返す
			*/
			static CColor32	CreateByHSV(
				const float hue,
				const float saturation,
				const float value, 
				const float alpha);
			
			//!	線形補間を使用して色値を作成する。
			/*!
			*	\param	[in]	C1	処理の基になるCColor32クラスへの参照
			*	\param	[in]	C2	処理の基になるCColor32クラスへの参照
			*	\param	[in]	s	色C1とC2を4Dベクトルとして処理し、これらの間を線形補間するパラメータ。s の値に制限はない。
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例で示すように、この関数は2つの色の間でCColor32クラスの赤 (red)、緑 (green)、青 (blue) の色成分を補間する。\n
			*	rtn->m_red = C1->m_red + s * (C2->m_red - C1->m_red)\n
			*	A 色と B 色の間で線形補間する場合に、s が 0 のとき、結果色は A となる。 s が 1 のとき、結果色は B となる。
			*/
			CColor32&	SetByLerp(
				const CColor32&	C1,
				const CColor32&	C2,
				const float s);
						
			//!	ある色値に対する負の色値を作成し、自身に代入する。
			/*!
			*	\return			演算後の自身への参照を返す
			*
			*	アルファ チャンネルは修正されない。\n
			*	次の例で示すように、この関数は2つの色の間で色成分から 1.0 を減算することで、負の色値を返す。\n
			*	rtn->m_red = 1.0f - m_red
			*/
			CColor32&	SetByNegative(void);
			
			//!	色値をスケーリングし、自身に代入する。
			/*!
			*	\param	[in]	scaling	スケーリング係数。色を 4D ベクトルのように処理して、それをスケーリングする。\n
			*					scalingの値に制限はない。scalingが1の場合、結果の色は元の色である。
			*
			*	\return			演算後の自身への参照を返す
			*
			*	次の例に示すように、この関数は、各色成分に指定されたスケーリング係数を乗算することで、スケーリングした色値を算出する。\n
			*	rtn->m_red = m_red * scaling
			*/
			CColor32&	SetByScale(const float scaling);

			//!	HSV系の値から色値を作成し、自身に代入する。
			/*!
			*	\param	[in]	hue			色相　0〜1
			*	\param	[in]	saturation	彩度　0〜1
			*	\param	[in]	value		明度　0〜1
			*	\param	[in]	alpha		色のアルファ値成分
			*
			*	\return			演算後の自身への参照を返す
			*/
			CColor32&	SetByHSV(
				const float hue,
				const float saturation,
				const float value, 
				const float alpha);			
		};

	};
};
namespace std
{
	MMLibrary::string_t<> to_string(const MMLibrary::Graphic::CColor32& color);
}
#endif