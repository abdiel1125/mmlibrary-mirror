/*!	\file		CGLFunctions.h
*	\details	CGLFunctionsクラスの宣言
*	\author		松裏征志
*	\date		2014/11/12
*/
#ifndef MMLIBRARY_CGLFUNCTIONS_H_
#define MMLIBRARY_CGLFUNCTIONS_H_
#ifdef USING_OPENGL

//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic\glext.h"
#include	"Utility\CSingletonBase.h"

namespace	MMLibrary
{
	namespace Graphic
	{
		//!	OpenGLの関数ポインタを持つSingletonクラス
		class CGLFunctions : public Utility::CSingletonBase < CGLFunctions >
		{
		public:
			//!	コンストラクタ
			CGLFunctions()
			{

#define LoadGLFunction(type, name) name = (type)wglGetProcAddress(#name);
#include "Graphic\GLFunctions.inl"
#undef	LoadGLFunction

			}

			//!	OpenGLの関数ポインタ郡
			//!	@{
#define LoadGLFunction(type, name) type name;
#include "Graphic\GLFunctions.inl"
#undef	LoadGLFunction
			//!	@}

			bool CheckErrer(bool isUseAssert = true)
			{
				GLenum errcode = glGetError();
				if (errcode != GL_NO_ERROR)
				{
					if (isUseAssert == false)
						return true;

					switch (errcode)
					{
					case GL_INVALID_ENUM:
						assert(0 && "無効な列挙　GLenum型の引数が範囲を超えている");
						break;
					case GL_INVALID_VALUE:
						assert(0 && "無効な値　引数が範囲を超えている");
						break;
					case GL_INVALID_OPERATION:
						assert(0 && "無効な演算。");
						break;
					case GL_STACK_OVERFLOW:
						assert(0 && "スタックオーバーフロー");
						break;
					case GL_STACK_UNDERFLOW:
						assert(0 && "スタックアンダーフロー");
						break;
					case GL_OUT_OF_MEMORY:
						assert(0 && "実行するのにメモリが足りない。");
						break;
					case 0x0506:	//GL_INVALID_FRAMEBUFFER_OPERATION
						assert(0 && "Given when doing anything that would attempt to read from or write/render to a framebuffer that is not complete.");
						break;
					default:
						assert(0 && "未知のエラー");
						break;
					}
				}
				return false;
			}
		};
	}
}

#endif // USING_OPENGL
#endif // MMLIBRARY_CGRAPHICMANAGER_H_