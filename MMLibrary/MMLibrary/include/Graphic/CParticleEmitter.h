/*!	\file		CParticleEmitter.h
*	\details	CParticleEmitterクラスの宣言
*	\author		松裏征志
*	\date		2014/09/02
*/
#ifndef MMLIBRARY_CPARTICLE_EMITTER_H_
#define MMLIBRARY_CPARTICLE_EMITTER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\CParticle.h"
#include "Graphic\Object\CObject.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	パーティクル放出クラス
		template<
			template <class> class Allocator = USE_ALLOCATOR >
		class CParticleEmitter	:	public CObject
		{
		private:
			vector_t<CParticle, Allocator> m_particles;
			int								m_frameCounter;
			Math::CVector<int, 2>			m_life;
			float							m_emitRange;
			float							m_defSpeed;
			int								m_oldFrame;
			int								m_emitInterval;
			CParticle::particleControler_t	m_pParticleControler;
			
			//!	パーティクル放出
			void	Emit(void);
		public:
			//!	コンストラクタ
			CParticleEmitter(
				int		lifeMin,
				int		lifeMax,
				int		emitInterval,
				float	emitRange,
				float	defSpeed);
			
			//!	更新
			void	Update(void);

			//!	描画
			void	Render(void);

			//!	最大パーティクル数の設定
			void SetParticlesNum(int num)
			{
				m_particles.resize(num);
				m_particles.shrink_to_fit();
			};

			//!	パーティクル制御関数を登録する
			void	SetParticleControler(CParticle::particleControler_t pParticleControler)
			{
				m_pParticleControler = pParticleControler;
			};

			//!	パーティクルの配列
			vector_t<CParticle, Allocator>& Particles()
			{
				return m_particles;
			};
		};
	};
};
#include "Graphic\CParticleEmitter.hpp"
#endif