/*!	\file		CCamera.h
*	\details	CCameraクラスの宣言
*	\author		松裏征志
*	\date		2014/05/15
*/
#ifndef MMLIBRARY_CCAMERA_H_
#define MMLIBRARY_CCAMERA_H_

//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic/CVector3.h"
#include	"Math/CQuaternion.h"

namespace	MMLibrary
{
	namespace	Graphic
	{
		class CMatrix4x4;

		/*!
		*	\brief	カメラ・パースペクティブに関する処理を提供するクラス
		*/
		class	CCamera
		{
		private:
			CVector3	m_position;
			CVector3	m_target;
			CVector3	m_upVector;

			float		m_fovY;
			float		m_aspect;
			float		m_nearZ;
			float		m_farZ;

		public:
			/*!
			*	\brief	コンストラクタ
			*/
			CCamera(void);

			/*!
			*	\brief	カメラ位置への参照
			*/
			CVector3& Position(void)
			{
				return m_position;
			};

			/*!
			*	\brief	カメラ位置への参照(読み取り専用)
			*/
			const CVector3& Position(void) const
			{
				return m_position;
			};

			/*!
			*	\brief	焦点位置への参照
			*/
			CVector3& Target(void)
			{
				return m_target;
			};

			/*!
			*	\brief	焦点位置への参照(読み取り専用)
			*/
			const CVector3& Target(void) const
			{
				return m_target;
			};

			/*!
			*	\brief	上方向ベクトルへの参照
			*/
			CVector3& UpVector(void)
			{
				return m_upVector;
			};

			/*!
			*	\brief	上方向ベクトルへの参照(読み取り専用)
			*/
			const CVector3& UpVector(void) const
			{
				return m_upVector;
			};

			/*!
			*	\brief	Y方向の視野角への参照
			*/
			float& FovY(void)
			{
				return m_fovY;
			};

			/*!
			*	\brief	Y方向の視野角への参照(読み取り専用)
			*/
			const float& FovY(void) const
			{
				return m_fovY;
			};

			/*!
			*	\brief	アスペクト比への参照
			*/
			float& Aspect(void)
			{
				return m_aspect;
			};

			/*!
			*	\brief	アスペクト比への参照(読み取り専用)
			*/
			const float& Aspect(void) const
			{
				return m_aspect;
			};

			/*!
			*	\brief	ビューボリュームの近平面までの距離への参照
			*/
			float& NearZ(void)
			{
				return m_nearZ;
			};

			/*!
			*	\brief	ビューボリュームの近平面までの距離への参照(読み取り専用)
			*/
			const float& NearZ(void) const
			{
				return m_nearZ;
			};

			/*!
			*	\brief	ビューボリュームの遠平面までの距離への参照
			*/
			float& FarZ(void)
			{
				return m_farZ;
			};

			/*!
			*	\brief	ビューボリュームの遠平面までの距離への参照(読み取り専用)
			*/
			const float& FarZ(void) const
			{
				return m_farZ;
			};

			/*!
			*	\brief	ビュー行列を取得する
			*/
			CMatrix4x4	GetViewMatrix(void);

			/*!
			*	\brief	プロジェクション行列を取得する
			*/
			CMatrix4x4	GetProjectionMatrix(void);

			CCamera& Move(const CVector3& translation)
			{
				Position() += translation;
				Target() += translation;

				return *this;
			};
			CCamera& Move(const float& x, const float& y, const float& z)
			{
				Position().at(0) += x;
				Position().at(1) += y;
				Position().at(2) += z;

				Target().at(0) += x;
				Target().at(1) += y;
				Target().at(2) += z;

				return *this;
			};
			CCamera& MoveLocal(const CVector3& translation);
			CCamera& MoveLocal(const float& x, const float& y, const float& z);

			CCamera& Rotation(const CMatrix4x4& rotation);
			CCamera& Rotation(const Math::CQuaternion<>& rotation);
			CCamera& Rotation(const CVector3& axis, const float& radian);

			CVector3 GetLocalXAxis(void) const
			{
				return GetLocalYAxis().Cross(GetLocalZAxis()).Normalize();
			};
			CVector3 GetLocalYAxis(void) const
			{
				return UpVector().ToClone().Normalize();
			};
			CVector3 GetLocalZAxis(void) const
			{
				return (Target() - Position()).Normalize();
			};
		};
	};
};

#endif