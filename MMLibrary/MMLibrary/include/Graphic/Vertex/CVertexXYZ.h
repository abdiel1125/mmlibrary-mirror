/*!	\file		CVertexXYZ.h
*	\details	CVertexXYZクラスの宣言
*	\author		松裏征志
*	\date		2014/06/10
*/
#ifndef MMLIBRARY_CVERTEX_XYZ_H_
#define MMLIBRARY_CVERTEX_XYZ_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/CVector3.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	xyz座標のみの頂点クラス
		class CVertexXYZ
		{
		private:
			CVector3	m_position;
		public:
			//!	引数付きコンストラクタ
			CVertexXYZ(
				float x	= 0, 
				float y	= 0, 
				float z	= 0);

			//!	引数付きコンストラクタ
			CVertexXYZ(const CVector3& position);

			//!	描画準備
			static void RenderBegin(
				size_t offset = 0,
				size_t vertexSize = sizeof(CVertexXYZ));

			//!	描画準備
			static void RenderBegin(
				CVertexXYZ& vertexTop,
				size_t vertexSize = sizeof(CVertexXYZ));

			//!	描画終了
			static void RenderEnd(void);
			
			//!	座標データへの参照
			CVector3& Position(void);
			
			//!	座標データへの参照(読み取り専用)
			const CVector3& Position(void) const;

#ifdef	USING_DIRECTX
static		D3DVERTEXELEMENT9	GetElement(uint_t offset);
#endif
		};
	};
};
#endif