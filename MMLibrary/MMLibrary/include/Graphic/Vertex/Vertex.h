/*!	\file		Vertex.h
*	\details	Vertexクラスの宣言・インクルード
*	\author		松裏征志
*	\date		2014/05/07
*/
#ifndef MMLIBRARY_VERTEX_H_
#define MMLIBRARY_VERTEX_H_

//***********************************************************
//インクルード
//***********************************************************
#include <d3dx9.h>

namespace MMLibrary
{
	namespace Graphic
	{
#ifdef USING_DIRECTX
		void SetDeclaration(const D3DVERTEXELEMENT9* pElement);
#endif
	};
};

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\Vertex\CVertexXYZ.h"
#include "Graphic\Vertex\CVertexWithDiffuse.h"
#include "Graphic\Vertex\CVertexFor2D.h"
#include "Graphic\Vertex\CVertexFor3D.h"


#endif