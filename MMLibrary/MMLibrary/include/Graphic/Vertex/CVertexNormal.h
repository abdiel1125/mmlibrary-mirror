/*!	\file		CVertexNormal.h
*	\details	CVertexNormalクラスの宣言
*	\author		松裏征志
*	\date		2014/06/11
*/
#ifndef MMLIBRARY_CVERTEX_NORMAL_H_
#define MMLIBRARY_CVERTEX_NORMAL_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/CVector3.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	3Dモデル向けの頂点クラス
		class CVertexNormal
		{
		private:
			CVector3	m_normal;
		public:
			//!	引数付きコンストラクタ
			CVertexNormal(
				float nx			= 0, 
				float ny			= 0, 
				float nz			= 0);

			//!	引数付きコンストラクタ
			CVertexNormal(
				const CVector3&		normal);

			//!	描画準備
			static void RenderBegin(
				size_t offset = 0,
				size_t vertexSize = sizeof(CVertexNormal));

			//!	描画準備
			static void RenderBegin(
				CVertexNormal& vertexTop,
				size_t vertexSize = sizeof(CVertexNormal));

			//!	描画終了
			static void RenderEnd(void);
			
			//!	法線データへの参照
			CVector3& Normal(void);
			
			//!	法線データへの参照(読み取り専用)
			const CVector3& Normal(void) const;

#ifdef	USING_DIRECTX
static		D3DVERTEXELEMENT9	GetElement(uint_t offset);
#endif
		};
	};
};
#endif