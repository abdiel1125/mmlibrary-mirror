/*!	\file		CVertexWithDiffuse.h
*	\details	CVertexWithDiffuseクラスの宣言
*	\author		松裏征志
*	\date		2014/05/12
*/
#ifndef MMLIBRARY_CVERTEX_WITH_DIFFUSE_H_
#define MMLIBRARY_CVERTEX_WITH_DIFFUSE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/CVertexXYZ.h"
#include "Graphic/Vertex/CVertexDiffuse.h"
#include "Graphic/Color/Color.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	xyz座標とディヒューズ色のみの頂点クラス

		class CVertexWithDiffuse : 
			public CVertexXYZ, 
			public CVertexDiffuse
		{
		private:
			CColor32 m_color;
		public:
			//!	引数付きコンストラクタ
			CVertexWithDiffuse(
				float x				= 0, 
				float y				= 0, 
				float z				= 0,
				uchar_t red	= 255,
				uchar_t green	= 255,
				uchar_t blue	= 255,
				uchar_t alpha	= 255);
			
			//!	引数付きコンストラクタ
			CVertexWithDiffuse(
				const CVector3& position,
				const CColor32&			color);

			//!	描画準備
			static void RenderBegin(
				size_t offset = 0,
				size_t vertexSize = sizeof(CVertexWithDiffuse));

			//!	描画準備
			static void RenderBegin(
				CVertexWithDiffuse& vertexTop,
				size_t vertexSize = sizeof(CVertexWithDiffuse));

			//!	描画準備
			static void RenderBeginNoBuffer(
				CVertexWithDiffuse& vertexTop,
				size_t vertexSize = sizeof(CVertexWithDiffuse));

			//!	描画終了
			static void RenderEnd(void);
		};
	};
};
#endif