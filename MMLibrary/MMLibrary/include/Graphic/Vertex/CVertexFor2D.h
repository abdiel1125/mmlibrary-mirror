/*!	\file		CVertexFor2D.h
*	\details	CVertexFor2Dクラスの宣言
*	\author		松裏征志
*	\date		2014/05/27
*/
#ifndef MMLIBRARY_CVERTEX_FOR_2D_H_
#define MMLIBRARY_CVERTEX_FOR_2D_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/CVertexXYZ.h"
#include "Graphic/Vertex/CVertexDiffuse.h"
#include "Graphic/Vertex/CVertexUV.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	2Dモデル向けの頂点クラス
		class CVertexFor2D : 
			public CVertexXYZ,
			public CVertexDiffuse,
			public CVertexUV

		{
		public:
			//!	引数付きコンストラクタ
			CVertexFor2D(
				float x				= 0, 
				float y				= 0, 
				float z				= 0,
				uchar_t red	= 255,
				uchar_t green	= 255,
				uchar_t blue	= 255,
				uchar_t alpha	= 255,
				float u				= 0,
				float v				= 0);

			//!	引数付きコンストラクタ
			CVertexFor2D(
				const CVector3&		position,
				const CColor32&					color,
				const Math::CVector<float, 2>&	uv);

			//!	描画準備
			static void RenderBegin(
				size_t offset = 0,
				size_t vertexSize = sizeof(CVertexFor2D));

			//!	描画準備
			static void RenderBegin(
				CVertexFor2D& vertexTop,
				size_t vertexSize = sizeof(CVertexFor2D));

			//!	描画準備
			static void RenderBeginNoBuffer(
				CVertexFor2D& vertexTop,
				size_t vertexSize = sizeof(CVertexFor2D));

			//!	描画終了
			static void RenderEnd(void);
		};
	};
};
#endif