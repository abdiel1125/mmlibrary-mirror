/*!	\file		CVertexDiffuse.h
*	\details	CVertexDiffuseクラスの宣言
*	\author		松裏征志
*	\date		2014/06/10
*/
#ifndef MMLIBRARY_CVERTEX_DIFFUSE_H_
#define MMLIBRARY_CVERTEX_DIFFUSE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Color/Color.h"
#include "Graphic/CGraphicManager.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	ディヒューズ色のみの頂点クラス
		class CVertexDiffuse
		{
		private:
			CColor32 m_color;
		public:
			//!	引数付きコンストラクタ
			CVertexDiffuse(
				uchar_t red	= 255,
				uchar_t green	= 255,
				uchar_t blue	= 255,
				uchar_t alpha	= 255);
			
			//!	引数付きコンストラクタ
			CVertexDiffuse(
				const CColor32&			color);

			//!	描画準備
			static void RenderBegin(
				size_t offset = 0,
				size_t vertexSize = sizeof(CVertexDiffuse));

			//!	描画準備
			static void RenderBegin(
				CVertexDiffuse& vertexTop,
				size_t vertexSize = sizeof(CVertexDiffuse));

			//!	描画終了
			static void RenderEnd(void);
			
			//!	データへの参照
			CColor32& Color(void);
			
			//!	データへの参照(読み取り専用)
			const CColor32& Color(void) const;

#ifdef	USING_DIRECTX
static		D3DVERTEXELEMENT9	GetElement(uint_t offset);
#endif
		};
	};
};
#endif