/*!	\file		CVertexFor3D.h
*	\details	CVertexFor3Dクラスの宣言
*	\author		松裏征志
*	\date		2014/05/29
*/
#ifndef MMLIBRARY_CVERTEX_FOR_3D_H_
#define MMLIBRARY_CVERTEX_FOR_3D_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/CVertexXYZ.h"
#include "Graphic/Vertex/CVertexNormal.h"
#include "Graphic/Vertex/CVertexUV.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	3Dモデル向けの頂点クラス
		class CVertexFor3D : 
			public CVertexXYZ,
			public CVertexNormal,
			public CVertexUV
		{
		public:
			//!	引数付きコンストラクタ
			CVertexFor3D(
				float x				= 0, 
				float y				= 0, 
				float z				= 0,
				float nx			= 0, 
				float ny			= 0, 
				float nz			= 0,
				float u				= 0,
				float v				= 0);

			//!	引数付きコンストラクタ
			CVertexFor3D(
				const CVector3&		position,
				const CVector3&		normal,
				const Math::CVector<float, 2>&	uv);

			//!	描画準備
			static void RenderBegin(
				size_t offset = 0,
				size_t vertexSize = sizeof(CVertexFor3D));

			//!	描画準備
			static void RenderBegin(
				CVertexFor3D& vertexTop,
				size_t vertexSize = sizeof(CVertexFor3D));

			//!	描画準備
			static void RenderBeginNoBuffer(
				CVertexFor3D& vertexTop,
				size_t vertexSize = sizeof(CVertexFor3D));

			//!	描画終了
			static void RenderEnd(void);
		};
	};
};
#endif