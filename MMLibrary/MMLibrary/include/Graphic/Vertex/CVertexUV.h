/*!	\file		CVertexUV.h
*	\details	CVertexUVクラスの宣言
*	\author		松裏征志
*	\date		2014/06/10
*/
#ifndef MMLIBRARY_CVERTEX_UV_H_
#define MMLIBRARY_CVERTEX_UV_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Math/CVector.h"

namespace MMLibrary
{
	namespace Graphic
	{
		//!	テクスチャ座標のみの頂点クラス
		class CVertexUV
		{
		private:
			Math::CVector<float, 2>	m_uv;
		public:
			//!	引数付きコンストラクタ
			CVertexUV(
				float u				= 0,
				float v				= 0);

			//!	引数付きコンストラクタ
			CVertexUV(
				const Math::CVector<float, 2>&	uv);

			//!	描画準備
			static void RenderBegin(
				size_t offset = 0,
				size_t vertexSize = sizeof(CVertexUV));

			//!	描画準備
			static void RenderBegin(
				CVertexUV& vertexTop,
				size_t vertexSize = sizeof(CVertexUV));

			//!	描画終了
			static void RenderEnd(void);
			
			//!	テクスチャ座標データへの参照
			Math::CVector<float, 2>& UV(void);
			
			//!	テクスチャ座標データへの参照(読み取り専用)
			const Math::CVector<float, 2>& UV(void) const;

#ifdef	USING_DIRECTX
static		D3DVERTEXELEMENT9	GetElement(uint_t offset);
#endif
		};
	};
};
#endif