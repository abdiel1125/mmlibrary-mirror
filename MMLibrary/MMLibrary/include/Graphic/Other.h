/*!	\file		Other.h
	\details	MMLibraryの描画に関する定数・構造体・関数を宣言する
	\author		松裏征志
	\date		2014/06/23
*/
#ifndef MMLIBRARY_GRAPHIC_OTHER_H_
#define MMLIBRARY_GRAPHIC_OTHER_H_

//***********************************************************
//インクルード
//***********************************************************
namespace MMLibrary
{
	namespace Graphic
	{
		/*!
		*	\brief ビューポートを表現する構造体
		*
		*	x、y、width、heightの各メンバは、レンダリングターゲットサーフェイス上のビューポートの位置とディメンジョンを記述する。\n
		*	通常、アプリケーションはターゲットサーフェイス全体にレンダリングするので、640 x 480 のサーフェイスにレンダリングするとき、\n
		*	これらのメンバはそれぞれ 0、0、640、480 になる。一般に、MinZおよびMaxZは0.0および1.0に設定されるが、他の値を設定して\n
		*	特殊効果を出すこともできる。たとえば、両方の値を 0.0 に設定すると、シーンのフォアグラウンドにオブジェクトがレンダリングされる。\n
		*	また、両方の値を1.0に設定すると、オブジェクトがバックグラウンドにレンダリングされる。
		*/
		typedef struct	_viewport
		{
			/*!		レンダリングターゲットサーフェイス上のビューポートの左上隅を表すピクセル座標。\n
			*		サーフェイスのサブセットにレンダリングするのでなければ、このメンバは0に設定できる。
			*/
			int		x;
			/*!		レンダリングターゲットサーフェイス上のビューポートの左上隅を表すピクセル座標。\n
			*		サーフェイスのサブセットにレンダリングするのでなければ、このメンバは0に設定できる。
			*/
			int		y;
			/*!		クリップボリュームの幅(ピクセル単位)。サーフェイスのサブセットに限定してレンダリングするのでなければ、\n
			*		このメンバにはレンダリングターゲットサーフェイスの幅を設定する。
			*/
			int		width;
			/*!		クリップボリュームの高さ(ピクセル単位)。サーフェイスのサブセットに限定してレンダリングするのでなければ、\n
			*		このメンバにはレンダリングターゲットサーフェイスの高さを設定する。
			*/
			int		height;
			/*!		MaxZ と共に使い、シーンをレンダリングする深度値の範囲を示す値。クリップボリュームの最小値と最大値。\n
			*		ほとんどのアプリケーションでは、この値を 0.0 に設定する。射影行列の適用後にクリッピングが行われる。
			*/
			float	minZ;
			/*!		MinZ と共に使い、シーンをレンダリングする深度値の範囲を示す値。クリップ ボリュームの最小値と最大値。\n
			*		ほとんどのアプリケーションでは、この値を1.0に設定する。射影行列の適用後にクリッピングが行われる。
			*/
			float	maxZ;
		}viewport_t;

#ifdef	USING_OPENGL
		//!	テクスチャハンドルを表す型
		typedef	GLuint	textureHandle_t;

		//!	カリングモード
		enum CULLING_MODE
		{
			CULL_NONE	,
			CULL_CW		= GL_BACK,
			CULL_CCW	= GL_FRONT
		};

		//!	ブレンディング演算
		enum BLEND_OPERATE
		{
			ADD			= GL_ADD		,      
			SUBTRACT	/*= GL_SUBTRACT		 */,
			REVSUBTRACT	/*= GL_REVSUBTRACT   */,
			MIN			/*= GL_MIN           */,
			MAX			/*= GL_MAX           */,
		};

		//!	ブレンディング要素
		enum BLEND_INTEGER
		{
			DST_ALPHA			= GL_DST_ALPHA,
			DST_COLOR			= GL_DST_COLOR,
			SRC_ALPHA			= GL_SRC_ALPHA,
			SRC_COLOR			= GL_SRC_COLOR,
			ONE_MINUS_DST_ALPHA = GL_ONE_MINUS_DST_ALPHA,
			ONE_MINUS_DST_COLOR = GL_ONE_MINUS_DST_COLOR,
			ONE_MINUS_SRC_ALPHA = GL_ONE_MINUS_SRC_ALPHA,
			ONE_MINUS_SRC_COLOR = GL_ONE_MINUS_SRC_COLOR,
		};

		//!	プリミティブの型を示すenum
		enum PRIMITIVE_TIPE : GLenum
		{
			POINTS = GL_POINTS,
			LINES = GL_LINES,
			LINE_STRIP = GL_LINE_STRIP,
			TRIANGLES = GL_TRIANGLES,
			TRIANGLE_STRIP = GL_TRIANGLE_STRIP,
			TRIANGLE_FAN = GL_TRIANGLE_FAN
		};

		//!	テクスチャブレンディングモード
		enum TEXTURE_BLEND_MODE
		{
			//!	頂点色*テクスチャ色
			MODULATE = GL_MODULATE,
			//!	テクスチャ色(アルファ不使用)
			//DECAL = GL_DECAL,
			//!	頂点色*(1-テクスチャ色)
			BLEND = GL_BLEND,
			//!	テクスチャ色
			REPLACE = GL_REPLACE
		};


		//!	頂点バッファを表す型
		using vertexBuffer_t = GLuint;

		//!	インデックスバッファを表す型
		using indexBuffer_t = GLuint;
#endif
#ifdef	USING_DIRECTX
		//!	テクスチャハンドルを表す型
		typedef	LPDIRECT3DTEXTURE9	textureHandle_t;

		//!	カリングモード
		enum CULLING_MODE
		{
			CULL_NONE	= D3DCULL_NONE,
			CULL_CW		= D3DCULL_CW,
			CULL_CCW	= D3DCULL_CCW
		};

		//!	ブレンディング演算
		enum BLEND_OPERATE
		{
			ADD			= D3DBLENDOP_ADD		,      
			SUBTRACT	= D3DBLENDOP_SUBTRACT   ,
			REVSUBTRACT	= D3DBLENDOP_REVSUBTRACT,
			MIN			= D3DBLENDOP_MIN        ,
			MAX			= D3DBLENDOP_MAX        ,
		};

		//!	ブレンディング要素
		enum BLEND_INTEGER
		{
			DST_ALPHA			= D3DBLEND_DESTALPHA,
			DST_COLOR			= D3DBLEND_DESTCOLOR,
			SRC_ALPHA			= D3DBLEND_SRCALPHA,
			SRC_COLOR			= D3DBLEND_SRCCOLOR,
			ONE_MINUS_DST_ALPHA = D3DBLEND_INVDESTALPHA,
			ONE_MINUS_DST_COLOR = D3DBLEND_INVDESTCOLOR,
			ONE_MINUS_SRC_ALPHA = D3DBLEND_INVSRCALPHA,
			ONE_MINUS_SRC_COLOR = D3DBLEND_INVSRCCOLOR,
		};

		//!	テクスチャブレンディングモード
		enum class TEXTURE_BLEND_MODE
		{
			//!	頂点色*テクスチャ色
			MODULATE,
			//!	テクスチャ色(アルファ不使用)
			//DECAL,
			//!	頂点色*(1-テクスチャ色)
			BLEND,
			//!	テクスチャ色
			REPLACE
		};

		//!	プリミティブの型を示すenum
		enum PRIMITIVE_TIPE
		{
			POINTS = D3DPT_POINTLIST,
			LINES = D3DPT_LINELIST,
			LINE_STRIP = D3DPT_LINESTRIP,
			TRIANGLES = D3DPT_TRIANGLELIST,
			TRIANGLE_STRIP = D3DPT_TRIANGLESTRIP,
			TRIANGLE_FAN = D3DPT_TRIANGLEFAN
		};

		//!	頂点バッファを表す型
		using vertexBuffer_t = IDirect3DVertexBuffer9*;

		//!	インデックスバッファを表す型
		using indexBuffer_t = IDirect3DIndexBuffer9*;
#endif
	};
};
#endif