/*!	\file		CModelBank.hpp
*	\details	CModelBankクラスの定義
*	\author		松裏征志
*	\date		2014/12/04
*/
#ifndef MMLIBRARY_CMODEL_BANK_HPP_
#define MMLIBRARY_CMODEL_BANK_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic/Model/CModelBank.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator>
			CModelBank<Allocator>::CModelBank()
				: Utility::CDirectionaryBase<std::shared_ptr<IModel>, Allocator>(
				[](const string_t<Allocator>& name)
			{
				std::shared_ptr<IModel> pModel;

				//name is filepath
				if (name.find(".") != name.npos)
				{
					auto* pTemp = Memory::NewWithAllocator< CModel<Allocator>, Allocator>();
					pTemp->LoadFromFile(name);

					pModel.reset(pTemp, Memory::DeleterForAllocator < IModel, Allocator >());
				}
				//name is not filepath
				else
				{
					if (name.find("2D") == 0)
					{
						auto* pTemp = Memory::NewWithAllocator< CVerticesHasTexture<CVertexFor2D, Allocator>, Allocator>();

						pModel.reset(pTemp, Memory::DeleterForAllocator < IModel, Allocator >());
					}
					else
					{
						auto* pTemp = Memory::NewWithAllocator< CVerticesHasTexture<CVertexFor3D, Allocator>, Allocator>();

						pModel.reset(pTemp, Memory::DeleterForAllocator < IModel, Allocator >());
					}
				}

				return pModel;
			})
			{}
		}
	}
}
#endif	//	MMLIBRARY_CMODEL_BANK_HPP_