/*!	\file		IModel.h
*	\details	IModelクラスの宣言
*	\author		松裏征志
*	\date		2014/05/07
*/
#ifndef MMLIBRARY_IMODEL_H_
#define MMLIBRARY_IMODEL_H_

//***********************************************************
//インクルード
//***********************************************************
#include <functional>

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	メッシュの描画用のインターフェイスクラス
			class IModel
			{
			protected:
				std::function<void(void)> m_pRenderBeginFunction;
			public:
				//!	コンストラクタ
				IModel(void)
					:m_pRenderBeginFunction(){};

				//!	コピーコンストラクタ
				IModel(const IModel& src)
					:m_pRenderBeginFunction(src.m_pRenderBeginFunction){};

				//!	ムーブコンストラクタ
				IModel(IModel&& src)
					:m_pRenderBeginFunction(src.m_pRenderBeginFunction){};

				//!	デストラクタ
				virtual	~IModel(void){};

				//!	描画
				virtual	void	Render(void) = 0;

				//!	描画前処理を設定する
				template<class Function>
				void	SetRenderBeginFunction(Function&& pRenderBeginFunction)
				{
					m_pRenderBeginFunction = pRenderBeginFunction;
				};
			};
		}
	}
}

#endif