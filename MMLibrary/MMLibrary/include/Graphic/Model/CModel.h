/*!	\file		CModel.h
*	\details	CMeshXクラスの宣言
*	\author		松裏征志
*	\date		2014/06/12
*/
#ifndef MMLIBRARY_CMODEL_H_
#define MMLIBRARY_CMODEL_H_

//***********************************************************
//インクルード
//***********************************************************
#include <vector>
#include <string>
#include <memory>
#include <assert.h>
#include "Utility\DefineClassCreateHelpers.h"
#include "Graphic/Model/CFrame.h"
#include "Graphic/Model/CMaterial.h"
#include "Graphic/Model/CAnimationPlayer.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator >
			class CMaterial;

			//!	3Dモデルを表すクラス
			template<template<class> class Allocator = USE_ALLOCATOR >
			class CModel	:	public IModel
			{
				template<template<class> class Allocator >
					friend class CMeshXLoader;
			protected:
				std::shared_ptr<CAnimationPlayer<Allocator> >	m_pAnimationPlayer;
				vector_t<CAnimationSet<Allocator>, Allocator>	m_animationSets;
				vector_t<CBone<Allocator>, Allocator>			m_bones;
				vector_t<CMesh<Allocator>, Allocator>			m_meshes;
	
			public:
				//!	コンストラクタ
				CModel(void);

				CREATE_REFERRENCE(Meshes, m_meshes);
				CREATE_CONST_REFERRENCE(Meshes, m_meshes);
				CREATE_GETTER(Meshes, m_meshes);

				CREATE_REFERRENCE(Bones, m_bones);
				CREATE_CONST_REFERRENCE(Bones, m_bones);
				CREATE_GETTER(Bones, m_bones);

				CREATE_REFERRENCE(AnimationSets, m_animationSets);
				CREATE_CONST_REFERRENCE(AnimationSets, m_animationSets);
				CREATE_GETTER(AnimationSets, m_animationSets);

				//!	描画する
				void	Render(void);

				void	LoadFromFile(const string_t<Allocator>& filePath);
				
				void	SaveToXbinFile(const string_t<Allocator>& filePath);

				std::weak_ptr<CAnimationPlayer<Allocator> >
				GetAnimationPlayer(void)
				{
					return m_pAnimationPlayer;
				};

				void	Dispose(void);

				void CreateAnimationPlayer(void);

static			CModel<Allocator>
					CreateSphere(
						uint_t slice,
						uint_t stack,
						float radius);

				CModel<Allocator>&
					SetBySphere(
						uint_t slice,
						uint_t stack,
						float radius);

static			CModel<Allocator>
					CreateByBox(void);

				CModel<Allocator>&
					SetByBox(void);

				void SetMaterial(
					uint_t					index,
					CMaterial<Allocator>&	material);
				
				void SizeNormalize(void);
			};
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CModel.hpp"

#include "Utility\UndefineClassCreateHelpers.h"

#endif