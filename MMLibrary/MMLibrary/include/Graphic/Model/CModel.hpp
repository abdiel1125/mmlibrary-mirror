/*!	\file		CModel.hpp
*	\details	CMeshXクラスの定義
*	\author		松裏征志
*	\date		2014/06/12
*/
#ifndef MMLIBRARY_CMODEL_HPP_
#define MMLIBRARY_CMODEL_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CModel.h"
#include "Graphic/CVector3.h"
#include "Graphic/Model/CMeshXLoader.h"
#include "Collision\CAxisAlignedBoundingBox.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator>
			void	CModel<Allocator>::Render(void)
			{
				if(m_pRenderBeginFunction)
					m_pRenderBeginFunction();

				for (auto& mesh : m_meshes)
					mesh.Render();
			};

			template<template<class> class Allocator>
			CModel<Allocator>::CModel(void)
			{};

			template<template<class> class Allocator>
			void	CModel<Allocator>::LoadFromFile(
				const string_t<Allocator>& filePath)
			{
				Dispose();
				LoadMeshTo(*this, filePath);
			};
			template<template<class> class Allocator>
			void	CModel<Allocator>::SaveToXbinFile(
				const string_t<Allocator>& filePath)
			{
				CModelSaver<Allocator> saver(*this);
				saver.SaveModelTo(filePath);
			};

			template<template<class> class Allocator>
			void	CModel<Allocator>::Dispose(void)
			{
				m_animationSets.clear();
				m_bones.clear();
				m_meshes.clear();
			};

			template<template<class> class Allocator>
			void CModel<Allocator>::CreateAnimationPlayer(void)
			{
				if (GetBones().size())
				{
					m_pAnimationPlayer.reset(new CAnimationPlayer<Allocator>);

					m_pAnimationPlayer->SetBonesPointer(m_bones);
					m_pAnimationPlayer->SetAnimationSetsPointer(m_animationSets);
					m_pAnimationPlayer->CalcMatrix(true);
				}
			};

			template<template<class> class Allocator>
			CModel<Allocator>
				CModel<Allocator>::CreateSphere(
				uint_t slice,
				uint_t stack,
				float radius)
			{
				return Model::CModel<Allocator>().SetBySphere(
					slice,
					stack,
					radius);
			};

			template<template<class> class Allocator>
			CModel<Allocator>&
				CModel<Allocator>::SetBySphere(
				uint_t slice,
				uint_t stack,
				float radius)
			{
				Dispose();

				m_bones.resize(1);

				m_meshes.resize(1);
				m_meshes[0].SetBySphere(
				slice,
				stack,
				radius);
				m_meshes[0].SetBonesPointer(&m_bones);

				return *this;
			};

			template<template<class> class Allocator>
			CModel<Allocator>
				CModel<Allocator>::CreateByBox(void)
			{
				return CModel<Allocator>().SetbyBox();
			};

			template<template<class> class Allocator>
			CModel<Allocator>&
				CModel<Allocator>::SetByBox(void)
			{
				Dispose();

				m_bones.resize(1);

				m_meshes.resize(1);
				m_meshes[0].SetByBox();
				m_meshes[0].SetBonesPointer(&m_bones);

				return *this;
			};

			template<template<class> class Allocator>
			void CModel<Allocator>::SetMaterial(
				uint_t					index,
				CMaterial<Allocator>&	material)
			{
				if (m_meshes.size() > index)
					m_meshes.at(index).SetMaterial(material);
			};

			template<template<class> class Allocator>
			void CModel<Allocator>::SizeNormalize(void)
			{
				if (m_bones.empty() == true) return;

				Collision::CAxisAlignedBoundingBox aabb;
				aabb.SetByCModel(*this);

				float ratio = 1 / std::max(
				{
					aabb.Max()[0], -aabb.Min()[0],
					aabb.Max()[1], -aabb.Min()[1],
					aabb.Max()[2], -aabb.Min()[2] 
				});
				auto matrix = CMatrix4x4::CreateByScaling(CVector3(ratio, ratio, ratio));

				for (auto& mesh : m_meshes)
				{
					auto index = mesh.GetBoneIndex();

					m_bones[index].SetOffsetMatrix(m_bones[index].GetOffsetMatrix() * matrix);
					m_bones[index].DrawMatrix() *= matrix;
				}
			};
		};
	};
};

#endif