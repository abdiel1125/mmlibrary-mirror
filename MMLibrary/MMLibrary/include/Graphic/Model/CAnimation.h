/*!	\file		CAnimation.h
*	\details	CAnimationクラスの宣言
*	\author		松裏征志
*	\date		2014/09/16
*/
#ifndef MMLIBRARY_CANIMATION_H_
#define MMLIBRARY_CANIMATION_H_

//***********************************************************
//インクルード
//***********************************************************
#include <vector>
#include "Graphic\CTransform.h"
#include "Utility/DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	キーフレームの情報を表すクラス
			class CkeyFrame
			{
				template<template<class> class Allocator>
				friend class CMeshXLoader;
				template<template<class> class Allocator>
				friend class CAnimationPlayer;
			private:
				uint_t						m_time;
				bool						m_isUseMatrix;
				CTransform			m_transformMatrixSet;
				CMatrix4x4					m_matrix;

			public:
				//!	コンストラクタ
				CkeyFrame(void)
					: m_time()
					, m_transformMatrixSet()
					, m_matrix()
				{};
			};

			//!	アニメーション情報を表すクラス
			template<template<class> class Allocator>
			class CAnimation
			{
				template<template<class> class Allocator>
				friend class CMeshXLoader;
				template<template<class> class Allocator>
				friend class CAnimationPlayer;
			private:
				string_t<Allocator>												m_name;
				string_t<Allocator>												m_boneName;
				vector_t<std::pair<uint_t, Math::CQuaternion<> >, Allocator>	m_keyFramesOfRotation;
				vector_t<std::pair<uint_t, CVector3>, Allocator>				m_keyFramesOfScale;
				vector_t<std::pair<uint_t, CVector3>, Allocator>				m_keyFramesOfTranslate;
				vector_t<std::pair<uint_t, CMatrix4x4>, Allocator>				m_keyFramesOfMatrix;

			public:
				//!	コンストラクタ
				CAnimation(void)
					: m_name()
					, m_boneName()
					, m_keyFramesOfRotation()
					, m_keyFramesOfScale()
					, m_keyFramesOfTranslate()
					, m_keyFramesOfMatrix()
				{};
#pragma region Reference

				//!	名前
				CREATE_REFERRENCE(Name, m_name);

				//!	対応するボーンの名前
				CREATE_REFERRENCE(BoneName, m_boneName);

				//!	回転についての情報
				CREATE_REFERRENCE(KeyFramesOfRotation, m_keyFramesOfRotation);

				//!	拡縮についての情報
				CREATE_REFERRENCE(KeyFramesOfScale, m_keyFramesOfScale);

				//!	平行移動についての情報
				CREATE_REFERRENCE(KeyFramesOfTranslate, m_keyFramesOfTranslate);

				//!	座標変換行列についての情報
				CREATE_REFERRENCE(KeyFramesOfMatrix, m_keyFramesOfMatrix);

#pragma endregion

#pragma region Getter

				//!	名前の取得
				CREATE_GETTER(Name, m_name);

				//!	対応するボーンの名前の取得
				CREATE_GETTER(BoneName, m_boneName);

				//!	回転についての情報の取得
				CREATE_GETTER(KeyFramesOfRotation, m_keyFramesOfRotation);

				//!	拡縮についての情報の取得
				CREATE_GETTER(KeyFramesOfScale, m_keyFramesOfScale);

				//!	平行移動についての情報の取得
				CREATE_GETTER(KeyFramesOfTranslate, m_keyFramesOfTranslate);

				//!	座標変換行列についての情報の取得
				CREATE_GETTER(KeyFramesOfMatrix, m_keyFramesOfMatrix);

				//!	最も多いフレーム数の取得
				size_t GetMaxFrameNum() const
				{
					return std::max(
					{
						m_keyFramesOfRotation.size(),
						m_keyFramesOfScale.size(),
						m_keyFramesOfTranslate.size(),
						m_keyFramesOfMatrix.size(),
					});
				}

#pragma endregion
				//!	指定した時間における回転についての情報
				Math::CQuaternion<> GetRotationAt(float time) const
				{
					if (m_keyFramesOfRotation.size() == 0)
						return Math::CQuaternion<>();

					if (m_keyFramesOfRotation.size() == 1)
						return m_keyFramesOfRotation[0].second;

					auto keyFrameIt = Range::FindIf(
						m_keyFramesOfRotation,
						[&](const std::pair<uint_t, Math::CQuaternion<> >& keyFrame)->bool
					{
						return keyFrame.first > time;
					});

					if (keyFrameIt == m_keyFramesOfRotation.begin())
					{
						return keyFrameIt->second;
					}
					else if (keyFrameIt == m_keyFramesOfRotation.end())
					{
						return (m_keyFramesOfRotation.end() - 1)->second;
					}
					else
					{
						auto beginkeyFrameIt = keyFrameIt - 1;
						float ratio = static_cast<float>(time - beginkeyFrameIt->first) / (keyFrameIt->first - beginkeyFrameIt->first);

						return Math::CQuaternion<>::CreateBySlerp(beginkeyFrameIt->second, keyFrameIt->second, ratio);
					}
				};

				//!	指定した時間における拡縮についての情報
				CVector3 GetScaleAt(float time) const
				{
					if (m_keyFramesOfScale.size() == 0)
						return CVector3(1);

					if (m_keyFramesOfScale.size() == 1)
						return m_keyFramesOfScale[0].second;

					auto keyFrameIt = Range::FindIf(
						m_keyFramesOfScale,
						[&](const std::pair<uint_t, CVector3>& keyFrame)->bool
					{
						return keyFrame.first > time;
					});

					if (keyFrameIt == m_keyFramesOfScale.begin())
					{
						return keyFrameIt->second;
					}
					else if (keyFrameIt == m_keyFramesOfScale.end())
					{
						return (m_keyFramesOfScale.end() - 1)->second;
					}
					else
					{
						auto beginkeyFrameIt = keyFrameIt - 1;
						float ratio = static_cast<float>(time - beginkeyFrameIt->first) / (keyFrameIt->first - beginkeyFrameIt->first);

						return Math::Interpolation::Linear(beginkeyFrameIt->second, keyFrameIt->second, ratio);
					}
				};

				//!	指定した時間における平行移動についての情報
				CVector3 GetTranslateAt(float time) const
				{
					if (m_keyFramesOfTranslate.size() == 0)
						return CVector3(0);

					if (m_keyFramesOfTranslate.size() == 1)
						return m_keyFramesOfTranslate[0].second;

					auto keyFrameIt = Range::FindIf(
						m_keyFramesOfTranslate,
						[&](const std::pair<uint_t, CVector3>& keyFrame)->bool
					{
						return keyFrame.first > time;
					});

					if (keyFrameIt == m_keyFramesOfTranslate.begin())
					{
						return keyFrameIt->second;
					}
					else if (keyFrameIt == m_keyFramesOfTranslate.end())
					{
						return (m_keyFramesOfTranslate.end() - 1)->second;
					}
					else
					{
						auto beginkeyFrameIt = keyFrameIt - 1;
						float ratio = static_cast<float>(time - beginkeyFrameIt->first) / (keyFrameIt->first - beginkeyFrameIt->first);

						return Math::Interpolation::Linear(beginkeyFrameIt->second, keyFrameIt->second, ratio);
					}
				};

				//!	指定した時間における座標変換行列についての情報
				CMatrix4x4 GetMatrixAt(float time) const
				{
					if (m_keyFramesOfMatrix.size() == 0)
						return CMatrix4x4::CreateByIdentity();

					if (m_keyFramesOfMatrix.size() == 1)
						return m_keyFramesOfMatrix[0].second;

					auto keyFrameIt = Range::FindIf(
						m_keyFramesOfMatrix,
						[&](const std::pair<uint_t, CMatrix4x4>& keyFrame)->bool
					{
						return keyFrame.first > time;
					});

					if (keyFrameIt == m_keyFramesOfMatrix.begin())
					{
						return keyFrameIt->second;
					}
					else if (keyFrameIt == m_keyFramesOfMatrix.end())
					{
						return (m_keyFramesOfMatrix.end() - 1)->second;
					}
					else
					{
						auto beginkeyFrameIt = keyFrameIt - 1;
						float ratio = static_cast<float>(time - beginkeyFrameIt->first) / (keyFrameIt->first - beginkeyFrameIt->first);

						return Math::Interpolation::Linear(beginkeyFrameIt->second, keyFrameIt->second, ratio);
					}

				};

				//!	無駄なデータを削減する
				void Optimize()
				{
					OptimizeImpl(m_keyFramesOfRotation);
					OptimizeImpl(m_keyFramesOfScale);
					OptimizeImpl(m_keyFramesOfTranslate);
					OptimizeImpl(m_keyFramesOfMatrix);
				}

				private:
					template<class T>
					void OptimizeImpl(vector_t<std::pair<uint_t, T >, Allocator>& container)
					{
						if (container.size() < 3) return;

						auto prev = container.begin();
						auto next = prev + 2;
						container.erase(
							std::remove_if(prev + 1, container.end() - 1,
							[&](std::pair<uint_t, T >& now)
						{
							bool rtn = true;

							if (prev->second != now.second) rtn &= false;
							if (now.second != next->second) rtn &= false;

							++prev;
							++next;

							return rtn;
						}), container.end());
					}
			};

			//!	アニメーションの集合を表すクラス
			template<template<class> class Allocator>
			class CAnimationSet
			{
				template<template<class> class Allocator>
				friend class CMeshXLoader;
				template<template<class> class Allocator>
				friend class CAnimationPlayer;
			public:
				//!	自身の型を表す
				using myType = CAnimationSet < Allocator >;
				//!	文字列の型を表す
				using string_t = string_t < Allocator >;
				//!	アニメーションの動的配列の型を表す
				using animations_t = vector_t<CAnimation<Allocator>, Allocator>;
			private:
				string_t		m_name;
				animations_t	m_animations;
				uint_t			m_maxTime;

			public:
				//!	コンストラクタ
				CAnimationSet(void)
					: m_name()
					, m_animations()
					, m_maxTime(0)
				{};

				//!	無駄なデータを削減する
				void Optimize()
				{
					OutputDebugString("Optimize Animation ");
					OutputDebugString(m_name.c_str());
					OutputDebugString("Begin \n");

					for (auto& animation : m_animations)
						animation.Optimize();
				}
#pragma region Reference

				//!	名前
				string_t&	Name()
				{
					return m_name;
				};
				//!	アニメーションの動的配列
				animations_t&
					Animations()
				{
					return m_animations;
				};
				//!	利用しうる最大時間
				uint_t&				MaxTime()
				{
					return m_maxTime;
				};

#pragma endregion

#pragma region Getter

				//!	名前の取得
				const string_t&	GetName() const
				{
					return m_name;
				};
				//!	アニメーションの動的配列の取得
				const animations_t&
					GetAnimations() const
				{
					return m_animations;
				};
				//!	利用しうる最大時間の取得
				const uint_t&	GetMaxTime() const
				{
					return m_maxTime;
				};
#pragma endregion

			};
		};
	};
};

#include "Utility/UndefineClassCreateHelpers.h"

#endif