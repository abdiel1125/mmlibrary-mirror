/*!	\file		CBillBoard.cpp
*	\details	CBillBoardクラスの定義
*	\author		松裏征志
*	\date		2014/07/07
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/CGraphicManager.h"
#include "Graphic/Model/CBillBoard.h"


namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator = USE_ALLOCATOR>
			CBillBoard<Allocator>::CBillBoard(void)
				:CVerticesHasTexture()
			{};

			template<template<class> class Allocator = USE_ALLOCATOR>
			void	CBillBoard<Allocator>::Render(void)
			{
				auto viewMatrix = CGraphicManager::GetManager().GetViewMatrix();
				CGraphicManager::GetManager().SetViewMatrix(CMatrix4x4());

				CMatrix4x4 worldMatrix =
					CGraphicManager::GetManager().Stack.
					GetTop();
				worldMatrix.Multiply(viewMatrix);
				auto scale = worldMatrix.GetScale();
				auto position = worldMatrix.GetPosition();

				CGraphicManager::GetManager().Stack.
					Push().
					LoadIdentity().
					MultiplyLocal(CMatrix4x4(
					scale[0], 0, 0, 0,
					0, scale[1], 0, 0,
					0, 0, scale[2], 0,
					position[0], position[1], position[2], 1));
				CGraphicManager::GetManager().SetWorldMatrix();

				CVerticesHasTexture::Render();

				CGraphicManager::GetManager().Stack.
					Pop();

				CGraphicManager::GetManager().SetViewMatrix(viewMatrix);
			};
		}
	}
}