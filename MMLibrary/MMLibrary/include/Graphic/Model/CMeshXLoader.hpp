/*!	\file		CMeshXLoader.hpp
*	\details	CMeshXLoaderクラスの定義
*	\author		松裏征志
*	\date		2014/09/08
*/
#ifndef MMLIBRARY_CMESHX_LOADER_HPP_
#define MMLIBRARY_CMESHX_LOADER_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include <numeric>
#include "Graphic/Model/CMeshXLoader.h"
#include "Memory\Memory.h"
#include "Utility/RangeAlgorithm.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator >
			CMeshXLoader<Allocator>::CMeshXLoader(void)
				: m_connects()
				, m_memory()
				, m_skinWeightSet()
				, m_animationSets()
				, m_frameNum(0)
				, m_frameRoot()
				, m_materials()
				, m_pFramesHasMesh()
				, m_pModel(nullptr)
				, m_pNowAnimationSet(nullptr)
				, m_pNowFrame(nullptr)
				, m_pNowMesh(nullptr)
				, m_vertices()
			{};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXPosition(void)
			{
				OutputDebugString("Load Position ");
				OutputDebugString(m_pNowMesh->m_name.c_str());
				OutputDebugString("Begin \n");

				//　頂点座標を取得
				m_vertices.resize(m_token.GetNextAsInt());

				for (auto& vertex : m_vertices)
				{
					LoadVector3(vertex.Position());
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXConnect(void)
			{
				OutputDebugString("Load Connect ");
				OutputDebugString(m_pNowMesh->m_name.c_str());
				OutputDebugString("Begin \n");

				//結線情報取得
				m_connects.resize(m_token.GetNextAsInt());
				size_t faceIndex = 0;

				for (auto& connect : m_connects)
				{
					m_token.Next();
					if (m_token.Check("3"))
					{
						connect.m_num = m_token.GetAsInt();

						for (int i = 0; i < 3; ++i)
						{
							connect.m_index.at(i) = m_token.GetNextAsInt();
						}
					}
					else if (m_token.Check("4"))
					{
						connect.m_num = m_token.GetAsInt();

						for (int i = 0; i < 4; ++i)
						{
							connect.m_index.at(i) = m_token.GetNextAsInt();
						}
					}
					++faceIndex;
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::CreateNormalData(void)
			{
				//法線情報の作成
				vector_t<vector_t<CVector3>, Allocator<vector_t<CVector3>>> faceNormalsPerVertex;
				faceNormalsPerVertex.resize(m_vertices.size());

				for (auto& face : m_pNowMesh->m_face)
				{
					CVector3 faceNormal;

					faceNormal = 
						(m_vertices[face.m_index[1]].Position() - m_vertices[face.m_index[0]].Position()).
						Cross(m_vertices[face.m_index[2]].Position() - m_vertices[face.m_index[0]].Position());

					faceNormal.Normalize();

					for (int i = 0; i < 3; ++i)
						if (!Range::AnyOf(
							faceNormalsPerVertex[face.m_index[i]],
							[&](CVector3& src)
						{
							return	faceNormal == src;
						}))
							faceNormalsPerVertex[face.m_index[i]].push_back(faceNormal);
				}

				for (uint_t i = 0; i < m_vertices.size(); ++i)
				{
					for (auto& normal : faceNormalsPerVertex[i])
						m_vertices[i].Normal() += *it;

					m_vertices[i].Normal() /= (float)faceNormalsPerVertex[i].size();

					m_vertices[i].Normal().Normalize();
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXMeshNormals(void)
			{
				OutputDebugString("Load Normal ");
				OutputDebugString(m_pNowMesh->m_name.c_str());
				OutputDebugString("Begin \n");

				//　'{'が出るまでスキップ
				SkipToBlockStart();

				//　法線ベクトル数を取得
				int nomalSize = m_token.GetNextAsInt();

				//　法線ベクトルを取得
				vector_t< CVector3, Allocator> normals;
				normals.resize(nomalSize);

				for (auto& normal : normals)
				{
					LoadVector3(normal);
				}

				//　法線インデックス数を取得
				int indicesSize = m_token.GetNextAsInt();

				//　法線インデックスを取得し正しい位置に格納する
				for (int i = 0; i < indicesSize; ++i)
				{
					int vertexNum = m_token.GetNextAsInt();
					for (int j = 0; j < vertexNum; ++j)
					{
						m_vertices.at(m_connects.at(i).m_index.at(j)).Normal() = normals.at(m_token.GetNextAsInt());
					}
				}

				SkipToBlockEnd();
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXMeshTextureCoords(void)
			{
				OutputDebugString("Load TextureCoords ");
				OutputDebugString(m_pNowMesh->m_name.c_str());
				OutputDebugString("Begin \n");

				//　'{'が出るまでスキップ
				SkipToBlockStart();

				//　テクスチャ座標数を取得
				int texCoordSize = 0;
				texCoordSize = m_token.GetNextAsInt();

				//　テクスチャ座標を取得
				vector_t<textureCoords_t, Allocator> texCoords(texCoordSize);
				for (auto& texCoord : texCoords)
				{
					texCoord.at(0) = m_token.GetNextAsFloat();
					texCoord.at(1) = m_token.GetNextAsFloat();
				}

				m_token.Next();
				//テクスチャ座標インデックスが存在しない
				if (m_token.Check("}"))
				{
					for (uint_t i = 0;
						i < m_vertices.size();
						++i)
					{
						m_vertices.at(i).UV() = texCoords.at(i);
					}
				}
				//テクスチャ座標インデックスが存在する
				else
				{
					//　法線インデックス数を取得
					int indicesSize = m_token.GetAsInt();

					//　テクスチャ座標インデックスを取得し正しい位置に格納する
					for (int i = 0; i < indicesSize; ++i)
					{
						int vertexNum = m_token.GetNextAsInt();
						for (int j = 0; j < vertexNum; ++j)
						{
							m_vertices.at(m_connects.at(i).m_index.at(j)).UV() = texCoords.at(m_token.GetNextAsInt());
						}
					}
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXMeshMaterialList(void)
			{
				OutputDebugString("Load MaterialList ");
				OutputDebugString(m_pNowMesh->m_name.c_str());
				OutputDebugString("Begin \n");

				// '{'が出るまでスキップ
				SkipToBlockStart();

				//　マテリアル数を取得
				int materialSize = m_token.GetNextAsInt();
				m_pNowMesh->m_materials.resize(materialSize);

				//　マテリアルインデックス数を取得
				int indicesSize = m_token.GetNextAsInt();

				//　マテリアルインデックス取得
				if (indicesSize == 1)
				{
					auto materialIndex = m_token.GetNextAsInt();
					for (auto& connect : m_connects)
					{
						connect.m_materialIndex = materialIndex;
					}
				}
				else
				{
					for (auto& connect : m_connects)
					{
						connect.m_materialIndex = m_token.GetNextAsInt();
					}
				}
				m_token.Next();
				for (int i = 0; i < materialSize; i++)
				{
					if (m_token.Check("{"))
					{
						string_t materialName = m_token.GetNextAsChar();

						auto it = Range::FindIf(
							m_materials,
							[&](CMaterial<Allocator>& material)->bool
						{
							return material.m_name == materialName;
						});

						if (it != m_materials.end())
						{
							m_pNowMesh->m_materials.at(i) = *it;
						}

						SkipToBlockEnd();
					}
					else
					{
						LoadXMaterial(m_pNowMesh->m_materials.at(i), i);
					}
					m_token.Next();
				}
				SkipToBlockEnd();
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXMaterial(CMaterial<Allocator>& material, int i)
			{
				OutputDebugString("Load Material ");
				OutputDebugString(m_pNowMesh->m_name.c_str());
				OutputDebugString("Begin \n");

				//　"Material"が出るまでスキップ
				while (!m_token.IsEnd())
				{
					if (m_token.Check("Material"))
						break;

					m_token.Next();
				}

				//　マテリアル名を取得
				LoadName(material.m_name, "material_", i);

				// Diffuse取得
				array_t<float, 4> diffuse;
				LoadVector4(diffuse);
				material.SetDiffuse(CColor32(diffuse));

				// Power取得
				material.SetPower(m_token.GetNextAsFloat());

				// Specular取得
				CVector3 specular;
				LoadVector3(specular);
				material.SetSpecular(CColor32(specular.at(0), specular.at(1), specular.at(2), 1.0f));

				// Emissive取得
				CVector3 emissive;
				LoadVector3(emissive);
				material.SetEmissive(CColor32(emissive.at(0), emissive.at(1), emissive.at(2), 1.0f));

				// 次のトークンを取得しチェックする
				m_token.Next();
				if (m_token.Check("}"))
				{
					return;
				}
				else if (
					m_token.Check("TextureFileName") ||
					m_token.Check("TextureFilename"))
				{
					// '{'が出るまでスキップ
					SkipToBlockStart();

					//　テクスチャファイル名取得
					material.SetTextureFilePath(m_token.GetNextAsChar());

					//	テクスチャ取得
					material.SetTexturePointer(
						CTextureBank<Allocator>::GetInstance().
						Request(material.GetTextureFilePath()));

					// '}'が出るまでスキップ
					SkipToBlockEnd();
					m_token.Next();
					SkipToBlockEnd();
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXSkinWeights(void)
			{
				//　'{'が出るまでスキップ
				SkipToBlockStart();

				tagSkinWeight skinWeight;

				//　フレーム名を取得する
				skinWeight.m_name = string_t(m_token.GetNextAsChar());
				OutputDebugString("Load SkinWeights ");
				OutputDebugString(skinWeight.m_name.c_str());
				OutputDebugString("Begin \n");

				//　ウェイト数を取得する
				int weightSize = m_token.GetNextAsInt();
				skinWeight.m_array.resize(weightSize);

				//　頂点番号を取得する
				for (auto& it : skinWeight.m_array)
				{
					it.m_index = m_token.GetNextAsInt();
				}

				//　ウェイトを取得する
				for (auto& it : skinWeight.m_array)
				{
					it.m_weight = m_token.GetNextAsFloat();
				}

				//　オフセット行列を取得する
				LoadMatrix4x4(skinWeight.m_offsetMatrix);

				// '}'が出るまでスキップ
				SkipToBlockEnd();

				//　格納
				m_skinWeightSet.push_back(skinWeight);
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXAnimationSet(void)
			{
				CAnimationSet<Allocator> animationSet;

				//　アニメーションセット名を取得
				LoadName(animationSet.m_name, "clip_", m_pModel->m_animationSets.size());

				OutputDebugString("Load AnimationSet ");
				OutputDebugString(animationSet.m_name.c_str());
				OutputDebugString("Begin \n");

				m_pModel->m_animationSets.push_back(animationSet);
				m_pNowAnimationSet = &m_pModel->m_animationSets.at(m_pModel->m_animationSets.size() - 1);
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXAnimation(void)
			{
				CAnimation<Allocator>		animation;

				//　アニメーション名を取得
				LoadName(animation.m_name, "animation_", m_pNowAnimationSet->m_animations.size());

				OutputDebugString("Load Animation ");
				OutputDebugString(animation.m_name.c_str());
				OutputDebugString("Begin \n");

				m_token.Next();
				if (m_token.Check("{"))
				{
					//　ボーン名を取得
					animation.m_boneName = m_token.GetNextAsChar();

					SkipToBlockEnd();
					m_token.Next();
				}
				while (m_token.Check("AnimationKey"))
				{
					SkipToBlockStart();

					//　タイプを取得
					int type = m_token.GetNextAsInt();

					//　タイプにより処理を分岐
					switch (type)
					{
					case 0:	//　回転
						//　キーサイズを取得
						animation.m_keyFramesOfRotation.resize(m_token.GetNextAsInt());

						//　キーサイズ分ループ
						for (auto& keyFrame : animation.m_keyFramesOfRotation)
						{
							//　時間とパラメータサイズを取得
							uint_t time = static_cast<uint_t>(m_token.GetNextAsInt());
							m_token.Next();

							//　時間を設定
							keyFrame.first = time;
							m_pNowAnimationSet->m_maxTime = std::max(m_pNowAnimationSet->m_maxTime, time);

							keyFrame.second.Scalar() = m_token.GetNextAsFloat();
							LoadVector3(keyFrame.second.Vector());
						}
						break;
					case 1:	//　拡大縮小
						//　キーサイズを取得
						animation.m_keyFramesOfScale.resize(m_token.GetNextAsInt());

						//　キーサイズ分ループ
						for (auto& keyFrame : animation.m_keyFramesOfScale)
						{
							//　時間とパラメータサイズを取得
							uint_t time = static_cast<uint_t>(m_token.GetNextAsInt());
							m_token.Next();

							//　時間を設定
							keyFrame.first = time;
							m_pNowAnimationSet->m_maxTime = std::max(m_pNowAnimationSet->m_maxTime, time);

							LoadVector3(keyFrame.second);
						}
						break;
					case 2:	//　平行移動
						//　キーサイズを取得
						animation.m_keyFramesOfTranslate.resize(m_token.GetNextAsInt());

						//　キーサイズ分ループ
						for (auto& keyFrame : animation.m_keyFramesOfTranslate)
						{
							//　時間とパラメータサイズを取得
							uint_t time = static_cast<uint_t>(m_token.GetNextAsInt());
							m_token.Next();

							//　時間を設定
							keyFrame.first = time;
							m_pNowAnimationSet->m_maxTime = std::max(m_pNowAnimationSet->m_maxTime, time);

							LoadVector3(keyFrame.second);
						}
						break;
					case 3:	//　行列
					case 4: // Tiny専用措置
						//　キーサイズを取得
						animation.m_keyFramesOfMatrix.resize(m_token.GetNextAsInt());

						//　キーサイズ分ループ
						for (auto& keyFrame : animation.m_keyFramesOfMatrix)
						{
							//　時間とパラメータサイズを取得
							uint_t time = static_cast<uint_t>(m_token.GetNextAsInt());
							m_token.Next();

							//　時間を設定
							keyFrame.first = time;
							m_pNowAnimationSet->m_maxTime = std::max(m_pNowAnimationSet->m_maxTime, time);

							LoadMatrix4x4(keyFrame.second);
						}
						break;
					}
					SkipToBlockEnd();
					m_token.Next();
				}
				if (m_token.Check("{"))
				{
					//　ボーン名を取得
					animation.m_boneName = m_token.GetNextAsChar();

					SkipToBlockEnd();
					m_token.Next();
				}
				if (m_token.Check("}"))
				{
					m_pNowAnimationSet->m_animations.push_back(animation);

					//　終了
					return;
				}
				else
				{
					m_pNowAnimationSet->m_animations.push_back(animation);
					//　ノードをチェック
					CheckNode();
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::CreateBoneFromFrame(
				CFrame<Allocator>* pFrame,
				int parentIndex)
			{
				if (!pFrame)
					return;

				CBone<Allocator> bone;
				bone.m_name = pFrame->m_name;
				bone.m_inverseOffsetMatrix = pFrame->m_transformMatrix;
				bone.m_offsetMatrix = pFrame->m_transformMatrix.ToClone().SetByInverse();
				bone.m_parentIndex = parentIndex;

				m_pModel->m_bones.push_back(bone);

				auto it = Range::FindIf(
					m_pFramesHasMesh,
					[&](CFrame<Allocator>* pFrame2) ->bool
				{
					return pFrame2 == pFrame;
				});

				if (it != m_pFramesHasMesh.end())
				{
					uint_t meshIndex = it - m_pFramesHasMesh.begin();

					m_pModel->m_meshes.at(meshIndex).m_boneIndex = m_pModel->m_bones.size() - 1;
				}

				if (pFrame->GetChild())
					CreateBoneFromFrame(pFrame->GetChild(), m_pModel->m_bones.size() - 1);

				if (pFrame->GetBrother())
					CreateBoneFromFrame(pFrame->GetBrother(), parentIndex);
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::CreateBones(void)
			{
				OutputDebugString("CreateBones ");
				OutputDebugString("Begin \n");

				m_pModel->m_bones.reserve(m_skinWeightSet.size());

				CreateBoneFromFrame(m_frameRoot.get(), -1);

				//スキンウェイトとボーンの関連付け
				for (auto skinWeights : m_skinWeightSet)
				{
					auto bone = Range::FindIf(
						m_pModel->m_bones,
						[&skinWeights](CBone<Allocator>& bone)->bool
					{
						return bone.GetName() == skinWeights.m_name;
					});

					if (bone != m_pModel->m_bones.end())
						bone->m_offsetMatrix = skinWeights.m_offsetMatrix;
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::CreateSubSet(void)
			{
				OutputDebugString("CreateSubSet ");
				OutputDebugString("Begin \n");

				if (m_pNowMesh->m_materials.empty())
				{
					//サブセット作成
					m_pNowMesh->m_subSets.resize(1);

					for (auto& subset : m_pNowMesh->m_subSets)
					{
						subset.m_boneIndices.fill(-1);
						subset.m_materialIndex = 0;
					}

					//頂点に対応する材質を設定
					for (const auto& connect : m_connects)
					{
						if (connect.m_num == 3)
						{
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(0));
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(1));
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(2));
						}
						else if (connect.m_num == 4)
						{
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(0));
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(1));
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(2));

							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(0));
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(2));
							m_pNowMesh->m_subSets[0].
								m_vertexIndices.
								push_back(connect.m_index.at(3));
						}
					}
				}
				else
				{
					//サブセット作成
					m_pNowMesh->m_subSets.resize(m_pNowMesh->m_materials.size());

					for (auto it = m_pNowMesh->m_subSets.begin();
						it != m_pNowMesh->m_subSets.end();
						++it)
					{
						it->m_boneIndices.fill(-1);
						it->m_materialIndex = it - m_pNowMesh->m_subSets.begin();
					}

					//頂点に対応する材質を設定
					for (const auto& connect : m_connects)
					{
						if (connect.m_num == 3)
						{
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(0));
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(1));
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(2));
						}
						else if (connect.m_num == 4)
						{
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(0));
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(1));
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(2));

							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(0));
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(2));
							m_pNowMesh->m_subSets[connect.m_materialIndex].
								m_vertexIndices.
								push_back(connect.m_index.at(3));
						}
					}

					for (uint_t i = 0; i < m_pNowMesh->m_subSets.size(); i++)
					{
						m_pNowMesh->m_subSets[i].m_vertexIndices.shrink_to_fit();
						m_pNowMesh->m_subSets[i].m_indexBuffer.SetData(
							m_pNowMesh->m_subSets[i].m_vertexIndices.data(),
							m_pNowMesh->m_subSets[i].m_vertexIndices.size());
					}
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXFrame(void)
			{
				//　フレーム名を取得
				LoadName(m_pNowFrame->m_name, "frame_", m_frameNum);
				++m_frameNum;
				OutputDebugString("Load Frame ");
				OutputDebugString(m_pNowFrame->m_name.c_str());
				OutputDebugString("Begin \n");
				
				//情報取得
				//　次のトークンが"FrameTransformMatrix"であるかを確認
				m_token.Next();
				if (m_token.Check("FrameTransformMatrix"))
				{
					// '{'が出るまでスキップ
					SkipToBlockStart();

					// 行列を取得
					LoadMatrix4x4(m_pNowFrame->m_transformMatrix);

					// '}'が出るまでスキップ
					SkipToBlockEnd();
					m_token.Next();
				}
				if (m_token.Check("Mesh"))
				{
					LoadXMesh();
					m_token.Next();
				}

				//　子フレームがあるかどうかチェック
				auto pThisFrame = m_pNowFrame;
				while (m_token.Check("Frame"))
				{
					//　子フレームがあるならば再帰呼び出し
					m_pNowFrame = pThisFrame;
					m_pNowFrame = m_pNowFrame->AddChild().get();
					LoadXFrame();

					m_token.Next();
				}

				CheckNode();
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::LoadXMesh(void)
			{
				m_pModel->m_meshes.emplace_back();
				m_pNowMesh = &m_pModel->m_meshes.at(m_pModel->m_meshes.size() - 1);
				m_pFramesHasMesh.push_back(m_pNowFrame);

				//名前をセット
				LoadName(m_pNowMesh->m_name, "mesh_", m_pModel->m_meshes.size());
				OutputDebugString("Load Mesh ");
				OutputDebugString(m_pNowMesh->m_name.c_str());
				OutputDebugString("Begin \n");


				//頂点情報・結線情報
				LoadXPosition();
				LoadXConnect();

				m_token.Next();
				while (!m_token.Check("}"))
				{
					if (m_token.Check("MeshNormals"))
					{
						LoadXMeshNormals();
					}
					else if (m_token.Check("MeshTextureCoords"))
					{
						LoadXMeshTextureCoords();
					}
					else if (m_token.Check("MeshVertexColors"))
					{
						//LoadXMeshVertexColors();
					}
					else if (m_token.Check("MeshMaterialList"))
					{
						LoadXMeshMaterialList();
					}
					else if (m_token.Check("XSkinMeshHeader"))
					{
						SkipToBlockEnd();
					}
					else if (m_token.Check("SkinWeights"))
					{
						LoadXSkinWeights();
					}
					else if (m_token.Check("VertexDuplicationIndices"))
					{
						SkipToBlockEnd();
					}
					m_token.Next();
				}

				m_pNowMesh->Vertices().resize(m_vertices.size());
				Range::Copy(
					m_vertices,
					m_pNowMesh->Vertices().begin());
				m_pNowMesh->SetVertexData(m_vertices);
				CreateSubSet();
				m_pNowMesh = nullptr;
				SkipToBlockEnd();
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::CheckNode(void)
			{
				if (m_token.Check("template"))
				{
					SkipNode();
				}
				else if (m_token.Check("Mesh"))
				{
					LoadXMesh();
				}
				else if (m_token.Check("Frame"))
				{
					m_pNowFrame = m_frameRoot->AddChild().get();
					LoadXFrame();
				}
				else if (m_token.Check("AnimationSet"))
				{
					LoadXAnimationSet();
				}
				else if (m_token.Check("Animation"))
				{
					LoadXAnimation();
				}
				else if (m_token.Check("Material"))
				{
					m_materials.push_back(CMaterial<Allocator>());
					LoadXMaterial(m_materials.at(m_materials.size() - 1), m_materials.size() - 1);
				}
			}

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::SkipNode(void)
			{
				// '{'が出るまでスキップ
				while (!m_token.IsEnd())
				{
					m_token.Next();
					if (m_token.Check("{"))
						break;
				}

				// ブロック数
				int count = 1;

				//　終端またはブロックが閉じ終わるまでループ
				while (!m_token.IsEnd() && count > 0)
				{
					//　トークンを取得
					m_token.Next();

					//　ブロック開始
					if (m_token.Check("{"))
					{
						count++;
					}
					//　ブロック終了
					else if (m_token.Check("}"))
					{
						count--;
					}
				}
				//　終端の場合もあるのでチェック
				if (count > 0)
				{
					std::cerr << "Error : カッコが一致していません\n";
				}
			}

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::SetSkinWeights(void)
			{
				//ウェイト設定済み数のバッファ
				vector_t<int, Allocator> buffer;
				buffer.resize(m_vertices.size());

				int i = 0;

				m_pNowMesh->m_skinWeights.resize(m_vertices.size());
				m_pNowMesh->m_skinWeightIndeces.resize(m_vertices.size());

				for (auto& skinWeightIndex : m_pNowMesh->m_skinWeightIndeces)
				{
					skinWeightIndex.fill(-1);
				}

				//頂点に対応するスキンウェイトを設定
				for (const auto& skinWeightSet : m_skinWeightSet)
				{
					auto boneName = skinWeightSet.m_name;
					auto it = Range::FindIf(
						m_pModel->m_bones,
						[&](CBone<Allocator>& bone)
					{
						return bone.GetName() == boneName;
					});
					if (it == m_pModel->m_bones.end())
						continue;

					int boneIndex = it - m_pModel->m_bones.begin();

					for (const auto& tuple : skinWeightSet.m_array)
					{
						uint_t vertexIndex = tuple.m_index;
						uint_t buf = buffer.at(vertexIndex);

						if (vertexIndex >= m_pNowMesh->m_skinWeightIndeces.size())		continue;
						if (buf >= m_pNowMesh->m_skinWeightIndeces[vertexIndex].size())
						{
							auto it = Range::FindIf(
								m_pNowMesh->m_skinWeights[vertexIndex],
								[&tuple](float weight)
							{
								return weight < tuple.m_weight;
							});

							if (it != m_pNowMesh->m_skinWeights[vertexIndex].end())
							{
								buf = it - m_pNowMesh->m_skinWeights[vertexIndex].begin();
							}
							else continue;
						}

						m_pNowMesh->m_skinWeights.at(vertexIndex).at(buf) = tuple.m_weight;
						m_pNowMesh->m_skinWeightIndeces.at(vertexIndex).at(buf) = boneIndex;
						++buffer.at(vertexIndex);
					}
				}

				//!	スキンウェイトの合計が1になるように調整
				for (auto& skinWeight : m_pNowMesh->m_skinWeights)
				{
					auto sum = Range::Accumurate(
						skinWeight,
						0.0f);

					auto ratio = 1 / sum;

					for (auto& weight : skinWeight)
					{
						weight *= ratio;
					}
				}
			};

			template<template<class> class Allocator >
			void CMeshXLoader<Allocator>::Dispose(void)
			{
				m_memory.clear();
				m_connects.clear();
				m_skinWeightSet.clear();

				m_pModel = nullptr;
				m_pNowMesh = nullptr;
				m_pNowFrame = nullptr;
			};

			template<template<class> class Allocator>
			void CMeshXLoader<Allocator>::LoadMeshTo(
				CModel<Allocator>&	model,
				const string_t& filePath)
			{
				Dispose();

				//ファイルの一括読み込み
				Utility::LoadFileTo<char, Allocator>(filePath, m_memory, true);

				//初期設定
				m_token.SetBuffer(m_memory.data(), m_memory.size());
				m_token.SetSeparator(" \t\r\n,;\"");
				m_token.SetCutOff("{}");
				m_token.SetSkipRenge("//", "\n");

				m_frameNum = 0;
				m_pModel = &model;
				m_pNowFrame = nullptr;
				m_pNowMesh = nullptr;
				m_pNowAnimationSet = nullptr;

				m_frameRoot.reset(Memory::NewWithAllocator<CFrame<Allocator>, Allocator>());
				m_pNowFrame = m_frameRoot.get();


				//　ファイル終端までループ
				while (!m_token.IsEnd())
				{
					m_token.Next();
					CheckNode();
				}

				for (auto& animationSet : model.AnimationSets())
					animationSet.Optimize();

				//ボーン情報の作成
				CreateBones();

				//ボーン情報へのポインタ設定
				for (auto& mesh : m_pModel->m_meshes)
				{
					if (m_skinWeightSet.size() != 0)
					{
						m_pNowMesh = &mesh;
						SetSkinWeights();
					}
					mesh.m_pBones = &m_pModel->m_bones;
				}
			};
		};
	};
};

#endif

