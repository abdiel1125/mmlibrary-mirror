/*!	\file		CMaterial.hpp
*	\details	CMaterialクラスの定義
*	\author		松裏征志
*	\date		2014/06/23
*/
#ifndef MMLIBRARY_CMATERIAL_HPP_
#define MMLIBRARY_CMATERIAL_HPP_


//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\Model\CMaterial.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator>
			CMaterial<Allocator>::CMaterial(void)
				:m_power(0)
			{
				m_diffuse.fill(0);
				m_specular.fill(0);
				m_ambient.fill(0);
				m_emissive.fill(0);
			};

			template<template<class> class Allocator>
			void CMaterial<Allocator>::SetToDevice(void) const
			{
#ifdef	USING_DIRECTX
				CGraphicManager::GetManager().
					GetDevice()->SetMaterial((D3DMATERIAL9*)this);
#endif
#ifdef	USING_OPENGL
				glMaterialfv(GL_FRONT, GL_DIFFUSE, m_diffuse.data());
				glMaterialfv(GL_FRONT, GL_AMBIENT, m_ambient.data());
				glMaterialfv(GL_FRONT, GL_SPECULAR, m_specular.data());
				glMaterialfv(GL_FRONT, GL_EMISSION, m_emissive.data());
				glMaterialf(GL_FRONT, GL_SHININESS, Utility::ClampIn<float>(0, 128, m_power * 128));

				CGLFunctions::GetInstance().CheckErrer();
#endif
				if (m_pTexture)
					m_pTexture->SetToDevice(0);
			};

		};
	};
};

#endif