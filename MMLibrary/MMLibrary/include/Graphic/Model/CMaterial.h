/*!	\file		CMaterial.h
*	\details	CMaterialクラスの宣言
*	\author		松裏征志
*	\date		2014/06/23
*/
#ifndef MMLIBRARY_CMATERIAL_H_
#define MMLIBRARY_CMATERIAL_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility\Macro.h"
#include "Graphic/CGraphicManager.h"
#include "Graphic/Color/CColor32.h"
#include "Graphic/Texture/CTextureBank.h"
#include "Graphic/Texture/CTexture.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	マテリアル情報を表すクラス
			template<template<class> class Allocator = USE_ALLOCATOR >
			class CMaterial
			{
				template<template<class> class Allocator >
				friend class CMeshXLoader;

				friend void Utility::CopyAndGoFromCharArray(char*&, CMaterial<Allocator>&);
			private:
				array_t<float, 4>		m_diffuse;
				array_t<float, 4>		m_ambient;
				array_t<float, 4>		m_specular;
				array_t<float, 4>		m_emissive;
				float					m_power;

				string_t<Allocator>		m_name;
				string_t<Allocator>		m_textureFilePath;
				std::shared_ptr<CTexture<Allocator> >			m_pTexture;
			public:
				//!	コンストラクタ
				CMaterial(void);

#pragma region Getter
				//!	名前を取得する
				CREATE_GETTER(Name, m_name);

				//!	ディフューズ色を取得する
				CREATE_GETTER(Diffuse, m_diffuse);

				//!	アンビエント色を取得する
				CREATE_GETTER(Ambient, m_ambient);

				//!	スペキュラ色を取得する
				CREATE_GETTER(Specular, m_specular);

				//!	エミッシブ色を取得する
				CREATE_GETTER(Emissive, m_emissive);

				//!	反射の強さを取得する
				CREATE_GETTER(Power, m_power);

				//!	テクスチャのファイルパスを取得する
				CREATE_GETTER(TextureFilePath, m_textureFilePath);

				//!	テクスチャのポインタを取得する
				CREATE_GETTER(TexturePointer, m_pTexture);

#pragma endregion

#pragma region Setter

				//!	名前を設定する
				CREATE_SETTER(Name, m_name);

				//!	ディフューズ色を設定する
				CREATE_SETTER(Diffuse, m_diffuse);

				//!	アンビエント色を設定する
				CREATE_SETTER(Ambient, m_ambient);

				//!	スペキュラ色を設定する
				CREATE_SETTER(Specular, m_specular);

				//!	エミッシブ色を設定する
				CREATE_SETTER(Emissive, m_emissive);

				//!	反射の強さを設定する
				CREATE_SETTER(Power, m_power);

				//!	テクスチャのファイルパスを設定する
				CREATE_SETTER(TextureFilePath, m_textureFilePath);

				//!	テクスチャのハンドルを設定する
				CREATE_SETTER(TexturePointer, m_pTexture);
#pragma endregion

				//!マテリアル情報をデバイスに設定する
				void SetToDevice(void) const;

			};
		};
	};
	namespace Utility
	{
		template<template<class> class Allocator = USE_ALLOCATOR>
		void CopyAndGoFromCharArray(char*& pMemory, Graphic::Model::CMaterial<Allocator>& material)
		{
			Utility::CopyAndGoFromCharArray(pMemory, material.m_name);
			Utility::CopyAndGoFromCharArray(pMemory, material.m_diffuse);
			Utility::CopyAndGoFromCharArray(pMemory, material.m_ambient);
			Utility::CopyAndGoFromCharArray(pMemory, material.m_specular);
			Utility::CopyAndGoFromCharArray(pMemory, material.m_emissive);
			Utility::CopyAndGoFromCharArray(pMemory, material.m_power);
			Utility::CopyAndGoFromCharArray(pMemory, material.m_textureFilePath);

			material.SetTexturePointer(
				Graphic::CTextureBank<Allocator>::GetInstance().
				Request(material.GetTextureFilePath()));
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CMaterial.hpp"

#endif