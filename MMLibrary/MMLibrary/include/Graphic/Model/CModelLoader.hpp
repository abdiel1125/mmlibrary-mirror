/*!	\file		CModelLoader.hpp
*	\details	CModelLoaderクラスの定義
*	\author		松裏征志
*	\date		2014/09/08
*/
#ifndef MMLIBRARY_CModel_LOADER_HPP_
#define MMLIBRARY_CModel_LOADER_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CModelLoader.h"


namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator >
			void CModelLoader<Allocator>::LoadMeshTo(
				CModel<Allocator>& model,
				const string_t<Allocator>& filePath)
			{
				Dispose();

				//ファイルの一括読み込み
				Utility::LoadFileTo<char, Allocator>(filePath, m_memory, false);

				m_pMemory = m_memory.data();
				m_pModel = &model;

				//メッシュ情報
				vector_t<CMesh<Allocator>, Allocator>::size_type meshesSize;
				Utility::CopyAndGoFromCharArray(m_pMemory, meshesSize);
				m_pModel->Meshes().resize(meshesSize);
				for (auto& mesh : m_pModel->Meshes())
				{
					LoadXBinMesh(mesh);
				}

				//ボーン情報
				vector_t<CBone<Allocator>, Allocator>::size_type bonesSize;
				Utility::CopyAndGoFromCharArray(m_pMemory, bonesSize);
				m_pModel->Bones().resize(bonesSize);
				for (auto& bone : m_pModel->Bones())
				{
					Utility::CopyAndGoFromCharArray(m_pMemory, bone);
				}
				//アニメーション情報
				std::remove_reference<decltype(m_pModel->AnimationSets())>::type::size_type animationSetsSize;
				Utility::CopyAndGoFromCharArray(m_pMemory, animationSetsSize);
				m_pModel->AnimationSets().resize(animationSetsSize);
				for (auto& animationSet : m_pModel->AnimationSets())
				{
					Utility::CopyAndGoFromCharArray(m_pMemory, animationSet.Name());
					Utility::CopyAndGoFromCharArray(m_pMemory, animationSet.MaxTime());

					std::remove_reference<decltype(animationSet.Animations())>::type::size_type animationsSize;
					Utility::CopyAndGoFromCharArray(m_pMemory, animationsSize);
					animationSet.Animations().resize(animationsSize);
					for (auto& animation : animationSet.Animations())
					{
						Utility::CopyAndGoFromCharArray(m_pMemory, animation.Name());
						Utility::CopyAndGoFromCharArray(m_pMemory, animation.BoneName());
						Utility::CopyAndGoFromCharArray(m_pMemory, animation.KeyFramesOfRotation());
						Utility::CopyAndGoFromCharArray(m_pMemory, animation.KeyFramesOfScale());
						Utility::CopyAndGoFromCharArray(m_pMemory, animation.KeyFramesOfTranslate());
						Utility::CopyAndGoFromCharArray(m_pMemory, animation.KeyFramesOfMatrix());
					}
				}
			};

			template<template<class> class Allocator >
			void CModelLoader<Allocator>::LoadXBinMesh(CMesh<Allocator>& mesh)
			{
				//頂点データ取得
				Utility::CopyAndGoFromCharArray(m_pMemory, mesh.Vertices());
				mesh.SetVertexData(mesh.Vertices());

				//マテリアル情報取得
				LoadXbinMaterials(mesh.Materials());

				//スキンウェイト情報取得
				Utility::CopyAndGoFromCharArray(m_pMemory, mesh.SkinWeights());
				Utility::CopyAndGoFromCharArray(m_pMemory, mesh.SkinWeightIndeces());

				//サブセット情報
				uint_t subSetsNum;
				Utility::CopyAndGoFromCharArray(m_pMemory, subSetsNum);
				mesh.SubSets().resize(subSetsNum);
				for (auto& subSet : mesh.SubSets())
				{
					Utility::CopyAndGoFromCharArray(m_pMemory, subSet.m_materialIndex);
					Utility::CopyAndGoFromCharArray(m_pMemory, subSet.m_boneIndices);
					Utility::CopyAndGoFromCharArray(m_pMemory, subSet.m_vertexIndices);

					subSet.m_indexBuffer.SetData(subSet.m_vertexIndices.data(), subSet.m_vertexIndices.size());
				}

				Utility::CopyAndGoFromCharArray(m_pMemory, mesh.BoneIndex());
				mesh.SetBonesPointer(&m_pModel->Bones());
			};

			template<template<class> class Allocator >
			void CModelLoader<Allocator>::LoadXbinMaterials(
				vector_t<CMaterial<Allocator>, Allocator>& materials)
			{
				uint_t materialsNum;
				Utility::CopyAndGoFromCharArray(m_pMemory, materialsNum);

				if (!materialsNum)	return;

				materials.resize(materialsNum);

				for (CMaterial<Allocator>& material : materials)
				{
					Utility::CopyAndGoFromCharArray(m_pMemory, material);

					material.SetTexturePointer(
						CTextureBank<Allocator>::GetInstance().
						Request(material.GetTextureFilePath()));
				}
			};

			template<template<class> class Allocator >
			void CModelLoader<Allocator>::Dispose(void)
			{
				m_memory.clear();
				m_memory.shrink_to_fit();

				m_pMemory = nullptr;
				m_pModel = nullptr;
			};
		};
	};
};

#endif

