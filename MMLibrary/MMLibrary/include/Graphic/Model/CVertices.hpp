/*!	\file		CVertices.hpp
*	\details	CVerticesクラスの定義
*	\author		松裏征志
*	\date		2014/05/07
*/
#ifndef MMLIBRARY_CPRIMITIVE_HPP_
#define MMLIBRARY_CPRIMITIVE_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\Model\CVertices.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<class Vertex, template<class> class Allocator>
			CVertices<Vertex, Allocator>::CVertices(void)
				: m_type(POINTS)
				, m_isDynamic(false)
			{};

			template<class Vertex, template<class> class Allocator>
			CVertices<Vertex, Allocator>::CVertices(
				const vector_t<Vertex, Allocator>& src)
			{
				m_vertices = src;
			};

			template<class Vertex, template<class> class Allocator>
			CVertices<Vertex, Allocator>::~CVertices(void)
			{};

			template<class Vertex, template<class> class Allocator>
			void	CVertices<Vertex, Allocator>::Render()
			{
				if (m_pRenderBeginFunction)
					m_pRenderBeginFunction();

				if (IsDynamic())
				{
					Vertex::RenderBegin(Vertices()[0]);

#ifdef	USING_OPENGL
					glDrawArrays(
						m_type,
						0,
						m_vertices.size());
#endif
#ifdef	USING_DIRECTX
					CheckDirectXErrer(
						CGraphicManager::GetManager().
						GetDevice()->DrawPrimitiveUP(
						(D3DPRIMITIVETYPE)m_type,
						GetPrimitiveNum(),
						Vertices().data(),
						sizeof(Vertex)));
#endif
				}
				else
				{
					m_vertexBuffer.Bind();
					Vertex::RenderBegin();

#ifdef	USING_OPENGL
					glDrawArrays(
						m_type,
						0,
						m_vertices.size());
					CGLFunctions::GetInstance().CheckErrer();
#endif
#ifdef	USING_DIRECTX
					CheckDirectXErrer(
						CGraphicManager::GetManager().
						GetDevice()->DrawPrimitive(
						(D3DPRIMITIVETYPE)m_type,
						0,
						GetPrimitiveNum()));
#endif

					m_vertexBuffer.UnBind();
				}
				Vertex::RenderEnd();
			};

			template<class Vertex, template<class> class Allocator>
			void	CVertices<Vertex, Allocator>::SetPrimitiveType(
				const PRIMITIVE_TIPE type)
			{
				m_type = type;
			};

			template<class Vertex, template<class> class Allocator>
			int	CVertices<Vertex, Allocator>::GetPrimitiveNum(void) const
			{
				return GetPrimitiveNum(m_vertexBuffer.GetSize());
			}

			template<class Vertex, template<class> class Allocator>
			size_t	CVertices<Vertex, Allocator>::GetPrimitiveNum(size_t vertexNum) const
			{
				switch (m_type)
				{
				case POINTS:
					return vertexNum;
				case LINES:
					return vertexNum / 2;
				case LINE_STRIP:
					return vertexNum - 1;
				case TRIANGLES:
					return vertexNum / 3;
				case TRIANGLE_STRIP:
					return vertexNum - 2;
				case TRIANGLE_FAN:
					return vertexNum - 2;
				}

				return 0;
			};

			template<class Vertex, template<class> class Allocator>
			void CVertices<Vertex, Allocator>::SizeNormalize(void)
			{
				auto aabb = Collision::CAxisAlignedBoundingBox::CreateByCVertices(*this);

				float max = max(
					max(aabb.Max().at(0), -aabb.Min().at(0)),
					max(max(aabb.Max().at(1), -aabb.Min().at(1)),
					max(aabb.Max().at(2), -aabb.Min().at(2))));

				for (auto it = List().begin();
					it != List().end();
					++it)
				{
					it->Position() /= max;
				}
			};

			template<class Vertex, template<class> class Allocator>
			CVector3 CVertices<Vertex, Allocator>::GetCenter(void) const
			{
				CVector3 center = CVector3(0, 0, 0);

				for (const auto& vertex : Vertices())
				{
					center += vertex.Position();
				}
				center /= static_cast<float>(Vertices().size());

				return center;
			}

			template<class Vertex, template<class> class Allocator>
			CVertices<Vertex, Allocator>
				CVertices<Vertex, Allocator>::CreateByQuads(void)
			{
				return CVertices<Vertex, Allocator>().SetByQuads();
			};

			template<class Vertex, template<class> class Allocator>
			CVertices<Vertex, Allocator>&
				CVertices<Vertex, Allocator>::SetByQuads(void)
			{
				const float halfWidth = 0.5f;
				const float halfHeight = 0.5f;

				Vertices().resize(4);
				Vertices()[0].Position() = CVector3({ -halfWidth, -halfHeight, 0 });
				Vertices()[0].UV()[0]	 =	0;
				Vertices()[0].UV()[1]	 =	1;

				Vertices()[1].Position() = CVector3({ -halfWidth, halfHeight, 0 });
				Vertices()[1].UV()[0]	 =	0;
				Vertices()[1].UV()[1]	 =	0;

				Vertices()[2].Position() = CVector3({ halfWidth, -halfHeight, 0 });
				Vertices()[2].UV()[0]	 =	1;
				Vertices()[2].UV()[1]	 =	1;

				Vertices()[3].Position() = CVector3({ halfWidth, halfHeight, 0 });
				Vertices()[3].UV()[0]	 =	1;
				Vertices()[3].UV()[1]	 =	0;

				SetVertexData(Vertices());

				SetPrimitiveType(
					TRIANGLE_STRIP);

				return *this;
			};

			template<class Vertex, template<class> class Allocator>
			bool CVertices<Vertex, Allocator>::IsDynamic(void) const
			{
				return m_isDynamic;
			};

			template<class Vertex, template<class> class Allocator>
			void CVertices<Vertex, Allocator>::IsDynamic(bool isDynamic)
			{
				m_isDynamic = isDynamic;
			};
		}
	}
}

#endif