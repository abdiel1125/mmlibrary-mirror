/*	\file		CFrame.h
*	\details	CFrameクラスを宣言する
*	\author		松裏征志
*	\date		2014/07/16
*/
#ifndef MMLIBRARY_CFRAME_H_
#define MMLIBRARY_CFRAME_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Graphic\CMatrix4x4.h"
#include "Graphic\Model\CMesh.h"

namespace MMLibrary
{
	namespace Graphic
	{
		class CMatrix4x4;

		namespace Model
		{
			//!	.xファイルのFrameを表すクラス
			template<template<class> class Allocator = USE_ALLOCATOR >
			class CFrame
			{
				template<template<class> class Allocator >
				friend class CMeshXLoader;
			private:
				typedef	string_t<Allocator>				string_t;
				string_t								m_name;
				CMatrix4x4								m_transformMatrix;
				std::unique_ptr<CMesh<Allocator> >		m_pMesh;
				std::shared_ptr<CFrame<Allocator> >		m_pBrother;
				std::shared_ptr<CFrame<Allocator> >		m_pChild;
				CFrame*									m_pParent;
				
				std::shared_ptr<CFrame<Allocator> >&	AddChild(void);
				std::shared_ptr<CFrame<Allocator> >&	AddBrother(CFrame*	pParent = nullptr);

			public:
				//!	コンストラクタ
				CFrame(void)
					:m_pParent(nullptr)
					,m_pBrother()
					,m_pChild()
					,m_pMesh()
				{
					m_name = "nameless";
					m_transformMatrix.SetByIdentity();
				};

				//!	描画
				void	Render(void);

				//!	座標変換行列を取得する
				const CMatrix4x4&
					GetMatrix(void) const
				{
					return m_transformMatrix;
				};

				//!	親フレームへのポインタを取得する　
				CFrame*	GetParent(void) const
				{
					return m_pParent;
				};

				//!	子フレームへのポインタを取得する　
				CFrame*	GetChild(void) const
				{
					return m_pChild ? m_pChild.get() : nullptr;
				};

				//!	兄弟フレームへのポインタを取得する　
				CFrame*	GetBrother(void) const
				{
					return m_pBrother ? m_pBrother.get() : nullptr;
				};

				//!	名前を取得する
				const string_t& GetName(void) const
				{
					return m_name;
				};

				//!	名前を設定する
				void	SetName(const string_t& name)
				{
					m_name = name;
				};

				//!	指定した名前のフレームのポインタを子・兄弟フレームから探す
				CFrame* FindFrame(string_t& string);
			};
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CFrame.hpp"


#endif