/*!	\file		Model.h
*	\details	Model関連の宣言・インクルード
*	\author		松裏征志
*	\date		2014/05/07
*/
#ifndef MMLIBRARY_MODEL_H_
#define MMLIBRARY_MODEL_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Graphic.h"

//***********************************************************
//共通の構造体等の定義
//***********************************************************
namespace MMLibrary
{
	namespace Graphic
	{
		/*!	\namespace	MMLibrary::Graphic::Model
		*	\brief	MMLibraryのメッシュやプリミティブの描画に関わる要素はこの名前空間に属する
		*/
		namespace Model
		{
		};
	};
};

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Vertex/Vertex.h"
#include "Graphic/Model/CVertices.h"
#include "Graphic/Model/CVerticesHasTexture.h"
#include "Graphic/Model/CBillBoard.h"
#include "Graphic/Model/CModel.h"
#include "Graphic/Model/ModelIO.h"
#include "Graphic/Model/CModelBank.h"

#endif