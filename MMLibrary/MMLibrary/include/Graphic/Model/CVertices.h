/*!	\file		CVertices.h
*	\details	CVerticesクラスの宣言
*	\author		松裏征志
*	\date		2014/05/07
*/
#ifndef MMLIBRARY_CPRIMITIVE_H_
#define MMLIBRARY_CPRIMITIVE_H_

//***********************************************************
//インクルード
//***********************************************************
#include <vector>
#include "MMLibraryDefine.h"
#include "Graphic\CBuffer.h"
#include "Graphic/Model/IModel.h"

#include "Utility/DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	頂点の集合クラス
			template<class Vertex, template<class> class Allocator = USE_ALLOCATOR>
			class CVertices : public IModel
			{
			protected:
				CVertexBuffer<Vertex>		m_vertexBuffer;
				vector_t<Vertex, Allocator>	m_vertices;
				PRIMITIVE_TIPE				m_type;
				bool						m_isDynamic;

			public:
				using vertexPointer_t = typename CVertexBuffer<Vertex>::CVertexBufferPointer;

				//!	コンストラクタ
				CVertices(void);

				//!	コピーコンストラクタ
				CVertices(const CVertices& src)
					: CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_vertexBuffer)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_vertices)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_type)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_isDynamic)
				{};

				//!	ムーブコンストラクタ
				CVertices(CVertices&& src)
					: CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_vertexBuffer)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_vertices)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_type)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_isDynamic)
				{};

				//!	引数付きコンストラクタ
				CVertices(const vector_t<Vertex, Allocator>& src);

				//!	デストラクタ
				~CVertices(void);

				//!	描画
				void	Render(void);

				//!	プリミティブタイプの設定
				void	SetPrimitiveType(const PRIMITIVE_TIPE type);

				//!	頂点リストの参照
				CREATE_REFERRENCE(Vertices, m_vertices);
				CREATE_CONST_REFERRENCE(Vertices, m_vertices);

				//!	プリミティブ数の取得
				int	GetPrimitiveNum(void) const;

				//!	頂点数に対応するプリミティブの数を取得する
				size_t GetPrimitiveNum(size_t vertexNum) const;

				//!	サイズを-1から1の間に収める
				void SizeNormalize(void);

				//!	重心を取得する
				CVector3 GetCenter(void) const;

				//!	頂点配列からバッファにデータを登録する
				void SetVertexData(vector_t<Vertex, Allocator>& src)
				{
					if (src.empty() == false)
						m_vertexBuffer.SetData(src.data(), src.size());
				};

				//!	正方形メッシュの作成
				static	CVertices<Vertex, Allocator>
					CreateByQuads(void);

				//!	正方形メッシュ化
				CVertices<Vertex, Allocator>&
					SetByQuads(void);

				//!	動的に頂点データを変更するかを取得する
				bool IsDynamic(void) const;

				//!	動的に頂点データを変更するかを設定する
				void IsDynamic(bool isDynamic);
			};
		}
	}
}

#include "Utility/UndefineClassCreateHelpers.h"

//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CVertices.hpp"

#endif