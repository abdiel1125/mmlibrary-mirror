/*!	\file		CBone.h
*	\details	CBoneクラスの宣言
*	\author		松裏征志
*	\date		2014/09/09
*/
#ifndef MMLIBRARY_CBONE_H_
#define MMLIBRARY_CBONE_H_

//***********************************************************
//インクルード
//***********************************************************
#include <string>
#include "Graphic\CMatrix4x4.h"

#include "Utility/DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	ボーン情報を表すクラス
			template<template<class> class Allocator = USE_ALLOCATOR>
			class CBone
			{
				template<template<class> class Allocator >
				friend class CMeshXLoader;
			protected:
				typedef	std::basic_string<char, std::char_traits<char>, Allocator<char> > string_t;
				string_t					m_name;
				int							m_parentIndex;
				Graphic::CMatrix4x4			m_offsetMatrix;
				Graphic::CMatrix4x4			m_inverseOffsetMatrix;
				Graphic::CMatrix4x4			m_drawMatrix;
				bool						m_isUpdated;

			public:
				//!	コンストラクタ
				CBone() 
					: m_parentIndex(-1)
					, m_isUpdated(false){};

				//!	描画に使う座標変換行列への参照
				CREATE_REFERRENCE(DrawMatrix, m_drawMatrix);
				CREATE_CONST_REFERRENCE(DrawMatrix, m_drawMatrix);

#pragma region Getter
				//!	名前を取得する
				CREATE_GETTER(Name, m_name);

				//!	更新済みであるかを取得する
				CREATE_GETTER_PRIMITIVE(IsUpdated, m_isUpdated);

				//!	親ボーンの添字を取得する
				CREATE_GETTER_PRIMITIVE(ParentIndex, m_parentIndex);

				//!	親ボーンからの相対的な座標変換行列を取得する(スキンウェイトの行列)
				CREATE_GETTER(OffsetMatrix, m_offsetMatrix);

				//!	親ボーンへの相対的な座標変換行列を取得する(Frameの行列)
				CREATE_GETTER(InverseOffsetMatrix, m_inverseOffsetMatrix);

				//!	World座標系からの座標変換行列を取得する
				Graphic::CMatrix4x4	GetWorldMatrix(const vector_t<CBone<Allocator>, Allocator>& bones) const
				{
					Graphic::CMatrix4x4 rtn = m_drawMatrix;
					int parentIndex = m_parentIndex;

					while (parentIndex != -1)
					{
						rtn.Multiply(bones[parentIndex].m_drawMatrix);
						parentIndex = bones[parentIndex].GetParentIndex();
					}

					return rtn;
				};

#pragma endregion

#pragma region Setter
				//!	名前を設定する
				CREATE_SETTER(Name, m_name);

				//!	更新済みであるかを設定する
				CREATE_SETTER_PRIMITIVE(IsUpdated, m_isUpdated);

				//!	親ボーンの添字を設定する
				CREATE_SETTER_PRIMITIVE(ParentIndex, m_parentIndex);

				//!	親ボーンからの相対的な座標変換行列を設定する(スキンウェイトの行列)
				CREATE_SETTER(OffsetMatrix, m_offsetMatrix);

				//!	親ボーンへの相対的な座標変換行列を設定する(Frameの行列)
				CREATE_SETTER(InverseOffsetMatrix, m_inverseOffsetMatrix);

#pragma endregion
				//!	代入演算子
				CBone& operator = (const CBone& bone)
				{
					m_name = bone.m_name;
					m_parentIndex = bone.m_parentIndex;
					m_offsetMatrix = bone.m_offsetMatrix;
					m_inverseOffsetMatrix = bone.m_inverseOffsetMatrix;
					return *this;
				};

				friend void Utility::CopyAndGoFromCharArray(char*& pMemory, CBone<Allocator>& bone);
			};
		};
	};
	namespace Utility
	{
		template<template<class> class Allocator = USE_ALLOCATOR>
		void CopyAndGoFromCharArray(char*& pMemory, Graphic::Model::CBone<Allocator>& bone)
		{
			CopyAndGoFromCharArray(pMemory, bone.m_name);
			CopyAndGoFromCharArray(pMemory, bone.m_parentIndex);
			CopyAndGoFromCharArray(pMemory, bone.m_offsetMatrix);
			CopyAndGoFromCharArray(pMemory, bone.m_inverseOffsetMatrix);
		};
	};
};

#include "Utility/UndefineClassCreateHelpers.h"

#endif