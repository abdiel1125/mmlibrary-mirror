/*!	\file		CVerticesHasTexture.hpp
*	\details	CVerticesHasTextureクラスの定義
*	\author		松裏征志
*	\date		2014/06/12
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CVerticesHasTexture.h"


namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<class VERTEX, template<class> class Allocator>
			CVerticesHasTexture<VERTEX, Allocator>::
				CVerticesHasTexture(void)
				:CVertices()
			{};

			template<class VERTEX, template<class> class Allocator>
			CVerticesHasTexture<VERTEX, Allocator>::
				CVerticesHasTexture(vector_t<VERTEX, Allocator>& src)
				:CVertices(src)
			{};

			template<class VERTEX, template<class> class Allocator>
			void	CVerticesHasTexture<VERTEX, Allocator>::
				Render(void)
			{
				if (m_pTexture)
				{
					m_pTexture->SetToDevice(0);
					CVertices::Render();
					m_pTexture->UnSetFromDevice(0);
				}
				else
				{
					CVertices::Render();
				}

			};
		}
	}
}