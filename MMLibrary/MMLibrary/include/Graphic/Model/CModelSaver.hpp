/*!	\file		CModelSaver.hpp
*	\details	CModelSaverクラスの定義
*	\author		松裏征志
*	\date		2014/09/30
*/
#ifndef MMLIBRARY_CMODEL_SAVER_HPP_
#define MMLIBRARY_CMODEL_SAVER_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic/Model/CModelSaver.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{

			template<template<class> class Allocator>
			void		CModelSaver<Allocator>::SaveMesh(const Model::CMesh<Allocator>& mesh)
			{
				//頂点情報.
				Utility::WriteToFile(m_file, mesh.Vertices());

				//材質情報
				Utility::WriteToFile(m_file, mesh.GetMaterials().size());
				for (auto& material : mesh.GetMaterials())
				{
					Utility::WriteToFile(m_file, material.GetName());
					Utility::WriteToFile(m_file, material.GetDiffuse());
					Utility::WriteToFile(m_file, material.GetAmbient());
					Utility::WriteToFile(m_file, material.GetSpecular());
					Utility::WriteToFile(m_file, material.GetEmissive());
					Utility::WriteToFile(m_file, material.GetPower());
					Utility::WriteToFile(m_file, material.GetTextureFilePath());
				}

				//スキンウェイト情報
				Utility::WriteToFile(m_file, mesh.GetSkinWeights());
				Utility::WriteToFile(m_file, mesh.GetSkinWeightIndeces());

				//サブセット情報
				Utility::WriteToFile(m_file, mesh.GetSubSets().size());
				for (auto& subSet : mesh.SubSets())
				{
					Utility::WriteToFile(m_file, subSet.m_materialIndex);
					Utility::WriteToFile(m_file, subSet.m_boneIndices);
					Utility::WriteToFile(m_file, subSet.m_vertexIndices);
				}

				Utility::WriteToFile(m_file, mesh.GetBoneIndex());
			};

			template<template<class> class Allocator>
			CModelSaver<Allocator>& CModelSaver<Allocator>::SaveModelTo(const string_t<Allocator>& filePath)
			{
				m_file.open(filePath, std::ios::binary);

				assert(m_file && "File can not open.");

				//メッシュ情報
				Utility::WriteToFile(m_file, m_pModel->GetMeshes().size());
				for (auto& mesh : m_pModel->Meshes())
				{
					SaveMesh(mesh);
				}

				//ボーン情報
				Utility::WriteToFile(m_file, m_pModel->GetBones().size());
				for (auto& bone : m_pModel->GetBones())
				{
					Utility::WriteToFile(m_file, bone.GetName());
					Utility::WriteToFile(m_file, bone.GetParentIndex());
					Utility::WriteToFile(m_file, bone.GetOffsetMatrix());
					Utility::WriteToFile(m_file, bone.GetInverseOffsetMatrix());
				}
				//アニメーション情報
				Utility::WriteToFile(m_file, m_pModel->GetAnimationSets().size());
				for (auto& animationSet : m_pModel->GetAnimationSets())
				{
					Utility::WriteToFile(m_file, animationSet.GetName());
					Utility::WriteToFile(m_file, animationSet.GetMaxTime());

					Utility::WriteToFile(m_file, animationSet.GetAnimations().size());
					for (auto& animation : animationSet.GetAnimations())
					{
						Utility::WriteToFile(m_file, animation.GetName());
						Utility::WriteToFile(m_file, animation.GetBoneName());
						Utility::WriteToFile(m_file, animation.GetKeyFramesOfRotation());
						Utility::WriteToFile(m_file, animation.GetKeyFramesOfScale());
						Utility::WriteToFile(m_file, animation.GetKeyFramesOfTranslate());
						Utility::WriteToFile(m_file, animation.GetKeyFramesOfMatrix());
					}
				}
				m_file.close();

				return *this;
			};
		};
	};
};

#endif