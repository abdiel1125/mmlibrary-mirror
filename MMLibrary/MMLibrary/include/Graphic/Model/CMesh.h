/*!	\file		CMesh.h
*	\details	CMeshクラスの宣言
*	\author		松裏征志
*	\date		2014/09/09
*/
#ifndef MMLIBRARY_CMESH_H_
#define MMLIBRARY_CMESH_H_

//***********************************************************
//インクルード
//***********************************************************
#include <vector>
#include <assert.h>
#include "Graphic/Model/CVertices.h"
#include "Graphic/Vertex/CVertexFor3D.h"
#include "Graphic/Model/CMaterial.h"
#include "Graphic/Model/CFace.h"
#include "Graphic/Model/CBone.h"
#include "Graphic/Model/CAnimation.h"
#include "Graphic/CBuffer.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	同一マテリアルのポリゴンの構成情報のセットを表す
			template<template<class> class Allocator>
			struct tagSubset
			{
				size_t			m_materialIndex;
				array_t<int, 4>	m_boneIndices;
				CIndexBuffer	m_indexBuffer;
				vector_t<CIndexBuffer::index_t, Allocator> m_vertexIndices;
			};

			//!	単一のメッシュを表すクラス
			template<template<class> class Allocator = USE_ALLOCATOR >
			class CMesh : public CVertices < CVertexFor3D >
			{
				template<template<class> class Allocator >
				friend class CMeshXLoader;
				template<template<class> class Allocator >
				friend class CMeshX;

				typedef	string_t<Allocator> string_t;
			protected:
				vector_t<CMaterial<Allocator>, Allocator>	m_materials;
				vector_t<Math::CVector<float, 4>, Allocator>	m_skinWeights;
				vector_t<Math::CVector<int, 4>, Allocator>	m_skinWeightIndeces;
				vector_t<tagSubset<Allocator>, Allocator>	m_subSets;
				vector_t<CVertexFor3D, Allocator>	m_vertexStream;
				vector_t<CBone<Allocator>, Allocator>*	m_pBones;

				string_t										m_name;
				uint_t											m_boneIndex;

				void	RenderSkinMesh(void);
				void	RenderNoSkinMesh(void);
				void	SetSubSetFor1Materials(void);
				void	CalculateVertices();
			public:
				//!	コンストラクタ
				CMesh(void);

				//!	コピーコンストラクタ
				CMesh(const CMesh& src)
					: CVertices(src)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_materials)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_skinWeights)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_skinWeightIndeces)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_subSets)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_vertexStream)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_pBones)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_name)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_boneIndex)
				{};

				//!	ムーブコンストラクタ
				CMesh(CMesh&& src)
					: CVertices(src)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_materials)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_skinWeights)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_skinWeightIndeces)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_subSets)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_vertexStream)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_pBones)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_name)
					, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_boneIndex)
				{};

				CREATE_REFERRENCE(Materials, m_materials);
				CREATE_REFERRENCE(SkinWeights, m_skinWeights);
				CREATE_REFERRENCE(SkinWeightIndeces, m_skinWeightIndeces);
				CREATE_REFERRENCE(SubSets, m_subSets);
				CREATE_REFERRENCE(BoneIndex, m_boneIndex);

				CREATE_CONST_REFERRENCE(Materials, m_materials);
				CREATE_CONST_REFERRENCE(SkinWeights, m_skinWeights);
				CREATE_CONST_REFERRENCE(SkinWeightIndeces, m_skinWeightIndeces);
				CREATE_CONST_REFERRENCE(SubSets, m_subSets);
				CREATE_CONST_REFERRENCE(BoneIndex, m_boneIndex);

				CREATE_GETTER(Name, m_name);
				CREATE_GETTER(Materials, m_materials);
				CREATE_GETTER(SkinWeights, m_skinWeights);
				CREATE_GETTER(SkinWeightIndeces, m_skinWeightIndeces);
				CREATE_GETTER(SubSets, m_subSets);
				CREATE_GETTER_PRIMITIVE(BoneIndex, m_boneIndex);

				CREATE_SETTER(Name, m_name);
				CREATE_SETTER(Materials, m_materials);
				CREATE_SETTER(SkinWeights, m_skinWeights);
				CREATE_SETTER(SkinWeightIndeces, m_skinWeightIndeces);
				CREATE_SETTER(SubSets, m_subSets);
				CREATE_SETTER_PRIMITIVE(BoneIndex, m_boneIndex);

				//!	描画
				void	Render(void);
				//!	初期化
				void	Dispose(void);

#pragma region Creator

				//!	球形メッシュの作成
				static			CMesh<Allocator>
					CreateSphere(
					uint_t slice,
					uint_t stack,
					float radius);

				//!	立方体メッシュの作成
				static			CMesh<Allocator>
					CreateByBox(void);

				//!	正方形メッシュの作成
				static			CMesh<Allocator>
					CreateByQuads(void);

#pragma endregion

				//!	球形メッシュ化
				CMesh<Allocator>&
					SetBySphere(
					uint_t slice,
					uint_t stack,
					float radius);

				//!	立方体メッシュ化
				CMesh<Allocator>&
					SetByBox(void);

				//!	正方形メッシュ化
				CMesh<Allocator>&
					SetByQuads(void);

				//!	マテリアルの設定
				void SetMaterial(
					CMaterial<Allocator>&	material);

				//!	ボーン配列へのポインタを設定する
				void SetBonesPointer(vector_t<CBone<Allocator>, Allocator>* pBones)
				{
					m_pBones = pBones;
				}

				//!	中心から最も遠い頂点と中心の距離が1になるように拡縮する
				void SizeNormalize(void);
			};
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CMesh.hpp"

#endif