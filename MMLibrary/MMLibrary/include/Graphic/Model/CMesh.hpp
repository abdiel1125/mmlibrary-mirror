/*!	\file		CMesh.hpp
*	\details	CMeshクラスの定義
*	\author		松裏征志
*	\date		2014/06/12
*/
#ifndef MMLIBRARY_CMESH_HPP_
#define MMLIBRARY_CMESH_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CMesh.h"
#include "Graphic/CVector3.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
#ifdef	USING_DIRECTX
			template<template<class> class Allocator>
			void	CMesh<Allocator>::RenderSkinMesh(void)
			{
				CalculateVertices();

				auto nowMaterialIndex = m_subSets.at(0).m_materialIndex;
				if (m_materials.size() > nowMaterialIndex)
					m_materials.at(nowMaterialIndex).SetToDevice();

				for (auto& subSet : m_subSets)
				{
					if (nowMaterialIndex != subSet.m_materialIndex)
					{
						nowMaterialIndex = subSet.m_materialIndex;
						if (m_materials.size() > nowMaterialIndex)
							m_materials.at(nowMaterialIndex).SetToDevice();
					}

					CVertexFor3D::RenderBegin(m_vertexStream[0]);

					CheckDirectXErrer(
						CGraphicManager::GetManager().
						GetDevice()->DrawIndexedPrimitiveUP(
						(D3DPRIMITIVETYPE)m_type,
						0,
						m_vertexStream.size(),
						subSet.m_vertexIndices.size() / 3,
						subSet.m_vertexIndices.data(),
						D3DFMT_INDEX32,
						m_vertexStream.data(),
						sizeof(CVertexFor3D)));

					CVertexFor3D::RenderEnd();
				}
			};

			template<template<class> class Allocator>
			void	CMesh<Allocator>::RenderNoSkinMesh(void)
			{
				m_vertexBuffer.Bind();

				CVertexFor3D::RenderBegin();

				auto nowMaterialIndex = m_subSets.at(0).m_materialIndex;
				if (m_materials.size() > nowMaterialIndex)
					m_materials.at(nowMaterialIndex).SetToDevice();

				for (auto& subSet : m_subSets)
				{
					if (nowMaterialIndex != subSet.m_materialIndex)
					{
						nowMaterialIndex = subSet.m_materialIndex;
						if (m_materials.size() > nowMaterialIndex)
							m_materials.at(nowMaterialIndex).SetToDevice();
					}

					subSet.m_indexBuffer.Bind();

					CheckDirectXErrer(
						CGraphicManager::GetManager().
						GetDevice()->DrawIndexedPrimitive(
						(D3DPRIMITIVETYPE)m_type,
						0,
						0,
						subSet.m_indexBuffer.GetSize(),
						0,
						subSet.m_indexBuffer.GetSize() / 3
						));

					subSet.m_indexBuffer.UnBind();
				}

				CVertexFor3D::RenderEnd();

				m_vertexBuffer.UnBind();
			};

#endif
#ifdef	USING_OPENGL
			template<template<class> class Allocator>
			void	CMesh<Allocator>::RenderSkinMesh(void)
			{
				CalculateVertices();

				CVertexFor3D::RenderBegin(m_vertexStream[0]);

				auto nowMaterialIndex = m_subSets.at(0).m_materialIndex;
				if (m_materials.size() > nowMaterialIndex)
					m_materials.at(nowMaterialIndex).SetToDevice();

				for (auto& subSet : m_subSets)
				{
					if (nowMaterialIndex != subSet.m_materialIndex)
					{
						nowMaterialIndex = subSet.m_materialIndex;
						if (m_materials.size() > nowMaterialIndex)
							m_materials.at(nowMaterialIndex).SetToDevice();
					}

					subSet.m_indexBuffer.Bind();

					glDrawElements(
						GL_TRIANGLES,
						subSet.m_indexBuffer.GetSize(),
						GL_UNSIGNED_INT,
						0);

					subSet.m_indexBuffer.UnBind();
				}

				CVertexFor3D::RenderEnd();
			};

			template<template<class> class Allocator>
			void	CMesh<Allocator>::RenderNoSkinMesh(void)
			{
				m_vertexBuffer.Bind();

				CVertexFor3D::RenderBegin();

				auto nowMaterialIndex = m_subSets.at(0).m_materialIndex;
				if (m_materials.size() > nowMaterialIndex)
					m_materials.at(nowMaterialIndex).SetToDevice();

				for (auto& subSet : m_subSets)
				{
					if (nowMaterialIndex != subSet.m_materialIndex)
					{
						nowMaterialIndex = subSet.m_materialIndex;
						if (m_materials.size() > nowMaterialIndex)
							m_materials.at(nowMaterialIndex).SetToDevice();
					}

					subSet.m_indexBuffer.Bind();

					glDrawElements(
						GL_TRIANGLES,
						subSet.m_indexBuffer.GetSize(),
						GL_UNSIGNED_INT,
						0);

					subSet.m_indexBuffer.UnBind();
				}

				CVertexFor3D::RenderEnd();

				m_vertexBuffer.UnBind();
			};
#endif

			template<template<class> class Allocator>
			CMesh<Allocator>::CMesh(void)
				: m_boneIndex(0)
				, m_pBones(nullptr)
			{
				SetPrimitiveType(TRIANGLES);
			};

			template<template<class> class Allocator>
			void	CMesh<Allocator>::Dispose(void)
			{
				CVertexFor3D buf[1];
				m_vertexBuffer.Dispose();

				SubSets().resize(0);
				SubSets().shrink_to_fit();

				Materials().resize(0);
				Materials().shrink_to_fit();
			};

			template<template<class> class Allocator>
			void	CMesh<Allocator>::Render(void)
			{
				if (Vertices().size() == 0)	return;

				if (m_boneIndex != -1)
				{
					CGraphicManager::GetManager().
						Stack.
						Push().
						MultiplyLocal(m_pBones->at(m_boneIndex).DrawMatrix());
					CGraphicManager::GetManager().SetWorldMatrix();
				}

				if (m_skinWeights.empty())
					RenderNoSkinMesh();
				else
					RenderSkinMesh();

				if (m_boneIndex != -1)
				{
					CGraphicManager::GetManager().
						Stack.
						Pop();
				}
			};

			template<template<class> class Allocator>
			void	CMesh<Allocator>::SetSubSetFor1Materials(void)
			{
				m_subSets.resize(1);
				for (auto& boneIndex : m_subSets[0].m_boneIndices)
				{
					boneIndex = -1;
				}
				m_subSets[0].m_materialIndex = 0;
				m_subSets[0].m_vertexIndices.reserve(m_vertexBuffer.GetSize());

				for (const auto& face : m_face)
				{
					for (const auto& index : face.m_index)
					{
						m_subSets[0].m_vertexIndices.push_back(index);
					}
				}

				m_subSets[0].m_indexBuffer.SetData(
					m_subSets[0].m_vertexIndices.data(),
					m_subSets[0].m_vertexIndices.size());
			};

			template<template<class> class Allocator>
			void CMesh<Allocator>::CalculateVertices()
			{
				if (m_vertexStream.size() != Vertices().size())
					m_vertexStream = Vertices();

				for (uint_t index = 0; index < m_vertexStream.size(); ++index)
				{
					if (std::any_of(
						std::cbegin(m_skinWeightIndeces[index]),
						std::cend(m_skinWeightIndeces[index]),
						[this](const int& index)
					{
						return (*m_pBones)[index].GetIsUpdated();
					}))
					//更新されたボーンが1つでもあれば
					{
						CMatrix4x4 matrixComposit;
						matrixComposit.SetBy1Element(0);
						for (int i = 0; i < 4; ++i)
						{
							size_t	skinWeightIndex = m_skinWeightIndeces[index][i];
							float	skinWeight = m_skinWeights[index][i];

							if (skinWeightIndex != -1)
								matrixComposit += (*m_pBones)[skinWeightIndex].DrawMatrix() * skinWeight;
						}
						m_vertexStream[index].Position() = Vertices()[index].Position();
						CVector3Helper::TransformCoord(m_vertexStream[index].Position(), matrixComposit);
						m_vertexStream[index].Normal() = Vertices()[index].Normal();
						CVector3Helper::TransformNormal(m_vertexStream[index].Normal(), matrixComposit);
					}
				}
			};

			template<template<class> class Allocator>
			CMesh<Allocator>
				CMesh<Allocator>::CreateSphere(
				uint_t slice,
				uint_t stack,
				float radius)
			{
				return CMesh<Allocator>().SetBySphere(
					slice,
					stack,
					radius);
			};

			template<template<class> class Allocator>
			CMesh<Allocator>&
				CMesh<Allocator>::SetBySphere(
				uint_t slice,
				uint_t stack,
				float radius)
			{
				vector_t<CVertexFor3D, Allocator> vertices;
				size_t faceNum = slice * (2 + (stack - 2) * 2);

				vertices.resize(slice * (stack - 1) + 2);
				SubSets().resize(1);
				tagSubset<Allocator>& subset = SubSets()[0];
				subset.m_vertexIndices.resize(3 * faceNum);

				for (uint_t i = 0;
					i < slice;
					++i)
				{
					subset.m_vertexIndices[i * 3 + 0] = 0;
					subset.m_vertexIndices[i * 3 + 1] = i + 1;
					subset.m_vertexIndices[i * 3 + 2] = (i + 1) % slice + 1;
				}

				for (uint_t i = 0;
					i < slice;
					++i)
				{
					size_t index = faceNum - (slice - i);

					subset.m_vertexIndices[index * 3 + 0] = vertices.size() - 1;
					subset.m_vertexIndices[index * 3 + 1] = (stack - 2) * slice + ((i + 1) % slice) + 1;
					subset.m_vertexIndices[index * 3 + 2] = (stack - 2) * slice + i + 1;
				}

				for (uint_t j = 1;
					j < stack - 1;
					++j)
				{
					for (uint_t i = 0;
						i < slice;
						++i)
					{
						size_t index = j * slice * 2 + i * 2 - slice;

						subset.m_vertexIndices[index * 3 + 0] = 1 + j		* slice + i;
						subset.m_vertexIndices[index * 3 + 1] = 1 + (j - 1)	* slice + ((i + 1) % slice);
						subset.m_vertexIndices[index * 3 + 2] = 1 + (j - 1)	* slice + i;

						index = j * slice * 2 + i * 2 - slice + 1;

						subset.m_vertexIndices[index * 3 + 0] = 1 + (j - 1)	* slice + ((i + 1) % slice);
						subset.m_vertexIndices[index * 3 + 1] = 1 + j		* slice + i;
						subset.m_vertexIndices[index * 3 + 2] = 1 + j		* slice + ((i + 1) % slice);
					}
				}

				subset.m_indexBuffer.SetData(
					subset.m_vertexIndices.data(), 
					subset.m_vertexIndices.size());

				vertices[0].Position()				= CVector3Helper::GetFront() * radius;
				vertices[0].Normal()				= CVector3Helper::GetFront();
				(vertices.end() - 1)->Position()	= CVector3Helper::GetBack() * radius;
				(vertices.end() - 1)->Normal()		= CVector3Helper::GetBack();
				(vertices.end() - 1)->UV()[0]		= 1;
				(vertices.end() - 1)->UV()[1]		= 1;

				for (uint_t i = 0;
					i < slice;
					++i)
				{
					float phi =
						(float)i / slice *
						2 *
						Math::PI<float>();

					for (uint_t j = 0;
						j < (stack - 1);
						++j)
					{
						float theta =
							(float)(j + 1) / stack *
							Math::PI<float>();
						auto& vertex = vertices.at(j * slice + i + 1);

						vertex.Normal() =
							CVector3{
							sin(theta) * cos(phi),
							sin(theta) * sin(phi),
							cos(theta) };

						vertex.Position() = radius * vertex.Normal();

						vertex.UV()[0] = (float)i / slice * 2.0f;
						vertex.UV()[1] = 1 - (cos(theta) + 1) * 0.5f;

						if (vertex.UV()[0] > 1.0f)
							vertex.UV()[0] = 2.0f - vertex.UV()[0];
					}
				}

				m_vertexBuffer.SetData(
					vertices.data(),
					vertices.size());

				return *this;
			};

			template<template<class> class Allocator>
			CMesh<Allocator>
				CMesh<Allocator>::CreateByBox(void)
			{
				return CMesh<Allocator>().SetbyBox();
			};

			template<template<class> class Allocator>
			CMesh<Allocator>&
				CMesh<Allocator>::SetByBox(void)
			{
				Dispose();
				vector_t<CVertexFor3D, Allocator> vertices;
				vertices.reserve(24);

				{
					//正面
					vertices.push_back(CVertexFor3D(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f));
					vertices.push_back(CVertexFor3D(-1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f));
					vertices.push_back(CVertexFor3D(1.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f));
					vertices.push_back(CVertexFor3D(1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f));

					//背面
					vertices.push_back(CVertexFor3D(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f));
					vertices.push_back(CVertexFor3D(1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f));
					vertices.push_back(CVertexFor3D(1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f));
					vertices.push_back(CVertexFor3D(-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f));

					//上面
					vertices.push_back(CVertexFor3D(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f));
					vertices.push_back(CVertexFor3D(-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f));
					vertices.push_back(CVertexFor3D(1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f));
					vertices.push_back(CVertexFor3D(1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f));

					//下面
					vertices.push_back(CVertexFor3D(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f));
					vertices.push_back(CVertexFor3D(1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f));
					vertices.push_back(CVertexFor3D(1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f));
					vertices.push_back(CVertexFor3D(-1.0f, -1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f));

					//左面
					vertices.push_back(CVertexFor3D(-1.0f, -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f));
					vertices.push_back(CVertexFor3D(-1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f));
					vertices.push_back(CVertexFor3D(-1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f));
					vertices.push_back(CVertexFor3D(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f));

					//右面
					vertices.push_back(CVertexFor3D(1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f));
					vertices.push_back(CVertexFor3D(1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f));
					vertices.push_back(CVertexFor3D(1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f));
					vertices.push_back(CVertexFor3D(1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f));
				}

				//結線情報
				{
					SubSets().resize(1);
					tagSubset<Allocator>& subset = SubSets()[0];
					subset.m_vertexIndices.resize(36);
					auto it = subset.m_vertexIndices.begin();

					//正面
					*it = 0; ++it; *it = 1; ++it; *it = 2; ++it;
					*it = 0; ++it; *it = 2; ++it; *it = 3; ++it;
					//背面
					*it = 4; ++it; *it = 5; ++it; *it = 6; ++it;
					*it = 4; ++it; *it = 6; ++it; *it = 7; ++it;
					//上面
					*it = 8; ++it; *it = 9; ++it; *it = 10; ++it;
					*it = 8; ++it; *it = 10; ++it; *it = 11; ++it;
					//下面
					*it = 12; ++it; *it = 13; ++it; *it = 14; ++it;
					*it = 12; ++it; *it = 14; ++it; *it = 15; ++it;
					//左面
					*it = 16; ++it; *it = 17; ++it; *it = 18; ++it;
					*it = 16; ++it; *it = 18; ++it; *it = 19; ++it;
					//右面
					*it = 20; ++it; *it = 21; ++it; *it = 22; ++it;
					*it = 20; ++it; *it = 22; ++it; *it = 23;

					subset.m_indexBuffer.SetData(
						subset.m_vertexIndices.data(), 
						subset.m_vertexIndices.size());
				}

				m_vertexBuffer.
					SetData(
					vertices.data(),
					vertices.size());

				return *this;
			};

			template<template<class> class Allocator>
			CMesh<Allocator>
				CMesh<Allocator>::CreateByQuads(void)
			{
				return CMesh<Allocator>().SetbyQuads();
			};

			template<template<class> class Allocator>
			CMesh<Allocator>&
				CMesh<Allocator>::SetByQuads(void)
			{
				const float halfWidth = 0.5f;
				const float halfHeight = 0.5f;
				Dispose();

				CVertices::SetByQuads();

				m_subSets.resize(1);
				m_subSets[0].m_vertexIndices.resize(6);
				for (int i = 0; i < 6; ++i)
					m_subSets[0].m_vertexIndices[i] = i;
				m_subSets[0].
					m_indexBuffer.
					SetData(
					m_subSets[0].m_vertexIndices.data(), 
					m_subSets[0].m_vertexIndices.size());

				return *this;
			};

			template<template<class> class Allocator>
			void CMesh<Allocator>::SetMaterial(
				CMaterial<Allocator>&	material)
			{
				m_materials.clear();
				m_materials.resize(1);

				m_materials[0].SetAmbient(
					material.GetAmbient());
				m_materials[0].SetDiffuse(
					material.GetDiffuse());
				m_materials[0].SetEmissive(
					material.GetEmissive());
				m_materials[0].SetSpecular(
					material.GetSpecular());
				m_materials[0].SetPower(
					material.GetPower());
				m_materials[0].SetTexturePointer(
					CTextureBank<Allocator>::GetInstance().
					Request(
					material.GetTextureFilePath()));

				for (auto& subset : m_subSets)
				{
					subset.m_materialIndex = 0;
				}
			};
		};
	};
};

#endif