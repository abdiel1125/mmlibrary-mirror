/*!	\file		CBillBoard.h
*	\details	CBillBoardクラスの宣言
*	\author		松裏征志
*	\date		2014/07/07
*/
#ifndef MMLIBRARY_CBILLBARD_H_
#define MMLIBRARY_CBILLBARD_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CVerticesHasTexture.h"
#include "Graphic/Vertex/CVertexFor2D.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	テクスチャ付プリミティブの集合クラス
			template<template<class> class Allocator = USE_ALLOCATOR>
			class CBillBoard : public CVerticesHasTexture < CVertexFor2D, Allocator >
			{
			public:
				//!	コンストラクタ
				CBillBoard(void);

				//!	描画
				void	Render(void);
			};
		}
	}
}
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CBillBoard.hpp"
#endif