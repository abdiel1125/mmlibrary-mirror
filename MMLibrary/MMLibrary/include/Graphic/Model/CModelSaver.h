/*!	\file		CModelSaver.h
*	\details	CModelSaverクラスの宣言
*	\author		松裏征志
*	\date		2014/09/30
*/
#ifndef MMLIBRARY_CMODEL_SAVER_H_
#define MMLIBRARY_CMODEL_SAVER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	モデルをファイルに保存する機能を提供する
			template<template<class> class Allocator = USE_ALLOCATOR >
			class CModelSaver
			{
			private:
				const CModel<Allocator>*	m_pModel;
				std::ofstream				m_file;

				void SaveMesh(const Model::CMesh<Allocator>& mesh);

			public:
				//!	コンストラクタ
				CModelSaver(void){};
				//!	コンストラクタ
				CModelSaver(const CModel<Allocator>&	model)
					:m_pModel(&model){};

				//!	保存するモデルを設定する
				CModelSaver& SetModel(const CModel<Allocator>&	model)
				{
					m_pModel = &model;
				};
				//!	モデルを保存する
				CModelSaver& SaveModelTo(const string_t<Allocator>& filePath);
			};
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CModelSaver.hpp"

#endif

