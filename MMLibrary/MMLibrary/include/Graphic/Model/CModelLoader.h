/*!	\file		CModelLoader.h
*	\details	CModelLoaderクラスの宣言
*	\author		松裏征志
*	\date		2014/09/30
*/
#ifndef MMLIBRARY_CMODEL_LOADER_H_
#define MMLIBRARY_CMODEL_LOADER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CModel.h"
#include "Math/CVector.h"
#include "Graphic/Texture/LoadTexture.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	モデルをロードする機能を提供するクラス
			template<template<class> class Allocator = USE_ALLOCATOR >
			class CModelLoader
			{
			private:
				vector_t<char, Allocator>						m_memory;
				vector_t<CVertexFor3D, Allocator>				m_vertices;
				CModel<Allocator>*								m_pModel;
				char*											m_pMemory;

				void LoadXBinMesh(CMesh<Allocator>& mesh);
				void LoadXbinMaterials(vector_t<CMaterial<Allocator>, Allocator>& materials);

				void Dispose(void);
			public:
				CModelLoader(void){};
				void LoadMeshTo(
					CModel<Allocator>&	model,
					const string_t<Allocator>&			filePath);
			};
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CModelLoader.hpp"

#endif

