/*!	\file		CFace.h
*	\details	CFaceクラスの宣言
*	\author		松裏征志
*	\date		2014/06/23
*/
#ifndef MMLIBRARY_CFACE_H_
#define MMLIBRARY_CFACE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	面の情報を表すクラス
			class CFace
			{
			public:
				//!	ポリゴンの各頂点の添字
				array_t<uint_t, 3>	m_index;
				//!	ポリゴンに対応するマテリアルの添字
				uint_t				m_materialIndex;
			};
		};
	};
};

#endif