/*!	\file		CModelBank.h
*	\details	CModelBankクラスの宣言
*	\author		松裏征志
*	\date		2014/12/04
*/
#ifndef MMLIBRARY_CMODEL_BANK_H_
#define MMLIBRARY_CMODEL_BANK_H_

//***********************************************************
//インクルード
//***********************************************************
#include <memory>
#include "MMLibraryDefine.h"
#include "Utility\CSingletonBase.h"
#include "Utility\CDirectionaryBase.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			class IModel;

			//!	モデルの取得、保存を行うシングルトンなクラス
			template<template<class> class Allocator = USE_ALLOCATOR>
			class CModelBank :
				public Utility::CSingletonBase < CModelBank<Allocator> >,
				public Utility::CDirectionaryBase < std::shared_ptr<IModel >, Allocator >

			{
			public:
				//!	コンストラクタ
				CModelBank(void);
			};
		}
	}
}

//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CModelBank.hpp"

#endif	//	MMLIBRARY_CMODEL_BANK_H_