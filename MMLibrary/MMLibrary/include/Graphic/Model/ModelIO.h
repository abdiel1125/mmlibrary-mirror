/*!	\file		ModelIO.h
*	\details	Modelファイルの入出力に関するファイルのインクルード
*	\author		松裏征志
*	\date		2014/09/30
*/

#ifndef MMLIBRARY_MODEL_IO_H_
#define MMLIBRARY_MODEL_IO_H_

//***********************************************************
//インクルード
//***********************************************************
#include	"Graphic/Model/CMeshXLoader.h"
#include	"Graphic/Model/CModelSaver.h"
#include	"Graphic/Model/CModelLoader.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	ファイルの拡張子から適した読み込みクラスを呼び出して読み込む
			template<template<class> class Allocator = USE_ALLOCATOR >
			void LoadMeshTo(
				CModel<Allocator>&	model,
				const string_t<Allocator>& filePath)
			{
				//読み込み関数の選択
				string_t<Allocator> fileExt = Utility::GetFileExtention(filePath);
				model.Dispose();

				if (fileExt == "x")
				{
					CMeshXLoader<Allocator> loader;
					loader.LoadMeshTo(
						model,
						filePath);
				}
				else if (fileExt == "xbin")
				{
					CModelLoader<Allocator> loader;
					loader.LoadMeshTo(
						model,
						filePath);
				}
				
				//アニメーションプレイヤーの作成
				model.CreateAnimationPlayer();
			};
		};
	};
};
#endif