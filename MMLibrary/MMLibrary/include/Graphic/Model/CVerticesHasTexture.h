/*!	\file		CVerticesHasTexture.h
*	\details	CVerticesHasTextureクラスの宣言
*	\author		松裏征志
*	\date		2014/06/12
*/
#ifndef MMLIBRARY_CPRIMITIVE_HAS_TEXTURE_H_
#define MMLIBRARY_CPRIMITIVE_HAS_TEXTURE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CVertices.h"
#include "Graphic/Texture/CTexture.h"
#include "Utility/DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	テクスチャ付頂点の集合クラス
			template<class VERTEX, template<class> class Allocator = USE_ALLOCATOR>
			class CVerticesHasTexture : public CVertices < VERTEX, Allocator >
			{
			private:
				std::shared_ptr<CTextureBase>	m_pTexture;

			public:
				//! コンストラクタ
				CVerticesHasTexture(void);

				//! 引数付きコンストラクタ
				CVerticesHasTexture(vector_t<VERTEX, Allocator>& src);

				//! 描画メソッド
				void	Render(void);

				//! テクスチャを設定する
				CREATE_SETTER(Texture, m_pTexture);

				//! テクスチャを取得する
				CREATE_GETTER(Texture, m_pTexture);
			};
		}
	}
}
#include	"Utility/UndefineClassCreateHelpers.h"
#include	"Graphic/Model/CVerticesHasTexture.hpp"
#endif