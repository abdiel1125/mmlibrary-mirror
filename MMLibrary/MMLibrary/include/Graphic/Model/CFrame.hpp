/*	\file		CFrame<Allocator>.hpp
*	\details	CFrame<Allocator>クラスを定義する
*	\author		松裏征志
*	\date		2014/07/16
*/
//***********************************************************
//インクルード
//***********************************************************
#include "Graphic\Model\CFrame.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			template<template<class> class Allocator>
			std::shared_ptr<CFrame<Allocator> >& CFrame<Allocator>::AddChild(void)
			{
				if(m_pChild)
					return m_pChild->AddBrother(this);
				else
				{
					m_pChild.reset(new CFrame<Allocator>);
					m_pChild->m_pParent = this;
					return m_pChild;
				}
			};

			template<template<class> class Allocator>
			std::shared_ptr<CFrame<Allocator>>& CFrame<Allocator>::AddBrother(CFrame<Allocator>*	pParent)
			{
				if(m_pBrother)
					return m_pBrother->AddBrother(pParent ? pParent : this);
				else
				{
					m_pBrother.reset(new CFrame<Allocator>);
					m_pBrother->m_pParent = pParent ? pParent : this;
					return m_pBrother;
				}
			};

			template<template<class> class Allocator>
			void	CFrame<Allocator>::Render(void)
			{
				CGraphicManager::GetManager().Stack.Push().MultiplyLocal(m_transformMatrix);
				if(m_pMesh)		m_pMesh->Render();
				if(m_pChild)	m_pChild->Render();
				CGraphicManager::GetManager().Stack.Pop();
				if(m_pBrother)	m_pBrother->Render();
			};

			template<template<class> class Allocator>
			CFrame<Allocator>* CFrame<Allocator>::FindFrame(string_t& string)
			{
				if(string == m_name)
					return this;

				auto rtn = m_pChild->FindFrame(string);
				if(rtn)
					return rtn;

				return m_pBrother->FindFrame(string);
			};
		};
	};
};