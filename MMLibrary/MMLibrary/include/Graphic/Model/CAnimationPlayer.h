/*!	\file		CAnimationPlayer.h
*	\details	CAnimationPlayerクラスの宣言
*	\author		松裏征志
*	\date		2014/09/15
*/
#ifndef MMLIBRARY_CANIMATION_PLAYER_H_
#define MMLIBRARY_CANIMATION_PLAYER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Graphic/Model/CAnimation.h"
#include "Graphic/Model/CBone.h"
#include "Utility/RangeAlgorithm.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	アニメーションの再生機能を提供するクラス
			template<template<class> class Allocator = USE_ALLOCATOR>
			class CAnimationPlayer
			{
			private:
				float											m_duration;
				float											m_frameCounter;
				uint_t											m_animationSetIndex;
				vector_t<CBone<Allocator>, Allocator>*			m_pBones;
				vector_t<CAnimationSet<Allocator>, Allocator>*	m_pAnimationSets; 
				vector_t<CMatrix4x4, Allocator>					m_transformMatrices;

			public:
				//!	コンストラクタ
				CAnimationPlayer(void)
					: m_duration(1)
					, m_frameCounter(0)
					, m_animationSetIndex(0)
					, m_pAnimationSets(nullptr)
					, m_pBones(nullptr)
				{};
				//!	ボーン配列へのポインタを設定する
				void SetBonesPointer(vector_t<CBone<Allocator>, Allocator>&	bones)
				{
					m_pBones = &bones;
					m_transformMatrices.resize(m_pBones->size());
				};
				//!	CAnimationSetの動的配列へのポインタを設定する
				void SetAnimationSetsPointer(vector_t<CAnimationSet<Allocator>, Allocator>&	animationSets)
				{
					m_pAnimationSets = &animationSets;
				};
				//!	利用するCAnimationSetの添字を設定する
				void SetAnimationSetIndex(uint_t animationSetIndex)
				{
					m_animationSetIndex = animationSetIndex;
				};
				//!	利用するCAnimationの添字を設定する
				void SetAnimationIndex(uint_t animationIndex)
				{
					m_animationIndex = animationIndex;
				};
				//!	更新時に進める時間を設定する
				void SetDuration(float duration)
				{
					m_duration = duration;
				};
				//!	次のCAnimationSetに進める
				void ToNext(void)
				{
					++m_animationSetIndex;
					if (m_animationSetIndex >= m_pAnimationSets->size())
						m_animationSetIndex = 0;
				};
				//!	前のCAnimationSetに戻る
				void ToPrevious(void)
				{
					--m_animationSetIndex;
					if (m_animationSetIndex >= m_pAnimationSets->size())
						m_animationSetIndex = m_pAnimationSets->size() - 1;
				};
				//!	現在の行列を計算する
				void CalcMatrix(bool isFirstTime = false)
				{
					//アニメーションセットがなければ終了
					if (m_pAnimationSets->empty()) return;

					auto& bones = *m_pBones;
					auto& animationSets = *m_pAnimationSets;
					
					for (auto& bone : bones)
					{
						bone.SetIsUpdated(false);
					}

					for (const auto& animation : animationSets[m_animationSetIndex].m_animations)
					{
						auto boneIt = Range::FindIf(
							bones,
							[&](const CBone<Allocator>& bone)->bool
						{
							return bone.GetName() == animation.m_boneName;
						});

						if (boneIt == bones.end())
							continue;

						auto& transformMatrix = m_transformMatrices[boneIt - bones.begin()];

						if (isFirstTime == false &&
							animation.GetMaxFrameNum() <= 1)
							continue;

						boneIt->SetIsUpdated(true);

						transformMatrix =
							CTransform(
							animation.GetTranslateAt(m_frameCounter),
							animation.GetScaleAt(m_frameCounter),
							animation.GetRotationAt(m_frameCounter)).
							GetMatrix().
							Multiply(animation.GetMatrixAt(m_frameCounter));

						if (transformMatrix.AsVector4(0).IsNan() ||
							transformMatrix.AsVector4(1).IsNan() ||
							transformMatrix.AsVector4(2).IsNan() ||
							transformMatrix.AsVector4(3).IsNan())
							OutputDebugString("transformMatrix is nan.");
					}

					for (uint_t i = 0; i < m_pBones->size(); ++i)
					{
						auto parentIndex = bones[i].GetParentIndex();
						bones[i].DrawMatrix() = bones[i].GetOffsetMatrix();
						bones[i].DrawMatrix().Multiply(m_transformMatrices[i]);

						while (parentIndex != -1)
						{
							bones[i].DrawMatrix().Multiply(m_transformMatrices[parentIndex]);
							parentIndex = bones[parentIndex].GetParentIndex();
						}
					}
				};

				//!	アニメーションの更新
				void Update(void)
				{
					if (!m_pAnimationSets || !m_pAnimationSets->size())
						return;

					CalcMatrix();

					m_frameCounter += m_duration;

					if (m_frameCounter >= (*m_pAnimationSets)[m_animationSetIndex].m_maxTime)
						m_frameCounter = 0;
				};
			};
		};
	};
};

#endif