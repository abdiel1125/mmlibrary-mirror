/*!	\file		CMeshXLoader.h
*	\details	CMeshXLoaderクラスの宣言
*	\author		松裏征志
*	\date		2014/09/08
*/
#ifndef MMLIBRARY_CMESHX_LOADER_H_
#define MMLIBRARY_CMESHX_LOADER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility\CTokenizer.h"
#include "Graphic/Model/CModel.h"
#include "Math/CVector.h"
#include "Graphic/Texture/LoadTexture.h"

namespace MMLibrary
{
	namespace Graphic
	{
		namespace Model
		{
			//!	.xファイルからモデルデータを取得する機能を提供するクラス
			template<template<class> class Allocator = USE_ALLOCATOR >
			class CMeshXLoader
			{
			private:
				using	textureCoords_t = Math::CVector < float, 2 > ;
				using	string_t = string_t < Allocator > ;

				struct tagWeightAndIndex
				{
					float	m_weight;
					int		m_index;
				};

				struct tagSkinWeight
				{
					string_t	m_name;
					CMatrix4x4	m_offsetMatrix;
					vector_t<tagWeightAndIndex, Allocator> m_array;
				};

				struct tagConnect
				{
					int				m_num;
					array_t<int, 4>	m_index;
					int				m_materialIndex;
				};

				vector_t<char, Allocator>		m_memory;
				vector_t<tagConnect, Allocator>		m_connects;
				vector_t<tagSkinWeight, Allocator>				m_skinWeightSet;
				vector_t<CFrame<Allocator>*, Allocator>			m_pFramesHasMesh;
				vector_t<CMaterial<Allocator>, Allocator>		m_materials;
				vector_t<CVertexFor3D, Allocator>				m_vertices;

				CModel<Allocator>*								m_pModel;
				CMesh<Allocator>*								m_pNowMesh;
				CFrame<Allocator>*								m_pNowFrame;
				CAnimationSet<Allocator>*						m_pNowAnimationSet;
				Utility::CTokenizer<Allocator>					m_token;
				int												m_frameNum;
				vector_t<CAnimationSet<Allocator>, Allocator>	m_animationSets;
				std::shared_ptr<CFrame<Allocator> >				m_frameRoot;

				void LoadMatrix4x4(CMatrix4x4& matrix)
				{
					for (auto& row : matrix)
						for (auto& collumn : row)
							collumn = m_token.GetNextAsFloat();
				};
				void LoadVector3(array_t<float, 3>& vector)
				{
					vector.at(0) = m_token.GetNextAsFloat();
					vector.at(1) = m_token.GetNextAsFloat();
					vector.at(2) = m_token.GetNextAsFloat();
				};
				void LoadVector4(array_t<float, 4>& vector)
				{
					vector.at(0) = m_token.GetNextAsFloat();
					vector.at(1) = m_token.GetNextAsFloat();
					vector.at(2) = m_token.GetNextAsFloat();
					vector.at(3) = m_token.GetNextAsFloat();
				};
				void LoadName(string_t& name, const string_t& defaultName, uint_t defaultNameNumber)
				{
					m_token.Next();
					if (m_token.Check("{"))
					{
						name = defaultName + std::to_string(defaultNameNumber);
					}
					else
					{
						//　フレーム名を取得
						name = m_token.GetAsChar();

						// '{'が出るまでスキップ
						SkipToBlockStart();
					}

				};

				void GetFrameEndPos(void);

				void CheckNode(void);
				void SkipNode(void);
				void SkipToBlockStart(void)
				{
					while (!m_token.IsEnd())
					{
						m_token.Next();
						if (m_token.Check("{"))
							break;
					}
				};
				void SkipToBlockEnd(void)
				{
					while (!m_token.IsEnd())
					{
						if (m_token.Check("}"))
							break;
						m_token.Next();
					}
				};
				void LoadXFrame(void);
				void LoadXMesh(void);
				void LoadXPosition(void);
				void LoadXConnect(void);
				void CreateNormalData(void);
				void LoadXMeshNormals(void);
				void LoadXMeshMaterialList(void);
				void LoadXMaterial(CMaterial<Allocator>& material, int i);
				void LoadXMeshTextureCoords(void);
				void LoadXSkinWeights(void);
				void LoadXAnimationSet(void);
				void LoadXAnimation(void);

				void CreateBoneFromFrame(CFrame<Allocator>* pFrame, int parentIndex);
				void CreateBones(void);
				void SetSkinWeights(void);
				void CreateSubSet(void);

				void Dispose(void);
			public:
				//!	コンストラクタ
				CMeshXLoader(void);

				//!	.xファイルからモデルデータを取得する			
				void LoadMeshTo(
					CModel<Allocator>&	model,
					const string_t& filePath);
			};
		};
	};
};
//***********************************************************
//定義のインクルード
//***********************************************************
#include	"Graphic/Model/CMeshXLoader.hpp"

#endif

