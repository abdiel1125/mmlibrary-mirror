/*!	\file		CQuaternion.h
*	\details	CQuaternionクラスの宣言
*	\author		松裏征志
*	\date		2014/04/26
*/
#ifndef MMLIBRARY_CQUATERNION_H_
#define MMLIBRARY_CQUATERNION_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Graphic/CVector3.h"

namespace MMLibrary
{
	namespace Graphic
	{
		class CMatrix4x4;
	};
	namespace Math
	{
		/*!
		*	\brief クオータニオンを表現するテンプレートクラス
		*
		*	\tparam T このクオータニオンの型を表す。デフォルトはfloat
		*	
		*	クォータニオンは、ベクトルを定義する値に第4の成分を追加し、任意の4Dベクトルを生成する。
		*	次に、正規化したクォータニオンの各成分が軸/角度の回転にどのように関係しているかを示す 
		*	(ここで、q は単位クォータニオン (x, y, z, w) を表す。
		*	axis は正規化され、theta は軸を回転軸とした CCW 回転である)。
		*/
		template<typename T = float>
		class CQuaternion
		{
		private:
			/*!
			*	クオータニオンのベクトル部を表す
			*/
			CVector<T, 3>	vector;
			/*!
			*	クオータニオンのスカラー部を表す
			*/
			T				scalar;
		public:
			/*!
			*	\brief コンストラクタ
			*
			*	すべての要素を0で初期化する
			*/
			CQuaternion(void);

			/*!
			*	\brief	引数付きコンストラクタ
			*/
			CQuaternion(
				T scalar,
				T vectorX,
				T vectorY, 
				T vectorZ);

			/*!
			*	\brief	引数付きコンストラクタ
			*/
			CQuaternion(
				T scalar,
				CVector<T, 3>& vector);
			
			/*!	
			*	\brief	自身のコピーを返す。
			*
			*	\return	自身のコピー
			*/
inline		CQuaternion<T> ToClone(void) const;

			/*!	
			*	\brief	スカラー部への参照を返す。
			*	
			*	\return	スカラー部への参照
			*/
inline		T&	Scalar(void);
			
			/*!	
			*	\brief	スカラー部への参照を返す。(値の取得専用)
			*	
			*	\return	スカラー部への参照
			*/
inline		const T&	Scalar(void) const;

			/*!	
			*	\brief	ベクトル部への参照を返す。
			*	
			*	\return	ベクトル部への参照
			*/
inline		CVector<T, 3>&	Vector(void);
			
			/*!	
			*	\brief	ベクトル部への参照を返す。(値の取得専用)
			*	
			*	\return	ベクトル部への参照
			*/
inline		const CVector<T, 3>&	Vector(void) const;
			
			/*!	
			*	\brief	加算演算子
			*
			*	\param	[in]	src	この引数の値を加算する
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	operator+(const CQuaternion<T>& src) const;

			/*!	
			*	\brief	減算演算子
			*
			*	\param	[in]	src	この引数の値を減算する
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	operator-(const CQuaternion<T>& src) const;

			/*!	
			*	\brief	乗算演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に乗算する
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	operator*(const CQuaternion<T>& src) const;

			/*!	
			*	\brief	除算演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に除算する
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	operator/(const CQuaternion<T>& src) const;
			
			/*!	
			*	\brief	加算代入演算子
			*
			*	\param	[in]	src	この引数の値を加算代入する
			*
			*	\return			演算後の自身への参照を返す
			*/
			CQuaternion<T>&	operator+=(const CQuaternion<T>& src);

			/*!	
			*	\brief	減算代入演算子
			*
			*	\param	[in]	src	この引数の値を減算代入する
			*
			*	\return			演算後の自身への参照を返す
			*/
			CQuaternion<T>&	operator-=(const CQuaternion<T>& src);

			/*!	
			*	\brief	乗算代入演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に乗算代入する
			*
			*	\return			演算後の自身への参照を返す
			*/
			CQuaternion<T>&	operator*=(const CQuaternion<T>& src);

			/*!	
			*	\brief	除算代入演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に除算代入する
			*
			*	\return			演算後の自身への参照を返す
			*/
			CQuaternion<T>&	operator/=(const CQuaternion<T>& src);
			
			/*!	
			*	\brief	乗算演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に乗算する
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	operator*(const T& src) const;
			
			/*!	
			*	\brief	乗算演算子
			*
			*	\param	[in]	srcF	この引数の値とsrcQを乗算する
			*	\param	[in]	srcQ	この引数の値とsrcFを乗算する
			*
			*	\return			演算結果を返す
			*/
			friend CQuaternion<T>	operator*(const T& srcF, const CQuaternion<T>& srcQ);

			/*!	
			*	\brief	除算演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に除算する
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	operator/(const T& src) const;

			/*!	
			*	\brief	乗算代入演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に乗算代入する
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>&	operator*=(const T& src);

			/*!	
			*	\brief	除算代入演算子
			*
			*	\param	[in]	src	この引数の値をすべての要素に除算代入する
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>&	operator/=(const T& src);

			/*!	
			*	\brief	負符号
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	operator-(void) const;

			/*!	
			*	\brief	等価演算子
			*
			*	\param	[in]	src	この引数の値と比較する
			*
			*	\return			演算結果を返す
			*/
			bool	operator==(const CQuaternion<T>& src) const;

			/*!	
			*	\brief	非等価演算子
			*
			*	\param	[in]	src	この引数の値と比較する
			*
			*	\return			演算結果を返す
			*/
			bool	operator!=(const CQuaternion<T>& src) const;

			/*!
			*	\brief	2 つのクォータニオンの内積を返す。
			*
			*	\param	[in]	src	処理の基になるCQuaternion<T>クラスへの参照
			*
			*	\return	演算結果を返す
			*/
			T	Dot(const CQuaternion<T>& src) const;
			
			/*!	
			*	\brief	クオータニオンの絶対値を取得する
			*
			*	\return			演算結果を返す
			*/
			T	GetNorm(void) const;
			
			/*!	
			*	\brief	クオータニオンの絶対値の二乗を取得する
			*
			*	\return			演算結果を返す
			*/
			T	GetNormSq(void) const;

			/*!
			*	\brief	単位クオータニオンであるかを判定する
			*	
			*	\return	単位クオータニオンであればtrue、単位クオータニオンでなければfalseを返す
			*/
			bool IsIdentity(void) const;

			/*!
			*	\brief	2 つのクォータニオンの積を計算し、自身に設定する。
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>&	Multiply(const CQuaternion<T>& src);

			/*!	
			*	\brief	共益なクオータニオンを作成する
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	CreateByConjugation(void);
			
			/*!	
			*	\brief	重心座標のクォータニオンを作成する
			*
			*	\param	[in]	Q1	処理の基になるCQuaternionへの参照
			*	\param	[in]	Q2	処理の基になるCQuaternionへの参照
			*	\param	[in]	Q3	処理の基になるCQuaternionへの参照
			*	\param	[in]	f	加重係数
			*	\param	[in]	g	加重係数
			*
			*	\return			演算結果を返す
			*
			*	次に示す球面線形補間演算子を実装する。\n
			*	Slerp(	Slerp(Q1, Q2, f+g),\n
			*			Slerp(Q1, Q3, f+g), \n
			*			g/(f+g))\n
			*/
static		CQuaternion<T>	CreateByBaryCentric(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	Q2,
				const CQuaternion<T>&	Q3,
				const float				f,
				const float				g);

			/*!
			*	\brief	指数関数を計算する。
			*
			*	\return			演算結果を返す
			*
			*	このメソッドは、純粋クォータニオンを単位クォータニオンに変換する。
			*	つまり、純粋クォータニオンが渡されるものと仮定し、w を無視して計算を行う (w == 0)。
			*
			*	このようなクオータニオンが渡されたと仮定する(vはベクトル部分を正規化したもの)
			*	q = (0, theta * v); 
			*
			*	次の式を用いて計算する
			*	exp(Q) = (cos(theta), sin(theta) * v)
			*/
static		CQuaternion<T>	CreateByExp(void);

			/*!
			*	\brief	恒等クォータニオン(1:0,0,0)を返す。
			*/
static		CQuaternion<T>	CreateByIdentity(void);

			/*!
			*	\brief	逆クオータニオンを返す。
			*
			*	\return			演算結果を返す
			*
			*	逆クオータニオンは次のように計算する
			*	Q^-1 = Q.Conjugate() / Q.GetNormSq
			*/
			CQuaternion<T>	CreateByInverse(void) const;

			/*!
			*	\brief	自然対数を計算する。
			*
			*	\return			演算結果を返す
			*
			*	このメソッドは、純粋クォータニオンを単位クォータニオンに変換する。
			*
			*	このようなクオータニオンが渡されたと仮定する(vはベクトル部分を正規化したもの)
			*	q = (cos(theta), sin(theta) * v) 
			*
			*	次の式を用いて計算する
			*	ln(Q) = (0, theta * v);
			*/
			CQuaternion<T>	CreateByLn(void) const;

			/*!
			*	\brief	正規化した長さのクォータニオンを計算する。
			*
			*	\return			演算結果を返す
			*/
			CQuaternion<T>	CreateByNormalize(void) const;

			/*!
			*	\brief	球面線形補間を使って、2 つのクォータニオン間を補間する。
			*
			*	\param	[in]	Q1	処理の基になるCQuaternion<T>クラスへの参照
			*	\param	[in]	Q2	処理の基になるCQuaternion<T>クラスへの参照
			*	\param	[in]	t	補間するクォータニオン間の間隔を示すパラメータ
			*
			*	\return	演算結果を返す
			*/
static		CQuaternion<T>	CreateBySlerp(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	Q2,
				const float				t);

			/*!	
			*	\brief	任意の軸を回転軸として回転させたクォータニオンを作成する。
			*
			*	\param	[in]	axis	軸の角度を識別するCVector3への参照
			*	\param	[in]	radian	回転の角度 (ラジアン単位)。角度は、回転軸を中心にして原点方向を向いた時計回りで測定したものである。
			*
			*	\return	演算結果を返す
			*/
static		CQuaternion<T>	CreateByRotationAxis(
				const	Graphic::CVector3& axis,
				const	T					radian);

			/*!
			*	\brief	回転行列からクォータニオンを作成する。
			*
			*	\param	[in]	rotation	処理の基になるCMatrix4x4への参照
			*
			*	\return	演算結果を返す
			*/
static		CQuaternion<T>	CreateByRotationMatrix(
				const Graphic::CMatrix4x4&	rotation);
				
			/*!
			*	\brief	ヨー・ピッチ・ロールを指定してクォータニオンを作成する。
			*
			*	\param	[in]	rotation	処理の基になるCVector3への参照
			*
			*	\return	演算結果を返す
			*/
static		CQuaternion<T>	CreateByRotationYawPitchRoll(
				const Graphic::CVector3&	rotation);
				
			/*!
			*	\brief	ヨー・ピッチ・ロールを指定してクォータニオンを作成する。
			*
			*	\param	[in]	x	演算に使用するX軸のラジアン角度(ピッチ)
			*	\param	[in]	y	演算に使用するY軸のラジアン角度(ヨー)
			*	\param	[in]	z	演算に使用するZ軸のラジアン角度(ロール)
			*
			*	\return	演算結果を返す
			*/
static		CQuaternion<T>	CreateByRotationYawPitchRoll(
				const T	x,
				const T	y,
				const T	z);

			/*!
			*	\brief	球面二次補間を使って、クォータニオン間を補間する。
			*
			*	\return	演算結果を返す
			*
			*	この関数は、次に示す球面線形補間演算を使う。\n
			*	Slerp(	Slerp(Q, C, t), \n
			*			Slerp(A, B, t),\n
			*			2t(1 - t))
			*/
static		CQuaternion<T>	CreateBySquad(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	A,
				const CQuaternion<T>&	B,
				const CQuaternion<T>&	Q2,
				const T							t);

			/*!
			*	\brief	球面二次補間の制御ポイントを作成する。
			*
			*	\return	演算結果を返す
			*
			*	CreateBySquad, SetBySquadで使われる制御ポイントを作成する。
			*	Aを作成する場合は、引数に始点のクオータニオンの前後を含めて3つのクオータニオンを渡す。
			*	Bを作成する場合は、引数に終点のクオータニオンの前後を含めて3つのクオータニオンを渡す。
			*/
static		CQuaternion<T>	CreateBySquadSetup(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	Q2,
				const CQuaternion<T>&	Q3);
			
			/*!
			*	\brief	球面二次補間を使って、クォータニオン間を補間する。
			*
			*	\return	演算結果を返す
			*
			*	次に、クォータニオン キーのセット (Q0、Q1、Q2、Q3) を使って制御ポイントを作成し,
			*	Q1,Q2間で球面線形補間演算を行う。
			*/
static		CQuaternion<T>	CreateBySquadEx(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	Q2,
				const CQuaternion<T>&	Q3,
				const CQuaternion<T>&	Q4,
				const T							t);

static		CQuaternion<T>	CreateBy2Vectors(
				const Graphic::CVector3& begin,
				const Graphic::CVector3& end);
			
			/*!	
			*	\brief	共益なクオータニオンを作成し、自身に設定する
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>&	SetByConjugation(void);
			
			/*!	
			*	\brief	重心座標のクォータニオンを作成し、自身に設定する
			*
			*	\param	[in]	Q1	処理の基になるCQuaternionへの参照
			*	\param	[in]	Q2	処理の基になるCQuaternionへの参照
			*	\param	[in]	Q3	処理の基になるCQuaternionへの参照
			*	\param	[in]	f	加重係数
			*	\param	[in]	g	加重係数
			*
			*	\return	演算後の自身への参照を返す
			*
			*	次に示す球面線形補間演算子を実装する。\n
			*	Slerp(	Slerp(Q1, Q2, f+g),\n
			*			Slerp(Q1, Q3, f+g), \n
			*			g/(f+g))\n
			*/
			CQuaternion<T>&	SetByBaryCentric(
				const CQuaternion<T>& Q1,
				const CQuaternion<T>& Q2,
				const CQuaternion<T>& Q3,
				const float f,
				const float g);

			/*!
			*	\brief	指数関数を計算し、自身に設定する。
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>& SetByExp(void);

			/*!
			*	\brief	恒等クォータニオン(1:0,0,0)を自身に設定する。
			*/
			CQuaternion<T>&	SetByIdentity(void);

			/*!
			*	\brief	逆クオータニオンを計算し、自身に設定する。
			*
			*	\return	演算後の自身への参照を返す
			*
			*	逆クオータニオンは次のように計算する
			*	Q^-1 = Q.Conjugate() / Q.GetNormSq
			*/
			CQuaternion<T>&	SetByInverse(void);

			/*!
			*	\brief	自然対数を計算し、自身に設定する。
			*
			*	\return	演算後の自身への参照を返す
			*
			*	このメソッドは、純粋クォータニオンを単位クォータニオンに変換する。
			*
			*	このようなクオータニオンが渡されたと仮定する(vはベクトル部分を正規化したもの)
			*	q = (cos(theta), sin(theta) * v) 
			*
			*	次の式を用いて計算する
			*	ln(Q) = (0, theta * v);
			*/
			CQuaternion<T>&	SetByLn(void);

			/*!
			*	\brief	正規化した長さのクォータニオンを計算し、自身に設定する。
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>&	SetByNormalize(void);

			/*!
			*	\brief	球面線形補間を使って、2 つのクォータニオン間を補間し、自身に設定する。
			*
			*	\param	[in]	Q1	処理の基になるCQuaternion<T>クラスへの参照
			*	\param	[in]	Q2	処理の基になるCQuaternion<T>クラスへの参照
			*	\param	[in]	t	補間するクォータニオン間の間隔を示すパラメータ
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>&	SetBySlerp(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	Q2,
				const float				t);

			/*!	
			*	\brief	任意の軸を回転軸としてクォータニオンを回転させる。
			*
			*	\param	[in]	axis	軸の角度を識別するCVector3への参照
			*	\param	[in]	radian	回転の角度 (ラジアン単位)。角度は、回転軸を中心にして原点方向を向いた時計回りで測定したものである。
			*
			*	\return	演算後の自身への参照を返す
			*/
			CQuaternion<T>&	SetByRotationAxis(
				const	Graphic::CVector3& axis,
				const	T					radian);
				
			/*!
			*	\brief	回転行列からクォータニオンを作成し、自身に設定する。
			*
			*	\param	[in]	rotation	処理の基になるCMatrix4x4への参照
			*
			*	\return	演算後の自身への参照
			*/
			CQuaternion<T>&	SetByRotationMatrix(
				const Graphic::CMatrix4x4&	rotation);
				
			/*!
			*	\brief	ヨー・ピッチ・ロールを指定してクォータニオンを作成し、自身に設定する。
			*
			*	\param	[in]	rotation	処理の基になるCVector3への参照
			*
			*	\return	演算後の自身への参照
			*/
			CQuaternion<T>&	SetByRotationYawPitchRoll(
				const Graphic::CVector3&	rotation);
				
			/*!
			*	\brief	ヨー・ピッチ・ロールを指定してクォータニオンを作成し、自身に設定する。
			*
			*	\param	[in]	x	演算に使用するX軸のラジアン角度(ピッチ)
			*	\param	[in]	y	演算に使用するY軸のラジアン角度(ヨー)
			*	\param	[in]	z	演算に使用するZ軸のラジアン角度(ロール)
			*
			*	\return	演算後の自身への参照
			*/
			CQuaternion<T>&	SetByRotationYawPitchRoll(
				const T	x,
				const T	y,
				const T	z);

			/*!
			*	\brief	球面二次補間を使って、クォータニオン間を補間し、自身に設定する。
			*
			*	\return	演算後の自身への参照
			*
			*	この関数は、次に示す球面線形補間演算を使う。\n
			*	Slerp(	Slerp(Q, C, t), \n
			*			Slerp(A, B, t),\n
			*			2t(1 - t))
			*/
			CQuaternion<T>&	SetBySquad(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	A,
				const CQuaternion<T>&	B,
				const CQuaternion<T>&	Q2,
				const T							t);

			/*!
			*	\brief	球面二次補間の制御ポイントを作成し、自身に設定する。
			*
			*	\return	演算後の自身への参照
			*
			*	CreateBySquad, SetBySquadで使われる制御ポイントを作成する。
			*	Aを作成する場合は、引数に始点のクオータニオンの前後を含めて3つのクオータニオンを渡す。
			*	Bを作成する場合は、引数に終点のクオータニオンの前後を含めて3つのクオータニオンを渡す。
			*/
			CQuaternion<T>&	SetBySquadSetup(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	Q2,
				const CQuaternion<T>&	Q3);
			
			/*!
			*	\brief	球面二次補間を使って、クォータニオン間を補間し、自身に設定する。
			*
			*	\return	演算後の自身への参照
			*
			*	次に、クォータニオン キーのセット (Q0、Q1、Q2、Q3) を使って制御ポイントを作成し,
			*	Q1,Q2間で球面線形補間演算を行う。
			*/
			CQuaternion<T>&	SetBySquadEx(
				const CQuaternion<T>&	Q1,
				const CQuaternion<T>&	Q2,
				const CQuaternion<T>&	Q3,
				const CQuaternion<T>&	Q4,
				const T							t);

			CQuaternion<T>&	SetBy2Vectors(
				const Graphic::CVector3& begin,
				const Graphic::CVector3& end);
		};
	};
};

//***********************************************************
//定義のインクルード
//***********************************************************
#include "CQuaternion.hpp"

#endif