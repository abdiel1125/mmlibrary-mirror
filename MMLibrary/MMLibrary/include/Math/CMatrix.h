/*!	\file		CMatrix.h
*	\details	CMatrixクラスの宣言
*	\author		松裏征志
*	\date		2014/04/21
*/
#ifndef MMLIBRARY_CMATRIX_H_
#define MMLIBRARY_CMATRIX_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "CValueArray.h"

namespace MMLibrary
{
	namespace Math
	{
		template<typename T, const uint_t N>
		class CVector;

		/*!	\class	CMatrix
		*	\brief	行列を表現するテンプレートクラス
		*
		*	\tparam T この行列の型を表す。デフォルトはfloat
		*	\tparam M この行列の行数を表す。デフォルトは4
		*	\tparam N この行列の列数を表す。デフォルトは4
		*
		*	T型N次元のベクトルを表現するためのクラス
		*	テンプレートメタプログラミングの練習を兼ねて作成
		*/
		template<
			typename			T = float,
			const uint_t	M = 4,
			const uint_t	N = 4>
		class CMatrix :
			public
			Utility::tagEqualComparable < CMatrix<T, M, N>,
			Utility::tagCanAdditionAssignment < CMatrix<T, M, N>,
			Utility::tagCanSubtractionAssignment < CMatrix<T, M, N>,
			Utility::tagCanMultiplicationAssignment2nd < CMatrix<T, M, N>, T,
			Utility::tagCanDivisionAssignment2nd < CMatrix<T, M, N>, T,
			CValueArray<CValueArray<T, N>, M > > > > > >
		{
		public:
#pragma region Operator

			//!	加算代入演算子
			friend CMatrix<T, M, N>& operator+=(CMatrix<T, M, N>& left, const CMatrix<T, M, N>& right)
			{
				for (uint_t m = 0; m < M; ++m)
				{
					for (uint_t n = 0; n < N; ++n)
					{
						left[m][n] += right[m][n];
					}
				}

				return left;
			};

			//! 減算代入演算子
			friend CMatrix<T, M, N>& operator-=(CMatrix<T, M, N>& left, const CMatrix<T, M, N>& right)
			{
				for (uint_t m = 0; m < M; ++m)
				{
					for (uint_t n = 0; n < N; ++n)
					{
						left[m][n] -= right[m][n];
					}
				}

				return left;
			};

			//!	乗算代入演算子
			friend CMatrix<T, M, N>& operator*=(CMatrix<T, M, N>& left, const T& right)
			{
				for (uint_t m = 0; m < M; ++m)
				{
					for (uint_t n = 0; n < N; ++n)
					{
						left[m][n] *= right;
					}
				}

				return left;
			};

			//!	除算代入演算子
			friend CMatrix<T, M, N>& operator/=(CMatrix<T, M, N>& left, const T& right)
			{
				for (uint_t m = 0; m < M; ++m)
				{
					for (uint_t n = 0; n < N; ++n)
					{
						left[m][n] /= right;
					}
				}

				return left;
			};

			//!	乗算演算子
			template<typename Ts, const uint_t Ns>
			friend CMatrix<T, M, Ns> operator*(
				const MMLibrary::Math::CMatrix<T, M, N>& left,
				const MMLibrary::Math::CMatrix<Ts, N, Ns>& right)
			{
				CMatrix<T, M, Ns> rtn;

				rtn.SetBy1Element(0);

				for (uint_t m = 0; m < M; ++m)
				{
					for (uint_t n = 0; n < Ns; ++n)
					{
						for (uint_t l = 0; l < N; ++l)
						{
							rtn[m][n] +=
								left[m][l] * right[l][n];
						}
					}
				}

				return rtn;
			};

			//!	乗算代入演算子
			template<typename Ts>
			friend CMatrix<T, M, N>& operator*=(
				MMLibrary::Math::CMatrix<T, M, N>& left,
				const MMLibrary::Math::CMatrix<Ts, N, N>& right)
			{
				left = left * right;
				return left;
			};

#pragma endregion

			//!	コンストラクタ
			/*!
			*	単位行列で初期化する
			*/
			CMatrix(void){ SetByIdentity(); };

			//!	自身のコピーを返す。
			/*!	
			*	\return	自身のコピー
			*/
inline		CMatrix<T, M, N> ToClone(void) const;

			//!	行列の行列式を返す。
			/*!	
			*	行列の行列式を返す。正方行列でない場合は0を返す。
			*/
			T	Determinant(void) const;

			//!	行列が単位行列かどうかを判定する。
			/*!
			*	\return			行列が単位行列ならtrue、
			*	行列が単位行列でないならfalse
			*/
			bool	IsIdentity(void);

			//!	行列との積を計算し、自身に代入する。
			/*!
			*	\param	[in] src 演算に用いる行列
			*
			*	\return			演算後の自身への参照を返す
			*/
			template<typename Ts>
				CMatrix<T, M, N>&	Multiply(const CMatrix<Ts, N, N>& src);

			//!	行列との積を逆順に計算し、自身に代入する。
			/*!
			*	\param	[in] src 演算に用いる行列
			*
			*	\return			演算後の自身への参照を返す
			*/
			template<typename Ts>
				CMatrix<T, M, N>&	MultiplyTransposed(const CMatrix<Ts, N, N>& src);

			//!	この行列が正方行列であるかを判定する
			/*!
			*	\return	正方行列であればtrue、正方行列でなければfalseを返す
			*/
			bool	IsSquareMatrix(void);

			//!	この行列の転置行列を作成して返す。
			/*!	
			*	\return	転置行列を返す
			*/
			CMatrix<T, N, M>	ToTransposedMatrix(void);
			
			//!	全要素を引数の値で指定した行列を作成する。
			/*!
			*	全要素をsrcで初期化する
			*/
			static CMatrix<T, M, N> CreateBy1Element(T src);

			//!	単位行列を作成する。
			/*!
			*	\return			演算結果を返す
			*/
			static CMatrix<T, M, N>	CreateByIdentity(void);

			//!	逆行列を作成する。
			/*!
			*	\return			演算結果を返す
			*
			*	逆行列を作成し、自身に代入する。\n
			*	逆行列が存在しない場合は単位行列を作成し、自身に代入する。
			*/
			CMatrix<T, M, N>	CreateByInverse(void) const;
			
			//!	全要素を一括で設定する
			/*!
			*	全要素をsrcで初期化する
			*/
			CMatrix<T, M, N>& SetBy1Element(T src);

			//!	単位行列を作成し、自身に代入する。
			/*!
			*	\return			演算後の自身への参照を返す
			*/
			CMatrix<T, M, N>&	SetByIdentity(void);

			//!	逆行列を作成し、自身に代入する。
			/*!
			*	\return			演算後の自身への参照を返す
			*
			*	逆行列を作成し、自身に代入する。\n
			*	逆行列が存在しない場合は単位行列を作成し、自身に代入する。
			*/
			CMatrix<T, M, N>&	SetByInverse(void);
			
			private:
			void CreateExtendMatrix(array_t<array_t<T, N * 2>, M>& work);

			bool DoGaussJordan(array_t<array_t<T, N * 2>, M>& work);
		};

#ifndef MMLIBRARY_CMATRIX_INSTANTIATED
		extern template class CMatrix < float, 4, 4 >;
#endif
	};
};

//***********************************************************
//定義のインクルード
//***********************************************************
#include "CMatrix.hpp"

#endif