/*!	\file		CRangeBindedValue.h
*	\details	CRangeBindedValueクラスの宣言を行う
*	\author		松裏征志
*	\date		2014/12/05
*/
#ifndef MMLIBRARY_CRANGE_BINDED_VALUE_H_
#define MMLIBRARY_CRANGE_BINDED_VALUE_H_

//***********************************************************
//インクルード
//***********************************************************
#include <limits>
#include "MMLibraryDefine.h"
#include "Utility\DefineClassCreateHelpers.h"
#include "Math\Function.h"

namespace MMLibrary
{
	namespace Math
	{
		//!	CRangeBindedValueの拘束方法を表すクラス、はみ出した分はなかったことにする
		struct tagClamp
		{
			template<class T>
			static void Copy(const T& min, const T& max, T& left, const T& right)
			{
				if (max < right)
					left = max;
				else if (min > right)
					left = min;
				else
					left = right;
			}
			template<class T>
			static void Addition(const T& min, const T& max, T& left, const T& right)
			{
				if (max - left > right)
					left += right;
				else
					left = max;
			}
			template<class T>
			static void Addition1(const T& min, const T& max, T& left)
			{
				if (left == max)
					left = max;
				else
					left++;
			}
			template<class T>
			static void Subtract(const T& min, const T& max, T& left, const T& right)
			{
				if (left - min > right)
					left -= right;
				else
					left = min;
			}
			template<class T>
			static void Subtract1(const T& min, const T& max, T& left)
			{
				if (left == min)
					left = min;
				else
					left--;
			}
		};

		//!	CRangeBindedValueの拘束方法を表すクラス、はみ出した分は循環させる
		struct tagLoop
		{
			template<class T>
			static void Copy(const T& min, const T& max, T& left, const T& right)
			{
				if (max < right)
					left = right % max;
				else if (min > right)
					left = max - ((min - right) % (max - min));
				else
					left = right;
			}
			template<class T>
			static void Addition(const T& min, const T& max, T& left, const T& right)
			{
				if (max - left > right)
					left += right;
				else
				{
					left = min + ((right - (max - left)) % (max - min));
				}
			}
			template<class T>
			static void Addition1(const T& min, const T& max, T& left)
			{
				if (left == max)
					left = min;
				else
					left++;
			}
			template<class T>
			static void Subtract(const T& min, const T& max, T& left, const T& right)
			{
				if (left - min > right)
					left -= right;
				else
				{
					left = max - ((right - (left - min)) % (max - min));
				}
			}
			template<class T>
			static void Subtract1(const T& min, const T& max, T& left)
			{
				if (left == min)
					left = max;
				else
					left--;
			}
		};

		//! @}

		//!	範囲で拘束された値を表すクラス
		template<class T, class ValueRange, class BindType = tagClamp>
		class CRangeBindedValue : public ValueRange
		{
		private:
			T m_value;

		public:
			using myType = CRangeBindedValue < T, ValueRange, BindType > ;

			//!	コンストラクタ
			//!	@{
#pragma region Constructor

			CRangeBindedValue()
				: m_value()
			{};

			CRangeBindedValue(const T& value)
				: m_value(value)
			{};

			CRangeBindedValue(const CRangeBindedValue& src)
				: ValueRange(src)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_value)
			{};

			CRangeBindedValue(CRangeBindedValue&& src)
				: ValueRange(src)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_value)
			{};

#pragma endregion
			//!	@}

#pragma region Operator

			operator T()
			{
				return m_value;
			}

			myType& operator =(const T& right)
			{
				BindType::Copy<T>(GetMin(), GetMax(), m_value, right);
				return *this;
			};
			friend myType&
				operator +=(myType& left, const T& right)
			{
				BindType::Addition<T>(left.GetMin(), left.GetMax(), left.m_value, right);
				return left;
			};
			friend myType&
				operator ++(myType& left)
			{
				BindType::Addition1<T>(left.GetMin(), left.GetMax(), left.m_value);
				return left;
			};
			friend myType&
				operator -=(myType& left, const T& right)
			{
				BindType::Subtract<T>(left.GetMin(), left.GetMax(), left.m_value, right);
				return left;
			};
			friend myType&
				operator --(myType& left)
			{
				BindType::Subtract1<T>(left.GetMin(), left.GetMax(), left.m_value);
				return left;
			};
#pragma endregion

			//!	値を最大値にする
			void ToMax()
			{
				m_value = GetMax();
			}

			//!	値を最小値にする
			void ToMin()
			{
				m_value = GetMin();
			}

			//!	値が最大値であるかを取得する
			bool IsMax()
			{
				return m_value == GetMax();
			}

			//!	値が最小値であるかを取得する
			bool IsMin()
			{
				return m_value == GetMin();
			}

			//!	値と最小値の差と最大値の比率を取得する
			float GetRatioBetweenMinMax()
			{
				return 
					static_cast<float>(m_value - GetMin()) / 
					static_cast<float>(GetLengthBetweenMinMax());
			}
		};

	}
}

#include "Utility\UndefineClassCreateHelpers.h"
#endif	//	MMLIBRARY_CRANGE_BINDED_VALUE_H_