/*!	\file		CPlane.h
*	\details	CPlaneクラスの宣言
*	\author		松裏征志
*	\date		2014/06/02
*/
#ifndef MMLIBRARY_CPLANE_H_
#define MMLIBRARY_CPLANE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "CVector.h"

namespace MMLibrary
{
	namespace Math
	{
		template<typename T, const uint_t N>
		class CVector;

		/*!	\class	CPlane
		*	\brief	平面を表現するクラス
		*
		*	CPlaneクラスのメンバは、一般平面方程式の形式をとる。\n
		*	これらは、一般平面方程式に a x + b y + c z + d w = 0 となるように適用される。
		*/
		class CPlane
		{
		private:
			/*!	
			*	\brief	一般平面方程式におけるクリップ面の a 係数。
			*/
			float a;
			/*!	
			*	\brief	一般平面方程式におけるクリップ面の b 係数。
			*/
			float b;
			/*!	
			*	\brief	一般平面方程式におけるクリップ面の c 係数。
			*/
			float c;
			/*!	
			*	\brief	一般平面方程式におけるクリップ面の d 係数。
			*/
			float d;

		public:
			/*!	
			*	\brief	コンストラクタ
			*
			*	全て0で初期化する
			*/
			CPlane(void);

			/*!	
			*	\brief	引数付きコンストラクタ
			*
			*	float型配列で初期化する
			*/
			CPlane(const float* pArray);

			/*!	
			*	\brief	引数付きコンストラクタ
			*
			*	対応する引数で初期化する
			*/
			CPlane(	const float a,
					const float b,
					const float c,
					const float d);

			/*!	
			*	\brief	自身のコピーを返す。
			*
			*	\return	自身のコピー
			*/
inline		CPlane ToClone(void) const;

			/*!
			*	\brief	float型配列の先頭アドレスへキャストする
			*/
inline		operator float*();

			/*!
			*	\brief	float型配列の先頭アドレスへキャストする(読み取り専用)
			*/
inline		operator const float*() const;

			/*!
			*	\brief	CVector<float, 4>型へキャストする
			*/
inline		operator CVector<float, 4>();

			/*!
			*	\brief	CVector<float, 4>型へキャストする(読み取り専用)
			*/
inline		operator const CVector<float, 4>() const;

			/*!
			*	\brief	加算演算子
			*/
			CPlane operator+(CPlane&) const;

			/*!
			*	\brief	減算演算子
			*/
			CPlane operator-(CPlane&) const;

			/*!
			*	\brief	加算代入演算子
			*/
			CPlane& operator+=(CPlane&);

			/*!
			*	\brief	減算代入演算子
			*/
			CPlane& operator-=(CPlane&);
			
			/*!
			*	\brief	等価演算子
			*/		
			bool operator == ( const CPlane& ) const;
			
			/*!
			*	\brief	非等価演算子
			*/		
			bool operator != ( const CPlane& ) const;

			/*!
			*	\brief	平面と 4D ベクトルの内積を計算する。
			*
			*	平面 (a, b, c, d) と 4D ベクトル (x, y, z, w) を指定した場合、\n
			*	この関数の戻り値は a*x + b*y + c*z + d*w になる。 \n
			*	関数 Dot は、平面と同次座標の関係を判断するときに役に立つ。\n
			*	たとえば、この関数を使うと、特定の座標が特定の平面上にあるかどうか、\n
			*	または特定の座標が特定の平面のどちら側にあるかを判断できる。
			*/
			float Dot(const CVector<float, 4>& vector) const;

			/*!
			*	\brief	平面と 3D ベクトルの内積を計算する。ベクトルの w パラメータは、0 であると見なされる。
			*
			*	平面 (a, b, c, d) と 3D ベクトル (x, y, z) を指定した場合、\n
			*	この関数の戻り値は a*x + b*y + c*z + d*1 になる。 \n
			*	関数 DotCoord は、平面と 3D 空間の座標の関係を判断するときに役に立つ。
			*/
			float DotCoord(const CVector<float, 3>& vector) const;

			/*!
			*	\brief	平面と 3D ベクトルの内積を計算する。ベクトルの w パラメータは、1 であると見なされる。
			*
			*	平面 (a, b, c, d) と 3D ベクトル (x, y, z) を指定した場合、\n
			*	この関数の戻り値は a*x + b*y + c*z + d*0 になる。 \n
			*	関数 DotCoord は、平面の法線と別の法線の角度を計算するときに役立つ。
			*/
			float DotNormal(const CVector<float, 3>& vector) const;
			
			/*!
			*	\brief	平面の法線への参照
			*/
			CVector<float, 3>& Normal(void);
			
			/*!
			*	\brief	平面の法線への参照(読み取り専用)
			*/
			const CVector<float, 3>& Normal(void) const;

			/*!
			*	\brief	平面上の点を取得する
			*/
			CVector<float, 3> GetPoint(void) const;

			/*!
			*	\brief	平面と直線の交点を調べる。
			*
			*	直線が平面に対して平行の場合、(0,0,0) が返される。
			*/
			CVector<float, 3> GetInterSectWithLine(
				const CVector<float, 3>& begin,
				const CVector<float, 3>& end) const;

			/*!
			*	\brief	平面の法線の長さを正規化するため、平面の係数を正規化する。
			*
			*	この関数は、|a,b,c| == 1 となるように平面を正規化する。
			*/
			CPlane& Normalize(void);

			/*!
			*	\brief	行列を使って平面をトランスフォームする。
			*/
			CPlane& Transform(CMatrix<float, 4, 4>& matrix);

			/*!
			*	\brief	点と法線から平面を作成する。
			*/
static		CPlane CreateByPointAndNormal(
				const CVector<float, 3>& point,
				const CVector<float, 3>& normal);

			/*!
			*	\brief	3つの点から平面を作成する。
			*/
static		CPlane CreateByPoint(
				const CVector<float, 3>& point1,
				const CVector<float, 3>& point2,
				const CVector<float, 3>& point3);

			/*!
			*	\brief	点と法線から平面を設定する。
			*/
			CPlane& SetByPointAndNormal(
				const CVector<float, 3>& point,
				const CVector<float, 3>& normal);

			/*!
			*	\brief	3つの点から平面を作成する。
			*/
			CPlane& SetByPoint(
				const CVector<float, 3>& point1,
				const CVector<float, 3>& point2,
				const CVector<float, 3>& point3);
		};
	};
};

#endif