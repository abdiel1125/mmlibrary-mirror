/*!	\file		Function.h
*	\details	MMLibraryMathの全ての定数・構造体を宣言する
*	\author		松裏征志
*	\date		2014/06/20
*/
#ifndef MMLIBRARY_MATH_FUNCTION_H_
#define MMLIBRARY_MATH_FUNCTION_H_

//***********************************************************
//インクルード
//***********************************************************
#include <stdlib.h>
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Math
	{
		/*!
		*	\brief 円周率
		*/
		template<typename T>
		const T PI(void)
		{
			return (T)3.14159265358979323846264338327950288;
		};
		
		/*!
		*	\brief 静的な階乗演算を行うメタ関数
		*/
		template<int N>
		struct StaticFactorial
		{
			/*!
			*	\brief	演算結果
			*/
			static const int value = 
				N * Factorial<N - 1>::value;
		};

		//!	静的な組み合わせ演算を行うメタ関数の特殊化
		template<>
		struct StaticFactorial<0>
		{
			/*!
			*	\brief	演算結果
			*/
			static const int value = 1;
		};
		
		//!	静的な組み合わせ演算を行うメタ関数
		template<int N, int M>
		struct StaticCombination
		{
			//!	演算結果
			static const int value = 
				Factorial<N>::value / 
				(	Factorial<M>::value * 
					Factorial<N - M>::value);
		};

		/*!
		*	\brief 度数法の角度をラジアン角度に変換する
		*/
		float DegreeToRadian(float degree);

		/*!
		*	\brief ラジアン角度を度数法の角度に変換する
		*/
		float RadianToDegree(float radian);

		/*!
		*	\brief	各bit単位での立っているビット数を数える関数(uchar_t)
		*
		*	\param	[in]	value	立っているビット数を数えたい値
		*
		*	\return 演算結果を返す
		*/
		int	CountBit(uchar_t		value);

		/*!
		*	\brief	各bit単位での立っているビット数を数える関数(ushort_t)
		*
		*	\param	[in]	value	立っているビット数を数えたい値
		*
		*	\return 演算結果を返す
		*/
		int	CountBit(ushort_t		value);

		/*!
		*	\brief	各bit単位での立っているビット数を数える関数(uint_t)
		*
		*	\param	[in]	value	立っているビット数を数えたい値
		*
		*	\return 演算結果を返す
		*/
		int	CountBit(uint_t		value);

		/*!
		*	\brief	最大有効ビット数（MSB：Most Significant Bit）を求める関数(uchar_t)
		*
		*	\param	[in]	value	最大有効ビット数を求めたい値 
		*
		*	\return 演算結果を返す 0を入力した場合は−1を返す
		*/
		int	GetMSB(uchar_t		value);

		/*!
		*	\brief	最大有効ビット数（MSB：Most Significant Bit）を求める関数(ushort_t)
		*
		*	\param	[in]	value	最大有効ビット数を求めたい値
		*
		*	\return 演算結果を返す 0を入力した場合は−1を返す
		*/
		int	GetMSB(ushort_t		value);

		/*!
		*	\brief	最大有効ビット数（MSB：Most Significant Bit）を求める関数(uint_t)
		*
		*	\param	[in]	value	最大有効ビット数を求めたい値
		*
		*	\return 演算結果を返す 0を入力した場合は−1を返す
		*/
		int	GetMSB(uint_t			value);

		/*!
		*	\brief	最小有効ビット数（LSB：Least Significant Bit）を求める関数(uchar_t)
		*
		*	\param	[in]	value	最小有効ビット数を求めたい値 
		*
		*	\return 演算結果を返す 0を入力した場合は−1を返す
		*/
		int	GetLSB(uchar_t		value);

		/*!
		*	\brief	最小有効ビット数（LSB：Least Significant Bit）を求める関数(ushort_t)
		*
		*	\param	[in]	value	最小有効ビット数を求めたい値 
		*
		*	\return 演算結果を返す 0を入力した場合は−1を返す
		*/
		int	GetLSB(ushort_t		value);

		/*!
		*	\brief	最小有効ビット数（LSB：Least Significant Bit）を求める関数(uint_t)
		*
		*	\param	[in]	value	最小有効ビット数を求めたい値 
		*
		*	\return 演算結果を返す 0を入力した場合は−1を返す
		*/
		int	GetLSB(uint_t			value);

		/*!
		*	\brief	整数valueを含む最小のべき乗数を求める関数(uchar_t)
		*
		*	\param	[in]	value	最小のべき乗数を求めたい値
		*
		*	\return 演算結果を返す
		*/
		int GetNextPow2(uchar_t	value);

		/*!
		*	\brief	整数valueを含む最小のべき乗数を求める関数(ushort_t)
		*
		*	\param	[in]	value	最小のべき乗数を求めたい値
		*
		*	\return 演算結果を返す
		*/
		int GetNextPow2(ushort_t	value);

		/*!
		*	\brief	整数valueを含む最小のべき乗数を求める関数(uint_t)
		*
		*	\param	[in]	value	最小のべき乗数を求めたい値
		*
		*	\return 演算結果を返す
		*/
		int GetNextPow2(uint_t	value);

		/*!
		*	\brief	0から1の間の乱数を返す
		*/
		float GetFloatRandam(void);

		//!	大きい方の値をvalueとして持つメタ関数
		template<class T, T Left, T Right>
		struct Max
		{
			static T value = std::max(Left, Right);
		};

		//!	小さい方の値をvalueとして持つメタ関数
		template<class T, T Left, T Right>
		struct Min
		{
			static T value = std::min(Left, Right);
		};
	};
};
#endif