/*!	\file		CVector.hpp
*	\details	CVectorクラスの定義
*	\author		松裏征志
*	\date		2014/04/16
*/
#ifndef MMLIBRARY_CVECTOR_HPP_
#define MMLIBRARY_CVECTOR_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Math\CVector.h"
#include "Math\CMatrix.h"

namespace MMLibrary
{
	namespace Math
	{
		template<typename T, const uint_t N>
		CVector<T, N>::CVector(void)
		{
			//値の代入
			fill(0);
		};

		template<typename T, const uint_t N>
		CVector<T, N>::CVector(T src)
		{
			fill(src);
		};

		template<typename T, const uint_t N>
		CVector<T, N>::CVector(std::initializer_list<T> src)
		{
			Set(src);
		}

		template<typename T, const uint_t N>
		CMatrix<T, 1, N> CVector<T, N>::ToRawMatrix() const
		{
			CMatrix<T, 1, N> rtn;

			for (uint_t i = 0; i < N; ++i)
				rtn[0][i] = at(i);

			return rtn;
		}

		template<typename T, const uint_t N>
		CMatrix<T, N, 1> CVector<T, N>::ToColumnMatrix() const
		{
			CMatrix<T, N, 1> rtn;

			for (uint_t i = 0; i < N; ++i)
				rtn.at(i).at(0) = at(i);

			return rtn;
		}

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::Cross(const CVector<T, N>& src)
		{
			CVector<T, N> work;

			//3次元
			if (N == 3)
			{
				work.at(0) =
					at(1) * src.at(2) -
					at(2) * src.at(1);
				work.at(1) =
					at(2) * src.at(0) -
					at(0) * src.at(2);
				work.at(2) =
					at(0) * src.at(1) -
					at(1) * src.at(0);
			}
			//7次元
			else if (N == 7)
			{
				work.at(0) =
					at(1) * src.at(2) -
					at(2) * src.at(1) -
					at(3) * src.at(4) +
					at(4) * src.at(3) -
					at(5) * src.at(6) +
					at(6) * src.at(5);
				work.at(1) = -
					at(0) * src.at(2) +
					at(2) * src.at(0) -
					at(3) * src.at(5) +
					at(4) * src.at(6) +
					at(5) * src.at(3) -
					at(6) * src.at(4);
				work.at(2) =
					at(0) * src.at(1) -
					at(1) * src.at(0) -
					at(3) * src.at(6) -
					at(4) * src.at(5) +
					at(5) * src.at(4) +
					at(6) * src.at(3);
				work.at(3) =
					at(0) * src.at(4) +
					at(1) * src.at(5) +
					at(2) * src.at(6) -
					at(4) * src.at(0) -
					at(5) * src.at(1) -
					at(6) * src.at(2);
				work.at(4) = -
					at(0) * src.at(3) -
					at(1) * src.at(6) +
					at(2) * src.at(5) +
					at(3) * src.at(0) -
					at(5) * src.at(2) +
					at(6) * src.at(1);
				work.at(5) =
					at(0) * src.at(6) -
					at(1) * src.at(3) -
					at(2) * src.at(4) +
					at(3) * src.at(1) +
					at(4) * src.at(2) -
					at(6) * src.at(0);
				work.at(6) = -
					at(0) * src.at(5) +
					at(1) * src.at(4) -
					at(2) * src.at(3) +
					at(3) * src.at(2) -
					at(4) * src.at(1) +
					at(5) * src.at(0);
			}
			else
				*this *= 0;

			*this = work;

			return *this;
		};

		template<typename T, const uint_t N>
		T CVector<T, N>::Dot(const CVector<T, N>& src) const
		{
			T rtn = 0;

			for (int i = 0; i < N; ++i)
				rtn += at(i) * src.at(i);

			return rtn;
		};

		template<typename T, const uint_t N>
		T CVector<T, N>::Length(void) const
		{
			double rtn = 0;

			for (int i = 0; i < N; ++i)
			{
				rtn += (double)at(i) * (double)at(i);
			}

			rtn = sqrt(rtn);

			return	((std::numeric_limits<T>::max)() > rtn) ?
				(T)rtn :
				(std::numeric_limits<T>::max)();
		};

		template<typename T, const uint_t N>
		T CVector<T, N>::LengthSq(void) const
		{
			double rtn = 0;

			for (int i = 0; i < N; ++i)
			{
				rtn += (double)at(i) * (double)at(i);
			}

			return	((std::numeric_limits<T>::max)() > rtn) ?
				(T)rtn :
				(std::numeric_limits<T>::max)();
		};

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::Normalize(void)
		{
			T length = Length();

			if (length == 0)
				return *this;

			for (uint_t i = 0; i < N; ++i)
				at(i) /= length;

			return *this;
		};

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::Scale(const T s)
		{
			*this *= s;

			return *this;
		}

		template<typename T, const uint_t N>
		CVector<T, N>&	CVector<T, N>::SetByBaryCentric(
			const CVector<T, N>& V1,
			const CVector<T, N>& V2,
			const CVector<T, N>& V3,
			const T				 f,
			const T				 g)
		{
			*this = V1 + (V2 - V1) * f + (V3 - V1) * g;

			return *this;
		};

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::SetByCatmulRom(
			const CVector<T, N>& V1,
			const CVector<T, N>& V2,
			const CVector<T, N>& V3,
			const CVector<T, N>& V4,
			const T				 s)
		{
			T	s2 = s * s, s3 = s * s * s;
			*this =
				V1 * (-1 * s3 + 2 * s2 - s) +
				V2 * (3 * s3 - 5 * s2 + 2) +
				V3 * (-3 * s3 + 4 * s2 + s) +
				V4 * (1 * s3 - 1 * s2);

			*this /= 2;

			return *this;
		};

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::SetByHermite(
			const CVector<T, N>& V1,
			const CVector<T, N>& T1,
			const CVector<T, N>& V2,
			const CVector<T, N>& T2,
			const T				 s)
		{
			T		s2 = s * s,
				s3 = s * s * s;
			*this = V1 * (s3 * 2 - s2 * 3 + 1) +
				V2 * (-s3 * 2 + s2 * 3) +
				T1 * (s3 * 1 - s2 * 2 + s) +
				T2 * (s3 * 1 - s2 * 1);

			return *this;
		};

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::SetByLerp(
			const CVector<T, N>& V1,
			const CVector<T, N>& V2,
			const T				 s)
		{
			*this = V1 + (V2 - V1) * s;

			return *this;
		}

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::SetByMaximize(
			const CVector<T, N>& V1,
			const CVector<T, N>& V2)
		{
			for (uint_t i = 0; i < N; ++i)
				at(i) =
				(V1.at(i) > V2.at(i)) ?
				V1.at(i) : V2.at(i);

			return *this;
		};

		template<typename T, const uint_t N>
		CVector<T, N>& CVector<T, N>::SetByMinimize(
			const CVector<T, N>& V1,
			const CVector<T, N>& V2)
		{
			for (uint_t i = 0; i < N; ++i)
				at(i) =
				(V1.at(i) < V2.at(i)) ?
				V1.at(i) : V2.at(i);

			return *this;
		};
	};

	template<typename T, const uint_t N>
	Math::CVector<T, N>	CFactory<Math::CVector<T, N>>::CreateByBaryCentric(
		const Math::CVector<T, N>& V1,
		const Math::CVector<T, N>& V2,
		const Math::CVector<T, N>& V3,
		const T				 f,
		const T				 g)
	{
		return Math::CVector<T, N>().
			SetByBaryCentric(
			V1,
			V2,
			V3,
			f,
			g);
	};

	template<typename T, const uint_t N>
	Math::CVector<T, N> CFactory<Math::CVector<T, N>>::CreateByCatmulRom(
		const Math::CVector<T, N>& V1,
		const Math::CVector<T, N>& V2,
		const Math::CVector<T, N>& V3,
		const Math::CVector<T, N>& V4,
		const T				 s)
	{
		return Math::CVector<T, N>().
			SetByCatmulRom(
			V1,
			V2,
			V3,
			V4,
			s);
	};

	template<typename T, const uint_t N>
	Math::CVector<T, N> CFactory<Math::CVector<T, N>>::CreateByHermite(
		const Math::CVector<T, N>& V1,
		const Math::CVector<T, N>& T1,
		const Math::CVector<T, N>& V2,
		const Math::CVector<T, N>& T2,
		const T				 s)
	{
		return Math::CVector<T, N>().
			SetByHermite(
			V1,
			T1,
			V2,
			T2,
			s);
	};

	template<typename T, const uint_t N>
	Math::CVector<T, N> CFactory<Math::CVector<T, N>>::CreateByLerp(
		const Math::CVector<T, N>& V1,
		const Math::CVector<T, N>& V2,
		const T				 s)
	{
		return Math::CVector<T, N>().
			SetByLerp(
			V1,
			V2,
			s);
	}

	template<typename T, const uint_t N>
	Math::CVector<T, N> CFactory<Math::CVector<T, N>>::CreateByMaximize(
		const Math::CVector<T, N>& V1,
		const Math::CVector<T, N>& V2)
	{
		return Math::CVector<T, N>().
			SetByMaximize(
			V1,
			V2);
	};

	template<typename T, const uint_t N>
	Math::CVector<T, N> CFactory<Math::CVector<T, N>>::CreateByMinimize(
		const Math::CVector<T, N>& V1,
		const Math::CVector<T, N>& V2)
	{
		return Math::CVector<T, N>().
			SetByMinimize(
			V1,
			V2);
	};

};
#endif