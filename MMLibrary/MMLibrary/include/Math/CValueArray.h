/*!	\file	CValueArray.h
\details	CValueArrayクラスを宣言する
\author		松裏征志
\date		2014/09/23
*/
#ifndef MMLIBRARY_CVALUE_ARRAY_H_
#define MMLIBRARY_CVALUE_ARRAY_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility\COperators.h"
#include <initializer_list>

namespace MMLibrary
{
	namespace Math
	{
		/*!	\class CValueArray
		*	\brief std::arrayに演算子のオーバーロードやメソッドを追加するクラス
		*
		*	\tparam	T	要素の型
		*	\tparam	N	要素の数
		*/
		template<typename T, uint_t N>
		class CValueArray : 
			public 
			Utility::tagCalculateOperators<CValueArray<T, N>,
			Utility::tagCalculateOperators2nd<CValueArray<T, N>, T,
			Utility::tagInjectCalculateMethods<CValueArray<T, N>,
			Utility::tagInjectCalculateMethods2nd<CValueArray<T, N>, T,
			array_t<T, N> > > > >
		{
		public:
			//!	自クラスを表すエイリアス
			using myType = CValueArray < T, N >;

			//! コンストラクタ
			CValueArray()
			{};

			//! コピーコンストラクタ
			CValueArray(const myType& right)
			{
				for (uint_t i = 0; i < this->size() && i < right.size(); ++i)
					(*this)[i] = right[i];
			};

#pragma region Operators
			friend myType operator-(const myType& src)
			{
				myType rtn(src);

				for (auto& element : rtn)
					element *= -1;

				return rtn;
			};

			//!	加算代入演算子
			friend myType& operator+=(myType& left, const myType& right)
			{
				for (uint_t i = 0; i < left.size(); ++i)
					left[i] += right[i];

				return left;
			};

			//! 減算代入演算子
			friend myType& operator-=(myType& left, const myType& right)
			{
				for (uint_t i = 0; i < left.size(); ++i)
					left[i] -= right[i];

				return left;
			};
			
			//!	乗算代入演算子
			friend myType& operator*=(myType& left, const myType& right)
			{
				for (uint_t i = 0; i < left.size(); ++i)
					left[i] *= right[i];

				return left;
			};

			//!	除算代入演算子
			friend myType& operator/=(myType& left, const myType& right)
			{
				for (uint_t i = 0; i < left.size(); ++i)
					left[i] /= right[i];

				return left;
			};

			//!	加算代入演算子
			friend myType& operator+=(myType& left, const T& right)
			{
				for (auto& element : left)
					element += right;

				return left;
			};
			
			//!	減算代入演算子
			friend myType& operator-=(myType& left, const T& right)
			{
				for (auto& element : left)
					element -= right;

				return left;
			};

			//!	乗算代入演算子
			friend myType& operator*=(myType& left, const T& right)
			{
				for (auto& element : left)
					element *= right;

				return left;
			};

			//!	除算代入演算子
			friend myType& operator/=(myType& left, const T& right)
			{
				for (auto& element : left)
					element /= right;

				return left;
			};

			//!	等価演算子
			friend bool operator==(const myType& left, const myType& right)
			{
				for (uint_t i = 0; i < left.size(); ++i)
					if (left[i] != right[i]) return false;

				return true;
			};

#pragma endregion
			myType& Negate(void)
			{
				*this = -*this;
				return *this;
			};
			myType& Set(const myType& src)
			{
				*this = src;
				return *this;
			};
			myType& Set(const std::initializer_list<T>& initList)
			{
				auto it = initList.begin();
				for (uint_t i = 0; i < size() && it != initList.end(); ++i)
				{
					at(i) = *it;
					++it;
				}

				return *this;
			};
			myType& Set(const array_t<T, N>& src)
			{
				for (uint_t i = 0; i < size(); ++i)
					at(i) = src.at(i);
				return *this;
			};

			myType	ToClone(void) const
			{
				return *this;
			};

			//!	非数が含まれているかを取得する
			bool	IsNan() const
			{
				return std::any_of(
					cbegin(),
					cend(),
					[](const T& element)
				{
					return std::isnan(element);
				});
			}
		};

#ifndef MMLIBRARY_CVALUE_ARRAY_INSTANTIATED
		extern template class CValueArray < float, 3 >;
		extern template class CValueArray < float, 4 >;
		extern template class CValueArray < CValueArray < float, 4 >, 4 >;
#endif
	};
};

namespace std
{
	template<typename T, MMLibrary::uint_t N>
	bool isnan(const MMLibrary::Math::CValueArray<T, N>& valueArray)
	{
		return valueArray.IsNan();
	}
}

#endif