/*!	\file		Interpolation.h
*	\details	Interpolation関連の宣言・インクルード
*	\author		松裏征志
*	\date		2014/06/04
*/
#ifndef MMLIBRARY_INTERPOLATION_H_
#define MMLIBRARY_INTERPOLATION_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"

//***********************************************************
//共通の構造体等の定義
//***********************************************************
namespace MMLibrary
{
	namespace Math
	{
		/*!	\namespace	MMLibrary::Math::Interpolation
		*	\brief	MMLibraryの補間に関する要素はこの名前空間に属する
		*/
		namespace Interpolation
		{
			/*!	
			*	\brief	線形補間
			*
			*	\tparam	T	補間する型\n
			*				T型同士の加算演算子とfloat型との乗算演算子が定義されていなければならない
			*/
			template<typename T>
			T Linear(const T& begin, const T& end, float s)
			{
				return T(begin * (1 - s) + end * s);
			};
			
			/*!	
			*	\brief	始まりがゆっくりになる補間
			*
			*	\tparam	T	補間する型\n
			*				T型同士の加算演算子とfloat型との乗算演算子が定義されていなければならない
			*/
			template<typename T>
			T EaseIn(const T& begin, const T& end, float s)
			{
				return Linear(begin, end, s * s);
			};
			
			/*!	
			*	\brief	終わりがゆっくりになる補間
			*
			*	\tparam	T	補間する型\n
			*				T型同士の加算演算子とfloat型との乗算演算子が定義されていなければならない
			*/
			template<typename T>
			T EaseOut(const T& begin, const T& end, float s)
			{
				return Linear(begin, end, s * (2 - s));
			};
			
			/*!	
			*	\brief	始まりと終わりがゆっくりになる補間
			*
			*	\tparam	T	補間する型\n
			*				T型同士の加算演算子とfloat型との乗算演算子が定義されていなければならない
			*/
			template<typename T>
			T EaseInEaseOut(const T& begin, const T& end, float s)
			{
				return Linear(begin, end, s * s * (3 - 2 * s));
			};
			
			/*!
			*	\brief カトマル・ロム曲線の計算を行う
			*/
			template<typename T>
			T CatmulRom(
				const T& V1,
				const T& V2,
				const T& V3,
				const T& V4,
				float s)
			{
				float	s2 = s * s, 
						s3 = s2 * s;

				return (V1 * (-1 * s3 + 2 * s2 - s) +
						V2 * ( 3 * s3 - 5 * s2 + 2) + 
						V3 * (-3 * s3 + 4 * s2 + s) + 
						V4 * ( 1 * s3 - 1 * s2    )) * 0.5f;
			};
			
			/*!
			*	\brief バーンスタイン基底関数を計算する
			*/
			template<int J, int N>
			float BernsteinBasisPolynomials(float t)
			{
				static const int value = 
					StaticCombination<N, J>::value *
					pow((1 - t), N - J) *
					pow(t, J);
			};
			
			/*!
			*	\brief ベジェ曲線の計算を行う
			*/
			template<typename T>
			T Bezier(
				const T& V1,
				const T& V2,
				const T& V3,
				const T& V4,
				float s)
			{
				return  V1 * BernsteinBasisPolynomials<0, 3>(t) +
						V2 * BernsteinBasisPolynomials<1, 3>(t) + 
						V3 * BernsteinBasisPolynomials<2, 3>(t) + 
						V4 * BernsteinBasisPolynomials<3, 3>(t);
			};
		};
	};
};

#endif