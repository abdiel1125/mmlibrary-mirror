/*!	\file		CValueRange.h
*	\details	CValueRangeクラスの宣言を行う
*	\author		松裏征志
*	\date		2014/12/05
*/
#ifndef MMLIBRARY_CVALUE_RANGE_H_
#define MMLIBRARY_CVALUE_RANGE_H_

//***********************************************************
//インクルード
//***********************************************************
#include <limits>
#include "MMLibraryDefine.h"
#include "Utility\DefineClassCreateHelpers.h"
#include "Math\Function.h"

namespace MMLibrary
{
	namespace Math
	{
		//!	静的なCValueRange
		template<class T, T Minimum, T Maximum>
		class CStaticValueRange
		{
			static_assert(Minimum < Maximum, "");
		public:
			CREATE_GETTER_PRIMITIVE(Min, Minimum);
			CREATE_GETTER_PRIMITIVE(Max, Maximum);
			CREATE_GETTER_PRIMITIVE(LengthBetweenMinMax, Maximum - Minimum);
		};

		//!	動的なCValueRange
		template<class T>
		class CDynamicValueRange
		{
		private:
			T m_min;
			T m_max;
		public:
			//!	コンストラクタ
			//!	@{
#pragma region Constructor

			CDynamicValueRange()
				: m_min(std::numeric_limits<T>::min())
				, m_max(std::numeric_limits<T>::max())
			{};

			CDynamicValueRange(const T& min, const T& max)
				: m_min(min)
				, m_max(max)
			{};

			CDynamicValueRange(const CDynamicValueRange<T>& src)
				: CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_min)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_max)
			{};

			CDynamicValueRange(CDynamicValueRange<T>&& src)
				: CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_min)
				, CALL_COPY_OR_MOVE_CONSTRUCTOR(src, m_max)
			{
				src.m_min = std::numeric_limits<T>::min();
				src.m_max = std::numeric_limits<T>::max();
			};
#pragma endregion
			//!@}


			CREATE_GETTER(Min, m_min);
			CREATE_GETTER(Max, m_max);
			CREATE_TYPE_SPECIFIED_GETTER_PRIMITIVE(LengthBetweenMinMax, (m_max - m_min), T);

			CREATE_SETTER(Min, m_min);
			CREATE_SETTER(Max, m_max);
		};
	}
}

#include "Utility\UndefineClassCreateHelpers.h"
#endif	//	MMLIBRARY_CVALUE_RANGE_H_