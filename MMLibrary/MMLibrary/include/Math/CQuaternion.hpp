/*!	\file		CQuaternion.hpp
*	\details	CQuaternionクラスの定義
*	\author		松裏征志
*	\date		2014/04/26
*/
#ifndef MMLIBRARY_CQUATERNION_HPP_
#define MMLIBRARY_CQUATERNION_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Math\CQuaternion.h"
#include "Graphic/CVector3.h"
#include "Graphic/CMatrix4x4.h"

namespace MMLibrary
{	
	namespace Math
	{
		template<typename T>
		CQuaternion<T>::CQuaternion(void)
			:scalar(1)
			,vector(0.0f)
		{
		};
		
		template<typename T>
		CQuaternion<T>::CQuaternion(
			T scalar,
			T vectorX,
			T vectorY,
			T vectorZ)
		:scalar(scalar)
		{
			vector.at(0) = vectorX;
			vector.at(1) = vectorY;
			vector.at(2) = vectorZ;
		};
		
		template<typename T>
		CQuaternion<T> CQuaternion<T>::ToClone(void) const
		{
			return *this;
		};
		
		template<typename T>
		T&	CQuaternion<T>::Scalar(void)
		{
			return scalar;
		};
		
		template<typename T>
		const T&	CQuaternion<T>::Scalar(void) const
		{
			return scalar;
		};
		
		template<typename T>
		CVector<T, 3>&	CQuaternion<T>::Vector(void)
		{
			return vector;
		};
		
		template<typename T>
		const CVector<T, 3>&	CQuaternion<T>::Vector(void) const
		{
			return vector;
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::operator+(const CQuaternion<T>& src) const
		{
			CQuaternion<T> work;
			
			work.Scalar() = Scalar() + src.Scalar();
			work.Vector() = Vector() + src.Vector();

			return work;
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::operator-(const CQuaternion<T>& src) const
		{
			CQuaternion<T> work;
			
			work.Scalar() = Scalar() - src.Scalar();
			work.Vector() = Vector() - src.Vector();

			return work;
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::operator*(const CQuaternion<T>& src)const
		{
			CQuaternion<T> work;
			
			work.Scalar() = 
				Scalar() * src.Scalar() - 
				Vector().ToClone().Dot(src.Vector());
			work.Vector() = 
				src.Vector() * Scalar() + 
				Vector() * src.Scalar() + 
				Vector().ToClone().Cross(src.Vector());

			return work;
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::operator/(const CQuaternion<T>& src)const
		{
			CQuaternion<T> work = *this;
			
			work = work * src.CreateByConjugation();

			work /= src.GetNorm();

			return work;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::operator+=(const CQuaternion<T>& src)
		{
			*this = *this + src;

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::operator-=(const CQuaternion<T>& src)
		{
			*this = *this - src;

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::operator*=(const CQuaternion<T>& src)
		{
			*this = *this * src;

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::operator/=(const CQuaternion<T>& src)
		{
			*this = *this / src;

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::operator*(const T& src)const
		{
			CQuaternion<T> work;
			
			work.Scalar() = Scalar() * src;
			work.Vector() = Vector() * src;

			return work;
		};
		
		template<typename T>
		CQuaternion<T>	operator*(const T& srcF, const CQuaternion<T>& srcQ)
		{
			return srcQ * srcF;
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::operator/(const T& src)const
		{
			CQuaternion<T> work;
			
			work.Scalar() = Scalar() / src;
			work.Vector() = Vector() / src;

			return work;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::operator*=(const T& src)
		{
			*this = *this * src;

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::operator/=(const T& src)
		{
			*this = *this / src;

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::operator-(void) const
		{
			CQuaternion<T> work;
			
			work.Scalar() = -Scalar();
			work.Vector() = -Vector();

			return work;
		};
		
		template<typename T>
		bool	CQuaternion<T>::operator==(const CQuaternion<T>& src) const
		{
			return	(Scalar() == src.Scalar()) &&
					(Vector() == src.Vector());
		};
			
		template<typename T>
		bool	CQuaternion<T>::operator!=(const CQuaternion<T>& src) const
		{
			return !(*this == src);
		};

		template<typename T>
		T	CQuaternion<T>::Dot(const CQuaternion<T>& src) const
		{
			T	work;

			work	=	Scalar()		* src.Scalar()		 +
						Vector().at(0)	* src.Vector().at(0) +
						Vector().at(1)	* src.Vector().at(1) +
						Vector().at(2)	* src.Vector().at(2);

			return	work;
		};
		
		template<typename T>
		T	CQuaternion<T>::GetNorm(void) const
		{
			T rtn = T(0);

			rtn = sqrt(GetNormSq());

			return rtn;
		};
		
		template<typename T>
		T	CQuaternion<T>::GetNormSq(void) const
		{
			T rtn = T(0);
			
			rtn =	Scalar()			* Scalar()				+ 
					Vector().at(0) * Vector().at(0)	+ 
					Vector().at(1) * Vector().at(1)	+ 
					Vector().at(2) * Vector().at(2);

			return rtn;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::Multiply(const CQuaternion<T>& src)
		{
			*this	*= src;

			return *this;
		};
	
		template<typename T>
		bool CQuaternion<T>::IsIdentity(void) const
		{
			return (GetNorm() == 1);
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByConjugation(void)
		{
			return CQuaternion<T>(*this).SetByConjugation();
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByBaryCentric(
			const CQuaternion<T>& Q1,
			const CQuaternion<T>& Q2,
			const CQuaternion<T>& Q3,
			const float f,
			const float g)
		{
			return std::move(CQuaternion<T>().
				SetByBaryCentric(
				Q1, 
				Q2, 
				Q3, 
				f, 
				g));
		};
		
		template<typename T>
		CQuaternion<T> CQuaternion<T>::CreateByExp(void)
		{
			return std::move(ToClone().SetByExp());
		};

		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByIdentity(void)
		{
			return std::move(CQuaternion<T>().SetByIdentity());
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByInverse(void) const
		{
			return std::move(ToClone().SetByInverse());
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByLn(void) const
		{
			return std::move(ToClone().SetByLn());
		};
				
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByNormalize(void) const
		{
			return std::move(ToClone().SetByNormalize());
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateBySlerp(
			const CQuaternion<T>& Q1,
			const CQuaternion<T>& Q2,
			const float t)
		{
			return std::move(CQuaternion<T>().
				SetBySlerp(
					Q1,
					Q2,
					t));
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByRotationAxis(
			const	Graphic::CVector3& axis,
			const	T					radian)
		{
			return CQuaternion<T>().
				SetByRotationAxis(
					axis,
					radian);
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByRotationMatrix(
			const Graphic::CMatrix4x4&	rotation)
		{
			return CQuaternion<T>().
				SetByRotationMatrix(rotation);
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByRotationYawPitchRoll(
			const Graphic::CVector3&	rotation)
		{
			return CQuaternion<T>().
				SetByRotationYawPitchRoll(rotation);
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateByRotationYawPitchRoll(
			const T	x,
			const T	y,
			const T	z)
		{
			return CQuaternion<T>().
				SetByRotationYawPitchRoll(
					x,
					y,
					z);
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateBySquad(
			const CQuaternion<T>&	Q1,
			const CQuaternion<T>&	A,
			const CQuaternion<T>&	B,
			const CQuaternion<T>&	Q2,
			const T							t)
		{
			CQuaternion<T>().
				SetBySquad(
					Q1,
					A,
					B,
					Q2,
					t);
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateBySquadSetup(
			const CQuaternion<T>&	Q1,
			const CQuaternion<T>&	Q2,
			const CQuaternion<T>&	Q3)
		{
			return CQuaternion<T>().
				SetBySquadSetup(
					Q1,
					Q2,
					Q3);
		};
		
		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateBySquadEx(
			const CQuaternion<T>&	Q1,
			const CQuaternion<T>&	Q2,
			const CQuaternion<T>&	Q3,
			const CQuaternion<T>&	Q4,
			const T							t)
		{
			return	CQuaternion<T>().
				SetBySquadEx(
						Q1,
						Q2,
						Q3,
						Q4,
						t);
		};

		template<typename T>
		CQuaternion<T>	CQuaternion<T>::CreateBy2Vectors(
			const Graphic::CVector3& begin,
			const Graphic::CVector3& end)
		{
			return	CQuaternion<T>().
				SetBy2Vectors(
				begin,
				end);
		};

		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByConjugation(void)
		{
			Vector() = -Vector();
			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByBaryCentric(
			const CQuaternion<T>& Q1,
			const CQuaternion<T>& Q2,
			const CQuaternion<T>& Q3,
			const float f,
			const float g)
		{
			SetBySlerp(
					SetBySlerp(Q1, Q2, f + g),
					SetBySlerp(Q1, Q3, f + g),
					g / (f + g));

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByExp(void)
		{
			T				theta	= Vector().Length();
			CVector<T, 3>	v		= Vector().Normalize();

			Scalar() = cos(theta);
			Vector() = v * sin(theta);

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByIdentity(void)
		{
			Scalar() = T(1);
			Vector().fill(T(0));

			return *this;
		};
	
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByInverse(void)
		{
			*this = SetByConjugation() / GetNormSq();

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByLn(void)
		{
			SetByNormalize();

			T theta			= asin(Vector().Length());

			Scalar()	= T(0);
			Vector()	= Vector().Normalize() * theta;

			return *this;
		};

		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByNormalize(void)
		{
			*this /= GetNorm();

			return *this;
		};

		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetBySlerp(
			const CQuaternion<T>& Q1,
			const CQuaternion<T>& Q2,
			const float t)
		{
			T epsilon;

			if (Q1.Dot(Q2) < 0.0f)	epsilon = -1.0f;
			else					epsilon =  1.0f;

			Vector().at(0) = (1.0f - t) * Q1.Vector().at(0) + epsilon * t * Q2.Vector().at(0);
			Vector().at(1) = (1.0f - t) * Q1.Vector().at(1) + epsilon * t * Q2.Vector().at(1);
			Vector().at(2) = (1.0f - t) * Q1.Vector().at(2) + epsilon * t * Q2.Vector().at(2);
			Scalar()	   = (1.0f - t) * Q1.Scalar()	    + epsilon * t * Q2.Scalar();
			return *this;
		};
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByRotationAxis(
			const	Graphic::CVector3& axis,
			const	T					radian)
		{
			Vector() = axis;
			Vector().Normalize();

			if(Vector().LengthSq() != T(0))
			{
				T s = sin(radian * T(0.5));

				Vector() *= s;
				Scalar() = cos(radian * T(0.5));

			}
			else
				SetByIdentity();

			return *this;
		};

		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByRotationMatrix(
			const	Graphic::CMatrix4x4& rotation)
		{
			// 最大成分を検索
			T	elem[4];
			elem[0] =	rotation.at(0).at(0) -
						rotation.at(1).at(1) -
						rotation.at(2).at(2) +
						1;
			elem[1] =  -rotation.at(0).at(0) +
						rotation.at(1).at(1) -
						rotation.at(2).at(2) +
						1;
			elem[2] =  -rotation.at(0).at(0) -
						rotation.at(1).at(1) +
						rotation.at(2).at(2) +
						1;
			elem[3] =	rotation.at(0).at(0) +
						rotation.at(1).at(1) +
						rotation.at(2).at(2) +
						1;

			int biggestIndex = 0;

			for(int i = 1; i < 4; ++i)
			{
				if(elem[i] > elem[biggestIndex])
					biggestIndex = i;
			}
			if(elem[biggestIndex] < 0.0f)
			{
				SetByIdentity();
				return *this;
			}

			//最大要素の値を算出
			T*	q[4] = 
			{
				&Vector().at(0),
				&Vector().at(1),
				&Vector().at(2),
				&Scalar()
			};

			T	v				= sqrt(elem[biggestIndex]) * T(0.5);
			*q[biggestIndex]	= v;
			T	mult			= T(0.25) / v;

			switch(biggestIndex)
			{
			case 0:
				*q[1] = (rotation.at(0).at(1) + rotation.at(1).at(0)) * mult;
				*q[2] = (rotation.at(2).at(0) + rotation.at(0).at(2)) * mult;
				*q[3] = (rotation.at(1).at(2) - rotation.at(2).at(1)) * mult;
				break;
			case 1:
				*q[0] = (rotation.at(0).at(1) + rotation.at(1).at(0)) * mult;
				*q[2] = (rotation.at(1).at(2) + rotation.at(2).at(1)) * mult;
				*q[3] = (rotation.at(2).at(0) - rotation.at(0).at(2)) * mult;
				break;
			case 2:
				*q[0] = (rotation.at(2).at(0) + rotation.at(0).at(2)) * mult;
				*q[1] = (rotation.at(1).at(2) + rotation.at(2).at(1)) * mult;
				*q[3] = (rotation.at(0).at(1) - rotation.at(1).at(0)) * mult;
				break;
			case 3:
				*q[0] = (rotation.at(1).at(2) - rotation.at(2).at(1)) * mult;
				*q[1] = (rotation.at(2).at(0) - rotation.at(0).at(2)) * mult;
				*q[2] = (rotation.at(0).at(1) - rotation.at(1).at(0)) * mult;
				break;
			}

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByRotationYawPitchRoll(
			const Graphic::CVector3&	rotation)
		{
			SetByRotationMatrix(
				Graphic::CMatrix4x4::
					CreateByRotationYawPitchRoll(rotation));

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetByRotationYawPitchRoll(
			const T	x,
			const T	y,
			const T	z)
		{
			SetByRotationMatrix(
					Graphic::CMatrix4x4::
					CreateByRotationYawPitchRoll(x, y, z));

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetBySquad(
			const CQuaternion<T>&	Q1,
			const CQuaternion<T>&	A,
			const CQuaternion<T>&	B,
			const CQuaternion<T>&	Q2,
			const T					t)
		{
			CQuaternion<T> q2;

			if((Q1 + Q2).GetNormSq() < (Q1 - Q2).GetNormSq())
				q2 = -Q2;
			else
				q2 = Q2;

			SetBySlerp(
				CreateBySlerp(Q1, q2, t),
				CreateBySlerp(A, B, t),
				2 * t * (1 - t));

			return *this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetBySquadSetup(
			const CQuaternion<T>&	Q1,
			const CQuaternion<T>&	Q2,
			const CQuaternion<T>&	Q3)
		{
			CQuaternion<T> q1, q3;
			
			if((Q1 + Q2).GetNormSq() < (Q1 - Q2).GetNormSq())
				q1 = -Q1;
			else
				q1 = Q1;
			
			if((Q2 + Q3).GetNormSq() < (Q2 - Q3).GetNormSq())
				q3 = -Q3;
			else
				q3 = Q3;
			
			*this =  (Q2.CreateByExp() * q3).CreateByLn();
			*this += (Q2.CreateByExp() * q1).CreateByLn();
			*this *= -0.25;

			SetByExp();

			*this *= Q2;

			return	*this;
		};
		
		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetBySquadEx(
			const CQuaternion<T>&	Q1,
			const CQuaternion<T>&	Q2,
			const CQuaternion<T>&	Q3,
			const CQuaternion<T>&	Q4,
			const T							t)
		{
			SetBySquad(
				Q1,
				CreateBySquadSetup(Q1, Q2, Q3),
				CreateBySquadSetup(Q2, Q3, Q4),
				Q2,
				t);

			return *this;
		};

		template<typename T>
		CQuaternion<T>&	CQuaternion<T>::SetBy2Vectors(
			const Graphic::CVector3& begin,
			const Graphic::CVector3& end)
		{
			Graphic::CVector3	axis = -begin.ToClone().Cross(end).Normalize();
			float		cost = begin.ToClone().Dot(end);

			if (cost < 1)	cost = 1;
			if (cost < -1)	cost = -1;
			auto theta = acos(cost);

			SetByRotationAxis(axis, theta);

			// nan値が出たら初期化
			// TODO:nan値が出る原因を突き止め、適切な修正を
			if (isnan(Scalar())) SetByIdentity();
			
			return *this;
		};
	};
};

#endif