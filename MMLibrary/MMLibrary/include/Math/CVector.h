/*!	\file		CVector.h
*	\details	CVectorクラスの宣言
*	\author		松裏征志
*	\date		2014/04/16
*/
#ifndef MMLIBRARY_CVECTOR_H_
#define MMLIBRARY_CVECTOR_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Math\CValueArray.h"
#include "Utility\COperatorExtender.h"

namespace MMLibrary
{
	namespace Math
	{
		template<
			typename		T,
			const uint_t	M,
			const uint_t	N>
		class CMatrix;

		/*!	\class	CVector
		*	\brief	ベクトルを表現するテンプレートクラス
		*
		*	\tparam T このベクトルの型を表す。デフォルトはfloat
		*	\tparam N このベクトルの次元を表す。デフォルトは3
		*
		*	T型N次元のベクトルを表現するためのクラス
		*/
		template<typename T = float, const uint_t N = 3>
		class CVector :
			public
			Utility::tagCalculateOperators < CVector<T, N>,
			Utility::tagCalculateOperators2nd < CVector<T, N>, T,
			Utility::tagInjectCalculateMethods < CVector<T, N>,
			Utility::tagInjectCalculateMethods2nd < CVector<T, N>, T,
			CValueArray < T, N > > > > >
		{
		public:
			using myType = CVector < T, N > ;
			using myBase = CValueArray < T, N > ;
#pragma region BaseClassMethod
			friend myType operator-(const myType& src)
			{
				return -static_cast<myBase>(src);
			};

			//!	加算代入演算子
			friend myType& operator+=(myType& left, const myType& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) +=
					static_cast<const myBase&>(right));
			};

			//! 減算代入演算子
			friend myType& operator-=(myType& left, const myType& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) -=
					static_cast<const myBase&>(right));
			};

			//!	乗算代入演算子
			friend myType& operator*=(myType& left, const myType& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) *=
					static_cast<const myBase&>(right));
			};

			//!	除算代入演算子
			friend myType& operator/=(myType& left, const myType& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) /=
					static_cast<const myBase&>(right));
			};

			//!	加算代入演算子
			friend myType& operator+=(myType& left, const T& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) +=
					static_cast<const T&>(right));
			};

			//!	減算代入演算子
			friend myType& operator-=(myType& left, const T& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) -=
					static_cast<const T&>(right));
			};

			//!	乗算代入演算子
			friend myType& operator*=(myType& left, const T& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) *=
					static_cast<const T&>(right));
			};

			//!	除算代入演算子
			friend myType& operator/=(myType& left, const T& right)
			{
				return static_cast<myType&>(
					static_cast<myBase&>(left) /=
					static_cast<const T&>(right));
			};

			//!	等価演算子
			friend bool operator==(const myType& left, const myType& right)
			{
				return
					static_cast<const myBase&>(left) ==
					static_cast<const myBase&>(right);
			};

			myType& Negate(void)
			{
				return static_cast<myType&>(myBase::Negate());
			};
			myType& Set(const myType& src)
			{
				return static_cast<myType&>(myBase::Set(src));
			};
			myType& Set(const std::initializer_list<T>& initList)
			{
				return static_cast<myType&>(myBase::Set(initList));
			};
			myType& Set(const array_t<T, 3>& src)
			{
				return static_cast<myType&>(myBase::Set(src));
			};
			myType	ToClone(void) const
			{
				return myBase::ToClone();
			};

#pragma endregion

			/*!
			*	\brief	コンストラクタ
			*
			*	すべての要素を０で初期化する
			*/
			CVector(void);

			/*!
			*	\brief	引数付コンストラクタ
			*
			*	与えられた引数で全ての値を初期化する
			*/
			CVector(T src);

			/*!
			*	\brief	引数付コンストラクタ
			*
			*	与えられた引数でを初期化する
			*/
			CVector(std::initializer_list<T> src);

			/*!
			*	\brief	引数付コンストラクタ
			*
			*	与えられた引数で全ての値を初期化する
			*/
			CVector(const myBase& src)
			{
				*this = static_cast<const myType&>(src);
			};

			/*!
			*	\brief	CMatrix<T, 1, N>を作成して、それを返す
			*/
			CMatrix<T, 1, N> ToRawMatrix() const;

			/*!
			*	\brief	CMatrix<T, N, 1>を作成して、それを返す
			*/
			CMatrix<T, N, 1> ToColumnMatrix() const;

			/*!
			*	\brief	2 つのベクトルの外積を計算する。
			*
			*	\param	[in]	src	このベクトルとの外積を計算する
			*
			*	\return			演算後の自身への参照を返す
			*
			*	外積が計算できる場合は演算結果を自身に代入し、\n
			*	計算不能な場合は０ベクトルを自身に代入する。
			*/
			myType& Cross(const myType& src);

			/*!
			*	\brief	2 つのベクトルの内積を計算する。
			*
			*	\param	[in]	src	このベクトルとの内積を計算する
			*
			*	\return			計算結果を返す。
			*
			*	内積を計算し、結果を返す。
			*/
			T Dot(const myType& src) const;

			/*!
			*	\brief	このベクトルの長さを返す。
			*
			*	\return			計算結果を返す。
			*
			*	このベクトルの長さをdouble型の精度で計算し、結果を返す。
			*/
			T Length(void) const;

			/*!
			*	\brief	このベクトルの長さの２乗を返す。
			*
			*	\return			計算結果を返す。
			*
			*	このベクトルの長さの２乗をdouble型の精度で計算し、結果を返す。
			*/
			T LengthSq(void) const;

			/*!
			*	\brief	正規化したベクトルを計算し、その結果を自身に設定する。
			*
			*	\return			演算後の自身への参照を返す
			*
			*	正規化したベクトルを計算し、その結果を自身に設定する。
			*/
			myType& Normalize(void);

			/*!
			*	\brief	ベクトルをスケーリングする。
			*
			*	\return	演算後の自身への参照を返す
			*
			*	ベクトルをスケーリングし、その結果を自身に設定する。
			*/
			myType& Scale(const T s);

			/*!
			*	\brief	指定したベクトルを使用して、重心座標の点を自身に設定する。
			*
			*	\param	[in]	V1	処理の基になるCVectorへの参照
			*	\param	[in]	V2	処理の基になるCVectorへの参照
			*	\param	[in]	V3	処理の基になるCVectorへの参照
			*	\param	[in]	f	加重係数
			*	\param	[in]	g	加重係数
			*
			*	\return			演算後の自身への参照を返す
			*
			*	V1 + f(V2 - V1) + g(V3 - V1)の結果である座標を表す。\n
			*	平面 V1V2V3 の点はいずれも、重心座標 (f,g) で表すことができる。\n
			*	パラメータ f は、V2 の結果に対する加重を制御し、\n
			*	パラメータ g は、V3 の結果に対する加重を制御する。\n
			*	最後に、1-f-g は V1 の結果に対する加重を制御する。\n
			*	\n
			*	(f >= 0  g >= 0  1 - f - g >= 0) の場合、点は三角形 V1V2V3 内にある。\n
			*	(f == 0  g >= 0  1 - f - g >= 0) の場合、点は線 V1V3 上にある。\n
			*	(f >= 0  g == 0  1 - f - g >= 0) の場合、点は線 V1V2 上にある。\n
			*	(f >= 0  g >= 0  1 - f - g == 0) の場合、点は線 V2V3 上にある。\n
			*/
			myType&	SetByBaryCentric(
				const myType&	V1,
				const myType&	V2,
				const myType&	V3,
				const T			f,
				const T			g);

			/*!
			*	\brief	指定したベクトルを使用して、Catmull-Rom 補間による座標を自身に設定する。
			*
			*	\param	[in]	V1	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	V2	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	V3	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	V4	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	s	加重係数　0〜1
			*
			*	\return			演算後の自身への参照を返す
			*
			*	Catmull-Rom 補間による曲線はV1,V2,V3,V4の4点を通る曲線の、\n
			*	V2からV3の部分を表す。
			*/
			myType& SetByCatmulRom(
				const myType&	V1,
				const myType&	V2,
				const myType&	V3,
				const myType&	V4,
				const T			s);

			/*!
			*	\brief	指定したベクトルを使用して、エルミートのスプライン補間を実行し、\n
			*			その結果を自身に設定する。
			*
			*	\param	[in]	V1	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	T1	接線ベクトルを示す、CVectorへの参照
			*	\param	[in]	V2	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	T2	接線ベクトルを示す、CVectorへの参照
			*	\param	[in]	s	加重係数　0〜1
			*
			*	\return			演算後の自身への参照を返す
			*
			*	エルミートのスプライン補間を使って(positionA, tangentA)から(positionB, tangentB)へ補間する。
			*/
			myType& SetByHermite(
				const myType&	V1,
				const myType&	T1,
				const myType&	V2,
				const myType&	T2,
				const T			s);

			/*!
			*	\brief	指定したベクトルを使用して、線形補間を実行し、\n
			*			その結果を自身に設定する。
			*
			*	\param	[in]	V1	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	V2	位置ベクトルを示す、CVectorへの参照
			*	\param	[in]	s	加重係数　0〜1
			*
			*	\return			演算後の自身への参照を返す
			*
			*	この関数は、次の式に基づいて線形補間を実行する。\n
			*	V1 + s(V2 - V1)
			*/
			myType& SetByLerp(
				const myType&	V1,
				const myType&	V2,
				const T			s);
			/*!
			*	\brief	2 つのベクトルの最大値で構成されるベクトルを計算し、\n
			*			その結果を自身に設定する。
			*
			*	\param	[in]	V1	処理の基になる、CVectorへの参照
			*	\param	[in]	V2	処理の基になる、CVectorへの参照
			*
			*	\return			演算後の自身への参照を返す
			*
			*	2 つのベクトルに含まれる各要素の最大値で作成されたベクトルを計算し、\n
			*	その結果を自身に設定する。
			*/
			myType& SetByMaximize(
				const myType&	V1,
				const myType&	V2);

			/*!
			*	\brief	2 つのベクトルの最小値で構成されるベクトルを計算し、\n
			*			その結果を自身に設定する。
			*
			*	\param	[in]	V1	処理の基になる、CVectorへの参照
			*	\param	[in]	V2	処理の基になる、CVectorへの参照
			*
			*	\return			演算後の自身への参照を返す
			*
			*	2 つのベクトルに含まれる各要素の最小値で作成されたベクトルを計算し、\n
			*	その結果を自身に設定する。
			*/
			myType& SetByMinimize(
				const myType&	V1,
				const myType&	V2);
		};

		extern template class CVector < float, 3 > ;
	};
	template<class Type>
	class CFactory {};

	//! CVector用のファクトリークラス
	template<class T, uint_t N>
	class CFactory < Math::CVector<T, N> >
	{
		using myType = Math::CVector < T, N > ;

	public:
		/*!
		*	\brief	指定したベクトルを使用して、重心座標の点を作成する。
		*
		*	\param	[in]	V1	処理の基になるCVectorへの参照
		*	\param	[in]	V2	処理の基になるCVectorへの参照
		*	\param	[in]	V3	処理の基になるCVectorへの参照
		*	\param	[in]	f	加重係数
		*	\param	[in]	g	加重係数
		*
		*	\return			演算結果を返す
		*
		*	V1 + f(V2 - V1) + g(V3 - V1)の結果である座標を表す。\n
		*	平面 V1V2V3 の点はいずれも、重心座標 (f,g) で表すことができる。\n
		*	パラメータ f は、V2 の結果に対する加重を制御し、\n
		*	パラメータ g は、V3 の結果に対する加重を制御する。\n
		*	最後に、1-f-g は V1 の結果に対する加重を制御する。\n
		*	\n
		*	(f >= 0  g >= 0  1 - f - g >= 0) の場合、点は三角形 V1V2V3 内にある。\n
		*	(f == 0  g >= 0  1 - f - g >= 0) の場合、点は線 V1V3 上にある。\n
		*	(f >= 0  g == 0  1 - f - g >= 0) の場合、点は線 V1V2 上にある。\n
		*	(f >= 0  g >= 0  1 - f - g == 0) の場合、点は線 V2V3 上にある。\n
		*/
		static		myType	CreateByBaryCentric(
			const myType&	V1,
			const myType&	V2,
			const myType&	V3,
			const T			f,
			const T			g);

		/*!
		*	\brief	指定したベクトルを使用して、Catmull-Rom 補間による座標を作成する。
		*
		*	\param	[in]	V1	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	V2	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	V3	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	V4	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	s	加重係数　0〜1
		*
		*	\return			演算結果を返す
		*
		*	Catmull-Rom 補間による曲線はV1,V2,V3,V4の4点を通る曲線の、\n
		*	V2からV3の部分を表す。
		*/
		static		myType CreateByCatmulRom(
			const myType&	V1,
			const myType&	V2,
			const myType&	V3,
			const myType&	V4,
			const T			s);

		/*!
		*	\brief	指定したベクトルを使用して、エルミートのスプライン補間を実行する。
		*
		*	\param	[in]	V1	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	T1	接線ベクトルを示す、CVectorへの参照
		*	\param	[in]	V2	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	T2	接線ベクトルを示す、CVectorへの参照
		*	\param	[in]	s	加重係数　0〜1
		*
		*	\return			演算結果を返す
		*
		*	エルミートのスプライン補間を使って(positionA, tangentA)から(positionB, tangentB)へ補間する。
		*/
		static		myType CreateByHermite(
			const myType&	V1,
			const myType&	T1,
			const myType&	V2,
			const myType&	T2,
			const T			s);

		/*!
		*	\brief	指定したベクトルを使用して、線形補間を実行する。
		*
		*	\param	[in]	V1	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	V2	位置ベクトルを示す、CVectorへの参照
		*	\param	[in]	s	加重係数　0〜1
		*
		*	\return			演算結果を返す
		*
		*	この関数は、次の式に基づいて線形補間を実行する。\n
		*	V1 + s(V2 - V1)
		*/
		static		myType CreateByLerp(
			const myType&	V1,
			const myType&	V2,
			const T			s);

		/*!
		*	\brief	2 つのベクトルの最大値で構成されるベクトルを計算する。
		*
		*	\param	[in]	V1	処理の基になる、CVectorへの参照
		*	\param	[in]	V2	処理の基になる、CVectorへの参照
		*
		*	\return			演算結果を返す
		*
		*	2 つのベクトルに含まれる各要素の最大値で作成されたベクトルを計算し、\n
		*	その結果を自身に設定する。
		*/
		static		myType CreateByMaximize(
			const myType&	V1,
			const myType&	V2);

		/*!
		*	\brief	2 つのベクトルの最小値で構成されるベクトルを計算する。
		*
		*	\param	[in]	V1	処理の基になる、CVectorへの参照
		*	\param	[in]	V2	処理の基になる、CVectorへの参照
		*
		*	\return			演算結果を返す
		*
		*	2 つのベクトルに含まれる各要素の最小値で作成されたベクトルを計算し、\n
		*	その結果を自身に設定する。
		*/
		static		myType CreateByMinimize(
			const myType&	V1,
			const myType&	V2);
	};
};

//***********************************************************
//定義のインクルード
//***********************************************************
#include "CVector.hpp"

#endif