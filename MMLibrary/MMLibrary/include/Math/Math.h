/*!	\file		Math.h
*	\details	Math関連の宣言・インクルード
*	\author		松裏征志
*	\date		2014/04/25
*/
#ifndef MMLIBRARY_MATH_H_
#define MMLIBRARY_MATH_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Math\Function.h"

//***********************************************************
//共通の構造体等の定義
//***********************************************************
namespace MMLibrary
{
	/*!	\namespace	MMLibrary::Math
	*	\brief	MMLibraryの数学的な要素はこの名前空間に属する
	*/
	namespace Math
	{
	};
};

//***********************************************************
//インクルード
//***********************************************************
#include "Math\Interpolation.h"
#include "Math\CMatrix.h"
#include "Math\CVector.h"
#include "Math\CQuaternion.h"
#include "Math\CValueArray.h"
#include "Math\CValueRange.h"
#include "Math\CRangeBindedValue.h"
#endif