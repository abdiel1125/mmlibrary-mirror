/*!	\file		CMatrix.hpp
*	\details	CMatrixクラスの定義
*	\author		松裏征志
*	\date		2014/04/21
*/
#ifndef MMLIBRARY_CMATRIX_HPP_
#define MMLIBRARY_CMATRIX_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Math\CMatrix.h"

namespace MMLibrary
{
	namespace Math
	{
		template<	typename T, const uint_t M, const uint_t N>
		bool operator==(const CMatrix<T, M, N>& left, const CMatrix<T, M, N>& right)
		{
			for(uint_t m = 0; m < M; ++m)
			{
				for(uint_t n = 0; n < N; ++n)
				{
					if (left[m][n] != right[m][n])
						return false;
				}
			}

			return true;
		};

		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, M, N> CMatrix<T, M, N>::ToClone() const
		{
			return *this;
		}

		///	\cond
		template<	typename T, const uint_t M, const uint_t N>
		T	Determinant(const CMatrix<T, M, N>& mat)
		{
			//正方行列でないなら0を返す
			if(M != N)
			{
				return 0;
			}
			//それ以外の場合は展開して計算
			{
				T rtn = 0;

				for(uint_t l = 0; l < M; ++l)
				{
					CMatrix<T, M - 1, N - 1>	temp;
					uint_t				mCounter = 0;
					uint_t				nCounter = 0;

					for(uint_t m = 0; m < M; ++m)
					{
						if(m == l)
							continue;
						for(uint_t n = 1; n < N; ++n)
						{
							temp[mCounter][nCounter] = 
								mat[m][n];
							++nCounter;
						}
						nCounter = 0;
						++mCounter;
					}

					rtn +=	mat[l][0] * 
						temp.Determinant() * 
						((l & 1)?-1:1);
				}
				rtn += mat[0][0] * mat[1][1];
				rtn += mat[0][1] * mat[1][0];

				return rtn;
			}
		}

		template<typename T>
		T	Determinant(const CMatrix<T, 1, 1>& mat)
		{
			return mat[0][0];
		}

		template<typename T>
		T	Determinant(const CMatrix<T, 2, 2>& mat)
		{
			T rtn = 0;

			rtn += mat[0][0] * mat[1][1];
			rtn += mat[0][1] * mat[1][0];

			return rtn;
		}
		///	\endcond

		template<	typename T, const uint_t M, const uint_t N>
		T	CMatrix<T, M, N>::Determinant(void) const
		{
			return MMLibrary::Math::Determinant(*this);
		};

		template<	typename T, const uint_t M, const uint_t N>
		bool	CMatrix<T, M, N>::IsIdentity(void)
		{
			for(uint_t m = 0; m < M; ++m)
			{
				for(uint_t n = 0; n < N; ++n)
				{
					if(m == n)
					{
						if(at(m).at(n) != 1)
							return false;
					}
					else
					{
						if(at(m).at(n) != 0)
							return false;
					}
				}
			}
			return true;
		};

		template<	typename T, const uint_t M, const uint_t N>
		template<typename Ts>
		CMatrix<T, M, N>&	CMatrix<T, M, N>::Multiply(const CMatrix<Ts, N, N>& src)
		{
			*this *= src;

			return *this;
		}

		template<	typename T, const uint_t M, const uint_t N>
		template<typename Ts>
		CMatrix<T, M, N>&	CMatrix<T, M, N>::MultiplyTransposed(const CMatrix<Ts, N, N>& src)
		{
			*this = src * *this;

			return *this;
		}
		
		template<	typename T, const uint_t M, const uint_t N>
		bool	CMatrix<T, M, N>::IsSquareMatrix(void)
		{
			return (M == N);
		};
		
		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, N, M>	CMatrix<T, M, N>::ToTransposedMatrix(void)
		{
			CMatrix<T, N, M> rtn;

			for(uint_t m = 0; m < M; ++m)
			{
				for(uint_t n = 0; n < N; ++n)
				{
					rtn[n][m] = data()[m][n];
				}
			}
			
			return std::move(rtn);
		};
				
		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, M, N>	CMatrix<T, M, N>::CreateBy1Element(T src)
		{
			return std::move(
				CMatrix<T, M, N>().SetBy1Element(src));
		};

		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, M, N>	CMatrix<T, M, N>::CreateByIdentity(void)
		{
			return std::move(CMatrix<T, M, N>().SetByIdentity());
		};
		
		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, M, N>	CMatrix<T, M, N>::CreateByInverse(void) const
		{
			return std::move(ToClone().SetByInverse());
		};
		
		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, M, N>& CMatrix<T, M, N>::SetBy1Element(T src)
		{
			for(uint_t m = 0; m < M; ++m)
			{
				for(uint_t n = 0; n < N; ++n)
				{
					data()[m][n] = src;
				}
			}

			return *this;
		};

		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, M, N>&	CMatrix<T, M, N>::SetByIdentity(void)
		{
			for(uint_t m = 0; m < M; ++m)
			{
				for(uint_t n = 0; n < N; ++n)
				{
					if(m == n)
						data()[m][n] = 1;
					else
						data()[m][n] = 0;
				}
			}

			return *this;
		};
		
		template<	typename T, const uint_t M, const uint_t N>
		CMatrix<T, M, N>&	CMatrix<T, M, N>::SetByInverse(void)
		{
			//正方行列でなければ単位行列を返す
			if(M != N)
			{
				SetByIdentity();
				return *this;
			}
			//行列式が0なら逆行列は存在しないので単位行列を返す
			if(Determinant() == 0)
			{
				SetByIdentity();
				return *this;
			}

			//ガウス・ジョルダン法により逆行列を求める
			//拡張行列を作成する
			array_t<array_t<T, N * 2>, M> work;
			CreateExtendMatrix(work);

			//Gauss-Jordan method
			if(!DoGaussJordan(work))
			{
				SetByIdentity();
				return *this;
			}

			//計算結果をCMatrixに代入
			CMatrix<T, M, N> work2;
			for(uint_t m = 0; m < M; ++m)
			{
				for(uint_t n = 0; n < N; ++n)
				{
					data()[m][n] = work[m][n + N];
				}
			}

			return *this;
		};
		
		template<	typename T, const uint_t M, const uint_t N>
		void CMatrix<T, M, N>::CreateExtendMatrix(array_t<array_t<T, N * 2>, M>& work)
		{
			for(uint_t m = 0; m < M; ++m)
			{
				for(uint_t n = 0; n < N; ++n)
				{
					work[m][n] = data()[m][n];
				}
				for(uint_t n = N; n < N * 2; ++n)
				{
					if(m == n - N)
						work[m][n] = 1;
					else
						work[m][n] = 0;
				}
			}
		};
		
		template<	typename T, const uint_t M, const uint_t N>
		bool CMatrix<T, M, N>::DoGaussJordan(array_t<array_t<T, N * 2>, M>& work)
		{
			for(uint_t l = 0; l < M; ++l){
				T ll = work[l][l];

				//ll==0の場合は失敗
				if(ll == 0)
				{
					return false;
				}

				// 対角要素を1にするために，l行目のすべての要素をllで割る
				for(uint_t n = 0; n < 2 * N; ++n)
				{
					work[l][n] /= ll;
				}

				// l列目の非対角要素を0にする
				for(uint_t m = 0; m < M; ++m)
				{
					if(m == l) continue;

					T ml = work[m][l];

					for(uint_t n = 0; n < 2 * N; ++n)
					{
						work[m][n] -= work[l][n] * ml;
					}
				}
			}

			return true;
		};
	};
};
#endif