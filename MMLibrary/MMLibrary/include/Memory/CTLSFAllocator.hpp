/*!	\file		CTLSFAllocator.hpp
*	\details	CTLSFAllocatorクラスのテンプレートメソッドの定義
*	\author		松裏征志
*	\date		2014/05/02
*/
#ifndef MMLIBRARY_CTLSFALLOCATOR_HPP_
#define MMLIBRARY_CTLSFALLOCATOR_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Memory/CTLSFAllocator.h"

namespace MMLibrary
{
	namespace Memory
	{			
		template<class T>
		void CTLSFAllocator::Construct(T* pointer, const T& src)
		{
			new( (void*)pointer ) T(src);
		};
			
		template<class T>
		T* CTLSFAllocator::Create(void)
		{
			return Create(T());
		};
			
		template<class T>
		T* CTLSFAllocator::Create(const T& src)
		{
			T* pointer = (T*)Allocate(sizeof(T));
			Construct(pointer, src);

			return pointer;
		};
					
		template<class T>
		void CTLSFAllocator::Destroy(T* pointer)
		{
			pointer->~T();
		};
			
		template<class T>
		bool CTLSFAllocator::Release(T* pointer)
		{
			if(	m_pMemoryPool	<= (char*)pointer 
				(char*)pointer	<= m_pMemoryPool + m_allAlocatedSize)
			{
				Destroy(pointer);
				return Deallocate(pointer);
			}

			return false;
		};
	};
};

#endif