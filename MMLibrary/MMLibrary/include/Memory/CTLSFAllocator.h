/*!	\file		CTLSFAllocator.h
*	\details	CTLSFAllocatorクラスの宣言
*	\author		松裏征志
*	\date		2014/05/02
*/
#ifndef MMLIBRARY_CTLSFALLOCATOR_H_
#define MMLIBRARY_CTLSFALLOCATOR_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Memory/CMemoryBlock.h"
#include <string>
#include <iostream>
#include <cassert>

namespace MMLibrary
{
	namespace Memory
	{
		
		template<class HEADER, class ENDTAG>
		class CMemoryBlock;
		
		class CTLSFAllocator;

		/*!
		*	\brief	TLSF用メモリブロックのヘッダークラス
		*/
		class CTLSFBlockHeader : public CMemoryBlockHeader 
		{
			friend class CTLSFAllocator;
		private:
			/*!
			*	\brief	ひとつ前にリンクされているメモリブロックのヘッダを指すポインタ
			*/
			CTLSFBlockHeader*	m_pPrev;

			/*!
			*	\brief	ひとつ後にリンクされているメモリブロックのヘッダを指すポインタ
			*/
			CTLSFBlockHeader*	m_pNext;

			//!	このブロックがアクテイブか否かの情報をsize変数から取り出す
			static const uint_t  
				THIS_BLOCK_USE_BIT_MASK = 0x40000000;
			
			//!	直前のブロックがアクテイブか否かの情報をsize変数から取り出す
			static const uint_t  
				PREV_BLOCK_USE_BIT_MASK = 0x80000000;

		public:
			/*!
			*	\brief	コンストラクタ
			*/
			CTLSFBlockHeader(void);

			/*!
			*	\brief	双方向リストへ入れる
			*/
			CMemoryBlock<CTLSFBlockHeader, uint_t>&	Register(
				CMemoryBlock<CTLSFBlockHeader, uint_t>* src);

			/*!
			*	\brief	双方向リストを抜ける
			*/
			CMemoryBlock<CTLSFBlockHeader, uint_t>&	Remove(void);

			/*!
			*	\brief	ブロックへの参照
			*/
			CMemoryBlock<CTLSFBlockHeader, uint_t>&	ToBlock(void);
			
			/*!
			*	\brief	メモリブロックのサイズを取得する
			*/
			uint_t	GetSize(void);

			/*!
			*	\brief	メモリブロックのサイズを設定する
			*/
			void			SetSize(uint_t size);

			/*!
			*	\brief	最後のブロックであるか
			*/
			bool			IsLast(void);

			/*!
			*	\brief	フリーなブロックであるか
			*/
			bool			IsFree(void);
			
			/*!
			*	\brief	このブロックをフリーにする
			*/
			void			SetFree(void);
			
			/*!
			*	\brief	このブロックをアクティブにする
			*/
			void			SetActive(void);

			/*!
			*	\brief	直前のブロックがフリーなブロックであるか
			*/
			bool			IsPrevFree(void);
			
			/*!
			*	\brief	直前のブロックをフリーにする
			*/
			void			SetPrevFree(void);
			
			/*!
			*	\brief	直前のブロックをアクティブにする
			*/
			void			SetPrevActive(void);
		};

		/*!
		*	\brief	TLSFを用いたアロケーター
		*
		*	\date		2014/05/22 Allocateメソッドのバグのため修正まで使用禁止
		*	\date		2014/05/23 開放処理を修正、使用中フラグの持ち方を変更
		*	\date		2014/05/26 しばらく使用禁止のまま放置
		*/
		class CTLSFAllocator
		{
		private:
			enum PRIVATE_CONST
			{
				/* 第二カテゴリー分割数のLog2 */
				SL_INDEX_COUNT_LOG2 = 5,
#if MML_64BIT
				/* All allocation sizes and addresses are aligned to 8 bytes. */
				ALIGN_SIZE_LOG2 = 3,
#else
				/* All allocation sizes and addresses are aligned to 4 bytes. */
				ALIGN_SIZE_LOG2 = 2,
#endif
				ALIGN_SIZE = (1 << ALIGN_SIZE_LOG2),

				/*
				** We support allocations of sizes up to (1 << FL_INDEX_MAX) bits.
				** However, because we linearly subdivide the second-level lists, and
				** our minimum size granularity is 4 bytes, it doesn't make sense to
				** create first-level lists for sizes smaller than SL_INDEX_COUNT * 4,
				** or (1 << (SL_INDEX_COUNT_LOG2 + 2)) bytes, as there we will be
				** trying to split size ranges into more slots than we have available.
				** Instead, we calculate the minimum threshold size, and place all
				** blocks below that size into the 0th first-level list.
				*/

#if MML_64BIT
				/*
				** TODO: We can increase this to support larger sizes, at the expense
				** of more overhead in the TLSF structure.
				*/
				FL_INDEX_MAX = 32,
#else
				FL_INDEX_MAX = 30,
#endif
				SL_INDEX_COUNT = (1 << SL_INDEX_COUNT_LOG2),
				FL_INDEX_SHIFT = (SL_INDEX_COUNT_LOG2 + ALIGN_SIZE_LOG2),
				FL_INDEX_COUNT = (FL_INDEX_MAX - FL_INDEX_SHIFT + 1),

				SMALL_BLOCK_SIZE = (1 << FL_INDEX_SHIFT),
			};

			/*!
			*	\brief	第二カテゴリー分割数
			*			(2^N分割)
			*/
			static const int					N = 2;
			
			/*!
			*	\brief	運用総メモリ
			*/
			uint_t						m_poolSize;
			
			/*!
			*	\brief	総確保メモリ
			*/
			uint_t						m_allAlocatedSize;
			
			/*!
			*	\brief	総分割数
			*/
			uint_t						m_maxDivideNum;

			char*								m_pMemoryPool;

			CMemoryBlock<CTLSFBlockHeader, uint_t>*	m_pFreeBlockList;
			CMemoryBlock<CTLSFBlockHeader, uint_t>*	m_pActiveBlockList;
			
			uint_t						m_fliFreeListbit;
			uint_t*						m_pSliFreeListbit;

			int BNS;//sizeof(CMemoryBlock)+sizeof(uint_t)
			int BBS;//sizeof(CMemoryBlock)

			uint_t	GetSecondLevelIndex(
				uint_t	size, 
				uchar_t	MSB) const;

			uchar_t	GetFreeListSLI(
				uchar_t	mySLI, 
				uint_t	freeListBit) const;
			
			uchar_t GetFreeListFLI(
				uchar_t	myFLI) const;
			
			uchar_t GetFreeListIndex(
				size_t			size) const;
			
			uchar_t GetFreeListIndex(
				uchar_t	FLI,
				uchar_t	SLI) const;
		public:

			/*!
			*	\brief	コンストラクタ
			*/
			CTLSFAllocator(
				uint_t poolSize = 4096, 
				int maxExpectDivideNum = -1);
			
			/*!
			*	\brief	初期化関数
			*/
			void Init(	uint_t poolSize, 
						int maxExpectDivideNum);

			/*!
			*	\brief	デストラクタ
			*/
			~CTLSFAllocator(void);
			
			/*!
			*	\brief	メモリを割り当てる
			*/
			void*	Allocate(const size_t size);
			
			/*!
			*	\brief	割当て済みの領域を初期化する
			*/
			template<class T>
			void	Construct(T* pointer, const T& src);
			
			/*!
			*	\brief	メモリを割り当て、割当て済みの領域を初期化する
			*/
			template<class T>
			T*	Create(void);
			
			/*!
			*	\brief	メモリを割り当て、割当て済みの領域を初期化する
			*/
			template<class T>
			T*	Create(const T& src);
			
			/*!
			*	\brief	メモリを解放する
			*/
			bool	Deallocate(void* pointer);
			
			/*!
			*	\brief	初期化済みの領域を削除する
			*/
			template<class T>
			void	Destroy(T* pointer);
			
			/*!
			*	\brief	初期化済みの領域を削除し、メモリを解放する
			*/
			template<class T>
			bool	Release(T* pointer);

			/*!
			*	\brief	全領域を開放する
			*/
			void	Clear(void);

			/*!
			*	\brief	プールサイズを取得する
			*/
			uint_t	GetPoolSize(void)
			{
				return m_poolSize;
			};
		};
	};
};

//***********************************************************
//定義のインクルード
//***********************************************************
#include "CTLSFAllocator.hpp"

#endif