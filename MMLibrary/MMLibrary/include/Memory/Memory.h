/*!	\file		Memory.h
*	\details	Memory関連の宣言・インクルード
*	\author		松裏征志
*	\date		2014/05/02
*/
#ifndef MMLIBRARY_MEMORY_H_
#define MMLIBRARY_MEMORY_H_

//***********************************************************
//共通の構造体等の定義
//***********************************************************
namespace MMLibrary
{
	/*!	\namespace	MMLibrary::Memory
	*	\brief	MMLibraryのメモリ管理に関わる要素はこの名前空間に属する
	*/
	namespace Memory
	{
		//!	STLスタイルのアロケータによるアロケートとコンストラクトを代行する
		template<class T, template<class> class Allocator = USE_ALLOCATOR, class ...Args>
		inline T* NewWithAllocator(Args&&... args)
		{
			Allocator<T> allocator;
			using traits = std::allocator_traits<Allocator<T>>;

			T* pointer = traits::allocate(allocator, 1);
			traits::construct(allocator, pointer, args...);

			return pointer;
		}

		//!	STLスタイルのアロケータによるデアロケートとデストラクトを代行する
		template<class T, template<class> class Allocator = USE_ALLOCATOR>
		inline void DeleteWithAllocator(T* pointer)
		{
			if (pointer == nullptr) return;

			Allocator<T> allocator;
			using traits = std::allocator_traits<Allocator<T>>;

			traits::destroy(allocator, pointer);
			traits::deallocate(allocator, pointer, 1);
		}

		//!	STLスタイルのアロケータによるshared_ptrの作成
		template<class T, template<class> class Allocator, class... Args>
		inline std::shared_ptr<T> MakeShared(Args... args)
		{
			std::shared_ptr<T> rtn;
			rtn.reset(
				Memory::NewWithAllocator<T, Allocator>(args...),
				Memory::DeleterForAllocator<T, Allocator>());

			return rtn;
		}

		//!	STLスタイルのアロケータに対応するデリーター
		template<class T, template<class> class Allocator = USE_ALLOCATOR>
		struct DeleterForAllocator
		{
			//!	コンストラクタ
			DeleterForAllocator() = default;
			
			//!	デストラクタ
			~DeleterForAllocator() = default;

			//!	関数呼び出し
			void operator()(T* pointer) const
			{
				DeleteWithAllocator<T, Allocator>(pointer);
			};
		};

	};
};

//***********************************************************
//インクルード
//***********************************************************

//以下はバグのため使用禁止
//#include "CMemoryBlock.h"
//#include "CMemoryBlockHeader.h"
//#include "CTLSFAllocator.h"
//#include "CTLSFAllocatorForSTL.h"

#endif