/*!	\file		CMemoryBlock.hpp
*	\details	CMemoryBlockクラスの定義
*	\author		松裏征志
*	\date		2014/05/02
*/
#ifndef MMLIBRARY_CMEMORYBLOCK_HPP_
#define MMLIBRARY_CMEMORYBLOCK_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Memory/CMemoryBlock.h"

namespace MMLibrary
{
	namespace Memory
	{
		template<class HEADER, class ENDTAG>
		void CMemoryBlock<HEADER, ENDTAG>::WriteEndTag(void) 
		{
			new((char*)this + GetBlockSize() - GetEndTagSize()) ENDTAG(GetBlockSize());
		};

		template<class HEADER, class ENDTAG>
		unsigned CMemoryBlock<HEADER, ENDTAG>::GetEndTagSize()
		{
			return sizeof(ENDTAG);
		};

		template<class HEADER, class ENDTAG>
		CMemoryBlock<HEADER, ENDTAG>::CMemoryBlock(unsigned size) 
		{
			m_header.SetSize(size);
			if(size)
				WriteEndTag();
		};

		template<class HEADER, class ENDTAG>
		void* CMemoryBlock<HEADER, ENDTAG>::GetMemory() 
		{
			return (char*)this + sizeof(CMemoryBlock);
		}

		template<class HEADER, class ENDTAG>
		unsigned CMemoryBlock<HEADER, ENDTAG>::GetMemorySize() 
		{
			return m_header.GetSize();
		}

		template<class HEADER, class ENDTAG>
		unsigned CMemoryBlock<HEADER, ENDTAG>::GetBlockSize() 
		{
			return sizeof(CMemoryBlock) + m_header.GetSize() + GetEndTagSize();
		}

		template<class HEADER, class ENDTAG>
		CMemoryBlock<HEADER, ENDTAG>* CMemoryBlock<HEADER, ENDTAG>::Next() 
		{
			return (CMemoryBlock<HEADER, ENDTAG>*)((char*)this + GetBlockSize());
		}

		template<class HEADER, class ENDTAG>
		CMemoryBlock<HEADER, ENDTAG>* CMemoryBlock<HEADER, ENDTAG>::Prev() 
		{
			unsigned *preSize = (unsigned*)((char*)this - GetEndTagSize());
			return (CMemoryBlock<HEADER, ENDTAG>*)((char*)this - *preSize);
		}

		template<class HEADER, class ENDTAG>
		void CMemoryBlock<HEADER, ENDTAG>::Marge() 
		{
			// タグを変更
			unsigned newSize = m_header.GetSize() + GetEndTagSize() +
				sizeof(CMemoryBlock) +	Next()->GetMemorySize();
			m_header.SetSize(newSize);
			WriteEndTag();
		};

		template<class HEADER, class ENDTAG>
		CMemoryBlock<HEADER, ENDTAG>* CMemoryBlock<HEADER, ENDTAG>::Split(uint_t size)
		{
			// 新規ブロックを作るサイズが無ければnullptr
			uint_t needSize = size + GetEndTagSize() + sizeof(CMemoryBlock);
			if (needSize > m_header.GetSize())
				return 0;

			// 新規ブロックのメモリサイズを算出
			uint_t newBlockMemSize = m_header.GetSize() - needSize;

			// 既存サイズを引数サイズに縮小
			m_header.SetSize(size);
			WriteEndTag();

			// 新規ブロックを作成
			CMemoryBlock *newBlock = Next();
			new (newBlock) CMemoryBlock(newBlockMemSize);

			return newBlock;
		};

		template<class HEADER, class ENDTAG>
		bool CMemoryBlock<HEADER, ENDTAG>::IsEnableSplit(unsigned size)
		{
			// 新規ブロックを作るサイズが無ければfalseを返す
			unsigned needSize = size + GetEndTagSize() + sizeof(CMemoryBlock);
			
			if (needSize > m_header.GetSize())
				return false;

			return true;
		};
		
		template<class HEADER, class ENDTAG>
		HEADER& CMemoryBlock<HEADER, ENDTAG>::Header()
		{
			return m_header;
		};
		
		template<class HEADER, class ENDTAG>
		ENDTAG* CMemoryBlock<HEADER, ENDTAG>::EndTag()
		{
			return (ENDTAG*)((char*)GetMemory() + GetMemorySize());
		};
	};
};

#endif