/*!	\file		CTLSFAllocatorForSTL.h
*	\details	CTLSFAllocatorForSTLクラスの宣言
*	\author		松裏征志
*	\date		2014/05/11
*/
#ifndef MMLIBRARY_CTLSFALLOCATOR_FOR_STL_H_
#define MMLIBRARY_CTLSFALLOCATOR_FOR_STL_H_

//***********************************************************
//インクルード
//***********************************************************
#include <limits>
#include <string>

namespace MMLibrary
{
	namespace Memory
	{
		/*!
		*	\brief	CTLSFAllocatorForSTLで共通に使うメモリプールのサイズ
		*/
#ifndef		ALLOCATOR_POOL_SIZE
#	define	ALLOCATOR_POOL_SIZE	(4096)
#endif
		/*!
		*	\brief	STLでの使用が可能なTLSFアロケータ
		*
		*	\tparam	T	このアロケーターに用いるクラスの型
		*
		*	ALLOCATOR_POOL_SIZEを設定したいプールサイズでデファインすることで、\n
		*	プールサイズを設定することが出来ます
		*/
		template<class T>
		class CTLSFAllocatorForSTL
		{
		public:
			// 型定義
			typedef size_t		size_type;			//!<	size_tの別名
			typedef ptrdiff_t	difference_type;	//!<	ptrdiff_tの別名
			typedef T*			pointer;			//!<	T*の別名
			typedef const T*	const_pointer;		//!<	const T*の別名
			typedef T&			reference;			//!<	T&の別名
			typedef const T&	const_reference;	//!<	const T&の別名
			typedef T			value_type;			//!<	Tの別名
	
			/*!
			*	\brief	アロケータをU型にバインドする
			*/
			template <class U>
			struct rebind
			{
				/*!
				*	\brief	別のクラス向けのアロケータ
				*/
				typedef CTLSFAllocatorForSTL<U> other;
			};
	
			/*!
			*	\brief	コンストラクタ
			*/
			CTLSFAllocatorForSTL() throw();

			/*!
			*	\brief	コピーコンストラクタ
			*/
			CTLSFAllocatorForSTL(const CTLSFAllocatorForSTL&) throw();

			/*!
			*	\brief	コピーコンストラクタ
			*/
			template <class U> 
			CTLSFAllocatorForSTL(const CTLSFAllocatorForSTL<U>&) throw();
			
			/*!
			*	\brief	デストラクタ
			*/
			~CTLSFAllocatorForSTL() throw();
	
			/*!
			*	\brief	メモリを割り当てる
			*/
			pointer allocate(
				size_type num, 
				CTLSFAllocatorForSTL<void>::const_pointer hint = 0)
			{
				return (pointer)( g_allocator.Allocate(num * sizeof(T)));
			};
			
			/*!
			*	\brief	割当て済みの領域を初期化する
			*/
			void construct(pointer p, const T& value);
	
			/*!
			*	\brief	メモリを解放する
			*/
			void deallocate(pointer p, size_type num);
			
			/*!
			*	\brief	初期化済みの領域を削除する
			*/
			void destroy(pointer p);
	
			/*!
			*	\brief	アドレスを返す
			*/
			pointer address(reference value) const;

			/*!
			*	\brief	アドレスを返す(読み取り専用)
			*/
			const_pointer address(const_reference value) const;
	
			/*!
			*	\brief	割当てることができる最大の要素数を返す
			*/
			size_type max_size() const throw();

			/*!
			*	\brief	全ブロックについての情報を取得する
			*/
			string_t GetAllBlocksInfomation(void)
			{return g_allocator.GetAllBlocksInfomation();};
		};
		
		/*!
		*	\brief	allocatorメソッドのヒントのために作成
		*/
		template<>
		class CTLSFAllocatorForSTL<void>
		{
		public:
			typedef void*			pointer;			//!<	T*の別名
			typedef const void*		const_pointer;		//!<	const T*の別名
			typedef void			value_type;			//!<	Tの別名
		};
		
		/*!
		*	\brief	allocator<T>にあわせるために作成
		*/
		template <class T1, class T2>
		bool operator==(
			const CTLSFAllocatorForSTL<T1>&, 
			const CTLSFAllocatorForSTL<T2>&) throw();
		
		/*!
		*	\brief	allocator<T>にあわせるために作成
		*/
		template <class T1, class T2>
		bool operator!=(
			const CTLSFAllocatorForSTL<T1>&, 
			const CTLSFAllocatorForSTL<T2>&) throw();

	};
};

//***********************************************************
//インクルード
//***********************************************************
#include "CTLSFAllocatorForSTL.hpp"

#endif