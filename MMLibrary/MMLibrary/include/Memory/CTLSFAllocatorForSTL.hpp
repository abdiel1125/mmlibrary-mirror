/*!	\file		CTLSFAllocatorForSTL.hpp
*	\details	CTLSFAllocatorForSTLクラスの定義
*	\author		松裏征志
*	\date		2014/05/11
*/
#ifndef MMLIBRARY_CTLSFALLOCATOR_FOR_STL_HPP_
#define MMLIBRARY_CTLSFALLOCATOR_FOR_STL_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include "Memory/CTLSFAllocatorForSTL.h"
#include "Memory/CTLSFAllocator.h"


namespace MMLibrary
{
	namespace Memory
	{

		static CTLSFAllocator	g_allocator(ALLOCATOR_POOL_SIZE);
		static uint_t 	g_allocatorNum = 0;

		template<class T>
		CTLSFAllocatorForSTL<T>::CTLSFAllocatorForSTL() throw()
		{
		};
		
		template<class T>
		CTLSFAllocatorForSTL<T>::CTLSFAllocatorForSTL(const CTLSFAllocatorForSTL&) throw()
		{
		};
		
		template<class T>
		template <class U> 
		CTLSFAllocatorForSTL<T>::CTLSFAllocatorForSTL(const CTLSFAllocatorForSTL<U>&) throw()
		{
		};
			
		template<class T>
		CTLSFAllocatorForSTL<T>::~CTLSFAllocatorForSTL() throw()
		{
		};
	
		template<class T>
		void CTLSFAllocatorForSTL<T>::construct(
			T* p, 
			const T& value)
		{
			g_allocator.Construct<T>(p,value);
		}
	
		template<class T>
		void CTLSFAllocatorForSTL<T>::deallocate(
			T* p, 
			size_type num)
		{
			g_allocator.Deallocate(p);
		}
			
		template<class T>
		void CTLSFAllocatorForSTL<T>::destroy(
			T* p)
		{
			g_allocator.Destroy<T>(p);
		}
	
		template<class T>
		T*
			CTLSFAllocatorForSTL<T>::address(
			T& value) const 
		{ 
			return &value; 
		}
		
		template<class T>
		const T* 
			CTLSFAllocatorForSTL<T>::address(
			const T& value) const 
		{
			return &value; 
		}
	
		template<class T>
		size_t 
			CTLSFAllocatorForSTL<T>::max_size() const throw()
		{
			return g_allocator.GetPoolSize() / sizeof(T);
		};
		
		template <class T1, class T2>
		bool operator==(
			const CTLSFAllocatorForSTL<T1>&, 
			const CTLSFAllocatorForSTL<T2>&) throw() 
		{ 
			return true; 
		}

		template <class T1, class T2>
		bool operator!=(
			const CTLSFAllocatorForSTL<T1>&, 
			const CTLSFAllocatorForSTL<T2>&) throw() 
		{ 
			return false; 
		}

	};
};
#endif