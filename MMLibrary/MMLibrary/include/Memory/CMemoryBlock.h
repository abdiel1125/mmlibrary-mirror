/*!	\file		CMemoryBlock.h
*	\details	CMemoryBlockクラスの宣言
*	\author		松裏征志
*	\date		2014/05/02
*/
#ifndef MMLIBRARY_CMEMORYBLOCK_H_
#define MMLIBRARY_CMEMORYBLOCK_H_

//***********************************************************
//インクルード
//***********************************************************
#include "Memory/CMemoryBlockHeader.h"

namespace MMLibrary
{
	namespace Memory
	{
		//!	Boundary Tagアルゴリズムを用いたメモリブロッククラス
		/*!
		*	\tparam	HEADER	ヘッダ情報用のクラスGetSizeとSetSizeメソッドを持つ必要がある
		*	\tparam	ENDTAG	終端情報用の型
		*/
		template<
			class HEADER = CMemoryBlockHeader, 
			class ENDTAG = uint_t>
		class CMemoryBlock {
		private:
			static const CMemoryBlock<HEADER, ENDTAG>
				nullBlock;

			HEADER m_header;

			/*!
			*	\brief	後端タグを書き込む
			*/
			void WriteEndTag(void);

			/*!
			*	\brief	後端タグサイズを取得する
			*/
			unsigned GetEndTagSize();

		public:
			/*!
			*	\brief	コンストラクタ
			*/
			CMemoryBlock(unsigned size = 0);

			/*!
			*	\brief	管理メモリへのポインタを取得する
			*/
			void* GetMemory();
			
			/*!
			*	\brief	管理メモリサイズを取得する
			*/
			unsigned GetMemorySize();
			
			/*!
			*	\brief	ブロックサイズを取得する
			*/
			unsigned GetBlockSize();
			
			/*!
			*	\brief	次のブロックへのポインタを取得する
			*/
			CMemoryBlock<HEADER, ENDTAG>* Next();
			
			/*!
			*	\brief	前のブロックへのポインタを取得する
			*/
			CMemoryBlock<HEADER, ENDTAG>* Prev();
			
			/*!
			*	\brief	右ブロックをマージする
			*/
			void Marge();
			
			/*!
			*	\brief	ブロックを分割する
			*/
			CMemoryBlock<HEADER, ENDTAG>* Split(unsigned size);
			
			/*!
			*	\brief	指定サイズに分割可能かを判定する
			*/
			bool IsEnableSplit(unsigned size);
			
			/*!
			*	\brief	ヘッダへの参照
			*/
			HEADER& Header();
			
			/*!
			*	\brief	終端タグへの参照
			*/
			ENDTAG* EndTag();
		};
	};
};

//***********************************************************
//定義のインクルード
//***********************************************************
#include "CMemoryBlock.hpp"

#endif