/*!	\file		CMemoryBlockHeader.h
*	\details	CMemoryBlockHeaderクラスの宣言
*	\author		松裏征志
*	\date		2014/05/02
*/
#ifndef MMLIBRARY_CMEMORYBLOCKHEADER_H_
#define MMLIBRARY_CMEMORYBLOCKHEADER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Memory
	{
		/*!
		*	\brief CMemoryBlock用ヘッダークラス
		*/
		class CMemoryBlockHeader 
		{
		private:
			/*!
			*	\brief	メモリブロックのサイズ
			*/
			uint_t	m_size;
		public:
			/*!
			*	\brief	コンストラクタ
			*/
			CMemoryBlockHeader(void);

			/*!
			*	\brief	メモリブロックのサイズを取得する
			*/
			uint_t	GetSize(void);

			/*!
			*	\brief	メモリブロックのサイズを設定する
			*/
			void			SetSize(uint_t size);
		};
	};
};

#endif