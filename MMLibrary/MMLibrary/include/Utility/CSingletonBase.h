/*!	\file		CSingletonBase.h
*	\details	CSingletonBaseクラスの宣言・定義
*	\author		松裏征志
*	\date		2014/11/04
*/
#ifndef MMLIBRARY_CSINGLETONBASE_H_
#define MMLIBRARY_CSINGLETONBASE_H_
//***********************************************************
//インクルード
//***********************************************************
#include "tagNonCopyable.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!	シングルトンクラスを作成するヘルパクラス
		template<class T>
		class CSingletonBase : private tagNonCopyable
		{
		public:
			//!	デストラクタ
			virtual ~CSingletonBase(){};

			//!	インスタンスを取得できる唯一のメソッド
			static T& GetInstance()
			{
				static T instance;
				return instance;
			}
		protected:
			//!	コンストラクタ
			CSingletonBase(){};
		};
	}
}
#endif //MMLIBRARY_CSINGLETONBASE_H_