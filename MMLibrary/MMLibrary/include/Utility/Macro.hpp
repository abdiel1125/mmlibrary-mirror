/*!	\file		Macro.hpp
*	\details	MMLibraryで使用するマクロやちょっとしたクラス等の宣言・定義
*	\author		松裏征志
*	\date		2014/04/16
*/
#ifndef MMLIBRARY_MACRO_HPP_
#define MMLIBRARY_MACRO_HPP_

//***********************************************************
//インクルード
//***********************************************************
#include <cstdarg>
#include <cmath>
#include <fstream>
#include <functional>
#include <ratio>
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Utility
	{

#pragma region Deleter

		//!	ポインタがnullptrを指していなければ、freeを実行してnullptrを代入する
		template<class T>
		void SafeFree(T*& pT)
		{
			if (pT != nullptr)
			{
				free(pT);
				pT = nullptr;
			}
		};

		//!	ポインタがnullptrを指していなければ、deleteを実行してnullptrを代入する
		template<class T>
		void SafeDelete(T*& pT)
		{
			if (pT != nullptr)
			{
				delete (pT);
				pT = nullptr;
			}
		};

		//!	ポインタがnullptrを指していなければ、delete[]を実行してnullptrを代入する
		/*!
		*	配列として領域を確保すると配列のサイズを格納する部分ができるので、通常のSafeDeleteの代用にはならない
		*/
		template<class T>
		void SafeDeleteArray(T*& pT)
		{
			if (pT != nullptr)
			{
				delete[](pT);
				pT = nullptr;
			}
		};

		//!	ポインタがnullptrを指していなければ、Release()を実行してnullptrを代入する
		template<class T>
		void SafeRelease(T*& pT)
		{
			if (pT != nullptr)
			{
				pT->Release();
				pT = nullptr;
			}
		};

#pragma endregion

		//!	valueをminimal〜maximumの間に収める。

		/*!
		*	valueがminimalより小さい時は、minimalを出力する。\n
		*	valueがmaximumより大きい時は、maximumを出力する。
		*/
		template<typename T>
		T ClampIn(
			const T& minimal,
			const T& maximum,
			const T& value)
		{
			if (value < minimal)
				return minimal;

			if (value > maximum)
				return maximum;

			return value;
		};

#pragma region WriteToFile

		//!	ある型の値をファイルに書き込む
		template<class T>
		inline void WriteToFile(
			std::ofstream&	file,
			const T&		augment)
		{
			file.write((const char*)&augment, sizeof(T));
		}

		//!	ある型のペアをファイルに書き込む
		template<class T, class U>
		inline void WriteToFile(
			std::ofstream&		file,
			const std::pair<T, U>&	augment)
		{
			WriteToFile(file, augment.first);
			WriteToFile(file, augment.second);
		}

		//!	文字列をサイズ、文字列の順でファイルに書き込む
		template<template<class> class Allocator>
		inline void WriteToFile(
			std::ofstream&	file,
			const string_t<Allocator>&	str)
		{
			WriteToFile(file, str.size());
			file.write(str.data(), str.size());
		}

		//!	動的配列をサイズ、配列の順でファイルに書き込む
		template<class T, template<class> class Allocator>
		inline void WriteToFile(
			std::ofstream&			file,
			const vector_t<T, Allocator>&	vector)
		{
			WriteToFile(file, vector.size());

			if (vector.empty()) return;

			if (std::is_pod<T>::value)
			{
				file.write((const char*)vector.data(), sizeof(T) * vector.size());
				return;
			}

			for (auto& elem : vector)
			{
				WriteToFile(file, elem);
			}
		}

#pragma endregion

#pragma region LoadFromFile

		//!	ある型の値をファイルから読み込む
		template<class T>
		inline void LoadFromFile(
			std::ifstream&	file,
			T&		augment)
		{
			file.read(reinterpret_cast<char*>(&augment), sizeof(T));
		}

		//!	ある型のペアをファイルから読み込む
		template<class T, class U>
		inline void LoadFromFile(
			std::ifstream&		file,
			std::pair<T, U>&	augment)
		{
			LoadFromFile(file, augment.first);
			LoadFromFile(file, augment.second);
		}

		//!	サイズ、文字列の順で保存された文字列をファイルから読み込む
		template<template<class> class Allocator>
		inline void LoadFromFile(
			std::ifstream&			file,
			string_t<Allocator>&	str)
		{
			string_t<Allocator>::size_type size;
			LoadFromFile(file, size);

			str.resize(size);
			file.read(const_cast<char*>(str.data()), str.size());
		}

		//!	サイズ、配列の順で保存された動的配列をファイルから読み込む
		template<class T, template<class> class Allocator>
		inline void LoadFromFile(
			std::ifstream&		file,
			vector_t<T, Allocator>&		vector)
		{
			vector_t<T, Allocator>::size_type size;
			LoadFromFile(file, size);

			vector.resize(size);
			if (vector.empty()) return;
			if (std::is_pod<T>::value)
			{
				file.read(reinterpret_cast<char*>(vector.data()), sizeof(T) * vector.size());
				return;
			}
			for (auto& elem : vector)
			{
				LoadFromFile(file, elem);
			}
		}

#pragma endregion

#pragma region CopyFromCharArray

		//!	ある型の値をchar型ポインタから読み込む
		template<class T>
		inline void CopyFromCharArray(
			const char* pMemory,
			T&			augment)
		{
			augment = *((T*)pMemory);
		}

		//!	ある型の値をchar型ポインタから読み込み、その分ポインタを進める
		template<class T>
		void CopyAndGoFromCharArray(
			char*&	pMemory,
			T&		augment)
		{
			CopyFromCharArray(pMemory, augment);
			pMemory += sizeof(T);
		}

		//!	サイズ、文字列の順で保存された文字列を文字列から読み込む
		template<template<class> class Allocator>
		inline void CopyFromCharArray(
			const char*		pMemory,
			string_t<Allocator>&	str)
		{
			string_t<Allocator>::size_type size;
			CopyFromCharArray(pMemory, size);

			if (size <= 0) return;

			str.assign(pMemory + size, size);
		}

		//!	サイズ、文字列の順で保存された文字列を文字列から読み込み、その分ポインタを進める
		template<template<class> class Allocator>
		void CopyAndGoFromCharArray(
			char*&					pMemory,
			string_t<Allocator>&	str)
		{
			string_t<Allocator>::size_type size;
			CopyAndGoFromCharArray(pMemory, size);

			if (size <= 0) return;

			str.assign(pMemory, size);
			pMemory += size;
		}

		//!	サイズ、配列の順で保存された動的配列を文字列から読み込む
		template<class T, template<class> class Allocator>
		inline void CopyFromCharArray(
			const char*				pMemory,
			vector_t<T, Allocator>&	vector)
		{
			string_t<Allocator>::size_type size;
			CopyFromCharArray(pMemory, size);

			if (size <= 0) return;

			vector.resize(size);
			memcpy(vector.data(),
				pMemory + size,
				sizeof(T) * size);
			pMemory += sizeof(T)* size;
		}

		//!	サイズ、配列の順で保存された動的配列を文字列から読み込み、その分ポインタを進める
		template<class T, template<class> class Allocator>
		void CopyAndGoFromCharArray(
			char*&					pMemory,
			vector_t<T, Allocator>&	vector)
		{
			vector_t<T, Allocator>::size_type size;
			CopyAndGoFromCharArray(pMemory, size);

			if (size <= 0) return;

			vector.resize(size);
			memcpy(vector.data(),
				pMemory,
				sizeof(T) * size);
			pMemory += sizeof(T) * size;
		}

#pragma endregion

		//!	ファイルパスからファイルの拡張子を取得する
		template<template<class> class Allocator = USE_ALLOCATOR>
		string_t<Allocator> GetFileExtention(
			const string_t<Allocator>& filePath)
		{
			//拡張子を取得する
			size_t dot = filePath.rfind('.');
			string_t<Allocator> fileExt = &filePath[dot + 1];

			//拡張子を小文字化する
			for (auto it = fileExt.begin();
				it != fileExt.end();
				++it)
				*it = tolower(*it);

			return fileExt;
		};

		//!	ファイルの内容を一括でvectorに保存する
		template<class T, template<class> class Allocator = USE_ALLOCATOR>
		void LoadFileTo(const string_t<Allocator>& filePath, vector_t<T, Allocator>& memory, bool isText)
		{
			std::ifstream file;

			if (isText)
			{
				file.open(filePath.c_str());
				assert(file && "File can not open.");

				memory.resize(Utility::GetFileSize(file) + 1);

			}
			else
			{
				file.open(filePath.c_str(), std::ios::binary);
				assert(file && "File can not open.");

				memory.resize(Utility::GetFileSize(file));
			}
			file.
				seekg(0, file.beg).
				read(reinterpret_cast<char*>(memory.data()), memory.size());

			if (isText)
				memory[memory.size() - 1] = '\0';
		};

		//!	数字列をテンプレートパラメータに持つ構造体
		template<size_t ...Seq>
		struct CountingSequence {};

#pragma region CountingSequence
		//!	MakeCountingSequence内部で使うテンプレートメタ関数
		template<size_t N, size_t ...Seq>
		struct MakeCountingSequence_impl
		{
			typedef typename MakeCountingSequence_impl<N - 1, N, Seq...>::type type;
		};

		//!	MakeCountingSequence内部で使うテンプレートメタ関数
		template<size_t ...Seq>
		struct MakeCountingSequence_impl < 0, Seq... >
		{
			typedef CountingSequence<(Seq - 1)...> type;
		};
#pragma endregion

		//!	最大値を指定して0から最大値をテンプレートパラメータに持つ構造体を定義する
		template<size_t Max>
		struct MakeCountingSequence
		{
			typedef typename MakeCountingSequence_impl<Max>::type type;
		};

		//!	2つのポインタ間のアドレスの差を取得する
		template<class T, class U>
		int GetDistance(T* left, U* right)
		{
			const char* const l = reinterpret_cast<char*>(left);
			const char* const r = reinterpret_cast<char*>(right);

			if (l < r)
				return	(r - l);
			if (l > r)
				return	(l - r);

			return 0;
		};
		
		//!	ワイド文字列からマルチバイト文字列に変換する
		template<template<class> class Allocator = USE_ALLOCATOR>
		void WstringToString(
			const wstring_t<Allocator> &src, 
			string_t<Allocator> &dest)
		{
			char* temp = new char[src.length() * MB_CUR_MAX + 1];
			wcstombs(temp, src.c_str(), src.length() * MB_CUR_MAX + 1);
			dest = temp;

			delete[] temp;
		}

		//!	マルチバイト文字列からワイド文字列に変換する
		template<template<class> class Allocator = USE_ALLOCATOR>
		void StringToWstring(
			const string_t<Allocator> &src,
			wstring_t<Allocator> &dest)
		{
			size_t NumOfCharConverted = src.length() + 1;
			wchar_t* temp = new wchar_t[NumOfCharConverted];
			mbstowcs_s(&NumOfCharConverted, temp, NumOfCharConverted, src.c_str(), src.length() + 1);
			dest = temp;

			delete[] temp;
		}

		//!	std::ratioを浮動小数点数に変換する
		template<typename T, class Ratio>
		T RatioToFloatType(const Ratio& ratio = Ratio())
		{
			return 
				static_cast<T>(ratio.num) / 
				ratio.den;
		};

		//!	std::ratioから逆数のstd::ratioを作成する
		template<class Ratio>
		using ReverseOf = std::ratio_divide < Ratio, std::ratio<1> > ;

		//!	erase-removeイディオムの関数化
		template<class Container, class Function>
		void EraseRemoveIf(Container& container, Function&& function)
		{
			container.erase(
				std::remove_if(
				container.begin(),
				container.end(), 
				function), container.end());
		}
	};
};

#endif