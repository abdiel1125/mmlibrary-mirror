/*!	\file		CDataContainer.h
*	\details	CDataContainerNXÌé¾
*	\author		¼ ªu
*	\date		2014/09/26
*/
#ifndef MMLIBRARY_CDATA_CONTAINER_H_
#define MMLIBRARY_CDATA_CONTAINER_H_

//***********************************************************
//CN[h
//***********************************************************
#include "MMLibraryDefine.h"

namespace	MMLibrary
{
	namespace	Utility
	{
		//!	f[^i[pNX
		template<template<class> class Allocator = USE_ALLOCATOR>
		class CDataContainer : public vector_t<char, Allocator>
		{
			using myType = CDataContainer<Allocator>;
		private:
			size_t m_seek;

		public:
			CDataContainer(void)
				:m_seek(0)
			{};

			myType& SeekTo(const size_t& position)
			{
				m_seek = position;

				return *this;
			};

			myType& ToNext(const size_t& size)
			{
				m_seek += size;

				return *this;
			};

			template<class T>
			T& GetNow(void)
			{
				return *GetNowPointer<T>();
			};

			template<class T>
			T* GetNowPointer(void)
			{
				return reinterpret_cast<T*>(data() + m_seek);
			};

			template<class T>
			myType& GetTo(T& dest)
			{
				dest = GetNow<T>();
				return *this;
			};

			template<class T>
			myType& GetAndToNext(T& dest)
			{
				GetTo(dest);
				ToNext(sizeof(T));

				return *this;
			};

			template<class T>
			myType& Register(const T& src)
			{
				if (sizeof(T) + m_seek > size())
				{
					resize(sizeof(T) + m_seek);
				}

				GetNow<T>() = src;

				ToNext(sizeof(T));

				return *this;
			};

			template<class T>
			myType& Register(const vector_t<T, Allocator>& src)
			{
				reserve(sizeof(src.size()) + sizeof(T) * src.size() + m_seek);

				Register(src.size());
				for (const auto& data : src)
					Register(data);

				return *this;
			};

			myType& Register(const string_t<Allocator>& src)
			{
				reserve(sizeof(src.size()) + src.size() + m_seek);

				Register(src.size());
				for (const auto& data : src)
					Register(data);

				return *this;
			};
		};
	};
};

#endif