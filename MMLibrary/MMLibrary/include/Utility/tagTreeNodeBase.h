/*!	\file		tagTreeNodeBase.h
*	\details	tagTreeNodeBase構造体の宣言
*	\author		松裏征志
*	\date		2014/11/26
*/
#ifndef MMLIBRARY_TAG_TREENODE_BASE_H_
#define MMLIBRARY_TAG_TREENODE_BASE_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!ツリー構造をなすクラスの基底クラス
		template<class T, class Base = tagEmptyBase>
		struct tagTreeNodeBase : public Base
		{
		public:
			//!	コンストラクタ
			tagTreeNodeBase(T* pThis)
				: m_pBrother(nullptr)
				, m_pChild(nullptr)
				, m_pParent(nullptr)
				, m_pThis(pThis){};

			//!	デストラクタ
			~tagTreeNodeBase(void)
			{
				RemoveParent();

			};

			//!	兄弟オブジェクトを追加する
			void AddBrother(T& brother)
			{
				if (HasBrother())
					m_pBrother->AddBrother(brother);
				else
					m_pBrother = &brother;

				brother.m_pParent = GetParent();
			};

			//!	子オブジェクトを追加する
			void AddChild(T& child)
			{
				if (HasChild())
					m_pChild->AddBrother(child);
				else
					m_pChild = &child;

				child.m_pParent = m_pThis;
			};

			//!	親オブジェクトを設定する
			void SetParent(T& parent)
			{
				RemoveParent();
				m_pParent = &parent;
			};

			//!	兄弟オブジェクトを切り離す
			void RemoveBrother(tagTreeNodeBase<T, Base>& brother)
			{
				if (HasBrother() == false)
					return;

				if (GetBrother() == &brother)
				{
					m_pBrother = brother.GetBrother();
					brother.m_pParent = nullptr;
					brother.m_pBrother = nullptr;
					return;
				}

				GetBrother()->RemoveBrother(brother);
			};

			//!	子オブジェクトを切り離す
			void RemoveChild(tagTreeNodeBase<T, Base>& child)
			{
				if (HasChild() == false)
					return;

				if (GetChild() == &child)
				{
					m_pChild = child.GetBrother();
					child.m_pParent = nullptr;
					child.m_pBrother = nullptr;
					return;
				}

				GetChild()->RemoveBrother(child);
			};

			//!	親オブジェクトを切り離す
			void RemoveParent()
			{
				if (HasParent())
				{
					GetParent()->RemoveChild(*this);
				}

				m_pParent = nullptr;
			};

			//!	兄弟オブジェクトを取得する
			 T* GetBrother(void) const
			 {
				 return m_pBrother;
			 };

			//!	子オブジェクトを取得する
			T* GetChild(void) const
			{
				return m_pChild;
			};

			//!	親オブジェクトを取得する
			T* GetParent(void) const
			{
				return m_pParent;
			};

			//!	すべての兄弟について指定した関数を実行する
			template<class Function>
			void DoAtAllBrother(Function&& function)
			{
				T* pBrother = m_pBrother;
				while (pBrother != nullptr)
				{
					function(*pBrother);
					pBrother = pBrother->GetBrother();
				}
			};

			//!	すべての子について指定した関数を実行する
			template<class Function>
			void DoAtAllChild(Function&& function)
			{
				T* pChild = m_pChild;
				while (pChild != nullptr)
				{
					function(*pChild);
					pChild->DoAtAllBrother(
						[function](T& arg)
					{
						function(arg);
						arg.DoAtAllChild(function);
					});
					pChild = pChild->GetChild();
				}
			};

			//!	すべての親について指定した関数を実行する
			template<class Function>
			void DoAtAllParent(Function&& function)
			{
				T* pParent = m_pParent;
				while (pParent != nullptr)
				{
					function(*pParent);
					pParent = pParent->GetParent();
				}
			};

			//!	兄弟を持つか否かを取得する
			bool HasBrother(void) const
			{
				return m_pBrother != nullptr;
			};

			//!	子を持つか否かを取得する
			bool HasChild(void) const
			{
				return m_pChild != nullptr;
			};

			//!	親を持つか否かを取得する
			bool HasParent(void) const
			{
				return m_pParent != nullptr;
			};

		private:
			T*	m_pBrother;
			T*	m_pChild;
			T*	m_pParent;
			T*	m_pThis;
		};
	}
}

#endif	//MMLIBRARY_TAG_TREENODE_BASE_H_