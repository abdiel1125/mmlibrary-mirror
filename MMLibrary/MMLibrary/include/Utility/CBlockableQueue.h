/*!	\file		CBlockableQueue.h
*	\details	CBlockableQueueクラスの宣言
*	\author		松裏征志
*	\date		2014/10/23
*/
#ifndef MMLIBRARY_BLOCKABLE_QUEUE_H
#define MMLIBRARY_BLOCKABLE_QUEUE_H

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include <queue>
#include <mutex>

namespace MMLibrary
{
	namespace Utility
	{
		//!	スレッドセーフなキュー
		template<class T, template <class> class Allocator = USE_ALLOCATOR >
		class CBlockableQueue
		{
		public:
			CBlockableQueue(size_t size);

			void Push(T data);
			T Pop();
			unsigned int GetSize(){ return m_queue.size(); };
		private:
			std::queue<T, std::deque<T, Allocator<T> > > m_queue;
			std::mutex m_mutex;
			std::condition_variable m_pushwait;
			std::condition_variable m_popwait;
			int m_maxNum = 1;

		};

		template<class T, template <class> class Allocator>
		CBlockableQueue<T, Allocator>::CBlockableQueue(size_t size)
			:m_maxNum(size)
		{}

		template<class T, template <class> class Allocator>
		void CBlockableQueue<T, Allocator>::Push(T data)
		{
			std::unique_lock<std::mutex> lock(m_mutex);

			m_pushwait.wait(
				lock,
				[this]
			{ 
				return (m_queue.size() != m_maxNum);
			});

			m_queue.push(data);

			m_popwait.notify_one();
		}

		template<class T, template <class> class Allocator>
		T CBlockableQueue<T, Allocator>::Pop()
		{
			std::unique_lock<std::mutex> lock(m_mutex);

			m_popwait.wait(
				lock,
				[this]
			{
				return (m_queue.size() != 0); 
			});

			T data = m_queue.front();
			m_queue.pop();

			m_pushwait.notify_one();
			return data;
		}
	}

}

#endif // MMLIBRARY_BLOCKABLE_QUEUE_H