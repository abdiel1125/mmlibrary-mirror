/*!	\file		tagNonCopyable.h
*	\details	tagNonCopyable構造体の宣言・定義
*	\author		松裏征志
*	\date		2014/11/18
*/
#ifndef MMLIBRARY_TAGNONCOPYABLE_H_
#define MMLIBRARY_TAGNONCOPYABLE_H_

namespace MMLibrary
{
	namespace Utility
	{
		//!	この構造体を継承したクラスがコピーされないことを保証する
		struct tagNonCopyable
		{
			//!	コンストラクタ
			tagNonCopyable() = default;
			//!	コピーコンストラクタは削除する
			tagNonCopyable(const tagNonCopyable&) = delete;
			//!	代入演算子は削除する
			tagNonCopyable& operator=(const tagNonCopyable&) = delete;
		};
	}
}

#endif //MMLIBRARY_TAGNONCOPYABLE_H_