/*!	\file		CStringView.h
*	\details	CStringViewクラスの宣言
*	\author		松裏征志
*	\date		2014/12/03
*/
#ifndef MMLIBRARY_CSTRING_VIEW_H_
#define MMLIBRARY_CSTRING_VIEW_H_

//***********************************************************
//インクルード
//***********************************************************
#include <limits>
#include "MMLibraryDefine.h"
#include "Utility\Macro.h"
#include "Utility\DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!	char* を文字列とみなして様々な処理を定義する
		class CStringView
		{
		private:
			char*	m_pointer;
			size_t	m_size;

		public:
			static const size_t npos = UINT_MAX;

			//!	コンストラクタ
			CStringView(const char* pString)
				:m_pointer(const_cast<char*>(pString))
				, m_size(strlen(pString))
			{}

			//!	コンストラクタ
			CStringView(const char* pString, size_t size)
				:m_pointer(const_cast<char*>(pString))
				, m_size(size)
			{}
			
			//!	指定文字列を探す
			size_t Find(const CStringView& string, size_t end = npos, size_t offset = 0)
			{
				char* p = m_pointer + offset;

				while (*p != '\0' && (unsigned)(p - m_pointer) < end)
				{
					size_t length = 0;
					while (*(p + length) == string[length])
					{
						++length;
						if (length == string.GetSize())
							return GetDistance(p, m_pointer);
					}
					++p;
				}

				return npos;
			};

			const char& operator[](size_t index) const
			{
				return m_pointer[index];
			}

			char& operator[](size_t index)
			{
				return m_pointer[index];
			}

			CREATE_GETTER_PRIMITIVE(Size, m_size);
		};
	}
}

#include "Utility\UndefineClassCreateHelpers.h"
#endif //	MMLIBRARY_CSTRING_VIEW_H_
