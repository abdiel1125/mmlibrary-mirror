/*!	\file		Operator++--TemplateSet.h
*	\details	オペレーターを定義するテンプレート
*	\author		松裏征志
*	\date		2015/01/28
*/
#ifndef MMLIBRARY_OPERATOR_TEMPLATE_SET_H_
#define MMLIBRARY_OPERATOR_TEMPLATE_SET_H_

template<typename T>
T& operator++(T& value)
{
	value = static_cast<T>(value + 1);
	return value;
}

template<class T>
T operator++(T& value, int)
{
	T prev = value;
	++value;
	return prev;
}

template<class T>
T& operator--(T& value)
{
	value = static_cast<T>(value - 1);
	return value;
}

template<class T>
T operator--(T& value, int)
{
	T prev = value;
	--value;
	return prev;
}


#endif	// MMLIBRARY_OPERATOR_TEMPLATE_SET_H_