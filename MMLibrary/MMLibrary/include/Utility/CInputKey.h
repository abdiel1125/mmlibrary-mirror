/*!	\file		CInputKey.h
*	\details	CInputKeyクラスの宣言
*	\author		松裏征志
*	\date		2014/09/24
*/
#ifndef MMLIBRARY_CINPUT_KEY_H_
#define MMLIBRARY_CINPUT_KEY_H_

//***********************************************************
//インクルード
//***********************************************************
#include <memory>
#include <limits>
#include "MMLibraryDefine.h"
#include "CSingletonBase.h"

namespace	MMLibrary
{
	namespace	Utility
	{
		//!	指定した関数によってキーの押下状態を調べるクラス
		class CInputKey
		{
		private:
			ushort_t					m_frameCounter;
			std::function<bool(void)>	m_function;

		public:
			//!	GetDownCountの戻り値について特殊な値を設定する
			enum COUNT : ushort_t
			{
				COUNT_OVER	= USHRT_MAX - 1,
				UP_NOW		= USHRT_MAX
			};

			//!	コンストラクタ
			template<typename Function>
			CInputKey(Function&& function)
				: m_frameCounter(0)
				, m_function(function)
			{};

			//!	キーの押下状態を取得する関数を実行しメンバの値を更新する
			void	Check(void)
			{
				if (m_function())
				{
					if (IsUpNow())
					{
						m_frameCounter = 1;
					}
					else if (m_frameCounter != COUNT_OVER)
					{
						++m_frameCounter;
					}
				}
				else
				{
					if (IsUpNow())
					{
						m_frameCounter = 0;
					}
					if (IsDown())
					{
						m_frameCounter = UP_NOW;
					}
				}
			};
			//!	キーが押下された直後であるかを返す
			bool		IsDownNow(void) const
			{
				return m_frameCounter == 1;
			};

			//!	キーが押下されているかを返す
			bool		IsDown(void) const
			{
				return 
					m_frameCounter != 0 &&
					m_frameCounter != UP_NOW;
			};

			//!	キーが離された直後であるかを返す
			bool		IsUpNow(void) const
			{
				return m_frameCounter == UP_NOW;
			};

			//!	キーが押下されてからの更新回数を返す
			/*!
			*	COUNT_OVERならカウント上限以上、
			*	0なら押下されていない
			*/
			ushort_t	GetDownCount(void) const
			{
				return 
					(m_frameCounter == UP_NOW) ?
					0 :
					m_frameCounter;
			};
		};

		//! キーボードの各キーの押下状態を調べるクラス
		template<template<class> class Allocator = USE_ALLOCATOR >
		class CInputAllKey : public CSingletonBase<CInputAllKey<Allocator> >
		{
		public:
			//!	コンストラクタ
			CInputAllKey(void)
			{
				Allocator<CInputKey> allocator;
				using allocatorTraits = std::allocator_traits<Allocator<CInputKey> >;

				for (size_t i = 0; i < m_inputKeys.size(); ++i)
				{
					auto function = [=]()->bool{return (GetAsyncKeyState(i) & 0x8000) != 0; };
					auto* pointer = allocatorTraits::allocate(allocator, 1);

					allocatorTraits::construct(allocator, pointer, function);
					m_inputKeys[i].reset(pointer);
				}
			};

			//!	キーの押下状態を取得する関数を実行しメンバの値を更新する
			void		Check(void)
			{
				for (auto& inputKey : m_inputKeys)
					inputKey->Check();
			};

			//!	指定したキーが押下された直後であるかを返す
			bool		IsDownNow(uchar_t vkCode) const
			{
				return m_inputKeys.at(vkCode)->IsDownNow();
			};

			//!	指定したキーが押下されているかを返す
			bool		IsDown(uchar_t vkCode) const
			{
				return m_inputKeys.at(vkCode)->IsDown();
			};

			//!	指定したキーが離された直後であるかを返す
			bool		IsUpNow(uchar_t vkCode) const
			{
				return m_inputKeys.at(vkCode)->IsUpNow();
			};

			//!	指定したキーが押下されてからの更新回数を返す
			/*!
			*	COUNT_OVERならカウント上限以上、
			*	0なら押下されていない
			*/
			ushort_t	GetDownCount(uchar_t vkCode) const
			{
				return m_inputKeys.at(vkCode)->GetDownCount();
			};

		private:
			array_t<std::shared_ptr<CInputKey>, 256>	m_inputKeys;
		};
	};
};

#endif