/*!	\file		RangeAlgorithm.h
*	\details	標準ライブラリのアルゴリズムを範囲ベースにするラッパーの定義
*	\author		松裏征志
*	\date		2015/02/19
*/
#ifndef MMLIBRARY_RANGE_ALGORITHM_H_
#define MMLIBRARY_RANGE_ALGORITHM_H_
//===========================================================
//ファイルインクルード
//===========================================================
#include <algorithm>
#include <numeric>

namespace MMLibrary
{
	//!	標準ライブラリのアルゴリズムを範囲ベースにするラッパーはこの名前空間に属する
	namespace Range
	{
		//!	始点と終点を受け取りRangeAlgorithmsの関数で利用できる型にするためのアダプター
		template<class T>
		class CRangeAdaptor
		{
		private:
			T		m_begin;
			T		m_end;

		public:
			//!	コンストラクタ
			CRangeAdaptor(T&& begin, T&& end)
				: m_begin(begin)
				, m_end(end)
			{}

			//!	始点を取得
			T begin() const
			{
				return m_begin;
			}

			//!	終点を取得
			T end() const
			{
				return m_end;
			}
		};

		//!	関数
		//!	@{

		//!	探索
		//!	@{

		//!	指定した値と等しい要素を取得する
		template<class Container, class Itelator, class T>
		Itelator Find(Container& container, const T& sample)
		{
			return std::find(
				std::begin(container),
				std::end(container),
				sample);
		}

		//!	指定した値と等しい要素を取得する
		template<class Container, class Itelator, class T>
		Itelator Find(const Container& container, const T& sample)
		{
			return std::find(
				std::cbegin(container),
				std::cend(container),
				sample);
		}

		//!	指定した式がtrueを返す要素を取得する
		template<class Container, class Function>
		auto FindIf(Container& container, Function&& function)->
			decltype(
			std::find_if(
			std::begin(container),
			std::end(container),
			function))
		{
			return std::find_if(
				std::begin(container),
				std::end(container),
				function);
		}

		//!	指定した式がtrueを返す要素を取得する
		template<class Container, class Function>
		auto FindIf(const Container& container, Function&& function)->
			decltype(
			std::find_if(
			std::cbegin(container),
			std::cend(container),
			function))
		{
			return std::find_if(
				std::cbegin(container),
				std::cend(container),
				function);
		}
		//!	@}

		//!	合計を計算する
		template<class Container, class T>
		T Accumurate(const Container& container, T beginValue)
		{
			return std::accumulate(
				container.begin(), 
				container.end(), 
				beginValue);
		}

		//!	合計を計算する
		template<class Container, class T, class Function>
		T Accumurate(Container& container, T beginValue, Function&& function)
		{
			return std::accumulate(
				container.begin(),
				container.end(),
				beginValue,
				function);
		}

		//!	条件に合う要素を含んでいるかを取得する
		template<class Container, class Function>
		bool AnyOf(Container& container, Function&& function)
		{
			return std::any_of(
				container.cbegin(),
				container.cend(),
				function);
		}

		//!	要素をコピーする
		template<class Container, class Iterator>
		Iterator Copy(Container& container, Iterator iterator)
		{
			return std::copy(
				container.cbegin(),
				container.cend(),
				iterator);
		}

		//!	@}
	}
}

#endif	//	MMLIBRARY_RANGE_ALGORITHM_H_