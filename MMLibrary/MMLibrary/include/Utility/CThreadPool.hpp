/*!	\file		CThreadPool.hpp
*	\details	CThreadPoolNXÌÀ
*	\author		¼ ªu
*	\date		2014/11/04
*/
#ifndef MMLIBRARY_THREAD_POOL_HPP
#define MMLIBRARY_THREAD_POOL_HPP

//***********************************************************
//CN[h
//***********************************************************
#include "Utility\CThreadPool.h"

namespace MMLibrary
{
	namespace Utility
	{
		template<template <class> class Allocator>
		CThreadPool<Allocator>::CThreadPool(uint_t threadNum, uint_t maxTaskNum)
			: m_queue(maxTaskNum)
		{
			m_isEnd.store(false);

			m_threads.reserve(threadNum);
			for (uint_t i = 0; i < threadNum; ++i)
			{
				m_threads.emplace_back(
					Memory::NewWithAllocator<std::thread, Allocator>(
					[&]()
				{
					while (m_isEnd.load() == false)
					{
						if (m_queue.GetSize() > 0)
							m_queue.Pop()->Run();
					}
				}),
					Memory::DeleteWithAllocator<std::thread, Allocator>);
			}
		};

		template<template <class> class Allocator>
		CThreadPool<Allocator>::~CThreadPool()
		{
			m_isEnd.store(true);

			for (auto& pThread : m_threads)
				pThread->join();
		};

		template<template <class> class Allocator>
		template <class ReturnType, class Function>
		auto CThreadPool<Allocator>::Request(Function&& function)->std::future < ReturnType >
		{
			std::promise<std::result_of_t<Function(void)>> promise;

			m_queue.Push(std::unique_ptr<ITask>(CreateTaskWithAllocator<Allocator>(promise, [](){return function(void); })));

			return promise.GetFuture();
		};
	}
}

#endif // MMLIBRARY_THREAD_POOL_HPP