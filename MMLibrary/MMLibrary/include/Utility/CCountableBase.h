/*!	\file		CCountableBase.h
*	\details	CCountableBase構造体の宣言・定義
*	\author		松裏征志
*	\date		2014/12/09
*/
#ifndef MMLIBRARY_CCOUNTABLE_BASE_H_
#define MMLIBRARY_CCOUNTABLE_BASE_H_
//***********************************************************
//インクルード
//***********************************************************
#include "Utility\DefineClassCreateHelpers.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!	このクラスを継承したクラスには各インスタンスに固有のIDが割り振られる
		template<class T, class B>
		class CCountableBase : public B
		{
		private:
			static size_t m_instanceNum;
			static size_t m_nextID;

			size_t m_ID;

		public:
			//!	コンストラクタ
			CCountableBase()
				:m_ID(m_nextID)
			{
				++m_instanceNum;
				++m_nextID;
			}

			//!	デストラクタ
			~CCountableBase()
			{
				--m_instanceNum;

				if (m_instanceNum == 0)
					m_nextID = 0;
			}

			CREATE_GETTER_PRIMITIVE(ID, m_ID);

		};

		template<class T, class B>
		size_t CCountableBase<T,B>::m_instanceNum = 0;

		template<class T, class B>
		size_t CCountableBase<T, B>::m_nextID = 0;
	}
}

#include "Utility\UndefineClassCreateHelpers.h"
#endif //MMLIBRARY_CCOUNTABLE_BASE_H_