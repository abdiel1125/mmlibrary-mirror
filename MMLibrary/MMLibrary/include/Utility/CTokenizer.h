/*!	\file		CTokenizer.h
*	\details	CTokenizerクラスの宣言
*	\author		松裏征志
*	\date		2014/09/11
*/
#ifndef MMLIBRARY_CTOKENIZER_H_
#define MMLIBRARY_CTOKENIZER_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include <iostream>
#include <cstdlib>
#include <string>
#include <unordered_map>
#include "Utility\CStringView.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!	文字列から要素を順に取り出すためのクラス
		/*!
		*	\tparam	Allocator	アロケータを指定する
		*/
		template<template<class> class Allocator >
		class CTokenizer
		{
			typedef	string_t<Allocator> string_t;
		public:
			//!	コンストラクタ
			CTokenizer(void)
				:m_buffer(nullptr)
				, m_originalBuffer(nullptr)
				, m_originalBufferSize(0)
			{
				m_buffer = nullptr;
				m_separator.clear();
				m_cutOff.clear();
				m_token.reserve(64);
			};

			//!	デストラクタ
			virtual ~CTokenizer()
			{
				m_separator.clear();
				m_cutOff.clear();
				m_token.clear();
			};

			//!	区切り文字を設定する.
			void	SetSeparator(const string_t& pSeparator)
			{
				m_separator = string_t(pSeparator);
			};

			//!	切り出し文字を設定する
			void	SetCutOff(const string_t& pCutoff)
			{
				m_cutOff = string_t(pCutoff);
			};

			//!バッファへのポインタを設定する
			void	SetBuffer(char*	pBuffer, size_t bufferSize)
			{
				m_originalBuffer = pBuffer;
				m_buffer = pBuffer;
				m_originalBufferSize = bufferSize;
			};

			//!	メモリ解放処理
			void	Release(void)
			{
				m_separator.clear();
				m_cutOff.clear();
				m_token.clear();
				m_buffer = nullptr;
			};

			//!	次のトークンを取得する
			void	Next(void)
			{
				if (IsEnd()) return;

				char *p = m_buffer;
				m_token.clear();

				// 区切り文字はスキップする
				while (
					(m_originalBuffer == p || IsFirstCodeOfMultiByteCode(*(p - 2)) == true || IsFirstCodeOfMultiByteCode(*(p - 1)) == false) &&
					(*p) != '\0' && 
					strchr(m_separator.c_str(), (*p)))
				{
					p++;
				}

				// 切り出し文字とヒットするか判定
				if ((m_originalBuffer == p || IsFirstCodeOfMultiByteCode(*(p - 2)) == true || IsFirstCodeOfMultiByteCode(*(p - 1)) == false) &&
					strchr(m_cutOff.c_str(), (*p)))
				{
					//切り出し文字とヒットしたら，単体トークンとする
					m_token.push_back(*(p++));
				}
				else
				{
					//区切り文字または切り出し文字以外ならトークンとする
					string_t split = m_separator + m_cutOff;
					while (
						(*p) != '\0' &&
						!strchr(split.c_str(), (*p)))
					{
						m_token.push_back(*(p++));

						if (IsFirstCodeOfMultiByteCode(*(p - 1)))
						{
							m_token.push_back(*(p++));
						}
					}
				}

				//文字列として返すためにNULL終端文字を加える
				m_token.push_back('\0');

				//トークンにスキップ範囲の始点が含まれていればスキップ
				//但し、終点がある場合のみとする
				for (auto& skipRenge : m_skipRenges)
				{
					auto& begin = skipRenge.first;
					auto& end = skipRenge.second;
					CStringView token(m_buffer,m_originalBufferSize - (m_buffer - m_originalBuffer));
					size_t skipSize = token.Find(end.c_str());
					auto result = token.Find(begin.c_str(), std::min(skipSize, m_token.size() - 1));

					if (result != CStringView::npos)
					{
						m_buffer += skipSize;
						Next();
						return;
					}
				}

				//抜き出した分だけバッファを進める
				m_buffer = p;
			};

			//!	指定された文字列とトークンが一致しているか判定する
			bool	Check(const char *token)
			{
				return strcmp(m_token.data(), token) == 0;
			};

			//!	終了判定
			/*!
			*	バッファがNULL終端文字になった場合、trueを返却.
			*	バッファへのポインタがNULLの場合，trueを返却.
			*	上記以外の場合は，falseを返却.
			*/
			bool	IsEnd(void)
			{
				return (*m_buffer) == '\0' || m_buffer == nullptr;
			};

			//!	トークンをchar*型として返却する
			char*	GetAsChar(void)
			{
				return m_token.data();
			};

			//!	トークンをdouble型として返却する
			double	GetAsDouble(void)
			{
				return atof(m_token.data());
			};

			//!	トークンをfloat型として返却する
			float	GetAsFloat(void)
			{
				return static_cast<float>(atof(m_token.data()));
			};

			//!	トークンをint型として返却する
			int		GetAsInt(void)
			{
				return atoi(m_token.data());
			};

			//!	次のトークンを取得し，char*型として返却する
			char*	GetNextAsChar(void)
			{
				Next();
				return GetAsChar();
			};

			//!	次のトークンを取得し，double型として返却する
			double  GetNextAsDouble(void)
			{
				Next();
				return GetAsDouble();
			};

			//!	次のトークンを取得し，float型として返却する
			float	GetNextAsFloat(void)
			{
				Next();
				return GetAsFloat();
			};

			//!	次のトークンを取得し，int型として返却する
			int		GetNextAsInt(void)
			{
				Next();
				return GetAsInt();
			};

			//!	スキップする範囲の始点と終点となる文字列を設定する
			void SetSkipRenge(const string_t& begin, const string_t& end)
			{
				m_skipRenges.emplace_back(std::make_pair(begin, end));
				m_skipRenges.shrink_to_fit();
			}

		private:
			char*							m_buffer;
			char*							m_originalBuffer;
			size_t							m_originalBufferSize;
			vector_t<char, Allocator >		m_token;
			string_t						m_separator;
			string_t						m_cutOff;
			vector_t<std::pair<string_t, string_t>, Allocator>	m_skipRenges;
		};
	};
};

#endif