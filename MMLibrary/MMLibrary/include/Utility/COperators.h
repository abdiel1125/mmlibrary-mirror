/*!	\file		COperators.h
*	\details	COperatorsクラスの宣言
*	\author		松裏征志
*	\date		2014/11/12
*/
#ifndef MMLIBRARY_COPERATORS_H_
#define MMLIBRARY_COPERATORS_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"

namespace	MMLibrary
{
	namespace Utility
	{
		//!	Barton-Nackman trickによって非等価演算子を自動生成するクラス
		template<class T, class B = tagEmptyBase>
		struct tagEqualComparable : B
		{
			//!	leftがrightと等しくないかを取得する
			friend bool operator!=(const T& left, const T& right)
			{
				return !(left == right);
			}
		};

		//!	Barton-Nackman trickによって比較演算子を自動生成するクラス
		template<class T, class B = tagEmptyBase>
		struct tagLessThanComparable : B
		{
			//!	leftがright以上であるかを取得する
			friend bool operator>=(const T& left, const T& right)
			{
				return !(left < right);
			}

			//!	leftがright以下であるかを取得する
			friend bool operator<=(const T& left, const T& right)
			{
				return !(right < left);
			}

			//!	leftがrightより大きいかを取得する
			friend bool operator>(const T& left, const T& right)
			{
				return right < left;
			}
		};

		//!	Barton-Nackman trickによって加算演算子を自動生成するクラス
		template<class T, class B = tagEmptyBase>
		struct tagCanAdditionAssignment : B
		{
			//!	加算を行う
			friend T operator+(const T& left, const T& right)
			{
				return T(left) += (right);
			}
		};
		//!	Barton-Nackman trickによって加算演算子を自動生成するクラス
		template<class lT, class rT, class B = tagEmptyBase>
		struct tagCanAdditionAssignment2nd : B
		{
			//!	加算を行う
			friend lT operator+(const lT& left, const rT& right)
			{
				return lT(left) += (right);
			}

			//!	加算を行う
			friend lT operator+(const rT& right, const lT& left)
			{
				return lT(left) += (right);
			}
		};

		//!	Barton-Nackman trickによって減算演算子を自動生成するクラス
		template<class T, class B = tagEmptyBase>
		struct tagCanSubtractionAssignment : B
		{
			//!	減算を行う
			friend T operator-(const T& left, const T& right)
			{
				return T(left) -= (right);
			}
		};
		//!	Barton-Nackman trickによって減算演算子を自動生成するクラス
		template<class lT, class rT, class B = tagEmptyBase>
		struct tagCanSubtractionAssignment2nd : B
		{
			//!	減算を行う
			friend lT operator-(const lT& left, const rT& right)
			{
				return lT(left) -= (right);
			}

			//!	減算を行う
			friend lT operator-(const rT& right, const lT& left)
			{
				return lT(left) -= (right);
			}
		};

		//!	Barton-Nackman trickによって乗算演算子を自動生成するクラス
		template<class T, class B = tagEmptyBase>
		struct tagCanMultiplicationAssignment : B
		{
			//!	乗算を行う
			friend T operator*(const T& left, const T& right)
			{
				return T(left) *= (right);
			}
		};
		//!	Barton-Nackman trickによって乗算演算子を自動生成するクラス
		template<class lT, class rT, class B = tagEmptyBase>
		struct tagCanMultiplicationAssignment2nd : B
		{
			//!	乗算を行う
			friend lT operator*(const lT& left, const rT& right)
			{
				return lT(left) *= (right);
			}

			//!	乗算を行う
			friend lT operator*(const rT& right, const lT& left)
			{
				return lT(left) *= (right);
			}
		};

		//!	Barton-Nackman trickによって除算演算子を自動生成するクラス
		template<class T, class B = tagEmptyBase>
		struct tagCanDivisionAssignment : B
		{
			//!	除算を行う
			friend T operator/(const T& left, const T& right)
			{
				return T(left) /= (right);
			}
		};
		//!	Barton-Nackman trickによって除算演算子を自動生成するクラス
		template<class lT, class rT, class B = tagEmptyBase>
		struct tagCanDivisionAssignment2nd : B
		{
			//!	除算を行う
			friend lT operator/(const lT& left, const rT& right)
			{
				return lT(left) /= (right);
			}

			//!	除算を行う
			friend lT operator/(const rT& right, const lT& left)
			{
				return lT(left) /= (right);
			}
		};

		//!	Barton-Nackman trickによって加減乗除の演算子を自動生成するクラス
		template<class T, class B = tagEmptyBase>
		struct tagCalculateOperators :
			public 
			tagCanAdditionAssignment < T,
			tagCanSubtractionAssignment < T,
			tagCanMultiplicationAssignment < T,
			tagCanDivisionAssignment < T, B > > > >
		{};
		//!	Barton-Nackman trickによって加減乗除の演算子を自動生成するクラス
		template<class lT, class rT, class B = tagEmptyBase>
		struct tagCalculateOperators2nd :
			public 
			tagCanAdditionAssignment2nd < lT, rT,
			tagCanSubtractionAssignment2nd < lT, rT,
			tagCanMultiplicationAssignment2nd < lT, rT,
			tagCanDivisionAssignment2nd < lT, rT, B > > > >
		{};

		//!	CRTPによるAddメソッドの追加
		template<class T, class B = tagEmptyBase>
		struct tagInjectAddMethod : B
		{
		public:
			//!	加算を行う
			T& Add(const T& src)
			{
				return *this += src;
			}
		};
		//!	CRTPによるAddメソッドの追加
		template<class T, class U, class B = tagEmptyBase>
		struct tagInjectAddMethod2nd : B
		{
		public:
			//!	加算を行う
			T& Add(const U& src)
			{
				return *this += src;
			}
		};

		//!	CRTPによるSubtractメソッドの追加
		template<class T, class B = tagEmptyBase>
		struct tagInjectSubtractMethod : B
		{
		public:
			//!	減算を行う
			T& Subtract(const T& src)
			{
				return *this -= src;
			}
		};
		//!	CRTPによるSubtractメソッドの追加
		template<class T, class U, class B = tagEmptyBase>
		struct tagInjectSubtractMethod2nd : B
		{
		public:
			//!	減算を行う
			T& Subtract(const U& src)
			{
				return *this -= src;
			}
		};


		//!	CRTPによるMultiplyメソッドの追加
		template<class T, class B = tagEmptyBase>
		struct tagInjectMultiplyMethod : B
		{
		public:
			//!	乗算を行う
			T& Multiply(const T& src)
			{
				return *this *= src;
			}
		};
		//!	CRTPによるMultiplyメソッドの追加
		template<class T, class U, class B = tagEmptyBase>
		struct tagInjectMultiplyMethod2nd : B
		{
		public:
			//!	乗算を行う
			T& Multiply(const U& src)
			{
				return *this *= src;
			}
		};

		//!	CRTPによるDivisionメソッドの追加
		template<class T, class B = tagEmptyBase>
		struct tagInjectDivisionMethod : B
		{
		public:
			//!	除算を行う
			T& Division(const T& src)
			{
				return *this /= src;
			}
		};
		//!	CRTPによるDivisionメソッドの追加
		template<class T, class U, class B = tagEmptyBase>
		struct tagInjectDivisionMethod2nd : B
		{
		public:
			//!	除算を行う
			T& Division(const U& src)
			{
				return *this /= src;
			}
		};


		//!	CRTPによる演算メソッドの追加
		template<class T, class B = tagEmptyBase>
		struct tagInjectCalculateMethods :
			public
			tagInjectAddMethod < T,
			tagInjectSubtractMethod < T,
			tagInjectMultiplyMethod < T,
			tagInjectDivisionMethod < T, B > > > >
		{};
		//!	CRTPによる演算メソッドの追加
		template<class T, class U, class B = tagEmptyBase>
		struct tagInjectCalculateMethods2nd :
			public
			tagInjectAddMethod2nd < T, U,
			tagInjectSubtractMethod2nd < T, U,
			tagInjectMultiplyMethod2nd < T, U,
			tagInjectDivisionMethod2nd < T, U, B > > > >
		{};
	}
}

#endif // MMLIBRARY_COPERATORS_H_