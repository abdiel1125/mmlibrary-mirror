/*!	\file		CFlyWeight.h
*	\details	CFlyWeightクラスの宣言・定義
*	\author		松裏征志
*	\date		2014/11/19
*/
#ifndef MMLIBRARY_CFLYWEIGHT_H_
#define MMLIBRARY_CFLYWEIGHT_H_
//***********************************************************
//インクルード
//***********************************************************
#include <unordered_map>
#include <memory>
#include <tuple>
#include "CSingletonBase.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!	コンストラクタに渡す引数と、その結果作成されるインスタンスの組を保存するクラス
		template<class T, class Arg>
		class CFlyWeight : public CSingletonBase<CFlyWeight<T, Arg> >
		{
		public:
			//!	引数からインスタンスのshared_ptrを取得する
			std::shared_ptr<T> Get(const Arg& arg)
			{
				auto it = m_pool.find(arg);

				if (it == m_pool.end())
				{
					auto p = std::make_shared<T>(arg);
					m_pool.insert(std::make_pair(arg, p));
					return p;
				}

				return it->second;
			};

			//!	引数からインスタンスのshared_ptrを取得する
			std::shared_ptr<T> Get(Arg&& arg)
			{
				auto it = m_pool.find(arg);

				if (it == m_pool.end())
				{
					return m_pool.insert(std::make_pair(arg, std::make_shared<T>(arg))).first->second;
				}
				 
				return it->second;
			};

		private:
			std::unordered_map<Arg, std::shared_ptr<T> > m_pool;
		};
	}
}
#endif //MMLIBRARY_CFLYWEIGHT_H_