/*!	\file		COperatorExtender.h
*	\details	COperatorExtenderクラスの宣言
*	\author		松裏征志
*	\date		2014/07/11
*/
#ifndef MMLIBRARY_COPERATOR_EXTENDER_H_
#define MMLIBRARY_COPERATOR_EXTENDER_H_

//***********************************************************
//インクルード
//***********************************************************

namespace MMLibrary
{
	namespace Utility
	{
		/*!
		*	\brief	演算子を流用したメソッドを追加するためのクラス
		*
		*	\tparam	T		メソッドを追加する対象のクラス
		*	\tparam	T		メソッドの戻り値となるクラス
		*/
		template<class T, class ReturnType = T>
		class COperatorExtender : public T
		{
		public:
			/*!
			*	\brief	ReturnTypeへのキャスト演算子
			*/
			operator ReturnType(void)
			{
				return *reinterpret_cast<ReturnType*>this;
			};

			/*!
			*	\brief	自身のコピーを返す
			*/
			ReturnType ToClone(void) const
			{
				return *static_cast<ReturnType*>(this);
			};

			/*!
			*	\brief	ReturnTypeへの代入を定義する
			*/
			ReturnType& operator=(const T& src)
			{
				static_cast<ReturnType&>(*this) = src;

				return *this;
			}

			/*!
			*	\brief	加算代入演算メソッド
			*/
			template<class U>
			ReturnType& Add(const U& src)
			{
				*this += src;
				return *this;
			};

			/*!
			*	\brief	減算代入演算メソッド
			*/
			template<class U>
			ReturnType& Subtract(const U& src)
			{
				*this -= src;
				return *this;
			};

			/*!
			*	\brief	乗算代入演算メソッド
			*/
			template<class U>
			ReturnType& Multiply(const U& src)
			{
				*this *= src;
				return *this;
			};

			/*!
			*	\brief	除算代入演算メソッド
			*/
			template<class U>
			ReturnType& Division(const U& src)
			{
				*this /= src;
				return *this;
			};
		};

		/*!
		*	\brief	COperatorExtenderクラスを作成する関数
		*/
		template<class T>
		COperatorExtender<T>& Extend(T& src)
		{
			return (COperatorExtender<T>&)src;
		}
	};
};

#endif