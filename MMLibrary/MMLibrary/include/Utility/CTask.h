/*!	\file		CTask.h
*	\details	CTaskクラスの宣言
*	\author		松裏征志
*	\date		2014/10/25
*/
#ifndef MMLIBRARY_TASK_H
#define MMLIBRARY_TASK_H

//***********************************************************
//インクルード
//***********************************************************
#include "Utility\Apply.h"
#include <future>
#include <type_traits>

namespace MMLibrary
{
	namespace Utility
	{
		//!	タスククラスの基底クラス
		class ITask
		{
		public:
			//!	デストラクタ
			virtual ~ITask(){};

			//!	タスクの実行
			virtual void Run(void) = 0;
		};

		//!	戻り値を持つタスクを格納するクラス
		template<class ReturnType>
		class CTask : public ITask
		{
		public:
			//!	コンストラクタ
			template<class... Args>
			CTask(
				std::promise<ReturnType>&& promise,
				std::function<ReturnType(Args...)>& function,
				const Args&... args);

			void Run(void) override final;

		private:
			std::promise<ReturnType> m_promise;
			std::function<ReturnType(void)>	m_function;
		};

		template<class ReturnType>
		template<class... Args>
		CTask<ReturnType>::CTask(
			std::promise<ReturnType>&& promise,
			std::function<ReturnType(Args...)>& function,
			const Args&... args)
			:m_promise(std::forward<std::promise<ReturnType>>(promise))
			, m_function(std::function<ReturnType(void)>(function, args...))
		{
		}

		template<class ReturnType>
		void CTask<ReturnType>::Run()
		{
			m_promise.set_value(m_function());
		}

		//!	戻り値を持たないタスクを格納するクラス
		class CTaskReturnVoid : public ITask
		{
		public:
			//!	コンストラクタ
			template<class... Args>
			CTaskReturnVoid(
				std::promise<bool>&& promise,
				std::function<void(Args...)>& function,
				const Args&... args);
			void Run() override final;

		private:
			std::promise<bool> m_promise;
			std::function<void(void)>	m_function;
		};

		template<class... Args>
		CTaskReturnVoid::CTaskReturnVoid(
			std::promise<bool>&& promise,
			std::function<void(Args...)>& function,
			const Args&... args)
			:m_promise(std::forward<std::promise<bool>>(promise))
			, m_function(std::function<void(void)>(function, args...))
		{
		}

		//!	戻り値を持つタスクを格納するクラスを作成する
		template<class Function, class ReturnType, class... Args>
		auto CreateTask(
			std::promise<ReturnType>&& promise,
			Function& function,
			const Args&... args) -> CTask < ReturnType >
		{
			return CTask<ReturnType>(std::forward<std::promise<ReturnType>>(promise), std::function<ReturnType(void)>(std::bind(function, args...)));
		};

		//!	戻り値を持たないタスクを格納するクラスを作成する
		template<class Function, class... Args>
		auto CreateTask(
			std::promise<bool>&& promise,
			Function& function,
			const Args&... args) ->
			CTaskReturnVoid
		{
			return CTaskReturnVoid(std::forward<std::promise<bool>>(promise), std::function<void(void)>(std::bind(function, args...)));
		};

		//!	アロケータと引数を使って戻り値を持つタスクを格納するクラスを作成する
		template<template<class>class Allocator = USE_ALLOCATOR, class Function, class ReturnType, class... Args>
		auto CreateTaskWithAllocator(
			std::promise<ReturnType>&& promise,
			Function& function,
			Args&&... args) ->
			ITask*
		{
			Allocator<CTask<ReturnType>> useAllocator;
			using traits = std::allocator_traits < Allocator<CTask<ReturnType>> >;

			CTask<ReturnType>* pointer = traits::allocate(useAllocator, 1);
			traits::construct(useAllocator, pointer, std::forward<std::promise<ReturnType>>(promise), std::function<ReturnType(void)>(std::bind(function, args...)));

			return pointer;
		};

		//!	アロケータを使って戻り値を持つタスクを格納するクラスを作成する
		template<template<class>class Allocator = USE_ALLOCATOR, class Function, class ReturnType>
		auto CreateTaskWithAllocator(
			std::promise<ReturnType>&& promise,
			Function& function) ->
			ITask*
		{
			Allocator<CTask<ReturnType>> useAllocator;
			using traits = std::allocator_traits < Allocator<CTask<ReturnType>> >;

			CTask<ReturnType>* pointer = traits::allocate(useAllocator, 1);
			traits::construct(useAllocator, pointer, std::forward<std::promise<ReturnType>>(promise), function);

			return pointer;
		};

		//!	アロケータと引数を使って戻り値を持たないタスクを格納するクラスを作成する
		template<template<class>class Allocator = USE_ALLOCATOR, class Function, class... Args>
		auto CreateTaskWithAllocator(
			std::promise<bool>&& promise,
			Function& function,
			Args&&... args) ->
			ITask*
		{
			Allocator<CTaskReturnVoid> useAllocator;
			using traits = std::allocator_traits < Allocator<CTaskReturnVoid> >;

			CTaskReturnVoid* pointer = traits::allocate(useAllocator, 1);
			traits::construct(useAllocator, pointer, std::forward<std::promise<bool>>(promise), std::function<void(void)>(std::bind(function, args...)));

			return pointer;
		};

		//!	アロケータを使って戻り値を持たないタスクを格納するクラスを作成する
		template<template<class>class Allocator = USE_ALLOCATOR, class Function>
		auto CreateTaskWithAllocator(
			std::promise<bool>&& promise,
			Function& function) ->
			ITask*
		{
			Allocator<CTaskReturnVoid> useAllocator;
			using traits = std::allocator_traits < Allocator<CTaskReturnVoid> > ;

			CTaskReturnVoid* pointer = traits::allocate(useAllocator, 1);
			traits::construct(useAllocator, pointer, std::forward<std::promise<bool>>(promise), std::function<void(void)>(function));

			return pointer;
		};

	}
}
#endif // !MMLIBRARY_TASK_H
