/*!	\file		CINILoader.h
*	\details	CINILoaderクラスの宣言
*	\author		松裏征志
*	\date		2014/12/16
*/
#ifndef MMLIBRARY_CINI_LOADER_H
#define MMLIBRARY_CINI_LOADER_H

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility/Macro.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!	iniファイルを読み込むクラス
		template<template <class> class Allocator = USE_ALLOCATOR >
		class CINILoader
		{
			using section_t = unordered_map_t < string_t<Allocator>, string_t<Allocator>, Allocator > ;
			template<class T>
			using optionalPair_t = std::pair < bool, T >;
		private:
			unordered_map_t<string_t<Allocator>, section_t, Allocator>	m_data;
			string_t<Allocator>											m_filePath;

			void LoadFromFile()
			{
				vector_t<char, Allocator> m_memory;
				Utility::LoadFileTo<char, Allocator>(m_filePath, m_memory, true);

				Utility::CTokenizer<Allocator> token;

				token.SetBuffer(m_memory.data(),m_memory.size());
				token.SetSeparator(" \t\r\n,;\"");
				token.SetCutOff("=[]");
				token.SetSkipRenge(";", "\n");
				token.Next();

				string_t<Allocator> nowSectionName;
				while (!token.IsEnd())
				{
					if (token.Check("["))
					{
						nowSectionName = token.GetNextAsChar();
						token.Next();
						assert(token.Check("]") && "Section name is not end.");
					}
					else
					{
						string_t<Allocator> parameterName = token.GetAsChar();
						token.Next();
						assert(token.Check("=") && "Where is = ?");
						string_t<Allocator> parameter = token.GetNextAsChar();

						m_data[nowSectionName].insert(std::make_pair(parameterName, parameter));
					}
					token.Next();
				}

			};

			void LoadLine(std::ifstream& file);
		public:
			//!	コンストラクタ
			CINILoader(){};

			//!	コンストラクタ
			CINILoader(const string_t<Allocator>& filePath)
			:m_filePath(filePath)
			{
				LoadFromFile();
			};

			//!	INIファイルのロード
			void LoadFromFile(const string_t<Allocator>& filePath)
			{
				m_data.clear();
				m_filePath = filePath;
				LoadFromFile();
			};

			//! データを文字列として取得
			optionalPair_t<string_t<Allocator>> GetParameterAsString(const string_t<Allocator>& parameterName)
			{
				for (auto& pair : m_data)
				{
					section_t& section = pair.second;
					auto parameterIt = section.find(parameterName);
					
					if (parameterIt != section.end())
						return std::make_pair(true, parameterIt->second);
				}

				return std::make_pair(false, "");
			};

			//! データをintとして取得
			optionalPair_t<int> GetParameterAsInt(const string_t<Allocator>& parameterName)
			{
				auto strOpt = GetParameterAsString(parameterName);

				if (strOpt.first) return std::make_pair(true, strtol(strOpt.second.c_str(), nullptr, 0));
				return std::make_pair(false, 0);
			};

			//! データをfloatとして取得
			optionalPair_t<float> GetParameterAsFloat(const string_t<Allocator>& parameterName)
			{
				auto strOpt = GetParameterAsString(parameterName);

				if (strOpt.first) return std::make_pair(true, strtof(strOpt.second.c_str(), nullptr));
				return std::make_pair(false, 0.0f);
			};

			//! データをboolとして取得
			optionalPair_t<bool> GetParameterAsBoolean(const string_t<Allocator>& parameterName)
			{
				auto strOpt = GetParameterAsString(parameterName);

				if (strOpt.first) 
					return std::make_pair(true, (strOpt.second == "true" ? true : false));
				return std::make_pair(false, false);
			};

			//! セクションを指定し、データを文字列として取得
			optionalPair_t<string_t<Allocator> > GetParameterAsString(
				const string_t<Allocator>& sectionName,
				const string_t<Allocator>& parameterName)
			{
				auto sectionIt = m_data.find(sectionName);

				if (sectionIt == m_data.end()) 
					return std::make_pair(false, "");

				auto parameterIt = sectionIt->second.find(parameterName);

				if (parameterIt == sectionIt->second.end()) 
					return std::make_pair(false, "");
				
				return std::make_pair(true, parameterIt->second);
			};

			//! セクションを指定し、データをintとして取得
			optionalPair_t<int> GetParameterAsInt(
				const string_t<Allocator>& sectionName,
				const string_t<Allocator>& parameterName)
			{
				auto strOpt = GetParameterAsString(sectionName, parameterName);

				if (strOpt.first) return std::make_pair(true, strtol(strOpt.second.c_str(), nullptr, 0));
				return std::make_pair(false, 0);
			};

			//! セクションを指定し、データをfloatとして取得
			optionalPair_t<float> GetParameterAsFloat(
				const string_t<Allocator>& sectionName,
				const string_t<Allocator>& parameterName)
			{
				auto strOpt = GetParameterAsString(sectionName, parameterName);

				if (strOpt.first) 
					return std::make_pair(true, strtof(strOpt.second.c_str(), nullptr));
				return std::make_pair(false, 0.0f);
			};

			//! セクションを指定し、データをboolとして取得
			optionalPair_t<bool> GetParameterAsBoolean(
				const string_t<Allocator>& sectionName,
				const string_t<Allocator>& parameterName)
			{
				auto strOpt = GetParameterAsString(sectionName, parameterName);

				if (strOpt.first)
					return std::make_pair(true, (strOpt.second == "true" ? true : false));
				return std::make_pair(false, false);
			};
		};
	}
}

#endif	//	MMLIBRARY_CINI_LOADER_H