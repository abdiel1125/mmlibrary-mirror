/*!	\file		CDoAtDestruct.h
*	\details	CDoAtDestructクラスの定義
*	\author		松裏征志
*	\date		2015/03/03
*/
#ifndef MMLIBRARY_CDO_AT_DESTRUCT_H_
#define MMLIBRARY_CDO_AT_DESTRUCT_H_
//===========================================================
//ファイルインクルード
//===========================================================
#include <functional>


namespace	MMLibrary
{
	namespace Utility
	{
		//!	指定した処理をデストラクト時に行うクラス
		class CDoAtDestruct
		{
		private:
			std::function<void(void)> m_function;

		public:
			//!	コンストラクタ
			CDoAtDestruct(void)
				:m_function()
			{}

			//!	コンストラクタ
			template<class Function>
			CDoAtDestruct(Function&& function)
				:m_function(function)
			{}

			//!	ムーブコンストラクタ
			CDoAtDestruct(CDoAtDestruct&& other)
			{
				other.m_function.swap(m_function);
			}

			//!	デストラクタ
			~CDoAtDestruct()
			{
				if (m_function)
					m_function();
			}
		};
	}
}


#endif //	MMLIBRARY_CDO_AT_DESTRUCT_H_