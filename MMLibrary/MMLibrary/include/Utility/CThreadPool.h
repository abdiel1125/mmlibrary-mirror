/*!	\file		CThreadPool.h
*	\details	CThreadPoolクラスの宣言
*	\author		松裏征志
*	\date		2014/10/23
*/
#ifndef MMLIBRARY_THREAD_POOL_H
#define MMLIBRARY_THREAD_POOL_H

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include "Utility\CBlockableQueue.h"
#include "Utility\CTask.h"
#include "Memory\Memory.h"
#include <thread>
#include <future>

namespace MMLibrary
{
	namespace Utility
	{
		//!	スレッドをはじめに作成しておき、与えられたタスクを順次実行していくクラス
		template<template <class> class Allocator = USE_ALLOCATOR >
		class CThreadPool
		{
		public:
			//!	コンストラクタ
			CThreadPool(uint_t threadNum = 1, uint_t maxTaskNum = 10);

			//!	デストラクタ
			~CThreadPool();

			//!	タスクの登録
			template <class ReturnType, class Function>
			auto Request(Function&& function)->
				std::future<ReturnType>;

			//!	未実行のタスクがあるかを取得する
			bool isEmpty(){ return m_queue.GetSize() == 0; };

		private:
			CBlockableQueue<std::shared_ptr<ITask>, Allocator>	m_queue;
			vector_t<std::shared_ptr<std::thread>, Allocator>	m_threads;
			std::atomic_bool									m_isEnd;
		};
	}
}

#include "CThreadPool.hpp"

#endif // MMLIBRARY_THREAD_POOL_H