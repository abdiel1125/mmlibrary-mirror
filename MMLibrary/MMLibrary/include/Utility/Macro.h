/*!	\file		Macro.h
*	\details	MMLibraryで使用するマクロやちょっとしたクラス等の宣言
*	\author		松裏征志
*	\date		2014/04/19
*/
#ifndef MMLIBRARY_MACRO_H_
#define MMLIBRARY_MACRO_H_

//***********************************************************
//インクルード
//***********************************************************
#include "MMLibraryDefine.h"
#include <functional>

namespace MMLibrary
{
	//!	MMLibraryのすべての便利なクラス・関数はこの名前空間に属する
	namespace Utility
	{
		//!	ファイルの大きさを取得する
		size_t GetFileSize(std::ifstream& file);

		//!	与えられた値がShift-JISの2バイト文字の1バイト目であるかを取得する
		bool IsFirstCodeOfMultiByteCode(char code);

		//!	与えられた値をUTF8のマルチバイト文字の1バイト目とした時の文字のバイト数を返す
		/*!
		*	マルチバイト文字の1バイト目でない場合は-1を返す
		*/
		int GetCodeNumByFirstCodeOfUTF8(wchar_t code);

		//!	与えられた値をuint_tに格納する
		//!	@{
		uint_t Packing(wchar_t code);
		uint_t Packing(wchar_t code1, wchar_t code2);
		uint_t Packing(wchar_t code1, wchar_t code2, wchar_t code3);
		uint_t Packing(wchar_t code1, wchar_t code2, wchar_t code3, wchar_t code4);

		uint_t Packing(char code);
		uint_t Packing(char code1, char code2);
		uint_t Packing(char code1, char code2, char code3);
		uint_t Packing(char code1, char code2, char code3, char code4);
		//!	@}
	};
};

#include "Macro.hpp"
#include "COperatorExtender.h"
#include "Utility\CTokenizer.h"
#include "Utility\CInputKey.h"
#include "Utility\CDataContainer.h"
#include "Utility\CBlockableQueue.h"
#include "Utility\Apply.h"
#include "Utility\CTask.h"
#include "Utility\CSingletonBase.h"
#include "Utility\COperators.h"
#include "Utility\CFlyWeight.h"
#include "Utility\CINILoader.h"
//#include "Utility\CThreadPool.h" // ToDo:未完成のためインクルード不可
#endif