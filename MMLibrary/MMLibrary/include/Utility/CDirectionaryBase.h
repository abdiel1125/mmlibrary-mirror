/*!	\file		CDirectionaryBase.h
*	\details	CDirectionaryBaseクラスの宣言
*	\author		松裏征志
*	\date		2014/12/12
*/
#ifndef MMLIBRARY_CDIRECTIONARY_BASE_H_
#define MMLIBRARY_CDIRECTIONARY_BASE_H_
//***********************************************************
//インクルード
//***********************************************************
#include <functional>
#include <mutex>
#include "MMLibraryDefine.h"
#include "tagNonCopyable.h"

namespace MMLibrary
{
	namespace Utility
	{
		//!	名前検索可能なオブジェクトリストの基底クラス
		template<class Data, template<class> class Allocator = USE_ALLOCATOR>
		class CDirectionaryBase
		{
		private:
			unordered_map_t<string_t<Allocator>, Data, Allocator>	m_data;
			std::function<Data(const string_t<Allocator>&)>			m_creator;


			std::recursive_mutex									m_mutex;

			std::unique_lock<std::recursive_mutex> GetLock()
			{
				return std::unique_lock<std::recursive_mutex>(m_mutex);
			}

		public:
			using pairType = std::pair < string_t<Allocator>, Data > ;

			//!	コンストラクタ
			template<class Function>
			CDirectionaryBase(Function&& creator)
			:m_creator(creator){};

			//!	名前からオブジェクトを取得する
			Data	Request(const string_t<Allocator>&	name)
			{
				auto lock = GetLock();

				if (IsRegistered(name))
				{
					return m_data[name];
				}
				else
				{
					Data data = m_creator(name);

					Register(name, data);

					return data;
				}
			};

			//!	オブジェクトを登録する
			bool	Register(
				const string_t<Allocator>&	name,
				Data		data)
			{
				auto lock = GetLock();

				if (IsRegistered(name)) return false;

				m_data.insert(std::make_pair(name, data));
				string_t<> str = "CDirectionaryBase::Register(";
				str += name;
				str += ")\n";
				OutputDebugString(str.c_str());
				return true;
			};

			//!	オブジェクトのファイルパスを指定してオブジェクトを開放する
			void	Dispose(const string_t<Allocator>&		name)
			{
				auto lock = GetLock();

				auto it = Range::FindIf(
					m_data,
					[&](const pairType& pair)
				{
					return pair.first == name;
				});

				if (it != m_data.end())
				{
					m_data.erase(it);
					string_t<> str = "CDirectionaryBase::Dispose(";
					str += name;
					str += ")\n";
					OutputDebugString(str.c_str());
				}
			};

			//!	全てのオブジェクトを開放する
			void	DisposeAll(void)
			{
				auto lock = GetLock();

				OutputDebugString("CDirectionaryBase::DisposeAll\n");
				m_data.clear();
			};

			//!	指定した名前のオブジェクトが登録されているかを取得する
			bool IsRegistered(const string_t<Allocator>& name)
			{
				auto lock = GetLock();

				return m_data.find(name) != m_data.end();
			};

			//! 登録数を取得する
			size_t GetRegisteredSize()
			{
				auto lock = GetLock();

				return m_data.size();
			}
		};
	}
}

#endif	//	MMLIBRARY_CDIRECTIONARY_BASE_H_