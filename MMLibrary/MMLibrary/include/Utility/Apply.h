/*!	\file		Apply.h
*	\details	ApplyÖÌé¾
*	\author		¼ ªu
*	\date		2014/10/25
*/
#ifndef MMLIBRARY_APPLY_H
#define MMLIBRARY_APPLY_H

//***********************************************************
//CN[h
//***********************************************************
#include <tuple>
#include "Utility\Macro.hpp"

namespace MMLibrary
{
	namespace Utility
	{
		/*	http://d.hatena.ne.jp/redboltz/20110811/1313024577ÌÀ
		template <class Function, class... Args, class... UnpackedArgs>
		typename std::enable_if<
			sizeof...(UnpackedArgs) == sizeof...(Args),
			typename std::function<Function>::result_type>::type
			apply_impl(Function& fun, std::tuple<Args...>& args, UnpackedArgs*... unpackedArgs)
		{
			return fun(*unpackedArgs...);
		}

		template <class Function, class... Args, class... UnpackedArgs>
		typename std::enable_if<
			sizeof...(UnpackedArgs) < sizeof...(Args),
			typename std::function<Function>::result_type::type>::type
			apply_impl(Function& fun, std::tuple<Args...>& args, UnpackedArgs*... unpackedArgs)
		{
			return apply_impl(fun, args, unpackedArgs..., std::get<sizeof...(UnpackedArgs)>(args));
		}

		template <class Function, class... Args>
		typename std::function<Function>::result_type
			Apply(Function& fun, std::tuple<Args...>& args)
		{
			return apply_impl(fun, args);
		}
		*/

		//http://d.hatena.ne.jp/DigitalGhost/20090202/1233617024ÌÀ

		template<typename F, typename ...Params, size_t ...IntSeq>
		auto apply_impl(F && f, std::tuple<Params...> && params, CountingSequence<IntSeq...> count)
			->decltype(f(std::get<IntSeq>(std::forward<std::tuple<Params...>>(params))...))
		{
			return f(std::get<IntSeq>(std::forward<std::tuple<Params...>>(params))...);
		}

		template<typename F, typename ...Params>
		auto Apply(F && f, std::tuple<Params...> && params)
			->decltype(apply_impl(std::forward<F>(f),
			std::forward<std::tuple<Params...>>(params),
			typename MakeCountingSequence<sizeof...(Params)>::type()))
		{
			return apply_impl(
				std::forward<F>(f),
				std::forward<std::tuple<Params...>>(params),
				typename MakeCountingSequence<sizeof...(Params)>::type());
		}

		template<typename F, typename ...Params, size_t ...IntSeq>
		auto apply_impl(F & f, std::tuple<Params...> & params, CountingSequence<IntSeq...> count)
			->decltype(f(std::get<IntSeq>(params)...))
		{
			return f(std::get<IntSeq>(params)...);
		}

		template<typename F, typename ...Params>
		auto Apply(F & f, std::tuple<Params...> & params)
			->decltype(apply_impl(f, params, typename MakeCountingSequence<sizeof...(Params)>::type()))
		{
			return apply_impl(
				f,
				params,
				typename MakeCountingSequence<sizeof...(Params)>::type());
		}

	}
}

#endif // !MMLIBRARY_APPLY_H
