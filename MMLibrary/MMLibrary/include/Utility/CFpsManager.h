/*!	\file		CFpsManager.h
*	\details	CFpsManagerクラスの宣言
*	\author		松裏征志
*	\date		2014/02/06
*/
#ifndef MMLIBRARY_CFPS_MANAGER_H_
#define MMLIBRARY_CFPS_MANAGER_H_

//***********************************************************
//インクルード
//***********************************************************
#include <chrono>
#include "MMLibraryDefine.h"
#include "CSingletonBase.h"

namespace MMLibrary
{
	namespace Utility
	{
		using std::chrono::time_point;
		using std::chrono::milliseconds;
		using std::chrono::system_clock;

		//!	60FPSを表すstd::ratio
		using FPS60 = std::ratio <60, 1000 >;
		//!	30FPSを表すstd::ratio
		using FPS30 = std::ratio <30, 1000 >;
		
		//!	FPSを管理するクラス
		class CFpsManager : public CSingletonBase<CFpsManager>
		{
		private:
			list_t<system_clock::duration>	m_msPerFrameSamples;
			system_clock::duration			m_sumOfSamples;
			system_clock::time_point		m_nowFrameStartTime;
			float							m_targetFps;
			size_t							m_sampleSize;

			friend CSingletonBase < CFpsManager > ;

			CFpsManager();

			float GetAverageOfSamples();
		public:

			//!	初期化
			void Initialize(
				size_t sampleSize	= 60,
				float targetFps		= 60);

			//! FPSを取得する
			float GetFps();

			//! フレームの更新にかかった時間の平均を取得する
			float GetMilliSecondsPerFrame();

			//! 前フレームのみのFPSを取得する
			float GetFpsAtPreviousFrame();

			//! 前フレームの更新にかかった時間を取得する
			float GetMilliSecondsPerFrameAtPreviousFrame();

			//!	フレームを更新すべきかを取得する
			bool IsRequestUpdateFrame();

			//!	フレームの更新を開始したことを通知する
			void FrameUpdate();
		};
	}

}
	
#endif	//	MMLIBRARY_CFPS_MANAGER_H_