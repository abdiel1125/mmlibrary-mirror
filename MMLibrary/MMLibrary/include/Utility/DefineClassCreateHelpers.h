/*!	\file		DefineClassCreateHelpers.h
*	\details	MMLibraryで使用するクラス定義時に利用するdefineマクロの定義
*	\author		松裏征志
*	\date		2014/12/04
*/
#ifndef DEFINE_CLASS_CREATE_HELPERS_H

#define CALL_COPY_OR_MOVE_CONSTRUCTOR(src, member) member(src.member)

#define	CREATE_TYPE_SPECIFIED_SETTER_PRIMITIVE(name, member, type) void Set##name(type src) {member = src;}
#define	CREATE_TYPE_SPECIFIED_GETTER_PRIMITIVE(name, member, type) type Get##name(void) const {return member;}
#define	CREATE_TYPE_SPECIFIED_SETTER(name, member, type) void Set##name(const type& src) {member = src;}
#define	CREATE_TYPE_SPECIFIED_GETTER(name, member, type) const type& Get##name(void) const {return member;}
#define	CREATE_TYPE_SPECIFIED_REFERRENCE(name, member, type)	type& name(void) {return member;}
#define	CREATE_TYPE_SPECIFIED_CONST_REFERRENCE(name, member, type) const type& name(void) const {return member;}

#define	CREATE_SETTER_PRIMITIVE(name, member)	CREATE_TYPE_SPECIFIED_SETTER_PRIMITIVE(name, member, decltype(member))
#define	CREATE_GETTER_PRIMITIVE(name, member)	CREATE_TYPE_SPECIFIED_GETTER_PRIMITIVE(name, member, decltype(member))
#define	CREATE_SETTER(name, member)				CREATE_TYPE_SPECIFIED_SETTER(name, member, decltype(member))
#define	CREATE_GETTER(name, member)				CREATE_TYPE_SPECIFIED_GETTER(name, member, decltype(member))
#define	CREATE_REFERRENCE(name, member)			CREATE_TYPE_SPECIFIED_REFERRENCE(name, member, decltype(member))
#define	CREATE_CONST_REFERRENCE(name, member)	CREATE_TYPE_SPECIFIED_CONST_REFERRENCE(name, member, decltype(member))

#define CREATE_STL_LIKE_METHOD_SIZE(value)		size_t size(){return value;}
#define CREATE_STL_LIKE_METHOD_BEGIN(value)		decltype(value) begin(){return value;}
#define CREATE_STL_LIKE_METHOD_END(value)		decltype(value) end(){return value;}

#endif // !DEFINE_CLASS_CREATE_HELPERS_H
