/*!	\file		MMLibraryDefine.h
	\details	MMLibraryの全ての定数・構造体を宣言する
	\author		松裏征志
	\date		2014/05/14
	*/
#ifndef MMLIBRARY_DEFINE_H_
#define MMLIBRARY_DEFINE_H_

//***********************************************************
//インクルード
//***********************************************************
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <array>
#include <memory>
#include <cmath>
#include <assert.h>
#include <map>
#include <unordered_map>

//!	このライブラリ内で使用するデフォルトのアロケーター
#ifndef	USE_ALLOCATOR
#	define USE_ALLOCATOR ::std::allocator
#else
template<class> class USE_ALLOCATOR;
#endif

//!	Windows.hのmin/maxマクロを削除する
#undef min
#undef max

namespace MMLibrary
{
	using uchar_t = unsigned char;
	using ushort_t = unsigned short;
	using ulong_t = unsigned long;
	using uint_t = unsigned int;

	template<class T, template<class> class Allocator = USE_ALLOCATOR>
	using vector_t = std::vector < T, Allocator<T> > ;

	template<class T, template<class> class Allocator = USE_ALLOCATOR>
	using list_t = std::list < T, Allocator<T> > ;

	template<class T, uint_t N>
	using array_t = std::array < T, N > ;

	template<template<class> class Allocator = USE_ALLOCATOR>
	using string_t = std::basic_string < char, std::char_traits<char>, Allocator<char> >;

	template<template<class> class Allocator = USE_ALLOCATOR>
	using wstring_t = std::basic_string < wchar_t, std::char_traits<wchar_t>, Allocator<wchar_t> >;

	template<class Key, class Value, template<class> class Allocator = USE_ALLOCATOR>
	using unordered_map_t =
		std::unordered_map <
		Key,
		Value,
		std::hash<Key>,
		std::equal_to<Key>,
		Allocator<std::pair<const Key, Value> > >;

	template<class Key, class Value, template<class> class Allocator = USE_ALLOCATOR>
	using map_t =
		std::map < Key, Value, std:: less<Key>, Allocator<std::pair<const Key, Value> > > ;

	//!	空の基底クラス
	/*!
	*	多重継承によるクラスサイズの増加を抑えるために継承を用いて、複数のクラスを利用する必要がある。
	*	このクラスは単独で利用する際に基底クラスとするためのクラスである。
	*/
	struct tagEmptyBase{};

};

#endif